/*----------------------------------------------------------------------------------
    Fichier       : RechercheClient.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de rechercher des clients dans la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

import java.sql.SQLException;

public class RechercheClient extends Recherche
{
   // Constructeur liste de tous les clients
   public RechercheClient() throws SQLException, ErreurConnexionBD
   {
      select("*");
      siPremier();
      requete += "idClient <> 0";
      requete += " ORDER BY nom, prenom;";
   }
   
   // Constructeur liste de tous les clients
   // Ici on peut entrer les champs voulu "champ1, champ2, ..."
   public RechercheClient(String champs) throws SQLException, ErreurConnexionBD
   {
      select(champs);
      siPremier();
      requete += "idClient <> 0";
      requete += " ORDER BY nom, prenom;";
   }
   
   // Constructeur de la recherche sur les clients
   // Pour ne pas prendre un compte un paramètre lors de la recherche
   // il suffi de passer null comme valeur.
   public RechercheClient(Integer idClient,
                          String  nom,
                          String  prenom,
                          String  profession,
                          String  noTel,
                          String  adresse,
                          Integer npa,
                          String  ville) throws ErreurConnexionBD, SQLException
   {
      select("*");
      where(idClient,
            nom,
            prenom,
            profession,
            noTel,
            adresse,
            npa,
            ville);
      siPremier();
      requete += "idClient <> 0";
      requete += " ORDER BY nom, prenom;";
   }
   
   // Constructeur de la recherche sur les clients
   // Pour ne pas prendre un compte un paramètre lors de la recherche
   // il suffi de passer null comme valeur.
   // Ici on peut entrer les champs voulu "champ1, champ2, ..."
   public RechercheClient(String  champs,
                          Integer idClient,
                          String  nom,
                          String  prenom,
                          String  profession,
                          String  noTel,
                          String  adresse,
                          Integer npa,
                          String  ville) throws ErreurConnexionBD, SQLException
   {
      select(champs);
      where(idClient,
            nom,
            prenom,
            profession,
            noTel,
            adresse,
            npa,
            ville);
      siPremier();
      requete += "idClient <> 0";
      requete += " ORDER BY nom, prenom;";
   }
   
   // Crée la partie SELECT avec les champs voulus
   private void select(String champs)
   {
      requete = "SELECT " + champs + " FROM clients";
   }
   
   // Crée la partie WHERE avec les critères voulus
   private void where(Integer idClient,
                      String  nom,
                      String  prenom,
                      String  profession,
                      String  noTel,
                      String  adresse,
                      Integer npa,
                      String  ville)
   {
      if(idClient!=null)
      {
         siPremier();
         requete += "idClient="+idClient;
      }
         
      if(nom!=null)
      {
         siPremier();
         requete += "nom LIKE \"%"+nom+"%\"";
      }
         
      if(prenom!=null)
      {
         siPremier();
         requete += "prenom LIKE \"%"+prenom+"%\"";
      }
         
      if(profession!=null)
      {
         siPremier();
         requete += "profession LIKE \"%"+profession+"%\"";
      }
         
      if(noTel!=null)
      {
         siPremier();
         requete += "(noTel1 LIKE \"%"+noTel+"%\" OR "+
                    "noTel2 LIKE \"%"+noTel+"%\")";
      }
            
      if(adresse!=null)
      {
         siPremier();
         requete += "adresse LIKE \"%"+adresse+"%\"";
      }
               
      if(npa!=null)
      {
         siPremier();
         requete += "npa="+npa;
      }
         
      if(ville!=null)
      {
         siPremier();
         requete += "ville LIKE \"%"+ville+"%\"";
      }
   }
}
