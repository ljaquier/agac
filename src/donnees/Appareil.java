/*----------------------------------------------------------------------------------
   Fichier       : Appareil.java

   Date          : 09.04.2008

   Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Class pour définir un appareil.

   VM            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class Appareil extends Element {

   // attributs d'un appareil
   public String genre;
   public String marque;
   public String type;
   public String noSerie;
   // la date de début (utilisée pour l'intervalle de recherche est prise comme date de vente
   // lors de l'ajout d'un appareil
   public String dateVenteDebut;
   public String dateVenteFin;
   public String prix;
   public String noFacture;
   public String proprietaire;
   // pour savoir d'où vient l'appareil
   public String status;

   private String[] champs = new String [] {"genre", "marque", "type", "noSerie", "dateVente", "prix", "noFacture", "proprietaire", "status", "idAppareil"};
   private int posId = 9;
   private String nomDansBase = "appareils";
   
   /**
    * constructeur (Pour les appareils lambdas)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */
   public Appareil(){}
   
   /**
    * constructeur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */
   public Appareil(Integer id, String genre, String marque, String type, String noSerie,
         String dateVenteDebut, String dateVenteFin, String prix, String noFacture, String IDproprietaire, String status)
   {
      this.id = id;
      this.genre = genre;
      this.marque = marque;
      this.type = type;
      this.noSerie = noSerie;
      this.dateVenteDebut = dateVenteDebut;
      this.dateVenteFin = dateVenteFin;
      this.prix = prix;
      this.noFacture = noFacture;   
      this.proprietaire = IDproprietaire;
      this.status = status;
   }

   /**
    * Renvoie les champs caractérisants un appareil
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return Les champs
    */
   public String[] getChamps(){
      return champs;
   }
   
   /**
    * Renvoie la position de l'Id dans les champs
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return La position
    */
   public int getPosId(){
      return posId;
   }
   
   /**
    * Renvoie le nom caractérisant un appareil dans la base de donnée
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return Le nom du type
    */
   public String getNomDansBase(){
      return nomDansBase;
   }
   
   /**
    * Test si le genre est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideGenre()
   {
      return TestDonnee.estUnTinyText(genre);
   }
   
   /**
    * Test si la marque est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideMarque()
   {
      return TestDonnee.estUnTinyText(marque);
   }
   
   /**
    * Test si le tape est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideType()
   {
      return TestDonnee.estUnTinyText(type);
   }
   
   /**
    * Test si le numéro de série est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideNoSerie()
   {
      return TestDonnee.estUnTinyText(noSerie);
   }
   
   /**
    * Test si la date de vente est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideDateVente()
   {
      return TestDonnee.estUneDate(dateVenteDebut);
   }
   
   /**
    * Test si le prix de vente est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValidePrix()
   {
      return TestDonnee.estUnFloat(prix);
   }

   /**
    * Test si le numéro de facture est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideNoFacture()
   {
      return TestDonnee.estUnTinyText(noFacture);
   }
   
   /**
    * Test si un appareil est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValide()
   {
      return estValideGenre()&&estValideMarque()&&estValideType()&&estValideNoSerie()&&
             estValideDateVente()&&estValidePrix()&&estValideNoFacture();
   }
}