/*----------------------------------------------------------------------------------
    Fichier       : ConnexionBD.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de se connecter à la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConnexionBD
{
   static final private String urlFichierConf = "./agac.conf";
   
   private Connection connection;
   private Statement stmt;
   
   private String url;
   private String dbName;
   private String user;
   private String password;
   
   public ConnexionBD() throws ErreurConnexionBD
   {
      try {
         lireDonneesConnexion();
         Connexion();
      } catch (IOException e) {throw new ErreurConnexionBD();}
        catch (SQLException e) {throw new ErreurConnexionBD(); }
   }
   
   public ConnexionBD(String url, String dbName, String user, String password) throws ErreurConnexionBD
   {
      this.url = url;
      this.dbName = dbName;
      this.user = user;
      this.password = password;
      try {
         Connexion();
      } catch (SQLException e) {
         throw new ErreurConnexionBD();
      }
   }
   
   private void lireDonneesConnexion() throws IOException
   {
      Properties prop = new Properties();
      FileInputStream in = new FileInputStream(urlFichierConf);
      prop.load(in);
      in.close();
      // Extraction des propriétés
      this.url = prop.getProperty("url");
      this.dbName = prop.getProperty("dbName");
      this.user = prop.getProperty("user");
      this.password = prop.getProperty("password"); 
   }
   
   private void Connexion() throws SQLException
   {
         DriverManager.registerDriver(new com.mysql.jdbc.Driver());
         connection = DriverManager.getConnection("jdbc:mysql://"+url+"/"+dbName, user, password);
         stmt = connection.createStatement();
   }
   
   public ResultSet ouvrirRS(String SQL) throws SQLException
   {
      stmt.setQueryTimeout(5);
      return stmt.executeQuery(SQL);
   }
   
   public void executerSQL(String SQL) throws SQLException
   {
      stmt.executeUpdate(SQL);
   }
   
   public void finalize()
   {
      try {
         if(!stmt.isClosed())
            stmt.close();
         if(!connection.isClosed())
            connection.close();
      } catch (SQLException e) {
         e.printStackTrace();
      }
   }
}
