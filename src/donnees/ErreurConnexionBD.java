/*----------------------------------------------------------------------------------
    Fichier       : ErreurConnexionBD.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de définir les erreurs de connexion à la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class ErreurConnexionBD extends Exception {

}