/*----------------------------------------------------------------------------------
    Fichier       : SuppressionClient.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de supprimer des clients de la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class SuppressionClient extends Modification
{
   public SuppressionClient(int idClient)
   {
      requete = "DELETE FROM clients WHERE idClient="+idClient+";";
   }
}
