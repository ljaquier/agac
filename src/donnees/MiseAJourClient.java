/*----------------------------------------------------------------------------------
    Fichier       : MiseAJourClient.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de modifier des clients dans la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class MiseAJourClient extends Modification
{
   // Constructeur pour la mise à jour d'un client dans la base de données
   public MiseAJourClient(int    idClient,
                          String nom,
                          String prenom,
                          String profession,
                          String noTel1,
                          String noTel2,
                          String adresse,
                          int    npa,
                          String ville)
   {
      requete = "UPDATE clients SET nom=\""+nom+"\", prenom=\""+prenom+"\", profession=\""+profession+"\", "+
                                   "noTel1=\""+noTel1+"\", noTel2=\""+noTel2+"\", adresse=\""+adresse+"\", "+
                                   "npa="+npa+", ville=\""+ville+"\" "+
                "WHERE idClient="+idClient+";";
   }
}