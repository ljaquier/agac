/*----------------------------------------------------------------------------------
    Fichier       : RechercheAppareil.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de rechercher des appareils dans la base de donnée.
    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

import java.sql.SQLException;

public class RechercheAppareil extends Recherche
{
   // Constructeur liste de tous les appareils
   public RechercheAppareil() throws SQLException, ErreurConnexionBD
   {
      select("idAppareil, genre, marque, type, noSerie, DATE_FORMAT(dateVente, \"%d/%m/%Y\"), prix, noFacture, noBulletin, receptionnePar, proprietaire, status ");
      requete += " ORDER BY dateVente, genre, marque, type;";
   }
   
   // Constructeur liste de tous les appareils
   // Ici on peut entrer les champs voulu "champ1, champ2, ..."
   public RechercheAppareil(String champs) throws SQLException, ErreurConnexionBD
   {
      select(champs);
      requete += " ORDER BY dateVente, genre, marque, type;";
   }
     
   // Constructeur de la recherche sur les appareils
   // Pour ne pas prendre un compte un paramètre lors de la recherche
   // il suffi de passer null comme valeur.
   // Format de la date : "YYYY:MM:DD"
   public RechercheAppareil(Integer idAppareil,
                            String  genre,
                            String  marque,
                            String  type,
                            String  noSerie,
                            String  dateVenteDebut,
                            String  dateVenteFin,
                            Float   prix,
                            String  noFacture,
                            String  noBulletin,
                            String  receptionnePar,
                            Integer proprietaire,
                            Integer status) throws ErreurConnexionBD, SQLException
   {
      select("*");
      where(idAppareil,
            genre,
            marque,
            type,
            noSerie,
            dateVenteDebut,
            dateVenteFin,
            prix,
            noFacture,
            noBulletin,
            receptionnePar,
            proprietaire,
            status);
      requete += " ORDER BY dateVente, genre, marque, type;";
   }
   
   // Constructeur de la recherche sur les appareils
   // Pour ne pas prendre un compte un paramètre lors de la recherche
   // il suffi de passer null comme valeur.
   // Format de la date : "YYYY:MM:DD"
   // Ici on peut entrer les champs voulu "champ1, champ2, ..."
   public RechercheAppareil(String  champs,
                            Integer idAppareil,
                            String  genre,
                            String  marque,
                            String  type,
                            String  noSerie,
                            String  dateVenteDebut,
                            String  dateVenteFin,
                            Float   prix,
                            String  noFacture,
                            String  noBulletin,
                            String  receptionnePar,
                            Integer proprietaire,
                            Integer status) throws ErreurConnexionBD, SQLException
   {
      select(champs);
      where(idAppareil,
            genre,
            marque,
            type,
            noSerie,
            dateVenteDebut,
            dateVenteFin,
            prix,
            noFacture,
            noBulletin,
            receptionnePar,
            proprietaire,
            status);
      requete += " ORDER BY dateVente, genre, marque, type;";
   }
   
   // Crée la partie SELECT avec les champs voulus
   private void select(String champs)
   {
      requete = "SELECT " + champs + " FROM clients RIGHT JOIN appareils ON idClient = proprietaire";
   }
   
   // Crée la partie WHERE avec les critères voulus
   private void where(Integer idAppareil,
                      String  genre,
                      String  marque,
                      String  type,
                      String  noSerie,
                      String  dateVenteDebut,
                      String  dateVenteFin,
                      Float   prix,
                      String  noFacture,
                      String  noBulletin,
                      String  receptionnePar,
                      Integer proprietaire,
                      Integer status)
   {
      if(idAppareil!=null)
      {
         siPremier();
         requete += "idAppareil="+idAppareil;
      }
         
      if(genre!=null)
      {
         siPremier();
         requete += "genre LIKE \"%"+genre+"%\"";
      }
         
      if(marque!=null)
      {
         siPremier();
         requete += "marque LIKE \"%"+marque+"%\"";
      }
         
      if(type!=null)
      {
         siPremier();
         requete += "type LIKE \"%"+type+"%\"";
      }
            
      if(noSerie!=null)
      {
         siPremier();
         requete += "noSerie LIKE \"%"+noSerie+"%\"";
      }
               
      if(dateVenteDebut!=null)
      {
         siPremier();
         requete += "dateVente>=\""+dateVenteDebut+"\"";
      }
      
      if(dateVenteFin!=null)
      {
         siPremier();
         requete += "dateVente<=\""+dateVenteFin+"\"";
      }
      
      if(prix!=null)
      {
         siPremier();
         requete += "abs(prix-"+prix+")<=0.001";
      }
         
      if(noFacture!=null)
      {
         siPremier();
         requete += "noFacture LIKE \"%"+noFacture+"%\"";
      }
      
      if(noBulletin!=null)
      {
         siPremier();
         requete += "noBulletin LIKE \"%"+noBulletin+"%\"";
      }
      
      if(receptionnePar!=null)
      {
         siPremier();
         requete += "receptionnePar LIKE \"%"+receptionnePar+"%\"";
      }
      
      if(proprietaire!=null)
      {
         siPremier();
         requete += "proprietaire="+proprietaire;
      }
      
      if(status!=null)
      {
         siPremier();
         requete += "status="+status;
      }
   }
}
