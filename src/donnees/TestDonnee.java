/*----------------------------------------------------------------------------------
   Fichier     : TestDonnee.java

   Date        : 16.04.2008

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Tester si des valeurs sont compatible avec des type de la base
                 de données.

   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

import java.util.regex.Pattern;

public class TestDonnee
{
   /**
    * Test si un String est un TinyText (MySQL)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param text
    * @return
    */
   public static boolean estUnTinyText(String text)
   {
      return text.length() < 256;
   }
   
   /**
    * Test si un String est un Text (MySQL)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param text
    * @return
    */ 
   public static boolean estUnText(String text)
   {
      return text.length() < 65536;
   }

   /**
    * Test si une String est un NPA
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param NPA
    * @return
    */
   public static boolean estUnNPA(String NPA)
   {
      try{
         int NPA_TMP = Integer.parseInt(NPA);
         return NPA_TMP > 999 && NPA_TMP < 10000;
      }catch(NumberFormatException e){return false;}
   }

   /**
    * Test si un String est un no de téléphone
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param noTel
    * @return
    */
   public static boolean estUnNoTel(String noTel)
   {
      return estUnTinyText(noTel);
   }

   /**
    * Test si un String est une date
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param date
    * @return
    */ 
   public static boolean estUneDate(String date)
   {
      if (date != null && !date.isEmpty() && !date.equalsIgnoreCase("NULL"))
         return Pattern.matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]", date);
      else
         return true;
   }
   
   /**
    * Test si un String est un float
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param prix
    * @return
    */ 
   public static boolean estUnFloat(String prix)
   {
      if (prix != null && !prix.isEmpty() && !prix.equalsIgnoreCase("NULL"))
         try{
            float prixTMP = Float.parseFloat(prix);
            return true;
         }catch(NumberFormatException e){return false;}
      else
         return true;   
   }
}
