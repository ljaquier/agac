/*----------------------------------------------------------------------------------
    Fichier       : MiseAJourAppareil.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de modifier des appareils dans la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class MiseAJourAppareil extends Modification
{
   // Constructeur pour la mise à jour d'un appareil dans la base de données
   public MiseAJourAppareil(int    idAppareil,
                            String genre,
                            String marque,
                            String type,
                            String noSerie,
                            String dateVente,
                            Float  prix,
                            String noFacture,
                            String noBulletin,
                            String receptionnePar,
                            int    proprietaire,
                            int    status)
   {
      String strDate = "NULL";
      String strPrix = "NULL";
      if (dateVente != null && !dateVente.isEmpty())
         strDate = "\"" + dateVente + "\"";
      if (prix != null)
         strPrix = ""+prix;
      requete = "UPDATE appareils SET genre=\""+genre+"\", marque=\""+marque+"\", type=\""+type+"\", "+
                                     "noSerie=\""+noSerie+"\", dateVente="+strDate+", prix="+strPrix+", "+
                                     "noFacture=\""+noFacture + "\", noBulletin=\""+noBulletin + "\", receptionnePar=\""+receptionnePar + "\", proprietaire="+proprietaire+", status="+status+" "+
                "WHERE idAppareil="+idAppareil+";";
   }
}