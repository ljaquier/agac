/*----------------------------------------------------------------------------------
    Fichier       : Recherche.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet d'effectuer des recherches dans la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Recherche
{
   protected String requete;
   protected ResultSet rs;
   protected boolean premier = true;
   
   // Méthode pour mettre WHERE si c'est la première clause ou AND sinon
   protected void siPremier()
   {
      if(premier)
      {
         requete += " WHERE ";
         premier = false;
      }
      else
         requete += " AND ";
   }
   
   // Execute la requète
   public ResultSet execute() throws SQLException, ErreurConnexionBD
   { 
      //System.out.println(requete); // Pour les tests
      return (new ConnexionBD()).ouvrirRS(requete);
   }
   
   // Permet d'obtenir la requéte SQL
   public String obtRequete()
   {
      return requete;
   }
}