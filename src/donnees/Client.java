/*----------------------------------------------------------------------------------
   Fichier       : Client.java

   Date          : 09.04.2008

	Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Class pour définir un client.

	VM			     : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class Client extends Element {
   // attributs d'un client
   public String nom;
   public String prenom; 
   public String profession;
   public String adresse; 
   public String NPA; 
   public String localite;
   public String noTelPrincipal;
   public String noTelSecondaire;
   
   private String[] champs = new String [] {"nom", "prenom", "ville", "profession", "noTel1", "noTel2", "adresse", "npa", "idClient"};
   private int posId = 8;
   private String nomDansBase = "clients";
   
   /**
    * constructeur (Pour les clients lambdas)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */
   public Client(){}
   
   /**
    * constructeur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */
   public Client(Integer id, String nom, String prenom, String profession, String adresse, 
         String NPA, String localite, String noTelPrincipal, String noTelSec)
   {
      this.id = id;
      this.nom = nom;
      this.prenom = prenom;
      this.profession = profession;
      this.adresse = adresse;
      this.NPA = NPA;
      this.localite = localite;
      this.noTelPrincipal = noTelPrincipal;
      this.noTelSecondaire = noTelSec;
   }

   /**
    * Renvoie les champs caractérisants un client
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return Les champs
    */
   public String[] getChamps(){
      return champs;
   }
   
   /**
    * Renvoie la position de l'Id dans les champs
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return La position
    */
   public int getPosId(){
      return posId;
   }
   
   /**
    * Renvoie le nom caractérisant un client dans la base de donnée
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return Le nom du type
    */
   public String getNomDansBase(){
      return nomDansBase;
   }
   
   /**
    * Test si le nom est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */
   public boolean estValideNom()
   {
      return TestDonnee.estUnTinyText(nom) && !nom.isEmpty();
   }

   /**
    * Test si le prénom est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */
   public boolean estValidePrenom()
   {
      return TestDonnee.estUnTinyText(prenom) && !prenom.isEmpty();
   }

   /**
    * Test si l'adresse est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */
   public boolean estValideAdresse()
   {
      return TestDonnee.estUnTinyText(adresse) && !adresse.isEmpty();
   }

   /**
    * Test si le NPA est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */
   public boolean estValideNPA()
   {
      return TestDonnee.estUnNPA(NPA);
   }

   /**
    * Test si la localité est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */
   public boolean estValideLocalite()
   {
      return TestDonnee.estUnTinyText(localite) && !localite.isEmpty();
   }

   /**
    * Test si le no de téléphone principal est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */
   public boolean estValideNoTel1()
   {
      return TestDonnee.estUnNoTel(noTelPrincipal);
   }

   /**
    * Test si le no de téléphone secondaire est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */
   public boolean estValideNoTel2()
   {
      return TestDonnee.estUnNoTel(noTelSecondaire);
   }

   /**
    * Test si le client est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */
   public boolean estValide()
   {
      return estValideNom()&&estValidePrenom()&&estValideAdresse()&&estValideNPA()&&
             estValideLocalite()&&estValideNoTel1()&&estValideNoTel2();
   }
}
