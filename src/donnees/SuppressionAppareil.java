/*----------------------------------------------------------------------------------
    Fichier       : SuppressionAppareil.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de supprimer des appareils de la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class SuppressionAppareil extends Modification
{
   public SuppressionAppareil(int idAppareil)
   {
      requete = "DELETE FROM appareils WHERE idAppareil="+idAppareil+";";
   }
}