/*----------------------------------------------------------------------------------
    Fichier       : AjoutClient.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet d'ajouter des clients à la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class AjoutClient extends Modification
{
   // Constructeur pour l'ajout d'un client dans la base de données
   public AjoutClient(String nom,
                      String prenom,
                      String profession,
                      String noTel1,
                      String noTel2,
                      String adresse,
                      int    npa,
                      String ville)
   {
      requete = "INSERT INTO clients(nom, prenom, profession, noTel1, noTel2, adresse, npa, ville) "+
                "VALUES(\""+nom+"\", \""+prenom+"\", \""+profession+"\", "+
                       "\""+noTel1+"\", \""+noTel2+"\", \""+adresse+"\", "+npa+", \""+ville+"\");";
   }
}