/*----------------------------------------------------------------------------------
   Fichier       : Element.java

   Date          : 09.04.2008

   Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : -

   Remarque(s)   : Les positions des éléments autres que celle de l'ID ne sont
                    pas renseignés, il s'agit donc de les préciser à la main en
                    cas de besoins. (/!\ Un peu dégeux mais bon, après faut recréer un type position et tout)
                  
   VM            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public abstract class Element {
   public Integer id = null;     // Inconnu à la base
   protected String[] champs;    // Les champs définissant l'élément
   protected int posId;          // La position de l'ID dans la liste de champs
   protected String nomDansBase; // Le nom de l'élément dans la base de donnée
   
   public abstract String[] getChamps();
   public abstract int getPosId();
   public abstract String getNomDansBase();
}