/*----------------------------------------------------------------------------------
    Fichier       : SuppressionReparation.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de supprimer des réparations de la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class SuppressionReparation extends Modification
{
   public SuppressionReparation(int idReparation)
   {
      requete = "DELETE FROM reparations WHERE idReparation="+idReparation+";";
   }
}
