/*----------------------------------------------------------------------------------
   Fichier       : Appareil.java

   Date          : 09.04.2008

   Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Class pour définir plus précisément un appareil du stock.

   VM            : Java 1.6.0_03
----------------------------------------------------------------------------------*/


package donnees;

public class AppareilStock extends Appareil{
   // Attributs spécifiques des appareils du stock
   public String noBulletin;
   public String receptionnePar;
   
   private String[] champs = new String [] {"genre", "marque", "type", "noSerie", "dateVente", "prix", "noFacture", "proprietaire", "status", "idAppareil", "noBulletin", "receptionnePar"};
   
   /**
    * Renvoie les champs caractérisants un appareil
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return Les champs
    */
   public String[] getChamps(){
      return champs;
   }
   
   /**
    * constructeur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */
   public AppareilStock(Integer id, String genre, String marque, String type, String noSerie,
         String dateVenteDebut, String dateVenteFin, String prix, String noFacture,
         String noBulletin, String receptionnePar, String IDproprietaire, String status)
   {
      super(id, genre, marque, type, noSerie, dateVenteDebut, dateVenteFin, prix, noFacture, IDproprietaire, status);
      this.noBulletin = noBulletin;
      this.receptionnePar = receptionnePar;
   }
}