/*----------------------------------------------------------------------------------
   Fichier       : Reparation.java

   Date          : 9.04.2008

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Class pour définir une réparation.
                  
	VM			     : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class Reparation extends Element{
	
	// attributs d'une réparation
	public String dateReparationDebut;
	public String dateReparationFin;
	public String probleme;
	public String travailEffectue;
	public String technicien;
	public String prix;
	public Boolean paye;
	public String  noFacture;
	public String appareil;
	public String proprietaire;
	
   //private String[] champs = new String [] {"dateReparationDebut", "dateReparationFin", "probleme", "travailEffectue", "technicien", "prix", "paye", "noFacture", "appareil", "proprietaire", "idReparation"};
	private String[] champs = new String [] {"dateReparation", "probleme", "travailEffectue", "technicien", "reparations.prix", "paye", "reparations.noFacture", "appareil", "proprietaire", "idReparation"};
   private int posId = 9;
   private String nomDansBase = "reparations";
	
   /**
    * constructeur (Pour les réparations lambdas)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */
   public Reparation(){}
   
	/**
	 * constructeur
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 * @param Y description à quoi sert Y
	 * @throws Exception description pourquoi elle est levée
	 */
	public Reparation(String dateReparationDebut, String dateReparationFin, String probleme, String travailEffectue,
			String technicien, String prix, Boolean paye, String noFacture, String appareil, String proprietaire, Integer idReparation) 
	{
		this.dateReparationDebut = dateReparationDebut;
		this.dateReparationFin = dateReparationFin;
		this.probleme = probleme;
		this.travailEffectue = travailEffectue;
		this.technicien = technicien;
		this.prix = prix;
		this.paye = paye;
		this.noFacture = noFacture;
		this.appareil = appareil;
		this.proprietaire = proprietaire;
		this.id = idReparation;
	}
	
   /**
    * Renvoie les champs caractérisants un appareil
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return Les champs
    */
   public String[] getChamps(){
      return champs;
   }
   
   /**
    * Renvoie la position de l'Id dans les champs
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return La position
    */
   public int getPosId(){
      return posId;
   }
   
   /**
    * Renvoie le nom caractérisant un appareil dans la base de donnée
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return Le nom du type
    */
   public String getNomDansBase(){
      return nomDansBase;
   }	
	
   /**
    * Test si la date de début de réparation est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideDateReparationDebut()
   {
      return TestDonnee.estUneDate(dateReparationDebut);
   }
   
   /**
    * Test si la date de fin de réparation est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideDateReparationFin()
   {
      return TestDonnee.estUneDate(dateReparationFin);
   }
   
   /**
    * Test si le problème est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideProbleme()
   {
      return TestDonnee.estUnText(probleme);
   }
   
   /**
    * Test si le travail effectué est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideTravailEffectue()
   {
      return TestDonnee.estUnText(travailEffectue);
   }
   
   /**
    * Test si le technicien est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValideTechnicien()
   {
      return TestDonnee.estUnTinyText(technicien);
   }
   
   /**
    * Test si le prix est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValidePrix()
   {
      return TestDonnee.estUnFloat(prix);
   }

   /**
    * Test si payé est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValidePaye()
   {
      return paye==true||paye==false;
   }
   
   /**
    * Test si la répararion est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public boolean estValide()
   {
      return estValideDateReparationDebut()&&estValideDateReparationDebut()&&estValideProbleme()&&estValideTravailEffectue()&&
             estValideTechnicien()&&estValidePrix()&&estValidePaye();
   }
}