/*----------------------------------------------------------------------------------
    Fichier       : AjoutAppareil.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet d'ajouter des appareils à la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class AjoutAppareil extends Modification
{
   // Constructeur pour l'ajout d'un appareil dans la base de données
   public AjoutAppareil(String genre,
                        String marque,
                        String type,
                        String noSerie,
                        String dateVente,
                        Float  prix,
                        String noFacture,
                        String noBulletin,
                        String receptionnePar,
                        int    proprietaire,
                        int    status)
   {
      String strDate = "NULL";
      String strPrix = ", NULL";
      if (dateVente != null && !dateVente.isEmpty())
         strDate = "\"" + dateVente + "\"";
      if (prix != null)
         strPrix = ", \""+prix+"\"";
      
      requete = "INSERT INTO appareils(genre, marque, type, noSerie, dateVente, prix, noFacture, noBulletin, receptionnePar, proprietaire, status) "+
                "VALUES(\""+genre+"\", \""+marque+"\", \""+type+"\", \""+noSerie+"\", "+strDate+ strPrix + ", \""+noFacture+"\", " + "\""+noBulletin+"\"" + ", \""+receptionnePar+"\", " + proprietaire+", "+status+");";
   }
}