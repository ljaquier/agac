/*----------------------------------------------------------------------------------
    Fichier       : MiseAJourReparation.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de modifier des réparations dans la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class MiseAJourReparation extends Modification
{
   // Constructeur pour la mise à jour d'une réparation dans la base de données
   public MiseAJourReparation(int     idReparation,
                              String  dateReparation,
                              String  probleme,
                              String  travailEffectue,
                              String  technicien,
                              float   prix,
                              boolean paye,
                              String  noFacture,
                              int     appareil)
   {
      requete = "UPDATE reparations SET dateReparation=\""+dateReparation+"\", probleme=\""+probleme+"\", "+
                                       "travailEffectue=\""+travailEffectue+"\", technicien=\""+technicien+"\", "+
                                       "prix="+prix+", paye="+paye+", noFacture=\""+noFacture+"\", "+
                                       "appareil="+appareil+" "+
                "WHERE idReparation="+idReparation+";";
   }
}