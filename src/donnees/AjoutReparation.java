/*----------------------------------------------------------------------------------
    Fichier       : AjoutReparation.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet d'ajouter des réparations à la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

public class AjoutReparation extends Modification
{
   // Constructeur pour l'ajout d'une réparation dans la base de données
   public AjoutReparation(String  dateReparation,
                          String  probleme,
                          String  travailEffectue,
                          String  technicien,
                          float   prix,
                          boolean paye,
                          String  noFacture,
                          int     appareil)
   {
      requete = "INSERT INTO reparations(dateReparation, probleme, travailEffectue, technicien, prix, paye, noFacture, appareil) "+
                "VALUES(\""+dateReparation+"\", \""+probleme+"\", \""+travailEffectue+"\", "+
                       "\""+technicien+"\", "+prix+", "+paye+", \""+noFacture+"\", "+appareil+");";
   }
}