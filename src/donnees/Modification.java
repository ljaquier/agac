/*----------------------------------------------------------------------------------
    Fichier       : Modification.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet d'effectuer des modifications dans la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

import java.sql.SQLException;

public class Modification
{
   protected String requete;
   
   // Execute la requète
   public void execute() throws SQLException, ErreurConnexionBD
   {
      //System.out.println(requete); // Pour les tests
      new ConnexionBD().executerSQL(requete);
   }
}