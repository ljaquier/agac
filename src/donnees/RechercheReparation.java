/*----------------------------------------------------------------------------------
    Fichier       : RechercheReparation.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de rechercher des réparations dans la base de donnée.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package donnees;

import java.sql.SQLException;

public class RechercheReparation extends Recherche
{
   
   // Constructeur liste de tous les réparations
   public RechercheReparation() throws SQLException, ErreurConnexionBD
   {
      select("*");
      requete += " ORDER BY dateReparation;";
   }
   
   // Constructeur liste de tous les réparations
   // Ici on peut entrer les champs voulu "champ1, champ2, ..."
   public RechercheReparation(String champs) throws SQLException, ErreurConnexionBD
   {
      select(champs);
      requete += " ORDER BY dateReparation;";
   }
   
   // Constructeur de la recherche sur les réparations
   // Pour ne pas prendre un compte un paramètre lors de la recherche
   // il suffi de passer null comme valeur.
   // Format de la date : "YYYY:MM:DD"
   public RechercheReparation(Integer idReparation,
                              String  dateReparationDebut,
                              String  dateReparationFin,
                              String  probleme,
                              String  travailEffectue,
                              String  technicien,
                              Float   prix,
                              Boolean paye,
                              String  noFacture,
                              Integer appareil,
                              Integer proprietaire) throws ErreurConnexionBD, SQLException
   {
      select("*");
      where(idReparation,
            dateReparationDebut,
            dateReparationFin,
            probleme,
            travailEffectue,
            technicien,
            prix,
            paye,
            noFacture,
            appareil,
            proprietaire);
      requete += " ORDER BY dateReparation;";
   }
   
   // Constructeur de la recherche sur les réparations
   // Pour ne pas prendre un compte un paramètre lors de la recherche
   // il suffi de passer null comme valeur.
   // Format de la date : "YYYY:MM:DD"
   // Ici on peut entrer les champs voulu "champ1, champ2, ..."
   public RechercheReparation(String  champs,
                              Integer idReparation,
                              String  dateReparationDebut,
                              String  dateReparationFin,
                              String  probleme,
                              String  travailEffectue,
                              String  technicien,
                              Float   prix,
                              Boolean paye,
                              String  noFacture,
                              Integer appareil,
                              Integer proprietaire) throws ErreurConnexionBD, SQLException
   {
      select(champs);
      where(idReparation,
            dateReparationDebut,
            dateReparationFin,
            probleme,
            travailEffectue,
            technicien,
            prix,
            paye,
            noFacture,
            appareil,
            proprietaire);
      requete += " ORDER BY dateReparation;";
   }
   
   // Crée la partie SELECT avec les champs voulus
   private void select(String champs)
   {
      requete = "SELECT " + champs + " FROM clients RIGHT JOIN appareils ON idClient = proprietaire " +
                                                   "RIGHT JOIN reparations ON idAppareil = Appareil";
   }
   
   // Crée la partie WHERE avec les critères voulus
   private void where(Integer idReparation,
                      String  dateReparationDebut,
                      String  dateReparationFin,
                      String  probleme,
                      String  travailEffectue,
                      String  technicien,
                      Float   prix,
                      Boolean paye,
                      String  noFacture,
                      Integer appareil,
                      Integer proprietaire)
   {
      if(idReparation!=null)
      {
         siPremier();
         requete += "idReparation="+idReparation;
      }
         
      if(dateReparationDebut!=null)
      {
         siPremier();
         requete += "dateReparation>=\""+dateReparationDebut+"\"";
      }
      
      if(dateReparationFin!=null)
      {
         siPremier();
         requete += "dateReparation<=\""+dateReparationFin+"\"";
      }
         
      if(probleme!=null)
      {
         siPremier();
         requete += "probleme LIKE \"%"+probleme+"%\"";
      }
         
      if(travailEffectue!=null)
      {
         siPremier();
         requete += "travailEffectue LIKE \"%"+travailEffectue+"%\"";
      }
         
      if(technicien!=null)
      {
         siPremier();
         requete += "technicien LIKE \"%"+technicien+"%\"";
      }
      
      if(prix!=null)
      {
         siPremier();
         requete += "abs(reparations.prix-"+prix+")<=0.001";
      }
            
      if(paye!=null)
      {
         siPremier();
         requete += "paye="+paye;
      }
               
      if(noFacture!=null)
      {
         siPremier();
         requete += "noFacture LIKE \"%"+noFacture+"%\"";
      }
         
      if(appareil!=null)
      {
         siPremier();
         requete += "appareil="+appareil;
      }
      
      if(proprietaire!=null)
      {
         siPremier();
         requete += "proprietaire="+proprietaire;
      }
   }
}
