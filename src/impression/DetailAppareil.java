package impression;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class DetailAppareil
{
   static final private String urlFichierConf = "./agac.conf";
   
   static private String url;
   static private String dbName;
   static private String user;
   static private String password;
   static private String pdf;
   
   static private void lireDonneesConnexion() throws IOException
   {
      Properties prop = new Properties();
      FileInputStream in = new FileInputStream(urlFichierConf);
      prop.load(in);
      in.close();
      // Extraction des propriétés
      url = prop.getProperty("url");
      dbName = prop.getProperty("dbName");
      user = prop.getProperty("user");
      password = prop.getProperty("password");
      pdf = prop.getProperty("pdf");
   }
   
   static public void print(int noAppareil) throws ErreurImpression
   {
      try {
         // Lire les donnée pour la connexion à la base de données
         lireDonneesConnexion();
         
         // Connexion à la base
         DriverManager.registerDriver(new com.mysql.jdbc.Driver());
         Connection connection = DriverManager.getConnection("jdbc:mysql://"+url+"/"+dbName, user, password);
         
         // Chargement et compilation du rapport
         JasperDesign jasperDesign = JRXmlLoader.load("./etats/Appareil_det.jrxml");
         JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
         
         // Paramètre à envoyer au rapport
         Map parameters = new HashMap();
         parameters.put("noAppareil", Integer.toString(noAppareil));
         
         // Execution du rapport
         JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection);
         JasperExportManager.exportReportToPdfFile(jasperPrint,"tmp.pdf");
         if(Runtime.getRuntime().exec(pdf+" tmp.pdf").waitFor() != 0)
            throw new ErreurImpression();
      
      } catch (IOException e) {
         throw new ErreurImpression();
      } catch (SQLException e) {
         throw new ErreurImpression();
      } catch (JRException e) {
         throw new ErreurImpression();
      } catch (InterruptedException e) {
         throw new ErreurImpression();
      }
   }

   /**
    * ... description de la méthode/classe/package/variable ...
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param args
    */
   public static void main(String[] args)
   {
      try {
         print(2);
      } catch (ErreurImpression e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }
}
