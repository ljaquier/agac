/*----------------------------------------------------------------------------------
    Fichier       : Utilitaire.java

    Date          : 23 juin 08

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

    But           : Fournis des outils 

	Remarque(s)    : -
                  
	VM			      : Java 1.6.0_03
----------------------------------------------------------------------------------*/
package gestion;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

public class Utilitaire {
	/**
	 * ... description de la méthode/classe/package/variable ...
	 * @author Lucien Chaubert / Louis Jaquier
	 */
	
   /**
    * Pour charger une liste à partir d'un fichier texte (un élément par ligne)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param liste Liste où seront chargés les lignes du fichier texte
    * @param nomFichier nom du fichier à charger (chemin)
    * @throws IOException Lors d'un problème de lecture
    * @throws FileNotFoundException Lorsque le fichier n'a pas été trouvé
    */ 
   public static LinkedList<String> chargerListe(String nomFichier) throws IOException, FileNotFoundException  
   {
      LinkedList<String>  liste = new LinkedList<String> ();
      // tampon de lecture
      BufferedReader inputStream = null;
      try {
         // on ouvre le fichier
         inputStream = new BufferedReader(new FileReader(nomFichier));
         // pour récupérer une ligne
         String line;
         // tant qu'il y a des lignes
         while ((line = inputStream.readLine()) != null)
            liste.add(line);

         return liste;
      }
      finally {
         if (inputStream != null) {
            // on faire le tampon
            inputStream.close();
         }
      }
   }
   
   /**
    * Pour convertir une date java en date SQL
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param date La date à convertir
    */ 
   public static String getSQLDate(java.util.Date date){
      if (date != null)
      {
         return (new java.sql.Date(date.getTime())).toString();
      }
         else
         return null;
   }
   
   /**
    * Pour convertir une date SQL en date java
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param date La date à convertir
    */ 
   public static java.util.Date getJavaDate(String date){
      if (date != null)
         return (Date.valueOf(date));
      else
         return null;
   }
   
   // on transforme la date en format sql
   
   /**
    * Pour transformer une date de type dd/MM/yyyy en yyyy-MM-dd 
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param date Date à transformer
    * @return Date au format SQL
    */ 
   public static String stdToSql(String date)
   {
      //   Definition du format utilise pour les dates
      SimpleDateFormat inFmt = new SimpleDateFormat("dd/MM/yyyy");
      SimpleDateFormat outFmt = new SimpleDateFormat("yyyy-MM-dd");
      String out = null;
		try {
	      // (Positions à la main, ça évite de les stocker juste pour ça)
			out = outFmt.format(inFmt.parse( date ) );
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return  out;
   }
}
