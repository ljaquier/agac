/*----------------------------------------------------------------------------------
    Fichier       : Gestion.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet d'effectuer les actions de l'utilisateur sur l'interface
                    graphique.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package gestion;

import donnees.*; 
import graphisme.ModeleTable;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*; import java.util.ArrayList; import java.util.LinkedList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import backupRestore.ErreurRestore;
import backupRestore.Restore;


public abstract class Gestion {
   ModeleTable tabModel; // Modèle de la table gérée
   Integer parentId1, parentId2; // ID des éventuels parents de l'élément traité

   /**
    * Permet de construire un gestionnaire de base de donnée
    * @param tabModel Le modèle de table qui est utilisé
    * @param typeDonnee Element définissant le type dont il fait partie
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public Gestion(ModeleTable tabModel){
      // Se connecte à la base de donnée
      try{
         new ConnexionBD();
         this.tabModel = tabModel;
      }
      catch (ErreurConnexionBD e){ // Informe l'utilisateur de l'erreur et quitte le programme
         if (demandeRestauration())
         {
            JFileChooser dialogue = new JFileChooser();
            String fichier;
            int retour = dialogue.showOpenDialog(null);
            if (retour == JFileChooser.APPROVE_OPTION)
            {
               fichier = dialogue.getSelectedFile().getPath();
               try {
                  // on essaie de faire une restauration
                  new Restore(fichier);
                  // on ressaie de se connecter à la base de donnée
                  new ConnexionBD();
               }
                  // si la connexion échoue alors c'est que le problème
                  // vient du serveur MySQL
                  catch (ErreurConnexionBD e2)
                  {
                     String titreErreur = "Errreur de restauration";
                     String erreur ="Le serveur MySQL n'est pas atteignable\n" +
                           "Le programme va maintenant se fermer."; 
                     // affiche un message d'erreur
                     JOptionPane.showMessageDialog(null, erreur, titreErreur, 
                           JOptionPane.ERROR_MESSAGE);
                     // on quitte le programme
                     System.exit(0);
                  }
                  // si la restauration échoue on ferme le programme
                  catch (ErreurRestore e1) {
                     String titreErreur = "Errreur de restauration";
                     String erreur ="Un problème lors de la restauration de la base de données\n" +
                     "à partir de " + fichier + " est survenu.\n" +
                     "Veuillez vérifier que le nom du fichier est correct,\n" +
                     "qu'il s'agit bien d'un fichier de sauvegarde MySQL\n" +
                     "et que le serveur MySQL est installé et démarré.\n" +
                     "Le programme va maintenant se fermer."; 
                     // affiche un message d'erreur
                     JOptionPane.showMessageDialog(null, erreur, titreErreur, 
                           JOptionPane.ERROR_MESSAGE);
                     // on quitte le programme
                     System.exit(0);
                  }
                 
                  this.tabModel = tabModel;
               }
            }
         else
         {
            String titreErreur = "Errreur de restauration";
            String erreur ="Le programme va maintenant se fermer."; 
            // affiche un message d'erreur
            JOptionPane.showMessageDialog(null, erreur, titreErreur, 
                  JOptionPane.ERROR_MESSAGE);
               System.exit(0);
         }
      }
   }
   

   private Boolean demandeRestauration()
   {
      String titreErreur = "Erreur de connexion";
      String erreur = "Le programme n'a pu se conencter à la base de données.\n"+
      "Il est possible que le serveur MySQL ne soit pas installé ou pas lancé.\n" +
      "Il est aussi possible que la base de donnée n'a pas encore été initialisée.\n"+
      "Si c'est le cas, il est nécessaire d'importer une base de données.\n" +
      "Désirez-vous importer une base de données à partir d'un fichier?";
      
      // affiche un message d'erreur
      return JOptionPane.showConfirmDialog(null, erreur, titreErreur, 
                  JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE) ==
                     JOptionPane.YES_OPTION;
   }
   
   /**
    * Permet d'indiquer l'ID des éventuels éléments parent
    * @param Id L'ID du parent
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void setParentID(Integer Id1, Integer Id2){
      parentId1 = Id1;
      parentId2 = Id2;
   }

   /**
    * Permet de vide la liste d'affichage
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void videListe(){
      tabModel.vide();
   }
  
   /**
    * Traitement des erreurs de saisies incompletes
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   void exSaisie(){
      JOptionPane.showMessageDialog(null, "Veuillez svp remplir correctement tous les champs obligatoires pour ajouter l'élément voulu.",
            "Erreur d'insertion", JOptionPane.ERROR_MESSAGE);
   }

   /**
    * Traitement des erreurs de connexion à la bdd
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
    void exBdCon(){
      JOptionPane.showMessageDialog(null, "                                         La connexion à la base de donnée est impossible!\n" +
            "Pour obtenir plus d'informations sur sa configuration, veuillez svp vous reporter au guide de l'utilisateur.",
            "Erreur de connexion", JOptionPane.ERROR_MESSAGE);
   }
    
    //////////////////// Méthodes abstraites ////////////////////
    /**
     * Permet d'afficher dans la jTable les champs d'un certain type voulus
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     */ 
    public abstract void afficherListe();
    
    /**
     * Permet d'obtenir les détails d'une entrée en fonction de sa ligne dans la table
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param ligne La ligne concernée
     * @return Element correspondant à la ligne de la table
     */ 
    public abstract Element getDetail(int ligne);
    
    /**
     * Permet d'ajouter un élément à la base de donnée (Avec contrôle des erreurs)
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param element L'élément à ajouter
     */ 
    public abstract void ajoute(Element element);
    
    /**
     * Permet de modifier un élément de la base de donnée (Avec contrôle des erreurs)
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param element L'élément à modifier
     */ 
    public abstract void modifie(Element element);
    
    /**
     * Permet de supprimer un élément de la base de donnée depuis la table
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param id L'ID de l'élément à supprimer
     */ 
    public abstract void supprime(int id);
    
    /**
     * Permet de rechercher un élément dans la base de donnée
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param element L'élément à rechercher
     */ 
    public abstract void recherche(Element element);
}