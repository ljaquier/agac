/*----------------------------------------------------------------------------------
    Fichier       : GestionReparation.java

    Date          : 23 juin 08

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

    But           : Pour gérer les réparations

	Remarque(s)    : -
                  
	VM			      : Java 1.6.0_03
----------------------------------------------------------------------------------*/
package gestion;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import graphisme.ModeleTable;
import donnees.AjoutReparation;
import donnees.Appareil;
import donnees.Client;
import donnees.Element;
import donnees.ErreurConnexionBD;
import donnees.MiseAJourAppareil;
import donnees.MiseAJourClient;
import donnees.MiseAJourReparation;
import donnees.RechercheAppareil;
import donnees.RechercheClient;
import donnees.RechercheReparation;
import donnees.Reparation;
import donnees.SuppressionAppareil;
import donnees.SuppressionClient;
import donnees.SuppressionReparation;

public class GestionReparation extends Gestion {
	
	private Reparation reparationRecherchee;
   // Traite les nombres à part. (Car ils sont convertis)
   private Float prix = null;

	/**
    * Permet de construire un gestionnaire de base de donnée
    * @param tabModel Le modèle de table qui est utilisé
    * @param afficherPayee Pour indiquer s'il faut afficher 
    * les réparations payées uniquement (true), non payées uniquement (false) 
    * ou les 2 (null)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public GestionReparation(ModeleTable tabModel, Boolean afficherPayee) {
		super(tabModel);
		reparationRecherchee = new Reparation(null,null,null,null,null,null,afficherPayee,null,null,null,null);
	}
	
	
   /**
    * Permet d'afficher dans la jTable les champs d'un certain type voulus
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void afficherListe(){
      String chnDeChmpsTmp = ""; // Pour créer la chaîne de champs sous forme compréensible par la bdd

      tabModel.vide();

      try {
         // Converti les champs en un format utilisable par la bdd
         for (int i = 0; i < reparationRecherchee.getChamps().length-1; i++)
            chnDeChmpsTmp += (reparationRecherchee.getChamps()[i].equals("dateReparation")?
            		 "DATE_FORMAT(dateReparation, \"%d/%m/%Y\")" :
            			 reparationRecherchee.getChamps()[i] ) + ", ";
         chnDeChmpsTmp += reparationRecherchee.getChamps()[reparationRecherchee.getChamps().length-1].equals("dateReparation")?
        		 "DATE_FORMAT(dateReparation, \"%d/%m/%Y\")" :
        			 reparationRecherchee.getChamps()[reparationRecherchee.getChamps().length-1];
         
         ResultSet tmp = new RechercheReparation(chnDeChmpsTmp, reparationRecherchee.id, reparationRecherchee.dateReparationDebut, reparationRecherchee.dateReparationFin, reparationRecherchee.probleme, reparationRecherchee.travailEffectue, reparationRecherchee.technicien, prix, reparationRecherchee.paye, reparationRecherchee.noFacture, parentId1, parentId2).execute();
         tabModel.addValue(tmp);
         tmp.close();
      } catch (SQLException e) {
         exSaisie();
      } catch (ErreurConnexionBD e) {
         exBdCon();
      }
   }

	 /**
    * Permet d'obtenir les détails d'une entrée en fonction de sa ligne dans la table
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param ligne La ligne concernée
    * @return Element corréspondant à la ligne de la table
    */ 
   public Reparation getDetail(int ligne){
      ArrayList<Object> listeDonnees = tabModel.getValueAt(ligne); // Permet de récupérer les données d'une ligne
      ArrayList<String> details = new ArrayList<String>();         // Permet de récupérer les détails
      Reparation reparation = null;                                      // Permet de créer un élément avec les détails trouvés
      
      for (Object i : listeDonnees) // Récupère les détails de l'élément de la ligne séléctionnée
         details.add(String.valueOf(i));
        
      reparation = new Reparation(Utilitaire.stdToSql( details.get(0)), "", details.get(1), details.get(2), details.get(3), details.get(4), Boolean.parseBoolean(details.get(5)) , details.get(6), details.get(7), details.get(8), Integer.parseInt(details.get(9)));
      
      return reparation; // Retourne l'élément trouvé
   }
   /**
    * Permet d'ajouter un élément à la base de donnée (Avec contrôle des erreurs)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param element L'élément à ajouter
    */ 
   public void ajoute(Element element){  
   	Reparation reparation = (Reparation) element;
      // Si la réparation est valide
      if (reparation.estValide()){
         // Ajoute la réparation et traîte les éventuelles erreurs
         try {
            new AjoutReparation(reparation.dateReparationDebut, reparation.probleme, reparation.travailEffectue, reparation.technicien, Float.parseFloat(reparation.prix), reparation.paye, reparation.noFacture, Integer.parseInt(reparation.appareil)).execute();
            afficherListe(); // Actualise l'affichage
         }
         catch (NumberFormatException e1) {
            exSaisie();
         }
         catch (SQLException e1) {
            exSaisie();
         }
         catch (ErreurConnexionBD e1) {
            exBdCon();
         }
      }
      else
         exSaisie();
   }
   
   /**
    * Permet de modifier un élément de la base de donnée (Avec contrôle des erreurs)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param element L'élément à modifier
    */ 
   public void modifie(Element element){
   	Reparation reparation = (Reparation) element;

      // Si la réparation est valide
      if (reparation.estValide()){
         // Modifie la réparation et traîte les éventuelles erreurs
         try {
            new MiseAJourReparation(reparation.id, reparation.dateReparationDebut, reparation.probleme, reparation.travailEffectue, reparation.technicien, Float.parseFloat(reparation.prix), reparation.paye, reparation.noFacture, Integer.parseInt(reparation.appareil)).execute();
            afficherListe(); // Actualise l'affichage
         }
         catch (NumberFormatException e1) {
            exSaisie();
         }
         catch (SQLException e1) {
            exSaisie();
         }
         catch (ErreurConnexionBD e1) {
            exBdCon();
         }
      }
   }
   
   /**
    * Permet de supprimer un élément de la base de donnée depuis la table
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param id L'ID de l'élément à supprimer
    */ 
   public void supprime(int id){      
      // Supprime en fonction de l'ID trouvé
      try {
         new SuppressionReparation(id).execute();
         // Actualise l'affichage
         afficherListe();
      }
      catch (NumberFormatException e) {}
      catch (SQLException e) {}
      catch (ErreurConnexionBD e) {
         exBdCon();
      }
   }
   
   /**
    * Permet de rechercher un élément dans la base de donnée
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param element L'élément à rechercher
    */ 
   public void recherche(Element element){
   	reparationRecherchee = (Reparation) element;

      String chnDeChmpsTmp = ""; // Pour créer la chaîne de champs sous forme compréensible par la bdd

      tabModel.vide(); // Nettoie (Vide) le modèle à afficher

      try {
         // Converti les champs en un format utilisable par la bdd
         for (int i = 0; i < reparationRecherchee.getChamps().length-1; i++)
            chnDeChmpsTmp += reparationRecherchee.getChamps()[i] + ", ";
         chnDeChmpsTmp += reparationRecherchee.getChamps()[reparationRecherchee.getChamps().length-1];
            

         // Donne une valeur nulle aux critères non-saisis et s'occupe des nombres
         if (reparationRecherchee.dateReparationDebut != null && reparationRecherchee.dateReparationDebut.isEmpty()) reparationRecherchee.dateReparationDebut = null;
         if (reparationRecherchee.dateReparationFin != null && reparationRecherchee.dateReparationFin.isEmpty()) reparationRecherchee.dateReparationFin = null;
         if (reparationRecherchee.probleme != null && reparationRecherchee.probleme.isEmpty()) reparationRecherchee.probleme = null;
         if (reparationRecherchee.travailEffectue != null && reparationRecherchee.travailEffectue.isEmpty()) reparationRecherchee.travailEffectue = null;
         if (reparationRecherchee.technicien != null && reparationRecherchee.technicien.isEmpty()) reparationRecherchee.technicien = null;
         if (reparationRecherchee.prix != null)
         	if (reparationRecherchee.prix.isEmpty()) 
         		prix = null; 
         	else 
         		prix = Float.parseFloat(reparationRecherchee.prix);
         if (reparationRecherchee.noFacture != null && reparationRecherchee.noFacture.isEmpty()) reparationRecherchee.noFacture = null;
         if (reparationRecherchee.appareil != null)
         	if(reparationRecherchee.appareil.isEmpty()) 
         		parentId1 = null; 
         	else 
         		parentId1 = Integer.parseInt(reparationRecherchee.appareil);
         if (reparationRecherchee.proprietaire != null)
         	if (reparationRecherchee.proprietaire.isEmpty()) 
         		parentId2 =null; 
         	else 
         		parentId2= Integer.parseInt(reparationRecherchee.proprietaire);

         ResultSet tmp = new RechercheReparation(chnDeChmpsTmp, reparationRecherchee.id, reparationRecherchee.dateReparationDebut, reparationRecherchee.dateReparationFin, reparationRecherchee.probleme, reparationRecherchee.travailEffectue, reparationRecherchee.technicien, prix, reparationRecherchee.paye, reparationRecherchee.noFacture, parentId1, parentId2).execute();
         tabModel.addValue(tmp);
         tmp.close();

      } catch (SQLException e) {
         exSaisie();
      } catch (ErreurConnexionBD e) {
         exBdCon();
      }
   }
	
}
