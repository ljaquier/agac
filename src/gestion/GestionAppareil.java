/*----------------------------------------------------------------------------------
   Fichier       : GestionAppareil.java

   Date          : 23 juin 08

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Pour gérer les appareils

	Remarque(s)   : -
                  
	VM			     : Java 1.6.0_03
----------------------------------------------------------------------------------*/
package gestion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import graphisme.ModeleTable;
import donnees.AjoutAppareil;
import donnees.AjoutClient;
import donnees.AjoutReparation;
import donnees.Appareil;
import donnees.Client;
import donnees.Element;
import donnees.ErreurConnexionBD;
import donnees.MiseAJourAppareil;
import donnees.MiseAJourClient;
import donnees.MiseAJourReparation;
import donnees.RechercheAppareil;
import donnees.RechercheClient;
import donnees.RechercheReparation;
import donnees.Reparation;
import donnees.SuppressionAppareil;
import donnees.SuppressionClient;
import donnees.SuppressionReparation;

public class GestionAppareil extends Gestion {
	
	private Appareil appareilRecherche;
   // Traite les nombres à part. (Car ils sont convertis)
   Float prix = null;
   Integer status = null;

	/**
    * Permet de construire un gestionnaire de base de donnée
    * @param tabModel Le modèle de table qui est utilisé
    * @param typeDonnee Element définissant le type dont il fait partie
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public GestionAppareil(ModeleTable tabModel, Integer statusDefaut) {
		super(tabModel);
		this.status = statusDefaut;
		appareilRecherche = new Appareil( null,null,null,null,null,null,null,null,null, null, status!=null?status.toString():null);
	}
	
	
	/**
	 * Permet d'obtenir le propriétaire un appareil
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 * @param appareil
	 * @return Le propriétaire
	 */ 
	public Client getProprietaire(Appareil appareil)
	{
		Client proprietaire = null;
		ResultSet result = null ;
		try {
			result =
			new RechercheClient(Integer.parseInt(appareil.proprietaire) ,null,null,null,null,null,null,null).execute() ;
			// on prend l'entrée
			result.next();
			// on crée le propriétaire
			proprietaire = new Client(Integer.parseInt(result.getString(1)), result.getString(2), result.getString(3), result.getString(4),
					result.getString(7),result.getString(8), result.getString(9), result.getString(5),result.getString(6) );
			result.close();
			
		} catch (NumberFormatException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			exSaisie();
		} catch (ErreurConnexionBD e) {
			exBdCon();
		}
		return proprietaire;		
	}
	
   /**
    * Permet d'afficher dans la jTable les champs d'un certain type voulus
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void afficherListe(){
      String chnDeChmpsTmp = ""; // Pour créer la chaîne de champs sous forme compréensible par la bdd

      tabModel.vide();
      try {
         // Converti les champs en un format utilisable par la bdd
         for (int i = 0; i < appareilRecherche.getChamps().length-1; i++)
            chnDeChmpsTmp += appareilRecherche.getChamps()[i] + ", ";
         chnDeChmpsTmp += appareilRecherche.getChamps()[appareilRecherche.getChamps().length-1];
         // Ajoute les données retournées dans la table
         ResultSet tmp = new RechercheAppareil(chnDeChmpsTmp, null, appareilRecherche.genre, appareilRecherche.marque, appareilRecherche.type, appareilRecherche.noSerie, appareilRecherche.dateVenteDebut, appareilRecherche.dateVenteFin, prix, appareilRecherche.noFacture, "", "", parentId1, status).execute();
         tabModel.addValue(tmp);
         tmp.close();
      
      } catch (SQLException e) {
      	e.printStackTrace();
         exSaisie();
      } catch (ErreurConnexionBD e) {
         exBdCon();
      }
   }
   
   /**
    * Permet d'obtenir les détails d'une entrée en fonction de sa ligne dans la table
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param ligne La ligne concernée
    * @return Element corréspondant à la ligne de la table
    */ 
   public Appareil getDetail(int ligne){
      ArrayList<Object> listeDonnees = tabModel.getValueAt(ligne); // Permet de récupérer les données d'une ligne
      ArrayList<String> details = new ArrayList<String>();         // Permet de récupérer les détails
      Appareil appareil = null;                                      // Permet de créer un élément avec les détails trouvés
      
      for (Object i : listeDonnees) // Récupère les détails de l'élément de la ligne séléctionnée
         details.add(String.valueOf(i));

      // (Positions à la main, ça évite de les stocker juste pour ça)
      appareil = new Appareil(Integer.parseInt(details.get(9)), details.get(0), details.get(1), details.get(2), details.get(3), details.get(4), null, details.get(5), details.get(6), details.get(7), details.get(8));
      
      return appareil; // Retourne l'élément trouvé
   }
   
   
   /**
    * Permet d'ajouter un élément à la base de donnée (Avec contrôle des erreurs)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param element L'élément à ajouter
    */ 
   public void ajoute(Element element){ 
   	Appareil appareil = (Appareil) element;

      Float prix = null;
      String date = null;
      
      // Si l'appareil est valide
      if (appareil.estValide()){
         // Ajoute l'appareil et traîte les éventuelles erreurs
         try {
            if (!appareil.prix.isEmpty())
               prix = Float.parseFloat(appareil.prix);
            if (!appareil.dateVenteDebut.isEmpty())
               date = appareil.dateVenteDebut;

            new AjoutAppareil(appareil.genre, appareil.marque, appareil.type, appareil.noSerie, date, prix, appareil.noFacture, "", "", Integer.parseInt(appareil.proprietaire), Integer.parseInt(appareil.status)).execute();
            afficherListe(); // Actualise l'affichage
         }
         catch (NumberFormatException e1) {
            exSaisie();
         }
         catch (SQLException e1) {
            exSaisie();
         }
         catch (ErreurConnexionBD e1) {
            exBdCon();
         }
      }
      else
         exSaisie();
   } 

   /**
    * Permet de modifier un élément de la base de donnée (Avec contrôle des erreurs)
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param element L'élément à modifier
    */ 
   public void modifie(Element element){
   	Appareil appareil = (Appareil) element;

      Float prix = null;
      String date = null;

      // Si l'appareil est valide
      if (appareil.estValide()){
         // Modifie l'appareil et traite les éventuelles erreurs
         try {
            if (appareil.prix != null && !appareil.prix.isEmpty() && !appareil.prix.equalsIgnoreCase("NULL"))
               prix = Float.parseFloat(appareil.prix);
            if (appareil.dateVenteDebut != null && !appareil.dateVenteDebut.isEmpty() && !appareil.dateVenteDebut.equalsIgnoreCase("NULL"))
               date = appareil.dateVenteDebut;
            new MiseAJourAppareil(appareil.id, appareil.genre, appareil.marque, appareil.type, appareil.noSerie, date, prix, appareil.noFacture, "", "", Integer.parseInt(appareil.proprietaire), Integer.parseInt(appareil.status)).execute();
            afficherListe(); // Actualise l'affichage
         }
         catch (NumberFormatException e1) {
            exSaisie();
         }
         catch (SQLException e1) {
            exSaisie();
         }
         catch (ErreurConnexionBD e1) {
            exBdCon();
         }
      }
   }
   
   /**
    * Permet de supprimer un élément de la base de donnée depuis la table
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param id L'ID de l'élément à supprimer
    */ 
   public void supprime(int id){      
      // Supprime en fonction de l'ID trouvé
      try {
         new SuppressionAppareil(id).execute();
         // Actualise l'affichage
         afficherListe();
      }
      catch (NumberFormatException e) {}
      catch (SQLException e) {}
      catch (ErreurConnexionBD e) {
         exBdCon();
      }
   }
	
   /**
    * Permet de rechercher un élément dans la base de donnée
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param element L'élément à rechercher
    */ 
   public void recherche(Element element){
   	appareilRecherche = (Appareil) element;

      String chnDeChmpsTmp = ""; // Pour créer la chaîne de champs sous forme compréensible par la bdd

      tabModel.vide(); // Nettoie (Vide) le modèle à afficher

      try {
         // Converti les champs en un format utilisable par la bdd
         for (int i = 0; i < appareilRecherche.getChamps().length-1; i++)
            chnDeChmpsTmp += appareilRecherche.getChamps()[i] + ", ";
         chnDeChmpsTmp += appareilRecherche.getChamps()[appareilRecherche.getChamps().length-1];

         // Donne une valeur nulle aux critères non-saisis et s'occupe des nombres
         if (appareilRecherche.genre != null && appareilRecherche.genre.isEmpty()) appareilRecherche.genre = null;
         if (appareilRecherche.marque != null && appareilRecherche.marque.isEmpty()) appareilRecherche.marque = null;
         if (appareilRecherche.type != null && appareilRecherche.type.isEmpty()) appareilRecherche.type = null;
         if (appareilRecherche.noSerie != null && appareilRecherche.noSerie.isEmpty()) appareilRecherche.noSerie = null;
         if (appareilRecherche.dateVenteDebut != null && appareilRecherche.dateVenteDebut.isEmpty()) appareilRecherche.dateVenteDebut = null;
         if (appareilRecherche.dateVenteFin != null && appareilRecherche.dateVenteFin.isEmpty()) appareilRecherche.dateVenteFin = null;
         if (appareilRecherche.prix != null)
         	if(appareilRecherche.prix.isEmpty()) 
         		prix = null; 
         	else 
         		prix = Float.parseFloat(appareilRecherche.prix);
         if (appareilRecherche.noFacture != null && appareilRecherche.noFacture.isEmpty()) appareilRecherche.noFacture = null;
         if (appareilRecherche.proprietaire != null)
         	if (appareilRecherche.proprietaire.isEmpty()) 
         		parentId1 = null; 
         	else 
         		parentId1 = Integer.parseInt(appareilRecherche.proprietaire);
         if (appareilRecherche.status != null)
         	if(appareilRecherche.status.isEmpty()) 
         		status = null; 
         	else 
         		status = Integer.parseInt(appareilRecherche.status);
                     
         ResultSet tmp = new RechercheAppareil(chnDeChmpsTmp, null, appareilRecherche.genre, appareilRecherche.marque, appareilRecherche.type, appareilRecherche.noSerie, appareilRecherche.dateVenteDebut, appareilRecherche.dateVenteFin, prix, appareilRecherche.noFacture, "", "", parentId1, status).execute();
         tabModel.addValue(tmp);
         tmp.close();
     
      } catch (SQLException e) {
         exSaisie();
      } catch (ErreurConnexionBD e) {
         exBdCon();
      }
   }
}
