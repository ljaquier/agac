/*----------------------------------------------------------------------------------
Fichier       : GestionClient.java

Date          : 23 juin 08

Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

But           : Pour gérer les clients

Remarque(s)    : -

VM			      : Java 1.6.0_03
----------------------------------------------------------------------------------*/
package gestion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import graphisme.ModeleTable;
import donnees.AjoutAppareil;
import donnees.AjoutClient;
import donnees.AjoutReparation;
import donnees.Appareil;
import donnees.Client;
import donnees.Element;
import donnees.ErreurConnexionBD;
import donnees.MiseAJourAppareil;
import donnees.MiseAJourClient;
import donnees.MiseAJourReparation;
import donnees.RechercheAppareil;
import donnees.RechercheClient;
import donnees.RechercheReparation;
import donnees.Reparation;
import donnees.SuppressionAppareil;
import donnees.SuppressionClient;
import donnees.SuppressionReparation;

public class GestionClient extends Gestion {

    private Client clientRecherche;
    private Integer NPA = null; // Traite les nombres à part. (Car ils sont convertis)

    /**
     * Permet de construire un gestionnaire de base de donnée
     * @param tabModel Le modèle de table qui est utilisé
     * @param typeDonnee Element définissant le type dont il fait partie
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     */
    public GestionClient(ModeleTable tabModel) {
        super(tabModel);
        clientRecherche = new Client(null, null, null, null, null, null, null, null, null);
    }

    /**
     * Permet d'afficher dans la jTable les champs d'un certain type voulus
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     */
    public void afficherListe() {
        String chnDeChmpsTmp = ""; // Pour créer la chaîne de champs sous forme compréensible par la bdd

        tabModel.vide();

        try {
            // Converti les champs en un format utilisable par la bdd
            for (int i = 0; i < clientRecherche.getChamps().length - 1; i++) {
                chnDeChmpsTmp += clientRecherche.getChamps()[i] + ", ";
            }
            chnDeChmpsTmp += clientRecherche.getChamps()[clientRecherche.getChamps().length - 1];
            // Ajoute les données retournées dans la table
            ResultSet tmp = new RechercheClient(chnDeChmpsTmp, null, clientRecherche.nom, clientRecherche.prenom, clientRecherche.profession, clientRecherche.noTelPrincipal, clientRecherche.adresse, NPA, clientRecherche.localite).execute();
            tabModel.addValue(tmp);
            tmp.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ErreurConnexionBD e) {
            exBdCon();
        }
    }

    /**
     * Permet d'obtenir les détails d'une entrée en fonction de sa ligne dans la table
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param ligne La ligne concernée
     * @return Element correspondant à la ligne de la table
     */
    public Client getDetail(int ligne) {
        Client client = null;     // Permet de créer un élément avec les détails trouvés
        // il est possible de déselectionner avec la touche control
        if (ligne >= 0) {
            ArrayList<Object> listeDonnees = tabModel.getValueAt(ligne); // Permet de récupérer les données d'une ligne
            ArrayList<String> details = new ArrayList<String>();         // Permet de récupérer les détails

            for (Object i : listeDonnees) // Récupère les détails de l'élément de la ligne séléctionnée
            {
                details.add(String.valueOf(i));
            }

            // (Positions à la main, ça évite de les stocker juste pour ça)
            client = new Client(Integer.parseInt(details.get(8)), details.get(0), details.get(1), details.get(3), details.get(6),
                    details.get(7), details.get(2), details.get(4), details.get(5));
        }
        return client; // Retourne l'élément trouvé
    }

    /**
     * Permet d'ajouter un élément à la base de donnée (Avec contrôle des erreurs)
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param element L'élément à ajouter
     */
    public void ajoute(Element element) {
        Client client = (Client) element;

        // Si le client est valide
        if (client.estValide()) {
            // Ajoute le client et traîte les éventuelles erreurs
            try {
                new AjoutClient(client.nom, client.prenom, client.profession, client.noTelPrincipal, client.noTelSecondaire, client.adresse, Integer.parseInt(client.NPA), client.localite).execute();
                afficherListe(); // Actualise l'affichage
            } catch (NumberFormatException e1) {
                exSaisie();
            } catch (SQLException e1) {
                exSaisie();
            } catch (ErreurConnexionBD e1) {
                exBdCon();
            }
        } else {
            exSaisie();
        }
    }

    /**
     * Permet de modifier un élément de la base de donnée (Avec contrôle des erreurs)
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param element L'élément à modifier
     */
    public void modifie(Element element) {
        Client client = (Client) element;
        // Si le client est valide
        if (client.estValide()) {
            // Modifie le client et traite les éventuelles erreurs
            try {
                new MiseAJourClient(client.id, client.nom, client.prenom, client.profession, client.noTelPrincipal, client.noTelSecondaire, client.adresse, Integer.parseInt(client.NPA), client.localite).execute();
                afficherListe(); // Actualise l'affichage
            } catch (NumberFormatException e1) {
                exSaisie();
            } catch (SQLException e1) {
                exSaisie();
            } catch (ErreurConnexionBD e1) {
                exBdCon();
            }
        }
    }

    /**
     * Permet de supprimer un élément de la base de donnée depuis la table
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param id L'ID de l'élément à supprimer
     */
    public void supprime(int id) {
        // Supprime en fonction de l'ID trouvé
        try {
            new SuppressionClient(id).execute();
            // Actualise l'affichage
            afficherListe();
        } catch (NumberFormatException e) {
        } catch (SQLException e) {
        } catch (ErreurConnexionBD e) {
            exBdCon();
        }
    }

    /**
     * Permet de rechercher un élément dans la base de donnée
     * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
     * @param element L'élément à rechercher
     */
    public void recherche(Element element) {
        clientRecherche = (Client) element;

        String chnDeChmpsTmp = ""; // Pour créer la chaîne de champs sous forme compréensible par la bdd

        tabModel.vide(); // Nettoie (Vide) le modèle à afficher

        try {
            // Converti les champs en un format utilisable par la bdd
            for (int i = 0; i < clientRecherche.getChamps().length - 1; i++) {
                chnDeChmpsTmp += clientRecherche.getChamps()[i] + ", ";
            }
            chnDeChmpsTmp += clientRecherche.getChamps()[clientRecherche.getChamps().length - 1];

            // Donne une valeur nulle aux critères non-saisis et s'occupe des nombres
            if (clientRecherche.nom != null && clientRecherche.nom.isEmpty()) {
                clientRecherche.nom = null;
            }
            if (clientRecherche.prenom != null && clientRecherche.prenom.isEmpty()) {
                clientRecherche.prenom = null;
            }
            if (clientRecherche.profession != null && clientRecherche.profession.isEmpty()) {
                clientRecherche.profession = null;
            }
            if (clientRecherche.adresse != null && clientRecherche.adresse.isEmpty()) {
                clientRecherche.adresse = null;
            }
            if (clientRecherche.NPA != null) {
                if (clientRecherche.NPA.isEmpty()) {
                    NPA = null;
                } else {
                    NPA = Integer.parseInt(clientRecherche.NPA);
                }
            }
            if (clientRecherche.localite != null && clientRecherche.localite.isEmpty()) {
                clientRecherche.localite = null;
            }
            if (clientRecherche.noTelPrincipal != null && clientRecherche.noTelPrincipal.isEmpty()) {
                clientRecherche.noTelPrincipal = null;
            }
//            if (clientRecherche.noTelSecondaire.isEmpty()) clientRecherche.noTelSecondaire = null;

            ResultSet tmp = new RechercheClient(chnDeChmpsTmp, null, clientRecherche.nom, clientRecherche.prenom, clientRecherche.profession, clientRecherche.noTelPrincipal, clientRecherche.adresse, NPA, clientRecherche.localite).execute();
            tabModel.addValue(tmp);
            tmp.close();

        } catch (SQLException e) {
            exSaisie();
        } catch (ErreurConnexionBD e) {
            exBdCon();
        }
    }
}
