package definition;
/*----------------------------------------------------------------------------------
    Fichier       : Definition.java

    Date          : 15 sept. 08

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

    But           : Définir les variables globales semblable à ifdef

	Remarque(s)    : -
                  
	VM			      : Java 1.6.0_03
----------------------------------------------------------------------------------*/

public final class Definition {
	
	private Definition(){};
	// pour définir si les dates peuvent être entrées au clavier
	public static Boolean dateManuelle = false;
	public final static String formatDateManuelle = "dd.MM.yyyy";	
	public final static String formatDateAutomatique = "EEE dd.MM.yyyy";	

}
