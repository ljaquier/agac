/*----------------------------------------------------------------------------------
    Fichier       : ErreurSaisie.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Permet de définir les erreurs de saisie.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package graphisme;

public class ErreurSaisie extends RuntimeException{
   public ErreurSaisie(){
      super();
   }
}
