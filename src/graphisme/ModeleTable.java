/*----------------------------------------------------------------------------------
    Fichier       : ModeleTable.java
    Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    Date          : 22.02.2008

    But           : Modèle de jTable adaptée à notre utilisation, implémentée sous
                    forme d'ArrayLists.

    Remarque(s)   : -

    VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package graphisme;

import java.sql.*; import java.util.*; import javax.swing.table.*;

import com.sun.crypto.provider.RSACipher;

public class ModeleTable extends AbstractTableModel {
   private static final long serialVersionUID = 1L; // Classe sérialisable
   private String [] labelColonne;    // Entêtes des colonnes
   private ArrayList<ArrayList<Object>> entrees = new ArrayList<ArrayList<Object>>(); // Permet de stocker les différentes entrées
   private ArrayList<Object> donnees; // Permet de stocker les données des différentes entrées

   public ModeleTable (String [] champs) {
      String [] titres = {champs[0], champs[1], champs[2]}; // N'affiche que ces champs dans la table (Convention, on n'affiche que les 3 premiers)
      labelColonne = titres; // Stock les titres des colonnes
   }
   
   // Ajoute une entrée à la structure
   public void addValue(String [] donnee) {
      donnees = new ArrayList<Object>(); // Prépare une nouvelle entrée
      for (int i = 0; i < donnee.length; i++)
         donnees.add(donnee[i]);  // Ajoute les données à l'entrée
      entrees.add(donnees);       // Ajoute l'entrée
      fireTableDataChanged();     // Actualise la table
   }

   // Ajoute à la structure le résultat d'une requête SQL
   public synchronized void addValue(ResultSet rs) {
      try{
         Object test;
         int nb;
         while(!rs.isClosed() && !rs.isAfterLast() && rs.next()){
            donnees = new ArrayList<Object>(); // Prépare une nouvelle entrée
            nb = rs.getMetaData().getColumnCount();
            for(int i = 1; i <= nb; i++)
               if (!rs.isClosed() && !rs.isAfterLast()){
               test = rs.getObject(i);
//               System.out.println(test);
               donnees.add(test);   // Ajoute les données
            }
            entrees.add(donnees);              // Ajoute les données à l'entrée
         }
      }//catch (SQLException e){e.printStackTrace();}
      catch (Exception e){}
      fireTableDataChanged(); // Actualise la table
   }
   
   // Permet de vider la structure
   public void vide(){
      entrees = new ArrayList<ArrayList<Object>>();
   }
   
   // Retourne les titres des colonnes
   public String[] getLabels(){
      return labelColonne;
   }

   // Retourne le titre d'une certaine colonne
   public String getColumnName (int col) {
      return labelColonne[col].toString();
   }

   // Retourne la valeur en fonction de sa position dans la structure
   public Object getValueAt (int row, int col) {
      return entrees.get(row).get(col);
   }
   
   // Retourne les données d'une ligne de la structure
   public ArrayList<Object> getValueAt (int row) {
      return entrees.get(row);
   }

   // Retourne le nombre de colonnes de la structure
   public int getColumnCount() {
      return labelColonne.length;
   }

   // Retourne le nombre de lignes de la structure
   public int getRowCount() {
      return entrees.size();
   }
}