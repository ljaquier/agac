/*----------------------------------------------------------------------------------
   Fichier       : InviteDateAssignation.java

   Date          : 27.06.2008

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Invite  de date de vente lors d'une assignation d'un appareil
   					 à un client.

   Remarque(s)   : -
                  
   VM		        : Java 1.6.0_03
----------------------------------------------------------------------------------*/
package graphisme;
import gestion.Utilitaire;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import org.jdesktop.swingx.*;

import definition.Definition;

public class InviteDateAssignation extends javax.swing.JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel jLabelDate;
	private JButton jButtonValidation;
	private JXDatePicker jXDatePickerDateVente;

	
	public InviteDateAssignation(JFrame frame) {
		super(frame);
		initGUI();
	}
	
	
	public String affiche()
	{
		this.setVisible(true);
		return Utilitaire.getSQLDate(jXDatePickerDateVente.getDate()); 
	}
	
	private void initGUI() {
		try {
			{
				this.setSize(new Dimension(265, 155));	
	         // pour centrer la fenêtre
	         this.setLocationRelativeTo(null);
	         this.setTitle("Choisir la date d'assignation");
	         this.setModal(true);
	         this.setResizable(false);
				getContentPane().setLayout(null);
				this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

				{
					jLabelDate = new JLabel();
					getContentPane().add(jLabelDate);
					jLabelDate.setText("Choisir la date de vente de l'appareil :");
					jLabelDate.setBounds(12, 12, 236, 14);
				}
				{
					jButtonValidation = new JButton();
					getContentPane().add(jButtonValidation);
					jButtonValidation.setText("OK");
					jButtonValidation.setBounds(84, 98, 77, 21);
					jButtonValidation.setEnabled(false);
					jButtonValidation.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							InviteDateAssignation.this.dispose();
						}
					});
				}
				
				{
					jXDatePickerDateVente = new JXDatePicker();
					getContentPane().add(jXDatePickerDateVente);
					jXDatePickerDateVente.setFont(new Font("Dialog", Font.PLAIN, 11));
					jXDatePickerDateVente.getEditor().setDisabledTextColor(Color.BLACK);
					jXDatePickerDateVente.setBounds(57, 49, 133, 21);
			   	// si l'utilisateur veut entrer les dates manuellement
					if (Definition.dateManuelle)
						jXDatePickerDateVente.setFormats(Definition.formatDateManuelle);
					else
						jXDatePickerDateVente.setFormats(Definition.formatDateAutomatique);
					jXDatePickerDateVente.getEditor().setEditable(Definition.dateManuelle);
					
					jXDatePickerDateVente.addActionListener(new ActionListener(){
		            public void actionPerformed(ActionEvent e) {
		            	// on permet d'enregistrer
		            	jButtonValidation.setEnabled(true);
		            }});
		      }
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
