/*----------------------------------------------------------------------------------
   Fichier       : FenetreRechercheAppareil.java

   Date          : 24.05.2008

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Permet de rechercher un appareil.

	Remarque(s)   : -
                  
	VM			     : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package graphisme;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import gestion.Utilitaire;

import definition.Definition;
import donnees.Appareil;
import java.util.Date;

public class FenetreRechercheAppareil extends JDialog {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField jTextFieldRechercheAppareilType = null;
	   private JTextField jTextFieldRechercheAppareilNoSerie = null;
	   private JTextField jTextFieldRechercheAppareilPrix = null;
	   private JTextField jTextFieldRechercheAppareilNoFacture = null;
	   private JComboBox jComboBoxRechercheAppareilGenre = null;
	   private JComboBox jComboBoxRechercheAppareilMarque = null;
	   private JButton jButtonRechercheAppareilOK = null;
	   private JPanel jContentPaneRechercheAppareil = null;
	   private JLabel jLabelRechercheAppareilGenre = null;
	   private JLabel jLabelRechercheAppareilMarque = null;
	   private JLabel jLabelRechercheAppareilType = null;
	   private JLabel jLabelRechercheAppareilNoSerie = null;
	   private JLabel jLabelRechercheAppareilDateVenteDebut = null;
	   private JLabel jLabelRechercheAppareilPrix = null;
	   private JLabel jLabelRechercheAppareilNoFacture = null;
	   private JLabel jLabelRemplirChampsRecherchePanne = null;
	   private JLabel jLabelRachercheAppareilDateVenteFin = null;
	   private JXDatePicker calendrierRechercheAppareilDateDebut = null;
	   private JXDatePicker calendrierRechercheAppareilDateFin = null;
	   private Appareil appareilRecherche = null;
	   private LinkedList<String> listeGenreAppareil = null;
		private LinkedList<String> listeMarqueAppareil = null;
	   
	   /**
	    * This method initializes jDialogRechercheAppareil   
	    *    
	    * @return javax.swing.JDialog   
	    */
	   public FenetreRechercheAppareil(LinkedList<String> listeGenreAppareil,
	   											LinkedList<String> listeMarqueAppareil)
   	 {
	   		super();
	      	this.listeGenreAppareil = listeGenreAppareil;
	         this.listeMarqueAppareil = listeMarqueAppareil;
	         this.setSize(new Dimension(325, 370));
	         // pour centrer la fenêtre
	         this.setLocationRelativeTo(null);
	         this.setTitle("Rechercher un appareil");
	         this.setModal(true);
	         this.setResizable(false);
	         this.setContentPane(getJContentPaneRechercheAppareil());
	         this.getRootPane().
	         setDefaultButton(getJButtonRechercheAppareilOK());
	     
	   }
	   
	   public Appareil affiche(Boolean val)
	   {
	   	this.setVisible(true);
	   	
	   	return appareilRecherche;
	   }
	   
	   /**
	    * This method initializes jContentPaneRechercheAppareil 
	    *    
	    * @return javax.swing.JPanel 
	    */
	   public JPanel getJContentPaneRechercheAppareil() {
	      if (jContentPaneRechercheAppareil == null) {
	         jLabelRachercheAppareilDateVenteFin = new JLabel();
	         jLabelRachercheAppareilDateVenteFin.setBounds(new Rectangle(15, 210, 136, 16));
	         jLabelRachercheAppareilDateVenteFin.setText("Date de vente (fin) :");
	         jLabelRemplirChampsRecherchePanne = new JLabel();
	         jLabelRemplirChampsRecherchePanne.setBounds(new Rectangle(15, 15, 241, 16));
	         jLabelRemplirChampsRecherchePanne.setText("Remplir les champs désirés :");
	         jLabelRechercheAppareilNoFacture = new JLabel();
	         jLabelRechercheAppareilNoFacture.setBounds(new Rectangle(17, 269, 136, 16));
	         jLabelRechercheAppareilNoFacture.setText("N° de facture :");
	         jLabelRechercheAppareilPrix = new JLabel();
	         jLabelRechercheAppareilPrix.setBounds(new Rectangle(17, 239, 136, 16));
	         jLabelRechercheAppareilPrix.setText("Prix :");
	         jLabelRechercheAppareilDateVenteDebut = new JLabel();
	         jLabelRechercheAppareilDateVenteDebut.setBounds(new Rectangle(15, 180, 151, 16));
	         jLabelRechercheAppareilDateVenteDebut.setText("Date de vente (début) :");
	         jLabelRechercheAppareilNoSerie = new JLabel();
	         jLabelRechercheAppareilNoSerie.setBounds(new Rectangle(15, 150, 136, 16));
	         jLabelRechercheAppareilNoSerie.setText("N° de série :");
	         jLabelRechercheAppareilType = new JLabel();
	         jLabelRechercheAppareilType.setBounds(new Rectangle(15, 120, 136, 16));
	         jLabelRechercheAppareilType.setText("Type :");
	         jLabelRechercheAppareilMarque = new JLabel();
	         jLabelRechercheAppareilMarque.setBounds(new Rectangle(15, 90, 136, 16));
	         jLabelRechercheAppareilMarque.setText("Marque :");
	         jLabelRechercheAppareilGenre = new JLabel();
	         jLabelRechercheAppareilGenre.setText("Genre :");
	         jLabelRechercheAppareilGenre.setSize(new Dimension(136, 16));
	         jLabelRechercheAppareilGenre.setName("");
	         jLabelRechercheAppareilGenre.setLocation(new Point(15, 60));
	         jContentPaneRechercheAppareil = new JPanel();
	         jContentPaneRechercheAppareil.setLayout(null);
	         jContentPaneRechercheAppareil.setPreferredSize(new java.awt.Dimension(315, 336));
	         jContentPaneRechercheAppareil.add(jLabelRechercheAppareilGenre, null);
	         jContentPaneRechercheAppareil.add(jLabelRechercheAppareilMarque, null);
	         jContentPaneRechercheAppareil.add(jLabelRechercheAppareilType, null);
	         jContentPaneRechercheAppareil.add(jLabelRechercheAppareilNoSerie, null);
	         jContentPaneRechercheAppareil.add(jLabelRechercheAppareilDateVenteDebut, null);
	         jContentPaneRechercheAppareil.add(jLabelRechercheAppareilPrix, null);
	         jContentPaneRechercheAppareil.add(jLabelRechercheAppareilNoFacture, null);
	         jContentPaneRechercheAppareil.add(getJTextFieldRechercheAppareilType(), null);
	         jContentPaneRechercheAppareil.add(getJTextFieldRechercheAppareilNoSerie(), null);
	         jContentPaneRechercheAppareil.add(getJTextFieldRechercheAppareilPrix(), null);
	         jContentPaneRechercheAppareil.add(getJTextFieldRechercheAppareilNoFacture(), null);
	         jContentPaneRechercheAppareil.add(getJComboBoxRechercheAppareilGenre(), null);
	         jContentPaneRechercheAppareil.add(getJComboBoxRechercheAppareilMarque(), null);
	         jContentPaneRechercheAppareil.add(getJButtonRechercheAppareilOK(), null);
	         jContentPaneRechercheAppareil.add(jLabelRemplirChampsRecherchePanne, null);
	         jContentPaneRechercheAppareil.add(jLabelRachercheAppareilDateVenteFin, null);
	         jContentPaneRechercheAppareil.add(getCalendrierRechercheAppareilDateDebut(), null);
	         jContentPaneRechercheAppareil.add(getCalendrierRechercheAppareilDateFin(), null);
	      }
	      return jContentPaneRechercheAppareil;
	   }

	   /**
	    * This method initializes calendrierRechercheAppareilDateDebut   
	    *    
	    * @return org.jdesktop.swingx.JXDatePicker  
	    */
	   private JXDatePicker getCalendrierRechercheAppareilDateDebut() {
	      if (calendrierRechercheAppareilDateDebut == null) {
	         calendrierRechercheAppareilDateDebut = new JXDatePicker();
	         calendrierRechercheAppareilDateDebut.setFont(new Font("Dialog", Font.PLAIN, 11));
	         calendrierRechercheAppareilDateDebut.setSize(new Dimension(136, 20));
	         calendrierRechercheAppareilDateDebut.setFormats(Definition.formatDateManuelle);
	         calendrierRechercheAppareilDateDebut.setLocation(new Point(165, 180));
                 calendrierRechercheAppareilDateDebut.setDate(new Date(0));
	      }
	      return calendrierRechercheAppareilDateDebut;
	   }


	   /**
	    * This method initializes calendrierRechercheAppareilDateFin  
	    *    
	    * @return org.jdesktop.swingx.JXDatePicker  
	    */
	   private JXDatePicker getCalendrierRechercheAppareilDateFin() {
	      if (calendrierRechercheAppareilDateFin == null) {
	         calendrierRechercheAppareilDateFin = new JXDatePicker();
	         calendrierRechercheAppareilDateFin.setFont(new Font("Dialog", Font.PLAIN, 11));
	         calendrierRechercheAppareilDateFin.setFormats(Definition.formatDateManuelle);
	         calendrierRechercheAppareilDateFin.setSize(new Dimension(136, 20));
	         calendrierRechercheAppareilDateFin.setLocation(new Point(165, 210));
                 calendrierRechercheAppareilDateFin.setDate(new Date());
	      }
	      return calendrierRechercheAppareilDateFin;
	   }
	   
   /**
    * This method initializes jTextFieldRechercheAppareilNoSerie  
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheAppareilNoSerie() {
      if (jTextFieldRechercheAppareilNoSerie == null) {
         jTextFieldRechercheAppareilNoSerie = new JTextField();
         jTextFieldRechercheAppareilNoSerie.setBounds(new Rectangle(165, 150, 136, 16));
      }
      return jTextFieldRechercheAppareilNoSerie;
   }

   /**
    * This method initializes jTextFieldRechercheAppareilPrix  
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheAppareilPrix() {
      if (jTextFieldRechercheAppareilPrix == null) {
         jTextFieldRechercheAppareilPrix = new JTextField();
         jTextFieldRechercheAppareilPrix.setBounds(new Rectangle(165, 239, 136, 17));
      }
      return jTextFieldRechercheAppareilPrix;
   }


   /**
    * This method initializes jTextFieldRechercheAppareilPrix  
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheAppareilNoFacture() {
      if (jTextFieldRechercheAppareilNoFacture == null) {
         jTextFieldRechercheAppareilNoFacture = new JTextField();
         jTextFieldRechercheAppareilNoFacture.setBounds(new Rectangle(165, 270, 136, 17));
      }
      return jTextFieldRechercheAppareilNoFacture;
   }


   /**
    * This method initializes jComboBoxRechercheAppareilGenre  
    *    
    * @return javax.swing.JComboBox 
    */
   private JComboBox getJComboBoxRechercheAppareilGenre() {
   
       if (jComboBoxRechercheAppareilGenre == null) {
         // si la liste a pu être chargée
         if (listeGenreAppareil != null)
         {
            jComboBoxRechercheAppareilGenre = new JComboBox(listeGenreAppareil.toArray());
            // doit être défini éditbale avant dêtre décorée sinon
            // l'auto-complémentation est stricte
            jComboBoxRechercheAppareilGenre.setEditable(false);
            // pour ajouter l'auto-complémentation
            AutoCompleteDecorator.decorate(jComboBoxRechercheAppareilGenre);
            jComboBoxRechercheAppareilGenre.setSelectedIndex(-1);

  
         }
         else
         {
            jComboBoxRechercheAppareilGenre = new JComboBox();
            jComboBoxRechercheAppareilGenre.setEditable(true);
         }   
         jComboBoxRechercheAppareilGenre.setLocation(new Point(165,60));
         jComboBoxRechercheAppareilGenre.setSize(new Dimension(136, 16));

      }
      return jComboBoxRechercheAppareilGenre;
   }


   /**
    * This method initializes jComboBoxRechercheAppareilMarque 
    *    
    * @return javax.swing.JComboBox 
    */
   private JComboBox getJComboBoxRechercheAppareilMarque() {
      if (jComboBoxRechercheAppareilMarque == null) {
          // si la liste a pu être chargée
         if (listeGenreAppareil != null)
         {
            jComboBoxRechercheAppareilMarque = new JComboBox(listeMarqueAppareil.toArray());
            // doit être défini éditbale avant dêtre décorée sinon
            // l'auto-complémentation est stricte
            jComboBoxRechercheAppareilMarque.setEditable(false);
            // pour ajouter l'auto-complémentation
            AutoCompleteDecorator.decorate(jComboBoxRechercheAppareilMarque);
            jComboBoxRechercheAppareilMarque.setSelectedIndex(-1);
         }
         else
         {
            jComboBoxRechercheAppareilMarque = new JComboBox();
            jComboBoxRechercheAppareilMarque.setEditable(true);
         }   
         jComboBoxRechercheAppareilMarque.setLocation(new Point(165,90));
         jComboBoxRechercheAppareilMarque.setSize(new Dimension(136, 16));
      }
      
      return jComboBoxRechercheAppareilMarque;
   }


   /**
    * This method initializes jButtonRechercheAppareilOK 
    *    
    * @return javax.swing.JButton   
    */
   private JButton getJButtonRechercheAppareilOK() {
      if (jButtonRechercheAppareilOK == null) {
         jButtonRechercheAppareilOK = new JButton();
         jButtonRechercheAppareilOK.setText("OK");
         jButtonRechercheAppareilOK.setLocation(new Point(105, 345));
         jButtonRechercheAppareilOK.setBounds(105, 308, 119, 14);
	       jButtonRechercheAppareilOK
	             .addActionListener(new java.awt.event.ActionListener() {
	                public void actionPerformed(java.awt.event.ActionEvent e) {
	                   appareilRecherche = new Appareil(null,
	                   getJComboBoxRechercheAppareilGenre().getSelectedIndex() == -1 ? null : 
	                  	 getJComboBoxRechercheAppareilGenre().getSelectedItem().toString() ,
	                   getJComboBoxRechercheAppareilMarque().getSelectedIndex() == -1 ? null :
	                  	 getJComboBoxRechercheAppareilMarque().getSelectedItem().toString(),
	                   getJTextFieldRechercheAppareilType().getText(),
	                   getJTextFieldRechercheAppareilNoSerie().getText(),
	                   Utilitaire.getSQLDate(getCalendrierRechercheAppareilDateDebut().getDate()), 
	                   Utilitaire.getSQLDate(getCalendrierRechercheAppareilDateFin().getDate()),  
	                   getJTextFieldRechercheAppareilPrix().getText(),
	                   getJTextFieldRechercheAppareilNoFacture().getText(),
	                   null,
	                   null);

	                   
	                   FenetreRechercheAppareil.this.dispose();
	                }
	             });
      }
      return jButtonRechercheAppareilOK;
   }

   
   /**
    * This method initializes jTextFieldAppareilType   
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheAppareilType() {
      if (jTextFieldRechercheAppareilType == null) {
      	jTextFieldRechercheAppareilType = new JTextField();
      	jTextFieldRechercheAppareilType.setBounds(new Rectangle(135, 90, 151, 16));
      	jTextFieldRechercheAppareilType.setBounds(165, 121, 136, 16);
      	jTextFieldRechercheAppareilType.setEditable(true);
      }
      return jTextFieldRechercheAppareilType;
   }
}