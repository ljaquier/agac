/*----------------------------------------------------------------------------------
   Fichier       : FenetreRechercheReparation.java

   Date          : 24.05.2008

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Permet de rechercher une réparation.

   Remarque(s)   : -
                  
   VM		        : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package graphisme;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;

import gestion.Utilitaire;

import definition.Definition;
import donnees.Reparation;
import java.util.Date;

public class FenetreRechercheReparation extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * ... description de la méthode/classe/package/variable ...
	 * @author Lucien Chaubert / Louis Jaquier
	 */
	
   private JLayeredPane jPanelRechercheReparationDetails = null;
   private JLabel jLabelRechercheReparationDate = null;
   private JLabel jLabelRechercheReparationPanne = null;
   private JLabel jLabelRechercheReparationTravailEff = null;
   private JLabel jLabelRechercheReparationTechnicien = null;
   private JLabel jLabelRechercheReparationPrix = null;
   private JTextField jTextFieldRechercheReparationTechnicien = null;
   private JLabel jLabelRechercheReparationPaye = null;
   private TextExtens jTextExtensRechercheReparationPanne = null;
   private TextExtens jTextExtensRechercheReparationTravailEffectue = null;
   private JTextField jTextFieldRechercheReparationPrix = null;
   private JCheckBox jCheckBoxRechercheReparationPaye = null;
   private JXDatePicker calendrierRechercheReparationDateDebut = null;
   private JButton jButtonRechercheReparationOK = null;
   private JLabel jLabelRechercheReparationDate1 = null;
   private JLabel jLabelRemplirChampsRechercheReparation = null;
   private JLabel jLabelRechercheReparationNoFacture = null;
   private JTextField jTextFieldRechercheReparationNoFacture = null;
   private JXDatePicker calendrierRechercheReparationDateFin = null;
   private Boolean AEncaisser = null;
   private Reparation reparationRecherchee = null;
	
	 /**
    * ... description de la méthode/classe/package/variable ...
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param AEncaisser pour savoir si l'option payé doit être affichle ou non
    * @return
    */ 
   
   
   public  FenetreRechercheReparation(Boolean AEncaisser) {
         super();
         this.setSize(new Dimension(360, 372));
         // pour centrer la fenêtre
         this.setLocationRelativeTo(null);
         this.setTitle("Rechercher une réparation");
         this.setModal(true);
         this.setResizable(false);
         this.setContentPane(getJPanelRechercheReparationDetails(AEncaisser));
         this.getRootPane().
         setDefaultButton(getJButtonRechercheReparationOK());
         this.AEncaisser = AEncaisser;
   }


   public Reparation affiche(boolean val)
   {
   	this.setVisible(val);
   	return reparationRecherchee;
   }

   /**
    * This method initializes jPanelRechercheReparationDetails 
    *    
    * @return javax.swing.JLayeredPane 
    */
   private JLayeredPane getJPanelRechercheReparationDetails(Boolean AEncaisser) {
      if (jPanelRechercheReparationDetails == null) {

         jLabelRechercheReparationNoFacture = new JLabel();
         jLabelRechercheReparationNoFacture.setBounds(new Rectangle(15, 240, 121, 16));
         jLabelRechercheReparationNoFacture.setText("N° de facture");
         jLabelRemplirChampsRechercheReparation = new JLabel();
         jLabelRemplirChampsRechercheReparation.setBounds(new Rectangle(15, 15, 178, 15));
         jLabelRemplirChampsRechercheReparation.setText("Remplir les champs désirés :");
         jLabelRechercheReparationDate1 = new JLabel();
         jLabelRechercheReparationDate1.setBounds(new Rectangle(16, 95, 179, 15));
         jLabelRechercheReparationDate1.setText("Date de réparation (fin) :");
         jLabelRechercheReparationPrix = new JLabel();
         jLabelRechercheReparationPrix.setBounds(new Rectangle(15, 210, 121, 16));
         jLabelRechercheReparationPrix.setText("Prix :");
         jLabelRechercheReparationTechnicien = new JLabel();
         jLabelRechercheReparationTechnicien.setBounds(new Rectangle(15, 180, 121, 16));
         jLabelRechercheReparationTechnicien.setText("Technicien :");
         jLabelRechercheReparationTravailEff = new JLabel();
         jLabelRechercheReparationTravailEff.setBounds(new Rectangle(15, 150, 121, 16));
         jLabelRechercheReparationTravailEff.setText("Travail effectué :");
         jLabelRechercheReparationPanne = new JLabel();
         jLabelRechercheReparationPanne.setBounds(new Rectangle(15, 120, 121, 16));
         jLabelRechercheReparationPanne.setText("Panne :");
         jLabelRechercheReparationDate = new JLabel();
         jLabelRechercheReparationDate.setBounds(new Rectangle(14, 60, 181, 16));
         jLabelRechercheReparationDate.setText("Date de réparation (début) :");
         jPanelRechercheReparationDetails = new JLayeredPane();
         jPanelRechercheReparationDetails.setLayout(null);
         jPanelRechercheReparationDetails.setName("");
         jPanelRechercheReparationDetails.add(jLabelRechercheReparationDate, null);
         jPanelRechercheReparationDetails.add(jLabelRechercheReparationPanne, null);
         jPanelRechercheReparationDetails.add(jLabelRechercheReparationTravailEff, null);
         jPanelRechercheReparationDetails.add(jLabelRechercheReparationTechnicien, null);
         jPanelRechercheReparationDetails.add(jLabelRechercheReparationPrix, null);
         jPanelRechercheReparationDetails.add(getJTextFieldRechercheReparationTechnicien(), null);
         jPanelRechercheReparationDetails.add(getJTextExtensRechercheReparationPanne(), null);
         jPanelRechercheReparationDetails.add(getJTextExtensRechercheReparationTravailEffectue(), null);
         jPanelRechercheReparationDetails.add(getJTextFieldRechercheReparationPrix(), null);
         jPanelRechercheReparationDetails.add(getCalendrierRechercheReparationDateDebut(), null);
         jPanelRechercheReparationDetails.add(getJButtonRechercheReparationOK(), null);
         jPanelRechercheReparationDetails.setLayer(getJTextExtensRechercheReparationPanne(), 200);
         jPanelRechercheReparationDetails.setLayer(getJTextExtensRechercheReparationTravailEffectue(), 200);
         jPanelRechercheReparationDetails.add(jLabelRechercheReparationDate1, null);
         jPanelRechercheReparationDetails.add(getCalendrierRechercheReparationDateFin(), null);
         jPanelRechercheReparationDetails.add(jLabelRemplirChampsRechercheReparation, null);
         jPanelRechercheReparationDetails.add(getJTextFieldRechercheReparationNoFacture(), null);
         jPanelRechercheReparationDetails.add(jLabelRechercheReparationNoFacture, null);
         // si on doit afficher l'option Payé
         if (!AEncaisser)
         {        
            jLabelRechercheReparationPaye = new JLabel();
            jLabelRechercheReparationPaye.setBounds(new Rectangle(15, 270, 121, 16));
            jLabelRechercheReparationPaye.setText("Payé :");
            jPanelRechercheReparationDetails.add(getJCheckBoxRechercheReparationPaye(), null);
            jPanelRechercheReparationDetails.add(jLabelRechercheReparationPaye, null);
            
         }
      }
      return jPanelRechercheReparationDetails;
   }


   /**
    * This method initializes jTextFieldRechercheReparationTechnicien   
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheReparationTechnicien() {
      if (jTextFieldRechercheReparationTechnicien == null) {
         jTextFieldRechercheReparationTechnicien = new JTextField();
         jTextFieldRechercheReparationTechnicien.setBounds(new Rectangle(194, 180, 136, 16));
      }
      return jTextFieldRechercheReparationTechnicien;
   }


   /**
    * This method initializes jPanelBoutonsClientStock12  
    *    
    * @return javax.swing.JPanel 
    */
   

   


   


   /**
    * This method initializes jTextExtensRechercheReparationPanne 
    *    
    * @return graphisme.TextExtens  
    */
   private TextExtens getJTextExtensRechercheReparationPanne() {
      if (jTextExtensRechercheReparationPanne == null) {
         jTextExtensRechercheReparationPanne = new TextExtens(136, 200);
         jTextExtensRechercheReparationPanne.setLocation(new Point(194, 120));
         jTextExtensRechercheReparationPanne.setSize(new Dimension(136, 19));
      }
      return jTextExtensRechercheReparationPanne;
   }


   /**
    * This method initializes jTextExtensRechercheReparationTravailEffectue   
    *    
    * @return graphisme.TextExtens  
    */
   private TextExtens getJTextExtensRechercheReparationTravailEffectue() {
      if (jTextExtensRechercheReparationTravailEffectue == null) {
         jTextExtensRechercheReparationTravailEffectue = new TextExtens(136, 200);
         jTextExtensRechercheReparationTravailEffectue.setBounds(new Rectangle(194, 150, 136, 19));
      }
      return jTextExtensRechercheReparationTravailEffectue;
   }


   /**
    * This method initializes jTextFieldRechercheReparationPrix   
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheReparationPrix() {
      if (jTextFieldRechercheReparationPrix == null) {
         jTextFieldRechercheReparationPrix = new JTextField();
         jTextFieldRechercheReparationPrix.setBounds(new Rectangle(194, 210, 136, 16));
      }
      return jTextFieldRechercheReparationPrix;
   }


   /**
    * This method initializes jCheckBoxRechercheReparationPaye 
    *    
    * @return javax.swing.JCheckBox 
    */
   private JCheckBox getJCheckBoxRechercheReparationPaye() {
      if (jCheckBoxRechercheReparationPaye == null) {
         jCheckBoxRechercheReparationPaye = new JCheckBox();
         jCheckBoxRechercheReparationPaye.setBounds(new Rectangle(195, 270, 136, 16));
      }
      return jCheckBoxRechercheReparationPaye;
   }


   /**
    * This method initializes calendrierRechercheReparationDateDebut 
    *    
    * @return org.jdesktop.swingx.JXDatePicker  
    */
   private JXDatePicker getCalendrierRechercheReparationDateDebut() {
      if (calendrierRechercheReparationDateDebut == null) {
         calendrierRechercheReparationDateDebut = new JXDatePicker();
         calendrierRechercheReparationDateDebut.setLocation(new Point(194, 60));
         calendrierRechercheReparationDateDebut.setFormats(Definition.formatDateManuelle);
         calendrierRechercheReparationDateDebut.setFont(new Font("Dialog", Font.PLAIN, 11));
         calendrierRechercheReparationDateDebut.setSize(new Dimension(137, 19));
         calendrierRechercheReparationDateDebut.setDate(new Date(0));
      }
      return calendrierRechercheReparationDateDebut;
   }


   /**
    * This method initializes jButtonRechercheReparationOK  
    *    
    * @return javax.swing.JButton   
    */
   private JButton getJButtonRechercheReparationOK() {
      if (jButtonRechercheReparationOK == null) {               
         jButtonRechercheReparationOK = new JButton();
         jButtonRechercheReparationOK.setText("OK");
         jButtonRechercheReparationOK.setSize(new Dimension(122, 16));
         jButtonRechercheReparationOK.setLocation(new Point(105, 315));
         jButtonRechercheReparationOK
               .addActionListener(new java.awt.event.ActionListener() {
                  public void actionPerformed(java.awt.event.ActionEvent e) {
                  	reparationRecherchee = new Reparation (
                  			Utilitaire.getSQLDate(getCalendrierRechercheReparationDateDebut().getDate()),
                  			Utilitaire.getSQLDate(getCalendrierRechercheReparationDateFin().getDate()),
                           getJTextExtensRechercheReparationPanne().getText(),
                           getJTextExtensRechercheReparationTravailEffectue().getText(),
                           getJTextFieldRechercheReparationTechnicien().getText(),
                           getJTextFieldRechercheReparationPrix().getText(),
                           getJCheckBoxRechercheReparationPaye().isSelected(), 
                           getJTextFieldRechercheReparationNoFacture().getText(),
                           null, null, null);
                  	
                  			FenetreRechercheReparation.this.dispose();
                  }
               });
      }

      return jButtonRechercheReparationOK;
   }


   /**
    * This method initializes calendrierRechercheReparationDateFin   
    *    
    * @return org.jdesktop.swingx.JXDatePicker  
    */
   private JXDatePicker getCalendrierRechercheReparationDateFin() {
      if (calendrierRechercheReparationDateFin == null) {
         calendrierRechercheReparationDateFin = new JXDatePicker();
         calendrierRechercheReparationDateFin.setBounds(new Rectangle(194, 90, 137, 20));
         calendrierRechercheReparationDateFin.setFormats(Definition.formatDateManuelle);
         calendrierRechercheReparationDateFin.setFont(new Font("Dialog", Font.PLAIN, 11));
         calendrierRechercheReparationDateFin.setDate(new Date());
      }
      return calendrierRechercheReparationDateFin;
   }


   /**
    * This method initializes jTextFieldRechercheReparationNoFacture 
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheReparationNoFacture() {
      if (jTextFieldRechercheReparationNoFacture == null) {
         jTextFieldRechercheReparationNoFacture = new JTextField();
         jTextFieldRechercheReparationNoFacture.setBounds(new Rectangle(195, 240, 136, 16));
      }
      return jTextFieldRechercheReparationNoFacture;
   }

	
	
}
