/*----------------------------------------------------------------------------------
Fichier       : FenetreAffichage.java
Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet
Date          : 22.02.2008

But           : Permet de créer la fenêtre d'affichage principale du programme
ainsi que ces divers éléments.

Remarque(s)   : -

VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/
package graphisme;

// <editor-fold defaultstate="collapsed" desc="Declarations">
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.autocomplete.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.net.*;
import javax.swing.border.*;
import definition.Definition;
import donnees.*;
import org.jdesktop.swingx.border.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import gestion.*;
import impression.*;
import backupRestore.*;
import java.awt.Font;
import java.awt.event.KeyEvent;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
// </editor-fold>

public class FenetreAffichage implements MouseListener {
	// Pour les différents éléments graphiques

	// <editor-fold defaultstate="collapsed" desc="Attributes">
	private JFrame jFrame = null;
	private JMenuBar jJMenuBar = null;
	private JMenu fileMenu = null;
	private JMenu helpMenu = null;
	private JMenuItem exitMenuItem = null;
	private JMenuItem aboutMenuItem = null;
	private JDialog aboutDialog = null;
	private JPanel aboutContentPane = null;
	private JLabel aboutVersionLabel = null;
	private JLabel imgLabel = null;
	private JTabbedPane jTabbedPanePrincipal = null;
	private JPanel jPanelClient = null;
	private JPanel jPanelStock = null;
	private JPanel jPanelVentes = null;
	private JTabbedPane jTabbedPaneClient = null;
	private JPanel jPanelDetailClient = null;
	private JPanel jPanelAppareils = null;
	private JPanel jPanelAEncaisserClient = null;
	private JPanel jPanelRappelClient = null;
	private JTabbedPane jTabbedPaneAppareils = null;
	private JPanel jPanelReparations = null;
	private JMenu jMenuImprimer = null;
	private JMenuItem jMenuItemImprimerDetails = null;
	private JMenuItem jMenuItemImprimerStock = null;
	private JMenuItem jMenuItemImprimerVente = null;
	private JLabel jLabelClientNom = null;
	private JPanel jPanelClientDetails = null;
	private JLabel jLabelClientPrenom = null;
	private JLabel jLabelClientProfession = null;
	private JLabel jLabelClientAdresse = null;
	private JLabel jLabelClientNPA = null;
	private JLabel jLabelClientVille = null;
	private JLabel jLabelClientTelPrincipal = null;
	private JLabel jLabelClientTelSecondaire = null;
	private JTextField jTextFieldNom = null;
	private JTextField jTextFieldPrenom = null;
	private JTextField jTextFieldProfession = null;
	private JTextField jTextFieldAdresse = null;
	private JTextField jTextFieldNPA = null;
	private JTextField jTextFieldLocalite = null;
	private JTextField jTextFieldTelPrincipal = null;
	private JTextField jTextFieldTelSecondaire = null;
	private JLabel jLabelRappelNom = null;
	private JLabel jLabelRappelPrenom = null;
	private JButton jButtonNouveauClient = null;
	private JButton jButtonSupprimerClient = null;
	private JButton jButtonModifierClient = null;
	private JButton jButtonEnregistrerClient = null;
	private JButton jButtonAnnulerClient = null;
	private JPanel jPanelBoutonsClient = null;
	private JPanel jPanelDetailAppareil = null;
	private LinkedList<String> listeGenreAppareil = null;
	private LinkedList<String> listeMarqueAppareil = null;
	private JPanel jPanelReparationTTAEncaisser = null;
	private JXTitledPanel jTitledPanelClientDetails = null;
	private JXTitledPanel jTitledPanelListeClients = null;
	private JXTitledPanel jTitledPanelListeAppareil = null;
	private JPanel jPanelListeAppareil = null;
	private JScrollPane jScrollPaneDetailsAppareils = null;
	private JButton jButtonAppareilRechercher = null;
	private JButton jButtonTtAfficherListeAppareil = null;
	private JXTitledPanel jTitledPanelAppareilDetails = null;
	private JPanel jPanelAppareilDetails = null;
	private JLabel jLabelAppareilGenre = null;
	private JLabel jLabelAppareilMarque = null;
	private JLabel jLabelAppareilType = null;
	private JLabel jLabelAppareilNoSerie = null;
	private JLabel jLabelAppareilDateVente = null;
	private JLabel jLabelAppareilPrix = null;
	private JLabel jLabelAppareilNoFacture = null;
	private JTextField jTextFieldAppareilType = null;
	private JTextField jTextFieldAppareilNoSerie = null;
	private JTextField jTextFieldAppareilPrix = null;
	private JTextField jTextFieldAppareilNoFacture = null;
	private JPanel jPanelBoutonsClientStock = null;
	private JButton jButtonAnnulerAppareil = null;
	private JButton jButtonSupprimerAppareil = null;
	private JButton jButtonModifierAppareil = null;
	private JButton jButtonEnregistrerAppareil = null;
	private JXTitledPanel jTitledPanelListeReparation = null;
	private JPanel jPanelListeReparation = null;
	private JScrollPane jScrollPaneDetailsReparation = null;
	private JButton jButtonReparationRechercher = null;
	private JButton jButtonTtAfficherListeReparation = null;
	private JXTitledPanel jTitledPanelReparationDetails = null;
	private JLayeredPane jPanelreparationDetails = null;
	private JLabel jLabelReparationDate = null;
	private JLabel jLabelReparationPanne = null;
	private JLabel jLabelReparationTravailEff = null;
	private JLabel jLabelReparationTechnicien = null;
	private JLabel jLabelReparationPrix = null;
	private JTextField jTextFieldReparationTechnicien = null;
	private JPanel jPanelBoutonsClientStock1 = null;
	private JButton jButtonAnnulerReparation = null;
	private JButton jButtonSupprimerReparation = null;
	private JButton jButtonNouvelleReparation = null;
	private JButton jButtonModifierReparation = null;
	private JButton jButtonEnregistrerReparation = null;
	private JLabel jLabelReparationPaye = null;
	private TextExtens jTextExtensReparationPanne = null;
	private TextExtens jTextExtensReparationTravailEffectue = null;
	private JTextField jTextFieldReparationPrix = null;
	private JCheckBox jCheckBoxReparationPaye = null;
	private JXDatePicker calendrierReparationDate = null;
	private JPanel jPanelListeClients = null;
	private JScrollPane jScrollPaneDetailsClient = null;
	private JButton jButtonClientRechercher = null;
	private JButton jButtonTtAfficherListeClient = null;
	private JPanel jPanelAEncaisserClient1 = null;
	private JXTitledPanel jTitledPanelListeReparationAEncaisser = null;
	private JPanel jPanelListeReparationAEncaisser = null;
	private JScrollPane jScrollPaneReparationAEncaisser = null;
	private JButton jButtonReparationAEncaisserRechercher = null;
	private JButton jButtonTtAfficherListeReparationAEncaisser = null;
	private JXTitledPanel jTitledPanelReparationDetailsAEncaisser = null;
	private JLayeredPane jPanelreparationDetails1 = null;
	private JLabel jLabelReparationDateAEncaisser = null;
	private JLabel jLabelReparationPanneAEncaisser = null;
	private JLabel jLabelReparationTravailEffAEncaisser = null;
	private JLabel jLabelReparationTechnicienAEncaisser = null;
	private JLabel jLabelReparationPrixAEncaisser = null;
	private JTextField jTextFieldReparationTechnicienAEncaisser = null;
	private JPanel jPanelBoutonsClientStock11 = null;
	private JButton jButtonAnnulerReparationAEncaisser = null;
	private JButton jButtonSupprimerReparationAEncaisser = null;
	private JButton jButtonNouvelleReparationAEncaisser = null;
	private JButton jButtonModifierReparationAEncaisser = null;
	private JButton jButtonEnregistrerReparationAEncaisser = null;
	private JLabel jLabelReparationAEncaisserPaye = null;
	private TextExtens jTextExtensReparationPanne1 = null;
	private TextExtens jTextExtensReparationTravailEffectue1 = null;
	private JTextField jTextFieldReparationPrix1 = null;
	private JCheckBox jCheckBoxReparationPaye1 = null;
	private JXDatePicker calendrierAEncaisserDate = null;
	private JPanel jPanel2;
	private JLabel jLabelAppareilVenduClientLocalite;
	private JLabel jLabelAppareilVenduClientPrenom;
	private JLabel jLabelAppareilVenduClientNom;
	private JTextField jTextFieldAppareilVenduClientLocalite;
	private JTextField jTextFieldAppareilVenduClientPrenom;
	private JTextField jTextFieldAppareilVenduClientNom;
	private JPanel jPanel1;
	private JLabel jLabel1;
	private JTextField jTextFieldAppareilVenduNom;
	private JPanel jPanelAppareilVenduInfoClient;
	private JLabel jLabelAppareilVenduPrenomClient;
	private JLabel jLabelAppareilVenduNomClient;
	private JTextField jTextFieldAppareilVenduLocaliteClient;
	private JTextField jTextFieldAppareilVenduPrenomClient;
	private JTextField jTextFieldAppareilVenduNomClient;
	private JPopupMenu jPopupMenu;
	private JMenu jMenuEdition;
	private JMenuItem jMenuItemCopier;
	private JMenuItem jMenuItemColler;
	private JMenuItem jMenuItemCouper;
	private JMenuItem jMenuItemEffacer;
	private JComboBox jComboBoxAppareilGenre = null;
	private JComboBox jComboBoxAppareilMarque = null;
	private JComboBox jComboBoxAppareilStatus = null;
	// pour le chemin du fichier texte contenant la liste
	// du genre des appareils
	private String cheminListeGenreAppareil = "ListeGenreAppareil.txt";
	// des marques des appareils
	private String cheminListeMarqueAppareil = "ListeMarqueAppareil.txt";
	private JXTitledPanel jTitledPanelListeAppareilStock = null;
	private JPanel jPanelListeAppareilStock = null;
	private JScrollPane jScrollPaneListeAppareilsStock = null;
	private JButton jButtonRechercherAppareilStock = null;
	private JButton jButtonTtAfficherListeAppareilStock = null;
	private JXTitledPanel jTitledPanelAppareilDetailsStock = null;
	private JPanel jPanelAppareilDetailsStock = null;
	private JLabel jLabelAppareilGenreStock = null;
	private JLabel jLabelAppareilMarqueStock = null;
	private JLabel jLabelAppareilTypeStock = null;
	private JLabel jLabelAppareilNoSerieStock = null;
	private JLabel jLabelAppareilDateReceptionStock = null;
	private JLabel jLabelAppareilPrixStock = null;
	private JLabel jLabelAppareilNoFactureStock = null;
	private JLabel jLabelAppareilNoBulletinStock = null;
	private JLabel jLabelAppareilReceptioneParStock = null;
	private JTextField jTextFieldAppareilTypeStock = null;
	private JTextField jTextFieldAppareilNoSerieStock = null;
	private JTextField jTextFieldAppareilPrixStock = null;
	private JTextField jTextFieldAppareilNoFactureStock = null;
	private JTextField jTextFieldAppareilNoBulletinStock = null;
	private JTextField jTextFieldAppareilReceptionneParStock = null;
	private JPanel jPanelBoutonsClientStock2 = null;
	private JButton jButtonAnnulerAppareilStock = null;
	private JButton jButtonSupprimerAppareilStock = null;
	private JButton jButtonNouvelAppareilStock = null;
	private JButton jButtonModifierAppareilStock = null;
	private JButton jButtonEnregistrerAppareilStock = null;
	private JComboBox jComboBoxAppareilGenreStock = null;
	private JComboBox jComboBoxAppareilMarqueStock = null;
	private JButton jButtonAssignerAClient = null;
	private JButton jButtonAssignerAppareil = null;
	// Pour la gestion de la table d'affichage des clients
	String[] EnTetesClient = {"Nom", "Prénom", "Ville"};
	private ModeleTable tabModelClient = new ModeleTable(EnTetesClient);
	private JXTable jTableDetailsClient = new JXTable(tabModelClient);
	private GestionClient gestionnaireClient = new GestionClient(tabModelClient);
	// Pour la gestion de la table d'affichage des appareils
	String[] EnTetesAppareil = {"Genre", "Marque", "Type"};
	private ModeleTable tabModelAppareils = new ModeleTable(EnTetesAppareil);
	private JXTable jTableDetailsAppareil = new JXTable(tabModelAppareils);
	private GestionAppareil gestionnaireAppareils = new GestionAppareil(tabModelAppareils, null);
	private ModeleTable tabModelAppareilsStock = new ModeleTable(EnTetesAppareil);
	private JXTable jTableDetailsAppareilStock = new JXTable(tabModelAppareilsStock);
	private GestionAppareilStock gestionnaireAppareilsStock = new GestionAppareilStock(tabModelAppareilsStock, 0);
	private ModeleTable tabModelAppareilVendu = new ModeleTable(EnTetesAppareil);
	private JXTable jTableDetailsAppareilVendu = new JXTable(tabModelAppareilVendu);
	private GestionAppareil gestionnaireAppareilVendu = new GestionAppareil(tabModelAppareilVendu, 1);
	String[] EnTetesReparation = {"Date", "Panne", "Travail Effectué"};
	private ModeleTable tabModelReparation = new ModeleTable(EnTetesReparation);
	private JXTable jTableDetailsReparation = new JXTable(tabModelReparation);
	private GestionReparation gestionnaireReparation = new GestionReparation(tabModelReparation, null);
	private ModeleTable tabModelReparationAEncaisser = new ModeleTable(EnTetesReparation);
	private JXTable jTableDetailsReparationAEncaisser = new JXTable(tabModelReparationAEncaisser);
	private GestionReparation gestionnaireReparationAEncaisser = new GestionReparation(tabModelReparationAEncaisser, false);
	private ModeleTable tabModelReparationTTAEncaisser = new ModeleTable(EnTetesReparation);
	private JXTable jTableDetailsReparationTTAEncaisser = new JXTable(tabModelReparationTTAEncaisser);
	private GestionReparation gestionnaireReparationTTAEncaisser = new GestionReparation(tabModelReparationTTAEncaisser, false);
	// Permet de savoir quelle action est voulue par l'utilisateur
	private String actionC = "";       // Pour les clients
	private String actionA = "";       // Pour les appareils
	private String actionAStock = "";  // Pour les appareils du stock
	private String actionR = "";       // Pour les réparations
	private String actionAV = "";      // Pour les appareils vendus
	private String actionRA = "";      // Pour les réparations d'un client à encaisser
	private String actionRTTA = "";    // Pour toutes les réparations à encaisser
	// Pour le contrôle du remplissage des champs obligatoires
	KeyAdapter saisieClientListener = null;
	KeyAdapter saisieAppareilListener = null;
	KeyAdapter saisieReparationListener = null;
	// pour le modèle des champs
	private ModeleChamps modeleChamps = new ModeleChamps();
	private JXTitledPanel jTitledPanelListeAppareilVendu = null;
	private JPanel jPanelListeAppareilVendu = null;
	private JScrollPane jScrollPaneDetailsAppareilsStock1 = null;
	private JButton jButtonRechercherAppareilVendu = null;
	private JButton jButtonTtAfficherListeAppareilVendu = null;
	private JXTitledPanel jTitledPanelAppareilVenduDetails = null;
	private JPanel jPanelAppareilVenduDetails = null;
	private JLabel jLabelAppareilVenduGenre = null;
	private JLabel jLabelAppareilVenduMarque = null;
	private JLabel jLabelAppareilVenduType = null;
	private JLabel jLabelAppareilVenduNoSerie = null;
	private JLabel jLabelAppareilVenduDateVente = null;
	private JLabel jLabelAppareilVenduPrix = null;
	private JLabel jLabelAppareilVenduNoFacture = null;
	private JTextField jTextFieldAppareilVenduType = null;
	private JTextField jTextFieldAppareilVenduNoSerie = null;
	private JTextField jTextFieldAppareilVenduPrix = null;
	private JTextField jTextFieldAppareilVenduNoFacture = null;
	private JPanel jPanelBoutonsClientStock21 = null;
	private JButton jButtonAnnulerAppareilVendu = null;
	private JButton jButtonSupprimerAppareilVendu = null;
	private JButton jButtonModifierAppareilVendu = null;
	private JButton jButtonEnregistrerAppareilVendu = null;
	private JComboBox jComboBoxAppareilGenre11 = null;
	private JComboBox jComboBoxAppareilMarque11 = null;
	private JXTitledPanel jTitledPanelListeTTRepparationAEncaisser = null;
	private JPanel jPanelListeReparationTTAEncaisser = null;
	private JScrollPane jScrollPaneDetailsAppareilsStock11 = null;
	private JButton jButtonRechercherReparationTTAEncaisser = null;
	private JButton jButtonTtAfficherListeReparationTTAEncaisser = null;
	private JLayeredPane jPanelReparationDetailsTTAEncaisser = null;
	private JLabel jLabelReparationDateTTAEncaisser = null;
	private JLabel jLabelReparationPanneTTAEncaisser = null;
	private JLabel jLabelReparationTravailEffTTAEncaisser = null;
	private JLabel jLabelReparationTechnicienTTAEncaisser = null;
	private JLabel jLabelReparationPrixTTAEncaisser = null;
	private JTextField jTextFieldReparationTechnicienTTAEncaisser = null;
	private JPanel jPanelBoutonsClientTTAEncaisser = null;
	private JButton jButtonAnnulerReparationTTAEncaisser = null;
	private JButton jButtonSupprimerReparationTTAEncaisser = null;
	private JButton jButtonModifierReparationTTAEncaisser = null;
	private JButton jButtonEnregistrerReparationTTAEncaisser = null;
	private JLabel jLabelReparationPayeTTAEncaisser = null;
	private TextExtens jTextExtensReparationPanneTTAEncaisser = null;
	private TextExtens jTextExtensReparationTravailEffectueTTAEncaisser = null;
	private JTextField jTextFieldReparationPrixTTAEncaisser = null;
	private JCheckBox jCheckBoxReparationPayeTTAEncaisser = null;
	private JXDatePicker calendrierReparationDateTTAEncaisser = null;
	private JXTitledPanel jTitledPanelDetailsReparationTTAEncaisser = null;
	private Boolean initStock = true; // Pour savoir quand on initialise
	private Boolean initVente = true; // Pour savoir quand on initialise
	private Boolean initTTAEncaisser = true; // Pour savoir quand on initialise
	private Boolean initAppareils = true; // Pour savoir quand on initialise
	private Boolean initReparations = true; // Pour savoir quand on initialise
	private Boolean initAEncaisser = true; // Pour savoir quand on initialise
	private JButton jButtonNouvelAppareil = null;
	private JTextField jTextFieldReparationNoFacture = null;
	private JLabel jLabelReparationNoFacture = null;
	private JLabel jLabelReparationAEncaisserNoFacture = null;
	private JTextField jTextFieldReparationAEncaisserNoFacture = null;
	private JTextField jTextFieldTTAEncaisserNoFacture = null;
	private JLabel jLabelTTAEncaisserNoFacture = null;
	private JLabel jLabelAppareilStatus = null;
	private JXDatePicker calendrierAppareilDateVente = null;
	private JMenu jMenu = null;
	private JMenu jMenuBDD = null;
	private JMenuItem jMenuItemRestaurer = null;
	private JMenuItem jMenuItemSauvegarder = null;
	private String[] status = {"En Stock", "Vendu", "Autre"};
	// pour le fichier de sauvegarde et restauration
	private String fichier = null;
	private JXDatePicker calendrierStockDate = null;
	private JXDatePicker calendrierVenteDate = null;
	private JButton jButtonAnnulerAssignationAppareil = null;
	private Reparation reparationRecherchee = null;
	private Appareil appareilRecherche = null;
	private Client clientRecherche = null;
	private String dateAssignation = null;
	// - L'image de l'application
	private URL urlProgIcon = FenetreAffichage.class.getResource("Logo.png");
	private Image imgProgIcon = Toolkit.getDefaultToolkit().getImage(urlProgIcon);
	// - L'icône de l'application
	private URL urlProgIconSmall = FenetreAffichage.class.getResource("LogoSmall.png");
	private ImageIcon progIcon = new ImageIcon(urlProgIconSmall);
// </editor-fold>

	public FenetreAffichage() {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				FenetreAffichage.this.getJFrame().setVisible(true);
				getJButtonNouveauClient().requestFocus();
			}
		});
		//Application.getInstance().getContext().getResourceMap(getClass()).injectComponents(getContentPane());
	}

	/**
	 * This method initializes jFrame
	 *
	 * @return javax.swing.JFrame
	 */
	private JFrame getJFrame() {
		if (jFrame == null) {
			// Définition de la police des jComboBox
			FontUIResource sansserifPlain11 = new FontUIResource("SansSerif", Font.PLAIN, 12);

			// Pour pouvoir utilise enter sur les boutons
			UIManager.put("Button.defaultButtonFollowsFocus", Boolean.TRUE);

			// Pour la représentation du texte des jComboBox
			UIManager.put("ComboBox.disabledForeground", Color.BLACK);
			UIManager.put("ComboBox.font", sansserifPlain11);

			jFrame = new JFrame();
			jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			jFrame.setResizable(false);
			jFrame.setJMenuBar(getJJMenuBar());
			jFrame.setSize(788, 610);
			jFrame.setLocationRelativeTo(null); // pour centrer la fenêtre
			jFrame.setTitle("AGAC");
			jFrame.setIconImage(imgProgIcon);   // Définit l'icône du programme
			listeGenreAppareil = chargerListes(cheminListeGenreAppareil);
			listeMarqueAppareil = chargerListes(cheminListeMarqueAppareil);
			jFrame.setContentPane(getJTabbedPanePrincipal());
			jPanelClientDetails.getRootPane().setDefaultButton(jButtonNouveauClient);
			jButtonNouveauClient.requestFocus();


		}
		return jFrame;
	}

	/**
	 * Pour charger une liste à partir d'un fichier texte (un élément par ligne).
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 * @param liste Liste où seront chargés les lignes du fichier texte
	 * @param nomFichier nom du fichier à charger (chemin)
	 * @throws Exception description pourquoi elle est levée
	 */
	private LinkedList<String> chargerListes(String nomFichier) {
		LinkedList<String> liste = null;

		try {
			// charge la liste
			liste = Utilitaire.chargerListe(nomFichier);
		} // traite les éventuelles erreurs
		// si le fichier n'a pas été trouvé
		catch (FileNotFoundException ex) {
			String titreErreur = "Fichier introuvable";
			String erreur = "Le fichier " + nomFichier +
							" n'a pas été trouvé.\n" +
							"Le programme va donc s'arrêter.";
			// affiche un message d'erreur
			JOptionPane.showMessageDialog(getJFrame(), erreur, titreErreur,
							JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		} // s'il y a eu une erreur d'entrée-sortie
		catch (IOException ex) {
			String titreErreur = "Errreur d'entrée-sortie";
			String erreur = "Un problème de lecture du fichier " +
							nomFichier + " est survenu.\n" +
							"Le programme va donc s'arrêter.";
			// affiche un message d'erreur
			JOptionPane.showMessageDialog(getJFrame(), erreur, titreErreur,
							JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		return liste;
	}

	/**
	 * This method initializes jJMenuBar
	 *
	 * @return javax.swing.JMenuBar
	 */
	private JMenuBar getJJMenuBar() {
		if (jJMenuBar == null) {
			jJMenuBar = new JMenuBar();
			jJMenuBar.add(getJMenuFichier());
			jJMenuBar.add(getJMenuEdition());
			jJMenuBar.add(getJMenuBDD());
			jJMenuBar.add(getJMenuImprimer());
			jJMenuBar.add(getJMenuAide());

		}
		return jJMenuBar;
	}

	/**
	 * This method initializes jMenu
	 *
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenuFichier() {
		if (fileMenu == null) {
			fileMenu = new JMenu();
			fileMenu.setText("Fichier");
			fileMenu.setMnemonic(KeyEvent.VK_F);
			//fileMenu.add(getSaveMenuItem()); // Innutil pour le moment
			fileMenu.add(getExitMenuItem());
		}
		return fileMenu;
	}

	/**
	 * This method initializes jMenu
	 *
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenuAide() {
		if (helpMenu == null) {
			helpMenu = new JMenu();
			helpMenu.setText("Aide");
			helpMenu.setMnemonic(KeyEvent.VK_D);
			helpMenu.add(getAboutMenuItem());
		}
		return helpMenu;
	}

	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getExitMenuItem() {
		if (exitMenuItem == null) {
			exitMenuItem = new JMenuItem();
			exitMenuItem.setText("Quitter");
			exitMenuItem.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
		}
		return exitMenuItem;
	}

	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getAboutMenuItem() {
		if (aboutMenuItem == null) {
			aboutMenuItem = new JMenuItem();
			aboutMenuItem.setText("À propos");
			aboutMenuItem.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					JDialog aboutDialog = getAboutDialog();
					aboutDialog.pack();
					Point loc = getJFrame().getLocation();
					loc.translate(20, 20);
					aboutDialog.setLocation(loc);
					aboutDialog.setVisible(true);
				}
			});
		}
		return aboutMenuItem;
	}

	/**
	 * This method initializes aboutDialog
	 *
	 * @return javax.swing.JDialog
	 */
	private JDialog getAboutDialog() {
		if (aboutDialog == null) {
			aboutDialog = new JDialog(getJFrame(), true);
			aboutDialog.setTitle("À propos");
			aboutDialog.setContentPane(getAboutContentPane());
		}
		return aboutDialog;
	}

	/**
	 * This method initializes aboutContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getAboutContentPane() {
		if (aboutContentPane == null) {
			aboutContentPane = new JPanel();
			aboutContentPane.setLayout(new BorderLayout());
			aboutContentPane.add(getImgLabel(), BorderLayout.NORTH);
			aboutContentPane.add(getAboutVersionLabel(), BorderLayout.CENTER);
		}
		return aboutContentPane;
	}

	/**
	 * This method initializes aboutVersionLabel
	 *
	 * @return javax.swing.JLabel
	 */
	private JLabel getAboutVersionLabel() {
		if (aboutVersionLabel == null) {
			aboutVersionLabel = new JLabel();
			aboutVersionLabel.setText("<html><body><br />AGAC (V 1.1 26092009)<br />" +
							"AGAC est un Gestionnaire d'Appareils et de Clients.<br />" +
							"Développé : <br />" +
							" par : Lucien Chaubert, Louis Jacquier et Thierry Forchelet.<br />" +
							" pour : TV Schneuwly<br /><br /></body></html>");
			aboutVersionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return aboutVersionLabel;
	}

	private JLabel getImgLabel() {
		if (imgLabel == null) {
			imgLabel = new JLabel();
			imgLabel.setIcon(progIcon);
		}
		return imgLabel;
	}

	/**
	 * This method initializes jTabbedPanePrincipal
	 *
	 * @return javax.swing.JTabbedPane
	 */
	private JTabbedPane getJTabbedPanePrincipal() {
		if (jTabbedPanePrincipal == null) {
			jTabbedPanePrincipal = new JTabbedPane();

			jTabbedPanePrincipal.setFont(new Font("Dialog", Font.BOLD, 12));
			jTabbedPanePrincipal.addTab("Clients", null, getJPanelClient(), null);
			jTabbedPanePrincipal.addTab("Stock", null, getJPanelStock(), null);
			jTabbedPanePrincipal.addTab("Ventes", null, getJPanelVentes(), null);
			jTabbedPanePrincipal.addTab("Réparations à encaisser (de tous les clients)", null, getJPanelReparationTTAEncaisser(), null);
			jTabbedPanePrincipal.addChangeListener(new ChangeListener() {

				public void stateChanged(ChangeEvent e) {
					if (jTabbedPanePrincipal.getSelectedIndex() == 0) {
						if (jTabbedPaneClient.getSelectedIndex() == 0) {
							jPanelDetailClient.getRootPane().setDefaultButton(jButtonNouveauClient);
							jButtonNouveauClient.requestFocus();
						} else if (jTabbedPaneClient.getSelectedIndex() == 1) {
							if (jTabbedPaneAppareils.getSelectedIndex() == 1) {
								jPanelreparationDetails.getRootPane().setDefaultButton(jButtonNouvelleReparation);
								jButtonNouvelleReparation.requestFocus();
							} else if (jTabbedPaneAppareils.getSelectedIndex() == 0) {
								jPanelDetailAppareil.getRootPane().setDefaultButton(jButtonNouvelAppareil);
								jButtonNouvelAppareil.requestFocus();
							}
						} else if (jTabbedPaneClient.getSelectedIndex() == 2) {
							jPanelAEncaisserClient.getRootPane().setDefaultButton(jButtonModifierReparationAEncaisser);
							jButtonModifierReparationAEncaisser.requestFocus();
						}
					} else if (jTabbedPanePrincipal.getSelectedIndex() == 1) {
						jPanelAppareilDetailsStock.getRootPane().setDefaultButton(jButtonNouvelAppareilStock);
						jButtonNouvelAppareilStock.requestFocus();
						if (initStock) {
							actualiseTableStock(); // Pas hyper optimal, mais pour faciliter...
							initStock = false;
						}
					} else if (jTabbedPanePrincipal.getSelectedIndex() == 2) {
						jPanelAppareilVenduDetails.getRootPane().setDefaultButton(jButtonModifierAppareilVendu);
						jButtonModifierAppareilVendu.requestFocus();
						if (initVente) {
							actualiseTableVente(); // Pas hyper optimal, mais pour faciliter...
							initVente = false;
						}
					} else if (jTabbedPanePrincipal.getSelectedIndex() == 3) {
						jPanelReparationDetailsTTAEncaisser.getRootPane().setDefaultButton(jButtonModifierReparationTTAEncaisser);
						jButtonModifierReparationTTAEncaisser.requestFocus();
						if (initTTAEncaisser) {
							actualiseTableReparationsTTAEncaisser(); // Pas hyper optimal, mais pour faciliter...
							initTTAEncaisser = false;
						}
					}
				}
			});
		}
		return jTabbedPanePrincipal;
	}

	/**
	 * This method initializes jPanelClient
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelClient() {
		if (jPanelClient == null) {
			jPanelClient = new JPanel();
			jPanelClient.setLayout(null);
			jPanelClient.add(getJTabbedPaneClient(), null);
			jPanelClient.add(getJPanelRappelClient(), null);

			// Paramètres par défaut des tables d'affichage
			jTableDetailsClient.setFocusable(false);
			jTableDetailsClient.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			jTableDetailsAppareil.setFocusable(false);
			jTableDetailsAppareil.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			jTableDetailsReparation.setFocusable(false);
			jTableDetailsReparation.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			jTableDetailsReparationAEncaisser.setFocusable(false);
			jTableDetailsReparationAEncaisser.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			// Initialise la table des clients
			jTableDetailsClient.addMouseListener(this); // Pour gérer les click sur la table

			jButtonNouveauClient.addKeyListener(new KeyAdapter() {

				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
										null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
							gestionnaireClient.supprime(gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow()).id);
							setClientTexte("", "", "", "", "", "", "", ""); // Le client n'existe plus...

							actualiseTableClients();
							actualiseTableAppareilsClients();
							actualiseTableReparationsAppareil();
							actualiseTableReparationsClientAEncaisser();
							actualiseTableReparationsTTAEncaisser();
							actualiseTableVente();
						}
					}
				}
			}); // Pour gérer les touches du clavier sur la table

			actualiseTableClients(); // L'affiche
		}
		return jPanelClient;
	}

	/**
	 * This method initializes jPanelStock
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelStock() {
		if (jPanelStock == null) {
			jPanelStock = new JPanel();
			jPanelStock.setLayout(null);
			jPanelStock.add(getJTitledPanelListeAppareilStock(), null);
			jPanelStock.add(getJTitledPanelAppareilDetailsStock(), null);

			// Paramètres par défaut des tables d'affichage
			jTableDetailsAppareilStock.setFocusable(false);
			jTableDetailsAppareilStock.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			jComboBoxAppareilGenreStock.setSelectedIndex(0);
			jComboBoxAppareilMarqueStock.setSelectedIndex(0);

			jComboBoxAppareilGenreStock.setEditable(false);
			jComboBoxAppareilGenreStock.setEnabled(false);
			jComboBoxAppareilMarqueStock.setEditable(false);
			jComboBoxAppareilMarqueStock.setEnabled(false);
			jTableDetailsAppareilStock.addMouseListener(this); // Pour gérer les click sur la table
		}
		return jPanelStock;
	}

	/**
	 * This method initializes jPanelVentes
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelVentes() {
		if (jPanelVentes == null) {
			jPanelVentes = new JPanel();
			jPanelVentes.setLayout(null);
			jPanelVentes.add(getJTitledPanelListeAppareilVendu(), null);
			jPanelVentes.add(getJTitledPanelAppareilVenduDetails(), null);

			// Paramètres par défaut des tables d'affichage
			jTableDetailsAppareilVendu.setFocusable(false);
			jTableDetailsAppareilVendu.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			jTableDetailsAppareilVendu.addMouseListener(this); // Pour gérer les click sur la table
			getJButtonModifierAppareilVendu().addKeyListener(new KeyAdapter() {

				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
										null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
							gestionnaireAppareilVendu.supprime(gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow()).id);
							setAppareilTexte("", "", "", "", "", "", "", ""); // L'appareil n'existe plus...
							actualiseTableVente();
							actualiseTableAppareilsClients();
							actualiseTableReparationsAppareil();
							actualiseTableReparationsClientAEncaisser();
							actualiseTableReparationsTTAEncaisser();

							if (tabModelAppareilVendu.getRowCount() != 0) {
								jTableDetailsAppareilVendu.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

								// Récupère les détails de l'appareil sélectionné
								Appareil appareilSelectione = gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow());
								Client proprietaire = gestionnaireAppareilVendu.getProprietaire(appareilSelectione);

								// Affiche les détails de l'appareil
								setAppareilVenduTexte(appareilSelectione.type, appareilSelectione.noSerie,
												appareilSelectione.dateVenteDebut, appareilSelectione.prix,
												appareilSelectione.noFacture, appareilSelectione.genre,
												appareilSelectione.marque, appareilSelectione.status,
												proprietaire.nom, proprietaire.prenom, proprietaire.localite);
							} else {
								jButtonSupprimerAppareilVendu.setEnabled(false);
							}
						}
					}
				}
			}); // Pour gérer les touches du clavier sur la table

			// Gestion de la saisie de touches du clavier dans une des zone obligatoire d'un appareil vendu
			saisieAppareilListener = new KeyAdapter() {

				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() != KeyEvent.VK_ENTER) {
						verifAppareilVente();
					}
				}
			};

			// Contrôle la saisie des champs obligatoires pour les appareils
			jTextFieldAppareilVenduType.addKeyListener(saisieAppareilListener);
			jTextFieldAppareilVenduNoSerie.addKeyListener(saisieAppareilListener);
			calendrierVenteDate.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierVenteDate.getEditor().setEditable(false);
					verifAppareilVente();
				}
			});
			jTextFieldAppareilVenduPrix.addKeyListener(saisieAppareilListener);
			jTextFieldAppareilVenduNoFacture.addKeyListener(saisieAppareilListener);
		}
		return jPanelVentes;
	}

	/**
	 * This method initializes jTabbedPaneClient
	 *
	 * @return javax.swing.JTabbedPane
	 */
	private JTabbedPane getJTabbedPaneClient() {
		if (jTabbedPaneClient == null) {
			jTabbedPaneClient = new JTabbedPane();
			jTabbedPaneClient.setBounds(new Rectangle(0, 45, 781, 496));
			jTabbedPaneClient.addTab("Détails client", null, getJPanelDetailClient(), null);
			jTabbedPaneClient.addTab("Appareils", null, getJPanelAppareils(), null);
			jTabbedPaneClient.addTab("Réparations à encaisser (du client)", null, getJPanelAEncaisserClient(), null);
			jTabbedPaneClient.addChangeListener(new ChangeListener() {

				public void stateChanged(ChangeEvent e) {
					if (jTabbedPaneClient.getSelectedIndex() == 0) {
						jPanelDetailClient.getRootPane().setDefaultButton(jButtonNouveauClient);
						jButtonNouveauClient.requestFocus();
					} else if (jTabbedPaneClient.getSelectedIndex() == 1) {
						if (jTabbedPaneAppareils.getSelectedIndex() == 1) {
							jPanelreparationDetails.getRootPane().setDefaultButton(jButtonNouvelleReparation);
							jButtonNouvelleReparation.requestFocus();
						} else if (jTabbedPaneAppareils.getSelectedIndex() == 0) {
							jPanelDetailAppareil.getRootPane().setDefaultButton(jButtonNouvelAppareil);
							jButtonNouvelAppareil.requestFocus();
						}

						if (initAppareils) {
							actualiseTableAppareilsClients(); // Pas hyper optimal, mais pour faciliter...
							initAppareils = false;
						}
					} else if (jTabbedPaneClient.getSelectedIndex() == 2) {
						jPanelAEncaisserClient.getRootPane().setDefaultButton(jButtonModifierReparationAEncaisser);
						jButtonModifierReparationAEncaisser.requestFocus();
						if (initAEncaisser) {
							actualiseTableReparationsClientAEncaisser(); // Pas hyper optimal, mais pour faciliter...
							initAEncaisser = false;
						}
					}
				}
			});
		}
		return jTabbedPaneClient;
	}

	/**
	 * This method initializes jPanelDetailClient
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelDetailClient() {
		if (jPanelDetailClient == null) {
			jPanelDetailClient = new JPanel();
			jPanelDetailClient.setLayout(null);
			jPanelDetailClient.add(getJPanelListeClients(), null);
			jPanelDetailClient.add(getJTitledPanelClientDetails(), null);
			jPanelDetailClient.add(getJTitledPanelListeClients(), null);
		}
		return jPanelDetailClient;
	}

	/**
	 * This method initializes jPanelAppareils
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelAppareils() {
		if (jPanelAppareils == null) {
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.fill = GridBagConstraints.BOTH;
			gridBagConstraints.gridy = 0;
			gridBagConstraints.weightx = 1.0;
			gridBagConstraints.weighty = 1.0;
			gridBagConstraints.gridx = 0;
			jPanelAppareils = new JPanel();
			jPanelAppareils.setLayout(new GridBagLayout());
			jPanelAppareils.add(getJTabbedPaneAppareils(), gridBagConstraints);

		}
		return jPanelAppareils;
	}

	/**
	 * This method initializes jPanelAEncaisserClient
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelAEncaisserClient() {
		if (jPanelAEncaisserClient == null) {
			jPanelAEncaisserClient = new JPanel();
			jPanelAEncaisserClient.setLayout(null);
			jPanelAEncaisserClient.add(getJPanelAEncaisserClient1(), null);
			jPanelAEncaisserClient.add(getJTitledPanelListeReparationAEncaisser(), null);
			jPanelAEncaisserClient.add(getJTitledPanelReparationDetailsAEncaisser(), null);
			jTableDetailsReparationAEncaisser.addMouseListener(this); // Pour gérer les click sur la table
			jButtonModifierReparationAEncaisser.addKeyListener(new KeyAdapter() {

				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
										null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
							gestionnaireReparation.supprime(gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow()).id);
							setReparationTexte("", "", "", "", "", "", false); // L'appareil n'existe plus...
							actualiseTableReparationsAppareil();
							actualiseTableReparationsClientAEncaisser();
							actualiseTableReparationsTTAEncaisser();

							if (tabModelReparation.getRowCount() != 0) {
								jTableDetailsReparation.setRowSelectionInterval(0, 0); // Séléctionne la première réparation par défaut

								// Récupère les détails de la réparation sélectionné
								Reparation reparationSelectione = (Reparation) gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow());

								// Affiche les détails de la réparation
								setReparationTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
							} else {
								jButtonSupprimerReparation.setEnabled(false);
							}
						}
					}
				}
			}); // Pour gérer les touches du clavier sur la table

			// Gestion de la saisie de touches du clavier dans une des zone obligatoire d'une réparation à encaisser pour un client
			saisieAppareilListener = new KeyAdapter() {

				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() != KeyEvent.VK_ENTER) {
						verifReparationAEncaisser();
					}
				}
			};

			// Contrôle la saisie des champs obligatoires pour les appareils
			jTextFieldReparationTechnicienAEncaisser.addKeyListener(saisieAppareilListener);
			jTextFieldReparationPrix1.addKeyListener(saisieAppareilListener);
			calendrierAEncaisserDate.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierAEncaisserDate.getEditor().setEditable(false);
					verifReparationAEncaisser();
				}
			});
			jTextFieldReparationAEncaisserNoFacture.addKeyListener(saisieAppareilListener);
			jTextExtensReparationPanne1.getPane().addKeyListener(saisieAppareilListener);
			jTextExtensReparationTravailEffectue1.getPane().addKeyListener(saisieAppareilListener);
			jCheckBoxReparationPaye1.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					verifReparationAEncaisser();
				}
			});
		}
		return jPanelAEncaisserClient;
	}

	/**
	 * This method initializes jPanelRappelClient
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelRappelClient() {
		if (jPanelRappelClient == null) {
			jLabelRappelPrenom = new JLabel();
			jLabelRappelPrenom.setBounds(new Rectangle(197, 15, 156, 16));
			jLabelRappelPrenom.setText("Prenom");
			jLabelRappelNom = new JLabel();
			jLabelRappelNom.setBounds(new Rectangle(28, 13, 139, 16));
			jLabelRappelNom.setText("Nom");
			jPanelRappelClient = new JPanel();
			jPanelRappelClient.setLayout(null);
			jPanelRappelClient.setBounds(new Rectangle(210, 0, 361, 46));
			jPanelRappelClient.setBorder(new DropShadowBorder(true));
			jPanelRappelClient.add(jLabelRappelNom, null);
			jPanelRappelClient.add(jLabelRappelPrenom, null);
		}
		return jPanelRappelClient;
	}

	/**
	 * This method initializes jTabbedPaneAppareils
	 *
	 * @return javax.swing.JTabbedPane
	 */
	private JTabbedPane getJTabbedPaneAppareils() {
		if (jTabbedPaneAppareils == null) {
			jTabbedPaneAppareils = new JTabbedPane();
			jTabbedPaneAppareils.addTab("Détails appareil", null, getJPanelDetailAppareil(), null);
			jTabbedPaneAppareils.addTab("Réparations", null, getJPanelReparations(), null);
			jTabbedPaneAppareils.addChangeListener(new ChangeListener() {

				public void stateChanged(ChangeEvent e) {
					if (jTabbedPaneAppareils.getSelectedIndex() == 1) {
						jPanelreparationDetails.getRootPane().setDefaultButton(jButtonNouvelleReparation);
						jButtonNouvelleReparation.requestFocus();

						if (initReparations) {
							actualiseTableReparationsAppareil(); // Pas hyper optimal, mais pour faciliter...
							initReparations = false;
						}
					} else if (jTabbedPaneAppareils.getSelectedIndex() == 0) {
						jPanelDetailAppareil.getRootPane().setDefaultButton(jButtonNouvelAppareil);
						jButtonNouvelAppareil.requestFocus();
					}

				}
			});
		}
		return jTabbedPaneAppareils;
	}

	/**
	 * This method initializes jPanelReparations
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelReparations() {
		if (jPanelReparations == null) {
			jPanelReparations = new JPanel();
			jPanelReparations.setLayout(null);
			jPanelReparations.add(getJTitledPanelListeReparation(), null);
			jPanelReparations.add(getJTitledPanelReparationDetails(), null);
			jPanelReparations.addMouseListener(this); // Pour gérer les click sur la table

			// Gestion de la saisie de touches du clavier dans une des zone obligatoire d'une réparation à encaisser pour un client
			saisieAppareilListener = new KeyAdapter() {

				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() != KeyEvent.VK_ENTER) {
						verifReparation();
					}
				}
			};

			// Contrôle la saisie des champs obligatoires pour les appareils
			jTextFieldReparationTechnicien.addKeyListener(saisieAppareilListener);
			jTextFieldReparationPrix.addKeyListener(saisieAppareilListener);
			calendrierReparationDate.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierReparationDate.getEditor().setEditable(false);
					verifReparationAEncaisser();
				}
			});
			jTextFieldReparationNoFacture.addKeyListener(saisieAppareilListener);
			jTextExtensReparationPanne.getPane().addKeyListener(saisieAppareilListener);
			jTextExtensReparationTravailEffectue.getPane().addKeyListener(saisieAppareilListener);
			jCheckBoxReparationPaye.addKeyListener(saisieAppareilListener);
		}
		return jPanelReparations;
	}

	/**
	 * This method initializes jMenuImprimer
	 *
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenuImprimer() {
		if (jMenuImprimer == null) {
			jMenuImprimer = new JMenu();
			jMenuImprimer.setToolTipText("");
			jMenuImprimer.setText("Imprimer");
			jMenuImprimer.add(getJMenuItemImprimerDetails());
			jMenuImprimer.setMnemonic(KeyEvent.VK_I);

			jMenuImprimer.add(getJMenuItemImprimerListe());

			jMenuImprimer.add(getJMenuItemImprimerVente());
			jMenuImprimer.setEnabled(true);
		}
		return jMenuImprimer;
	}

	/**
	 * This method initializes jMenuItemImprimerDetails
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemImprimerDetails() {
		if (jMenuItemImprimerDetails == null) {
			jMenuItemImprimerDetails = new JMenuItem();
			jMenuItemImprimerDetails.setText("Imprimer les détails de la fiche");
			jMenuItemImprimerDetails.setMnemonic(KeyEvent.VK_D);
			jMenuItemImprimerDetails.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					Integer row = null;
					Integer noSelectione = null;

					if (getJPanelClientDetails().isShowing() && (row = jTableDetailsClient.getSelectedRow()) > -1) {
						// Récupère les détails de l'appareil sélectionné
						noSelectione = gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow()).id;
						try {
							DetailClient.print(noSelectione);
						} catch (ErreurImpression e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else if (getJPanelAppareilDetails().isShowing() && (row = jTableDetailsAppareil.getSelectedRow()) > -1) {
						// Récupère les détails de l'appareil sélectionné
						noSelectione = gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow()).id;
						try {
							DetailAppareil.print(noSelectione);
						} catch (ErreurImpression e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else if (getJPanelAppareilVenduDetails().isShowing() && (row = jTableDetailsAppareilVendu.getSelectedRow()) > -1) {
						// Récupère les détails de l'appareil sélectionné
						noSelectione = gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow()).id;
						try {
							DetailAppareil.print(noSelectione);
						} catch (ErreurImpression e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else if (getjPanelAppareilDetailsStock().isShowing() && (row = jTableDetailsAppareilStock.getSelectedRow()) > -1) {
						// Récupère les détails de l'appareil sélectionné
						noSelectione = gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow()).id;
						try {
							DetailAppareil.print(noSelectione);
						} catch (ErreurImpression e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else if (getJPanelreparationDetails().isShowing() && (row = jTableDetailsReparation.getSelectedRow()) > -1) {
						// Récupère les détails de l'appareil sélectionné
						noSelectione = gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow()).id;
						try {
							DetailReparation.print(noSelectione);
						} catch (ErreurImpression e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else if (getJPanelreparationDetailsAEncaisser().isShowing() && (row = jTableDetailsReparationAEncaisser.getSelectedRow()) > -1) {
						// Récupère les détails de l'appareil sélectionné
						noSelectione = gestionnaireReparationAEncaisser.getDetail(jTableDetailsReparationAEncaisser.getSelectedRow()).id;
						try {
							DetailReparation.print(noSelectione);
						} catch (ErreurImpression e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else if (getJPanelReparationDetailsTTAEncaisser().isShowing() && (row = jTableDetailsReparationTTAEncaisser.getSelectedRow()) > -1) {
						// Récupère les détails de l'appareil sélectionné
						noSelectione = gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow()).id;
						try {
							DetailReparation.print(noSelectione);
						} catch (ErreurImpression e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}




//               System.out.print("c'est imprimé");
//               try {
//               	
//                  MessageFormat headerFormat = new MessageFormat("Page {0}");
//                  MessageFormat footerFormat = new MessageFormat("- {0} -");
//                  PrintRequestAttributeSet attr = new HashPrintRequestAttributeSet();
//                  
//                  jTableDetailsClient.print(JTable.PrintMode.FIT_WIDTH, headerFormat, footerFormat, true, attr, true);
//                    jTableDetailsClient.print(); // Service d'impression introuvable, mais à tester avec une autre jvm/win/autre... -_-
//               } catch (PrinterException e1) {
//                  // TODO Auto-generated catch block
//                  e1.printStackTrace();
//               }
				}
			});
		}
		return jMenuItemImprimerDetails;
	}

	/**
	 * This method initializes jMenuItemImprimerListe
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemImprimerListe() {
		if (jMenuItemImprimerStock == null) {
			jMenuItemImprimerStock = new JMenuItem();
			jMenuItemImprimerStock.setEnabled(true);
			jMenuItemImprimerStock.setText("Imprimer la liste du stock");
			// pour créer le racourcis avec l
			jMenuItemImprimerStock.setMnemonic(KeyEvent.VK_L);
			// pour souligner le 'l' à la position 12
			jMenuItemImprimerStock.setDisplayedMnemonicIndex(12);

			jMenuItemImprimerStock.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					try {
						ListeStock.print();
					} catch (ErreurImpression e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
		}
		return jMenuItemImprimerStock;
	}

	/**
	 * This method initializes jMenuItemImprimerListe
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemImprimerVente() {
		if (jMenuItemImprimerVente == null) {
			jMenuItemImprimerVente = new JMenuItem();
			jMenuItemImprimerVente.setEnabled(true);
			jMenuItemImprimerVente.setText("Imprimer la liste des ventes");
			// pour créer le racourcis avec
			jMenuItemImprimerVente.setMnemonic(KeyEvent.VK_V);

			jMenuItemImprimerVente.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					String[] dates;
					dates = new FenetreDateVenteImpression().affiche(true);
					if (dates[0] != null && !dates[0].isEmpty() && dates[1] != null && !dates[1].isEmpty()) {
						try {
							ListeVente.print(dates[0], dates[1]);
						} catch (ErreurImpression e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			});
		}
		return jMenuItemImprimerVente;
	}

	/**
	 * This method initializes jTitledPanelClientDetails
	 *
	 * @return javax.swing.JPanel
	 */
	private JXTitledPanel getJTitledPanelClientDetails() {

		if (jTitledPanelClientDetails == null) {
			jTitledPanelClientDetails = new JXTitledPanel("Détails");
			jTitledPanelClientDetails.setBounds(new Rectangle(450, 15, 311, 451));
			jTitledPanelClientDetails.setBorder(new DropShadowBorder(true));
			jTitledPanelClientDetails.setTitleForeground(Color.white);
			jTitledPanelClientDetails.add(getJPanelClientDetails(), null);
		}
		return jTitledPanelClientDetails;
	}

	/**
	 * This method initializes jPanelClientDetails
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelClientDetails() {
		jLabelClientTelSecondaire = new JLabel();
		jLabelClientTelSecondaire.setBounds(new Rectangle(15, 240, 117, 16));
		jLabelClientTelSecondaire.setText("N° tél secondaire :");
		jLabelClientTelPrincipal = new JLabel();
		jLabelClientTelPrincipal.setBounds(new Rectangle(15, 210, 106, 16));
		jLabelClientTelPrincipal.setText("N° tél principal :");
		jLabelClientVille = new JLabel();
		jLabelClientVille.setBounds(new Rectangle(15, 180, 106, 16));
		jLabelClientVille.setText("*Localité :");
		jLabelClientNPA = new JLabel();
		jLabelClientNPA.setBounds(new Rectangle(15, 150, 106, 16));
		jLabelClientNPA.setText("*NPA :");
		jLabelClientAdresse = new JLabel();
		jLabelClientAdresse.setBounds(new Rectangle(15, 120, 106, 16));
		jLabelClientAdresse.setText("*Adresse :");
		jLabelClientProfession = new JLabel();
		jLabelClientProfession.setBounds(new Rectangle(15, 90, 106, 16));
		jLabelClientProfession.setText("Profession :");
		jLabelClientPrenom = new JLabel();
		jLabelClientNom = new JLabel();
		jLabelClientNom.setText("*Nom :");
		jLabelClientNom.setBounds(new Rectangle(15, 30, 106, 16));
		jLabelClientPrenom.setBounds(new Rectangle(15, 60, 106, 16));
		jLabelClientPrenom.setText("*Prenom :");

		if (jPanelClientDetails == null) {
			jPanelClientDetails = new JPanel();
			jPanelClientDetails.setLayout(null);
			jPanelClientDetails.setBounds(new Rectangle(450, 15, 301, 421));
			jPanelClientDetails.setName("");
			jPanelClientDetails.add(jLabelClientPrenom, null);
			jPanelClientDetails.add(jLabelClientProfession, null);
			jPanelClientDetails.add(jLabelClientAdresse, null);
			jPanelClientDetails.add(jLabelClientNPA, null);
			jPanelClientDetails.add(jLabelClientVille, null);
			jPanelClientDetails.add(jLabelClientTelPrincipal, null);
			jPanelClientDetails.add(jLabelClientTelSecondaire, null);
			jPanelClientDetails.add(getJTextFieldNom(), null);
			jPanelClientDetails.add(getJTextFieldPrenom(), null);
			jPanelClientDetails.add(getJTextFieldProfession(), null);
			jPanelClientDetails.add(getJTextFieldAdresse(), null);
			jPanelClientDetails.add(getJTextFieldNPA(), null);
			jPanelClientDetails.add(getJTextFieldLocalite(), null);
			jPanelClientDetails.add(getJTextFieldTelPrincipal(), null);
			jPanelClientDetails.add(getJTextFieldTelSecondaire(), null);
			jPanelClientDetails.add(getJPanelBoutonsClient(), null);
			jPanelClientDetails.add(jLabelClientNom, null);
			jPanelClientDetails.add(getJButtonAssignerAppareil(), null);
			jPanelClientDetails.add(getJButtonAnnulerAssignationAppareil(), null);

			// Gestion de la saisie de touches du clavier dans une des zone obligatoire du client
			saisieClientListener = new KeyAdapter() {

				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() != KeyEvent.VK_ENTER) {
						verifChampsClient();
					}
				}
			};

			// Contrôle la saisie des champs obligatoires pour le client
			jTextFieldNom.addKeyListener(saisieClientListener);
			jTextFieldPrenom.addKeyListener(saisieClientListener);
			jTextFieldProfession.addKeyListener(saisieClientListener);
			jTextFieldAdresse.addKeyListener(saisieClientListener);
			jTextFieldNPA.addKeyListener(saisieClientListener);
			jTextFieldLocalite.addKeyListener(saisieClientListener);
			jTextFieldTelPrincipal.addKeyListener(saisieClientListener);
			jTextFieldTelSecondaire.addKeyListener(saisieClientListener);
		}
		return jPanelClientDetails;
	}

	/**
	 * This method initializes jTextFieldNom
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldNom() {
		if (jTextFieldNom == null) {
			jTextFieldNom = new JTextField();
			jTextFieldNom.setBounds(new Rectangle(135, 30, 151, 16));
			jTextFieldNom.setEditable(false);
		}
		return jTextFieldNom;
	}

	/**
	 * This method initializes jTextFieldPrenom
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldPrenom() {
		if (jTextFieldPrenom == null) {
			jTextFieldPrenom = new JTextField();
			jTextFieldPrenom.setBounds(new Rectangle(135, 60, 151, 16));
		}
		return jTextFieldPrenom;
	}

	/**
	 * This method initializes jTextFieldProfession
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldProfession() {
		if (jTextFieldProfession == null) {
			jTextFieldProfession = new JTextField();
			jTextFieldProfession.setBounds(new Rectangle(135, 90, 151, 16));
		}
		return jTextFieldProfession;
	}

	/**
	 * This method initializes jTextFieldAdresse
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldAdresse() {
		if (jTextFieldAdresse == null) {
			jTextFieldAdresse = new JTextField();
			jTextFieldAdresse.setBounds(new Rectangle(135, 120, 151, 16));
		}
		return jTextFieldAdresse;
	}

	/**
	 * This method initializes jTextFieldNPA
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldNPA() {
		if (jTextFieldNPA == null) {
			jTextFieldNPA = new JTextField("", 0);
			jTextFieldNPA.setDocument(modeleChamps.obtNPA(jTextFieldNPA));
			jTextFieldNPA.setBounds(new Rectangle(135, 150, 151, 16));
		}
		return jTextFieldNPA;
	}

	/**
	 * This method initializes jTextFieldLocalite
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldLocalite() {
		if (jTextFieldLocalite == null) {
			jTextFieldLocalite = new JTextField();
			jTextFieldLocalite.setBounds(new Rectangle(135, 180, 151, 16));
		}
		return jTextFieldLocalite;
	}

	/**
	 * This method initializes jTextFieldTelPrincipal
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldTelPrincipal() {
		if (jTextFieldTelPrincipal == null) {
			jTextFieldTelPrincipal = new JTextField("", 0);
			jTextFieldTelPrincipal.setDocument(modeleChamps.obtTel(jTextFieldTelPrincipal));
			jTextFieldTelPrincipal.setBounds(new Rectangle(135, 210, 151, 16));
		}

		return jTextFieldTelPrincipal;
	}

	/**
	 * This method initializes jTextFieldTelSecondaire
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldTelSecondaire() {
		if (jTextFieldTelSecondaire == null) {
			jTextFieldTelSecondaire = new JTextField();
			jTextFieldTelSecondaire.setDocument(modeleChamps.obtTel(jTextFieldTelSecondaire));
			jTextFieldTelSecondaire.setBounds(new Rectangle(135, 240, 151, 16));
		}
		return jTextFieldTelSecondaire;
	}

	/**
	 * This method initializes jButtonNouveauClient
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonNouveauClient() {
		if (jButtonNouveauClient == null) {
			jButtonNouveauClient = new JButton();
			jButtonNouveauClient.setMnemonic(KeyEvent.VK_N);
			jButtonNouveauClient.setToolTipText("Cliquer sur ce bouton pour ajouter un nouveau client");
			jButtonNouveauClient.setBounds(new Rectangle(15, 15, 106, 16));
			jButtonNouveauClient.setActionCommand("Nouveau");
			jButtonNouveauClient.setText("Nouveau");
			jButtonNouveauClient.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					setClientTexte("", "", "", "", "", "", "", ""); // Initialise la saisie
					setClientEditable(true); // Rends la saisie possible

					// Permet d'annuler l'action en cours
					jButtonAnnulerClient.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelClientDetails.getRootPane().setDefaultButton(jButtonAnnulerClient);

					// Rends les boutons présentement innutils innactifs
					jButtonNouveauClient.setEnabled(false);
					jButtonModifierClient.setEnabled(false);

					// Empêche la séléction d'un autre client
					jTableDetailsClient.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionC = "Ajouter";

					jTextFieldNom.requestFocus();

					setOngletsSelectable(false);
				}
			});
		}
		return jButtonNouveauClient;
	}

	/**
	 * This method initializes jButtonSupprimerClient
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonSupprimerClient() {
		if (jButtonSupprimerClient == null) {
			jButtonSupprimerClient = new JButton();
			jButtonSupprimerClient.setMnemonic(KeyEvent.VK_S);
			jButtonSupprimerClient.setToolTipText("Cliquer sur ce bouton pour supprimer les données d'un client existant (appareils et réparations comrpis)");
			jButtonSupprimerClient.setBounds(new Rectangle(150, 15, 106, 16));
			jButtonSupprimerClient.setText("Supprimer");
			jButtonSupprimerClient.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
									null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
						gestionnaireClient.supprime(gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow()).id);
						setClientTexte("", "", "", "", "", "", "", ""); // Le client n'existe plus...

						actualiseTableClients();
						actualiseTableAppareilsClients();
						actualiseTableReparationsAppareil();
						actualiseTableReparationsClientAEncaisser();
						actualiseTableReparationsTTAEncaisser();
						actualiseTableVente();
					}
				}
			});
		}
		return jButtonSupprimerClient;
	}

	/**
	 * This method initializes jButtonModifierClient
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonModifierClient() {
		if (jButtonModifierClient == null) {
			jButtonModifierClient = new JButton();
			jButtonModifierClient.setMnemonic(KeyEvent.VK_M);
			jButtonModifierClient.setToolTipText("Cliquer sur ce bouton pour modifier les données d'un client existant");
			jButtonModifierClient.setBounds(new Rectangle(15, 45, 106, 16));
			jButtonModifierClient.setText("Modifier");
			jButtonModifierClient.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Initialise la saisie
					Client clientSelectione = (Client) gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());
					setClientTexte(clientSelectione.nom, clientSelectione.prenom, clientSelectione.profession, clientSelectione.noTelPrincipal, clientSelectione.noTelSecondaire, clientSelectione.adresse, clientSelectione.NPA, clientSelectione.localite);
					setClientEditable(true); // Rends la saisie possible

					// Rends les boutons présentement innutils innactifs
					jButtonNouveauClient.setEnabled(false);
					jButtonModifierClient.setEnabled(false);

					// Permet d'annuler l'action en cours
					jButtonAnnulerClient.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelClientDetails.getRootPane().setDefaultButton(jButtonAnnulerClient);

					// Empêche la séléction d'un autre client
					jTableDetailsClient.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionC = "Modifier";

					jTextFieldNom.requestFocus();

					setOngletsSelectable(false);
				}
			});
		}
		return jButtonModifierClient;
	}

	/**
	 * This method initializes jButtonEnregistrerClient
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonEnregistrerClient() {
		if (jButtonEnregistrerClient == null) {
			jButtonEnregistrerClient = new JButton();
			jButtonEnregistrerClient.setText("Enregistrer");
			jButtonEnregistrerClient.setToolTipText("Cliquer sur ce bouton pour enregister les données d'un clients après un ajout ou une modification");
			jButtonEnregistrerClient.setEnabled(false);
			jButtonEnregistrerClient.setBounds(new Rectangle(150, 45, 106, 16));
			jButtonEnregistrerClient.setMnemonic(KeyEvent.VK_E);
			jButtonEnregistrerClient.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (actionC.equalsIgnoreCase("Ajouter")) {
						// Ajoute le client saisi à la base de donnée (Avec contrôle de la saisie)
						gestionnaireClient.ajoute(new Client(null, jTextFieldNom.getText(), jTextFieldPrenom.getText(),
										jTextFieldProfession.getText(), jTextFieldAdresse.getText(),
										jTextFieldNPA.getText(), jTextFieldLocalite.getText(),
										jTextFieldTelPrincipal.getText(), jTextFieldTelSecondaire.getText()));

						jPanelClientDetails.getRootPane().setDefaultButton(jButtonNouveauClient);
						jButtonNouveauClient.requestFocus();

						String titreInformation = "Ajout réussi.";
						String information = "M(me) " + jTextFieldNom.getText() + " a bien\n" +
										"été ajouté(e) dans la base de données";
						// affiche un message d'avertissement
						JOptionPane.showMessageDialog(getJFrame(), information, titreInformation,
										JOptionPane.INFORMATION_MESSAGE);
					} else if (actionC.equalsIgnoreCase("Modifier")) {
						// Modifie le client saisi à la base de donnée (Avec contrôle de la saisie)
						gestionnaireClient.modifie(new Client(gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow()).id, jTextFieldNom.getText(), jTextFieldPrenom.getText(),
										jTextFieldProfession.getText(), jTextFieldAdresse.getText(),
										jTextFieldNPA.getText(), jTextFieldLocalite.getText(),
										jTextFieldTelPrincipal.getText(), jTextFieldTelSecondaire.getText()));
						jPanelClientDetails.getRootPane().setDefaultButton(jButtonModifierClient);
						jButtonModifierClient.requestFocus();
					}
					// L'action a été effectuée
					actionC = "";

					// Désactive les actions devenues impossibles
					jButtonEnregistrerClient.setEnabled(false);
					jButtonAnnulerClient.setEnabled(false);

					setClientTexte("", "", "", "", "", "", "", ""); // Réinitialise la saisie du client

					// Autorise la séléction d'un autre client
					jTableDetailsClient.setEnabled(true);

					// Le client n'est plus éditable
					setClientEditable(false);

					// Active les actions à nouveau possible
					jButtonNouveauClient.setEnabled(true);

					actualiseTableClients();
					actualiseTableAppareilsClients();
					actualiseTableReparationsAppareil();
					actualiseTableReparationsClientAEncaisser();

					// Autorise la séléction d'un autre client
					jTableDetailsClient.setEnabled(true);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonEnregistrerClient;
	}

	/**
	 * This method initializes jButtonAnnulerClient
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAnnulerClient() {
		if (jButtonAnnulerClient == null) {
			jButtonAnnulerClient = new JButton();
			jButtonAnnulerClient.setMnemonic(KeyEvent.VK_A);
			jButtonAnnulerClient.setSelected(true);
			jButtonAnnulerClient.setEnabled(false);
			jButtonAnnulerClient.setToolTipText("Cliquer sur ce bouton pour " +
							"annuler l'ajout ou la modification en cours");
			jButtonAnnulerClient.setBounds(new Rectangle(60, 75, 136, 16));
			jButtonAnnulerClient.setText("Annuler");
			jButtonAnnulerClient.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					jButtonNouveauClient.setEnabled(true);

					if (tabModelClient.getRowCount() != 0) {
						jButtonModifierClient.setEnabled(true);
						jButtonSupprimerClient.setEnabled(true);
					}

					if (actionC.equalsIgnoreCase("ajouter")) {
						jPanelClientDetails.getRootPane().setDefaultButton(jButtonNouveauClient);
						jButtonNouveauClient.requestFocus();
					} else if (actionC.equalsIgnoreCase("modifier")) {
						jPanelClientDetails.getRootPane().setDefaultButton(jButtonModifierClient);
						jButtonModifierClient.requestFocus();
					}

					jButtonEnregistrerClient.setEnabled(false);
					jButtonAnnulerClient.setEnabled(false);
					setClientTexte("", "", "", "", "", "", "", ""); // Réinitialise la saisie du client

					if (tabModelClient.getRowCount() != 0) {
						// Récupère les détails du client sélectionné
						Client clientSelectione = (Client) gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());

						// Affiche les détails du client
						setClientTexte(clientSelectione.nom, clientSelectione.prenom, clientSelectione.profession, clientSelectione.noTelPrincipal, clientSelectione.noTelSecondaire, clientSelectione.adresse, clientSelectione.NPA, clientSelectione.localite);
					}

					// Autorise la séléction d'un autre client
					jTableDetailsClient.setEnabled(true);

					// Le client n'est plus éditable
					setClientEditable(false);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonAnnulerClient;
	}

	/**
	 * This method initializes jPanelBoutonsClient
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelBoutonsClient() {
		if (jPanelBoutonsClient == null) {
			jPanelBoutonsClient = new JPanel();
			jPanelBoutonsClient.setLayout(null);
			jPanelBoutonsClient.setBounds(new Rectangle(15, 300, 271, 106));
			jPanelBoutonsClient.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			jPanelBoutonsClient.add(getJButtonAnnulerClient(), null);
			jPanelBoutonsClient.add(getJButtonSupprimerClient(), null);
			jPanelBoutonsClient.add(getJButtonNouveauClient(), null);
			jPanelBoutonsClient.add(getJButtonModifierClient(), null);
			jPanelBoutonsClient.add(getJButtonEnregistrerClient(), null);
		}
		return jPanelBoutonsClient;
	}

	/**
	 * This method initializes jTitledPanelClientDetails
	 *
	 * @return javax.swing.JPanel
	 */
	private JXTitledPanel getJTitledPanelListeClients() {

		if (jTitledPanelListeClients == null) {
			jTitledPanelListeClients = new JXTitledPanel("Liste des clients");
			jTitledPanelListeClients.setBounds(new Rectangle(15, 15, 401, 451));
			jTitledPanelListeClients.setBorder(new DropShadowBorder(true));
			jTitledPanelListeClients.setTitleForeground(Color.white);
			jTitledPanelListeClients.add(getJPanelListeClients(), null);
		}
		return jTitledPanelListeClients;
	}

	/**
	 * This method initializes jPanelDetailAppareil
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelDetailAppareil() {
		if (jPanelDetailAppareil == null) {
			jPanelDetailAppareil = new JPanel();
			jPanelDetailAppareil.setLayout(null);
			jPanelDetailAppareil.add(getJTitledPanelListeAppareil(), null);
			jPanelDetailAppareil.add(getJTitledPanelAppareilDetails(), null);
		}
		return jPanelDetailAppareil;
	}

	/**
	 * This method initializes jPanelReparationTTAEncaisser
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelReparationTTAEncaisser() {
		if (jPanelReparationTTAEncaisser == null) {
			jPanelReparationTTAEncaisser = new JPanel();
			jPanelReparationTTAEncaisser.setLayout(null);
			jPanelReparationTTAEncaisser.add(getJTitledPanelListeTTRepparationAEncaisser(), null);
			jPanelReparationTTAEncaisser.add(getJTitledPanelReparationDetails11(), null);

			jTableDetailsReparationTTAEncaisser.setFocusable(false);
			jTableDetailsReparationAEncaisser.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			jTableDetailsReparationTTAEncaisser.addMouseListener(this); // Pour gérer les click sur la table
			jButtonModifierReparationTTAEncaisser.addKeyListener(new KeyAdapter() {

				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
										null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
							gestionnaireReparationTTAEncaisser.supprime(gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow()).id);
							setReparationTTAEncaisserTexte("", "", "", "", "", "", false); // L'appareil n'existe plus...
							//actualise les listes
							actualiseTableReparationsTTAEncaisser();
							actualiseTableReparationsClientAEncaisser();
							actualiseTableReparationsAppareil();

							if (tabModelReparationTTAEncaisser.getRowCount() != 0) {
								jTableDetailsReparationTTAEncaisser.setRowSelectionInterval(0, 0); // Séléctionne la première réparation par défaut

								// Récupère les détails de la réparation sélectionné
								Reparation reparationSelectione = (Reparation) gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow());

								// Affiche les détails de la réparation
								setReparationTTAEncaisserTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
							} else {
								jButtonSupprimerReparationTTAEncaisser.setEnabled(false);
							}
						}
					}
				}
			}); // Pour gérer les touches du clavier sur la table

			// Gestion de la saisie de touches du clavier dans une des zone obligatoire d'un appareil
			saisieAppareilListener = new KeyAdapter() {

				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() != KeyEvent.VK_ENTER) {
						verifReparationTTAEncaisser();
					}
				}
			};

			// Contrôle la saisie des champs obligatoires pour les appareils
			jTextFieldReparationTechnicienTTAEncaisser.addKeyListener(saisieAppareilListener);
			jTextFieldReparationPrixTTAEncaisser.addKeyListener(saisieAppareilListener);
			calendrierReparationDateTTAEncaisser.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierReparationDateTTAEncaisser.getEditor().setEditable(false);
					verifReparationTTAEncaisser();
				}
			});
			jTextFieldTTAEncaisserNoFacture.addKeyListener(saisieAppareilListener);
			jTextExtensReparationPanneTTAEncaisser.getPane().addKeyListener(saisieAppareilListener);
			jTextExtensReparationTravailEffectueTTAEncaisser.getPane().addKeyListener(saisieAppareilListener);
			jCheckBoxReparationPayeTTAEncaisser.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					verifReparationTTAEncaisser();
				}
			});
		}
		return jPanelReparationTTAEncaisser;
	}

	/**
	 * This method initializes jTitledPanelListeAppareil
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelListeAppareil() {
		if (jTitledPanelListeAppareil == null) {
			jTitledPanelListeAppareil = new JXTitledPanel("Liste des appareils");
			jTitledPanelListeAppareil.setBounds(new Rectangle(15, 15, 386, 421));
			jTitledPanelListeAppareil.setBorder(new DropShadowBorder(true));
			jTitledPanelListeAppareil.setTitleForeground(Color.white);
			jTitledPanelListeAppareil.add(getJPanelListeAppareil(), null);
		}
		return jTitledPanelListeAppareil;
	}

	/**
	 * This method initializes jPanelListeAppareil
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelListeAppareil() {
		if (jPanelListeAppareil == null) {
			jPanelListeAppareil = new JPanel();
			jPanelListeAppareil.setLayout(null);
			jPanelListeAppareil.setBounds(new Rectangle(15, 15, 373, 421));
			jPanelListeAppareil.add(getJScrollPaneDetailsAppareil(), null);
			jPanelListeAppareil.add(getJButtonAppareilRechercher(), null);
			jPanelListeAppareil.add(getJButtonTtAfficherListeAppareil(), null);

			jTableDetailsAppareil.addMouseListener(this); // Pour gérer les click sur la table
			getJButtonNouvelAppareil().addKeyListener(new KeyAdapter() {

				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
										null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
							gestionnaireAppareils.supprime(gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow()).id);
							setAppareilTexte("", "", "", "", "", "", "", ""); // L'appareil n'existe plus...
							actualiseTableAppareilsClients();
							actualiseTableReparationsAppareil();
							actualiseTableReparationsClientAEncaisser();
							actualiseTableReparationsTTAEncaisser();
							actualiseTableVente();

							if (tabModelAppareils.getRowCount() != 0) {
								jTableDetailsAppareil.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

								// Récupère les détails de l'appareil sélectionné
								Appareil appareilSelectione = (Appareil) gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow());

								// Affiche les détails de l'appareil
								setAppareilTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);
							} else {
								jButtonSupprimerAppareil.setEnabled(false);
							}
						}
					}
				}
			}); // Pour gérer les touches du clavier sur la table
		}
		return jPanelListeAppareil;
	}

	/**
	 * This method initializes jScrollPaneDetailsAppareils
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPaneDetailsAppareil() {
		if (jScrollPaneDetailsAppareils == null) {
			jScrollPaneDetailsAppareils = new JScrollPane();
			jScrollPaneDetailsAppareils.setBounds(new Rectangle(15, 30, 346, 316));
			jScrollPaneDetailsAppareils.setViewportView(getJXTableDetailsAppareil());
		}
		return jScrollPaneDetailsAppareils;
	}

	/**
	 * This method initializes jTableDetailsAppareil
	 *
	 * @return javax.swing.JXTable
	 */
	private JXTable getJXTableDetailsAppareil() {
		if (jTableDetailsAppareil == null) {
			jTableDetailsAppareil = new JXTable();

		}
		return jTableDetailsAppareil;
	}

	/**
	 * This method initializes jButtonAppareilRechercher
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAppareilRechercher() {
		if (jButtonAppareilRechercher == null) {
			jButtonAppareilRechercher = new JButton();
			jButtonAppareilRechercher.setBounds(new Rectangle(30, 360, 136, 16));
			jButtonAppareilRechercher.setMnemonic(KeyEvent.VK_R);
			jButtonAppareilRechercher.setSelected(true);
			jButtonAppareilRechercher.setText("Rechercher");
			jButtonAppareilRechercher.setToolTipText("Cliquer sur ce bouton pour rechercher un client");
			jButtonAppareilRechercher.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// ouvre la fenetre de recherche
					appareilRecherche = new FenetreRechercheAppareil(listeGenreAppareil,
									listeMarqueAppareil).affiche(true);

					if (appareilRecherche != null) {
						if (tabModelClient.getRowCount() != 0) { // Si on peut séléctionner un client
							appareilRecherche.proprietaire = ((Client) gestionnaireClient.getDetail(getJXTableDetailsClient().getSelectedRow())).id.toString();
							gestionnaireAppareils.recherche(appareilRecherche);
							if (tabModelAppareils.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
							{
								jTableDetailsAppareil.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
							}
						}
						actualiseTableAppareilsClients();
						actualiseTableReparationsAppareil();
					}
				}
			});
		}
		return jButtonAppareilRechercher;
	}

	/**
	 * This method initializes jButtonTtAfficherListeAppareil
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonTtAfficherListeAppareil() {
		if (jButtonTtAfficherListeAppareil == null) {
			jButtonTtAfficherListeAppareil = new JButton();
			jButtonTtAfficherListeAppareil.setBounds(new Rectangle(210, 360, 136, 16));
			jButtonTtAfficherListeAppareil.setMnemonic(KeyEvent.VK_T);
			jButtonTtAfficherListeAppareil.setText("Tout afficher");
			jButtonTtAfficherListeAppareil.setToolTipText("Cliquer sur ce bouton pour afficher la liste intégralle des appareils");
			jButtonTtAfficherListeAppareil.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (tabModelClient.getRowCount() != 0) { // Si on peut séléctionner un client
						String proprietaire = ((Client) gestionnaireClient.getDetail(getJXTableDetailsClient().getSelectedRow())).id.toString();
						gestionnaireAppareils.recherche(new Appareil(null, "", "", "", "", "", "", "", "", proprietaire, ""));
						if (tabModelAppareils.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
						{
							jTableDetailsAppareil.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
						}
						actualiseTableAppareilsClients();
						actualiseTableReparationsClientAEncaisser();
					}
				}
			});
		}
		return jButtonTtAfficherListeAppareil;
	}

	/**
	 * This method initializes jTitledPanelAppareilDetails
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelAppareilDetails() {
		if (jTitledPanelAppareilDetails == null) {
			jTitledPanelAppareilDetails = new JXTitledPanel("Détails");
			jTitledPanelAppareilDetails.setBounds(new Rectangle(436, 16, 312, 420));
			jTitledPanelAppareilDetails.setBorder(new DropShadowBorder(true));
			jTitledPanelAppareilDetails.setTitleForeground(Color.white);
			jTitledPanelAppareilDetails.add(getJPanelAppareilDetails(), null);
		}
		return jTitledPanelAppareilDetails;
	}

	/**
	 * This method initializes jPanelAppareilDetails
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelAppareilDetails() {
		if (jPanelAppareilDetails == null) {
			jLabelAppareilStatus = new JLabel();
			jLabelAppareilStatus.setBounds(new Rectangle(15, 240, 106, 16));
			jLabelAppareilStatus.setText("Status :");
			jLabelAppareilStatus.setVisible(false); // Pas besoins pour le moment
			jLabelAppareilNoFacture = new JLabel();
			jLabelAppareilNoFacture.setBounds(new Rectangle(15, 210, 105, 16));
			jLabelAppareilNoFacture.setText("N° de facture :");
			jLabelAppareilPrix = new JLabel();
			jLabelAppareilPrix.setBounds(new Rectangle(15, 180, 106, 16));
			jLabelAppareilPrix.setText("Prix :");
			jLabelAppareilDateVente = new JLabel();
			jLabelAppareilDateVente.setBounds(new Rectangle(15, 150, 106, 16));
			jLabelAppareilDateVente.setText("Date de vente :");
			jLabelAppareilNoSerie = new JLabel();
			jLabelAppareilNoSerie.setBounds(new Rectangle(15, 120, 106, 16));
			jLabelAppareilNoSerie.setText("N° de série:");
			jLabelAppareilType = new JLabel();
			jLabelAppareilType.setBounds(new Rectangle(15, 90, 106, 16));
			jLabelAppareilType.setText("Type :");
			jLabelAppareilMarque = new JLabel();
			jLabelAppareilMarque.setBounds(new Rectangle(15, 60, 106, 16));
			jLabelAppareilMarque.setText("*Marque :");
			jLabelAppareilGenre = new JLabel();
			jLabelAppareilGenre.setBounds(new Rectangle(15, 30, 106, 16));
			jLabelAppareilGenre.setLabelFor(jLabelAppareilGenre);
			jLabelAppareilGenre.setText("*Genre :");
			jPanelAppareilDetails = new JPanel();
			jPanelAppareilDetails.setLayout(null);
			jPanelAppareilDetails.setBounds(new Rectangle(450, 15, 301, 421));
			jPanelAppareilDetails.setName("");
			jPanelAppareilDetails.add(jLabelAppareilGenre, null);
			jPanelAppareilDetails.add(jLabelAppareilMarque, null);
			jPanelAppareilDetails.add(jLabelAppareilType, null);
			jPanelAppareilDetails.add(jLabelAppareilNoSerie, null);
			jPanelAppareilDetails.add(jLabelAppareilDateVente, null);
			jPanelAppareilDetails.add(jLabelAppareilPrix, null);
			jPanelAppareilDetails.add(jLabelAppareilNoFacture, null);
			jPanelAppareilDetails.add(getJTextFieldAppareilType(), null);
			jPanelAppareilDetails.add(getJTextFieldAppareilNoSerie(), null);
			jPanelAppareilDetails.add(getJTextFieldAppareilPrix(), null);
			jPanelAppareilDetails.add(getJTextFieldAppareilNoFacture(), null);
			jPanelAppareilDetails.add(getjPanelBoutonsClientStock(), null);
			jPanelAppareilDetails.add(getJComboBoxAppareilGenre(), null);
			jPanelAppareilDetails.add(getJComboBoxAppareilMarque(), null);
			jPanelAppareilDetails.add(getJComboBoxAppareilStatus(), null);
			jPanelAppareilDetails.add(jLabelAppareilStatus, null);
			jPanelAppareilDetails.add(getCalendrierAppareilDateVente(), null);

			// Gestion de la saisie de touches du clavier dans une des zone obligatoire d'un appareil
			saisieAppareilListener = new KeyAdapter() {

				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() != KeyEvent.VK_ENTER) {
						verifChampsAppareil();
					}
				}
			};

			// Contrôle la saisie des champs obligatoires pour les appareils
			jTextFieldAppareilType.addKeyListener(saisieAppareilListener);
			jTextFieldAppareilNoSerie.addKeyListener(saisieAppareilListener);
			calendrierAppareilDateVente.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierAppareilDateVente.getEditor().setEditable(false);
					verifChampsAppareil();
				}
			});
			jTextFieldAppareilPrix.addKeyListener(saisieAppareilListener);
			jTextFieldAppareilNoFacture.addKeyListener(saisieAppareilListener);
		}
		return jPanelAppareilDetails;
	}

	private void verifChampsClient() {
		// Si les champs obligatoires ne sont pas remplis, grise le bouton d'enregistrement
		if (!jTextFieldNom.isEditable() || jTextFieldNom.getText().isEmpty() || jTextFieldPrenom.getText().isEmpty() || jTextFieldAdresse.getText().isEmpty() || jTextFieldNPA.getText().isEmpty() || jTextFieldLocalite.getText().isEmpty() || jTextFieldNPA.getText().length() < 4) {
			jButtonEnregistrerClient.setEnabled(false);
			jPanelClientDetails.getRootPane().setDefaultButton(jButtonAnnulerClient);
		} // Sinon le dégrise
		else {
			jButtonEnregistrerClient.setEnabled(true);
			jPanelClientDetails.getRootPane().setDefaultButton(jButtonEnregistrerClient);
		}
	}

	private void verifChampsAppareil() {
		// Si les champs obligatoires ne sont pas remplis, grise le bouton d'enregistrement
		if (jComboBoxAppareilGenre.getSelectedIndex() == -1 || jComboBoxAppareilMarque.getSelectedIndex() == -1) {
			jButtonEnregistrerAppareil.setEnabled(false);
			jPanelAppareilDetails.getRootPane().setDefaultButton(jButtonAnnulerAppareil);
		} // Sinon le dégrise
		else {
			jButtonEnregistrerAppareil.setEnabled(true);
			jPanelAppareilDetails.getRootPane().setDefaultButton(jButtonEnregistrerAppareil);
		}
	}

	private void verifAppareilStock() {
		// Si les champs obligatoires ne sont pas remplis, grise le bouton d'enregistrement
		if (jComboBoxAppareilGenreStock.getSelectedIndex() == -1 || jComboBoxAppareilMarqueStock.getSelectedIndex() == -1) {
			jButtonEnregistrerAppareilStock.setEnabled(false);
			jPanelAppareilDetailsStock.getRootPane().setDefaultButton(jButtonAnnulerAppareilStock);
		} // Sinon le dégrise
		else {
			jButtonEnregistrerAppareilStock.setEnabled(true);
			jPanelAppareilDetailsStock.getRootPane().setDefaultButton(jButtonEnregistrerAppareilStock);
		}
	}

	private void verifReparation() {
		// Si les champs obligatoires ne sont pas remplis, grise le bouton d'enregistrement
		if (!jTextFieldReparationTechnicien.isEditable() || jTextFieldReparationTechnicien.getText().isEmpty() || jTextExtensReparationPanne.getPane().getText().isEmpty() || jTextExtensReparationTravailEffectue.getPane().getText().isEmpty() || jTextFieldReparationPrix.getText().isEmpty() || calendrierReparationDate.getDate() == null) {
			jButtonEnregistrerReparation.setEnabled(false);
			jPanelReparations.getRootPane().setDefaultButton(jButtonAnnulerReparation);
		} // Sinon le dégrise
		else {
			jButtonEnregistrerReparation.setEnabled(true);
			jPanelReparations.getRootPane().setDefaultButton(jButtonEnregistrerReparation);
		}
	}

	private void verifReparationAEncaisser() {
		// Si les champs obligatoires ne sont pas remplis, grise le bouton d'enregistrement
		if (!jTextFieldReparationTechnicienAEncaisser.isEditable() || jTextFieldReparationTechnicienAEncaisser.getText().isEmpty() || jTextExtensReparationPanne1.getPane().getText().isEmpty() || jTextExtensReparationTravailEffectue1.getPane().getText().isEmpty() || jTextFieldReparationPrix1.getText().isEmpty() || calendrierAEncaisserDate.getDate() == null) {
			jButtonEnregistrerReparationAEncaisser.setEnabled(false);
			jPanelAEncaisserClient1.getRootPane().setDefaultButton(jButtonAnnulerReparationAEncaisser);
		} // Sinon le dégrise
		else {
			jButtonEnregistrerReparationAEncaisser.setEnabled(true);
			jPanelAEncaisserClient1.getRootPane().setDefaultButton(jButtonEnregistrerReparationAEncaisser);
		}
	}

	private void verifReparationTTAEncaisser() {
		// Si les champs obligatoires ne sont pas remplis, grise le bouton d'enregistrement
		if (!jTextFieldReparationTechnicienTTAEncaisser.isEditable() || jTextFieldReparationTechnicienTTAEncaisser.getText().isEmpty() || jTextExtensReparationPanneTTAEncaisser.getPane().getText().isEmpty() || jTextExtensReparationTravailEffectueTTAEncaisser.getPane().getText().isEmpty() || jTextFieldReparationPrixTTAEncaisser.getText().isEmpty() || calendrierReparationDateTTAEncaisser.getDate() == null) {
			jButtonEnregistrerReparationTTAEncaisser.setEnabled(false);
			jPanelReparationDetailsTTAEncaisser.getRootPane().setDefaultButton(jButtonAnnulerReparationTTAEncaisser);
		} // Sinon le dégrise
		else {
			jButtonEnregistrerReparationTTAEncaisser.setEnabled(true);
			jPanelReparationDetailsTTAEncaisser.getRootPane().setDefaultButton(jButtonEnregistrerReparationTTAEncaisser);
		}
	}

	private void verifAppareilVente() {
		// Si les champs obligatoires ne sont pas remplis, grise le bouton d'enregistrement
		if (jComboBoxAppareilGenre11.getSelectedIndex() == -1 || jComboBoxAppareilMarque11.getSelectedIndex() == -1) {
			jButtonEnregistrerAppareilVendu.setEnabled(false);
			jPanelVentes.getRootPane().setDefaultButton(jButtonAnnulerAppareilVendu);
		} // Sinon le dégrise
		else {
			jButtonEnregistrerAppareilVendu.setEnabled(true);
			jPanelVentes.getRootPane().setDefaultButton(jButtonEnregistrerAppareilVendu);
		}
	}

	/**
	 * This method initializes jTextFieldAppareilType
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldAppareilType() {
		if (jTextFieldAppareilType == null) {
			jTextFieldAppareilType = new JTextField();
			jTextFieldAppareilType.setBounds(new Rectangle(135, 90, 151, 16));
			jTextFieldAppareilType.setBounds(new Rectangle(135, 90, 151, 16));
			jTextFieldAppareilType.setEditable(false);
		}
		return jTextFieldAppareilType;
	}

	/**
	 * This method initializes jTextFieldAppareilNoSerie
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldAppareilNoSerie() {
		if (jTextFieldAppareilNoSerie == null) {
			jTextFieldAppareilNoSerie = new JTextField();
			jTextFieldAppareilNoSerie.setBounds(new Rectangle(135, 120, 151, 16));
			jTextFieldAppareilNoSerie.setBounds(new Rectangle(135, 120, 151, 16));
		}
		return jTextFieldAppareilNoSerie;
	}

	/**
	 * This method initializes jTextFieldAppareilPrix
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldAppareilPrix() {
		if (jTextFieldAppareilPrix == null) {
			jTextFieldAppareilPrix = new JTextField();
			jTextFieldAppareilPrix.setBounds(new Rectangle(135, 180, 151, 16));
			jTextFieldAppareilPrix.setBounds(new Rectangle(135, 180, 151, 16));
			jTextFieldAppareilPrix.setDocument(modeleChamps.obtFloat(jTextFieldAppareilPrix));
		}
		return jTextFieldAppareilPrix;
	}

	/**
	 * This method initializes jTextFieldAppareilNoFacture
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldAppareilNoFacture() {
		if (jTextFieldAppareilNoFacture == null) {
			jTextFieldAppareilNoFacture = new JTextField();
			jTextFieldAppareilNoFacture.setBounds(new Rectangle(135, 210, 151, 16));
			jTextFieldAppareilNoFacture.setBounds(new Rectangle(135, 210, 151, 16));
		}
		return jTextFieldAppareilNoFacture;
	}

	/**
	 * This method initializes jPanelBoutonsClientStock
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getjPanelBoutonsClientStock() {
		if (jPanelBoutonsClientStock == null) {
			jPanelBoutonsClientStock = new JPanel();
			jPanelBoutonsClientStock.setLayout(null);
			jPanelBoutonsClientStock.setBounds(new Rectangle(15, 270, 271, 106));
			jPanelBoutonsClientStock.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			jPanelBoutonsClientStock.add(getJButtonAnnulerAppareil(), null);
			jPanelBoutonsClientStock.add(getJButtonSupprimerAppareil(), null);
			jPanelBoutonsClientStock.add(getJButtonModifierAppareil(), null);
			jPanelBoutonsClientStock.add(getJButtonEnregistrerAppareil(), null);
			jPanelBoutonsClientStock.add(getJButtonNouvelAppareil(), null);
		}
		return jPanelBoutonsClientStock;
	}

	/**
	 * This method initializes jButtonAnnulerAppareil
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAnnulerAppareil() {
		if (jButtonAnnulerAppareil == null) {
			jButtonAnnulerAppareil = new JButton();
			jButtonAnnulerAppareil.setBounds(new Rectangle(60, 75, 136, 16));
			jButtonAnnulerAppareil.setToolTipText("Cliquer sur ce bouton pour " + "annuler l\'ajout ou la modification en cours");
			jButtonAnnulerAppareil.setMnemonic(KeyEvent.VK_A);
			jButtonAnnulerAppareil.setSelected(true);
			jButtonAnnulerAppareil.setText("Annuler");
			jButtonAnnulerAppareil.setEnabled(false);
			jButtonAnnulerAppareil.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					jButtonNouvelAppareil.setEnabled(true);

					if (tabModelAppareils.getRowCount() != 0) {
						jButtonModifierAppareil.setEnabled(true);
						jButtonSupprimerAppareil.setEnabled(true);
					}

					if (actionA.equalsIgnoreCase("ajouter")) {
						jPanelAppareilDetails.getRootPane().setDefaultButton(jButtonNouvelAppareil);
						jButtonNouvelAppareil.requestFocus();
					} else if (actionA.equalsIgnoreCase("modifier")) {
						jPanelAppareilDetails.getRootPane().setDefaultButton(jButtonModifierAppareil);
						jButtonModifierAppareil.requestFocus();
					}

					jButtonEnregistrerAppareil.setEnabled(false);
					jButtonAnnulerAppareil.setEnabled(false);
					setAppareilTexte("", "", "", "", "", "", "", ""); // Réinitialise la saisie de l'appareil

					if (tabModelAppareils.getRowCount() != 0) {
						// Récupère les détails de l'appareil sélectionné
						Appareil appareilSelectione = (Appareil) gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow());

						// Affiche les détails de l'appareil
						setAppareilTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);
					}

					// Autorise la séléction d'un autre appareil
					jTableDetailsAppareil.setEnabled(true);

					// L'appareil n'est plus éditable
					setAppareilEditable(false);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonAnnulerAppareil;
	}

	/**
	 * This method initializes jButtonSupprimerAppareil
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonSupprimerAppareil() {
		if (jButtonSupprimerAppareil == null) {
			jButtonSupprimerAppareil = new JButton();
			jButtonSupprimerAppareil.setBounds(new Rectangle(150, 15, 106, 16));
			jButtonSupprimerAppareil.setMnemonic(KeyEvent.VK_S);
			jButtonSupprimerAppareil.setText("Supprimer");
			jButtonSupprimerAppareil.setToolTipText("Cliquer sur ce bouton pour supprimer les données d\'un appareil existant (réparations comrpises)");
			jButtonSupprimerAppareil.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
									null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
						gestionnaireAppareils.supprime(gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow()).id);
						setAppareilTexte("", "", "", "", "", "", "", ""); // L'appareil n'existe plus...
						actualiseTableAppareilsClients();
						actualiseTableReparationsAppareil();
						actualiseTableReparationsClientAEncaisser();
						actualiseTableReparationsTTAEncaisser();
						actualiseTableVente();

						if (tabModelAppareils.getRowCount() != 0) {
							jTableDetailsAppareil.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

							// Récupère les détails de l'appareil sélectionné
							Appareil appareilSelectione = (Appareil) gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow());

							// Affiche les détails de l'appareil
							setAppareilTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);
						} else {
							jButtonSupprimerAppareil.setEnabled(false);
						}
					}
				}
			});
		}
		return jButtonSupprimerAppareil;
	}

	/**
	 * This method initializes jButtonModifierAppareil
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonModifierAppareil() {
		if (jButtonModifierAppareil == null) {
			jButtonModifierAppareil = new JButton();
			jButtonModifierAppareil.setBounds(new Rectangle(15, 45, 106, 16));
			jButtonModifierAppareil.setToolTipText("Cliquer sur ce bouton pour modifier les données d\'un appareil existant");
			jButtonModifierAppareil.setMnemonic(KeyEvent.VK_M);
			jButtonModifierAppareil.setText("Modifier");
			jButtonModifierAppareil.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Initialise la saisie
					Appareil appareilSelectione = (Appareil) gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow());
					setAppareilTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);
					setAppareilEditable(true); // Rends la saisie possible

					// Rends les boutons présentement innutils innactifs
					jButtonNouvelAppareil.setEnabled(false);
					jButtonModifierAppareil.setEnabled(false);

					// Permet d'annuler l'action en cours
					jButtonAnnulerAppareil.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelAppareilDetails.getRootPane().setDefaultButton(jButtonAnnulerAppareil);

					// Empêche la séléction d'un autre appareil
					jTableDetailsAppareil.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionA = "Modifier";

					jTextFieldAppareilType.requestFocus();

					setOngletsSelectable(false);
				}
			});
		}
		return jButtonModifierAppareil;
	}

	/**
	 * This method initializes jButtonEnregistrerAppareil
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonEnregistrerAppareil() {
		if (jButtonEnregistrerAppareil == null) {
			jButtonEnregistrerAppareil = new JButton();
			jButtonEnregistrerAppareil.setBounds(new Rectangle(150, 45, 106, 16));
			jButtonEnregistrerAppareil.setToolTipText("Cliquer sur ce bouton pour enregister les données d\'un appareil après un ajout ou une modification");
			jButtonEnregistrerAppareil.setMnemonic(KeyEvent.VK_E);
			jButtonEnregistrerAppareil.setText("Enregistrer");
			jButtonEnregistrerAppareil.setEnabled(false);

			jButtonEnregistrerAppareil.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					String dateVente = "";

					if (getCalendrierAppareilDateVente().getDate() != null) {
						dateVente = Utilitaire.getSQLDate(getCalendrierAppareilDateVente().getDate());
					}

					if (actionA.equalsIgnoreCase("Ajouter")) {
						// Ajoute l'appareil saisi à la base de donnée (Avec contrôle de la saisie)
						gestionnaireAppareils.ajoute(new Appareil(null,
										(String) (getJComboBoxAppareilGenre().getSelectedItem()),
										(String) (getJComboBoxAppareilMarque().getSelectedItem()),
										getJTextFieldAppareilType().getText(),
										getJTextFieldAppareilNoSerie().getText(),
										dateVente,
										"",
										getJTextFieldAppareilPrix().getText(),
										getJTextFieldAppareilNoFacture().getText(),
										getClientSelectionne().toString(),
										"2")); // Integer.toString(getJComboBoxAppareilStatus().getSelectedIndex() xxx


						jPanelAppareilDetails.getRootPane().setDefaultButton(jButtonNouvelAppareil);
						jButtonNouvelAppareil.requestFocus();
						String titreInformation = "Ajout réussi.";
						String information = "L'appareil " + (String) (getJComboBoxAppareilMarque().getSelectedItem()) + " " +
										getJTextFieldAppareilType().getText() + "\n" +
										"a bien été ajouté dans la base de données";
						JOptionPane.showMessageDialog(getJFrame(), information, titreInformation,
										JOptionPane.INFORMATION_MESSAGE);
					} else if (actionA.equalsIgnoreCase("Modifier")) {
						// Récupère les détails de l'appareil sélectionné
						Appareil appareilSelectione = (Appareil) gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow());

						// Modifie l'appareil saisi à la base de donnée (Avec contrôle de la saisie)
						gestionnaireAppareils.modifie(new Appareil(gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow()).id,
										(String) (getJComboBoxAppareilGenre().getSelectedItem()),
										(String) (getJComboBoxAppareilMarque().getSelectedItem()),
										getJTextFieldAppareilType().getText(),
										getJTextFieldAppareilNoSerie().getText(),
										dateVente,
										"",
										getJTextFieldAppareilPrix().getText(),
										getJTextFieldAppareilNoFacture().getText(),
										getClientSelectionne().toString(),
										appareilSelectione.status)); // Integer.toString(getJComboBoxAppareilStatus().getSelectedIndex()) xxx

						jPanelAppareilDetails.getRootPane().setDefaultButton(jButtonModifierAppareil);
						jButtonModifierAppareil.requestFocus();
					}
					// L'action a été effectuée
					actionA = "";

					// Désactive les actions devenues impossibles
					jButtonEnregistrerAppareil.setEnabled(false);
					jButtonAnnulerAppareil.setEnabled(false);

					setAppareilTexte("", "", "", "", "", "", "", ""); // Réinitialise la saisie de l'appareil

					// Autorise la séléction d'un autre appareil
					jTableDetailsAppareil.setEnabled(true);

					// L'appareil n'est plus éditable
					setAppareilEditable(false);

					actualiseTableAppareilsClients();
					actualiseTableReparationsAppareil();
					actualiseTableReparationsClientAEncaisser();
					actualiseTableReparationsTTAEncaisser();
					actualiseTableVente();

					// Autorise la séléction d'un autre appareil
					jTableDetailsAppareil.setEnabled(true);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonEnregistrerAppareil;
	}

	/**
	 * This method initializes jTitledPanelListeReparation
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelListeReparation() {
		if (jTitledPanelListeReparation == null) {
			jTitledPanelListeReparation = new JXTitledPanel("Liste des appareils");
			jTitledPanelListeReparation.setBorder(new DropShadowBorder(true));
			jTitledPanelListeReparation.setTitleForeground(Color.white);
			jTitledPanelListeReparation.setLocation(new Point(15, 15));
			jTitledPanelListeReparation.setSize(new Dimension(386, 421));
			jTitledPanelListeReparation.setTitle("Liste des réparations");
			jTitledPanelListeReparation.add(getJPanelListeReparation(), null);
		}
		return jTitledPanelListeReparation;
	}

	/**
	 * This method initializes jPanelListeReparation
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelListeReparation() {
		if (jPanelListeReparation == null) {
			jPanelListeReparation = new JPanel();
			jPanelListeReparation.setLayout(null);
			jPanelListeReparation.setBounds(new Rectangle(15, 15, 373, 421));
			jPanelListeReparation.add(getJScrollPaneDetailsReparation(), null);
			jPanelListeReparation.add(getJButtonReparationRechercher(), null);
			jPanelListeReparation.add(getJButtonTtAfficherListeReparation(), null);
		}
		return jPanelListeReparation;
	}

	/**
	 * This method initializes jScrollPaneDetailsReparation
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPaneDetailsReparation() {
		if (jScrollPaneDetailsReparation == null) {
			jScrollPaneDetailsReparation = new JScrollPane();
			jScrollPaneDetailsReparation.setBounds(new Rectangle(15, 30, 346, 316));
			jScrollPaneDetailsReparation.setViewportView(getJXTableDetailsReparation());
		}
		return jScrollPaneDetailsReparation;
	}

	/**
	 * This method initializes jTableDetailsReparation
	 *
	 * @return javax.swing.JXTable
	 */
	private JXTable getJXTableDetailsReparation() {
		if (jTableDetailsReparation == null) {
			jTableDetailsReparation = new JXTable();
		}
		return jTableDetailsReparation;
	}

	/**
	 * This method initializes jButtonReparationRechercher
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonReparationRechercher() {
		if (jButtonReparationRechercher == null) {
			jButtonReparationRechercher = new JButton();
			jButtonReparationRechercher.setBounds(new Rectangle(30, 360, 136, 16));
			jButtonReparationRechercher.setMnemonic(KeyEvent.VK_R);
			jButtonReparationRechercher.setSelected(true);
			jButtonReparationRechercher.setText("Rechercher");
			jButtonReparationRechercher.setToolTipText("Cliquer sur ce bouton pour rechercher une réparation");
			jButtonReparationRechercher.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					reparationRecherchee = new FenetreRechercheReparation(true).affiche(true);

					if (reparationRecherchee != null) {
						// si une réparation est sélectionnées
						if (tabModelAppareils.getRowCount() != 0) { // Si on peut séléctionner un client
							reparationRecherchee.appareil = (gestionnaireAppareils.getDetail(getJXTableDetailsAppareil().getSelectedRow())).id.toString();

							gestionnaireReparation.recherche(reparationRecherchee);
							if (tabModelReparation.getRowCount() != 0) {
								jTableDetailsReparation.setRowSelectionInterval(0, 0); // Séléctionne la première reparation par défaut
							}
						}
						actualiseTableReparationsAppareil();
					}
				}
			});
		}
		return jButtonReparationRechercher;
	}

	/**
	 * This method initializes jButtonTtAfficherListeReparation
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonTtAfficherListeReparation() {
		if (jButtonTtAfficherListeReparation == null) {
			jButtonTtAfficherListeReparation = new JButton();
			jButtonTtAfficherListeReparation.setBounds(new Rectangle(210, 360, 136, 16));
			jButtonTtAfficherListeReparation.setMnemonic(KeyEvent.VK_T);
			jButtonTtAfficherListeReparation.setText("Tout afficher");
			jButtonTtAfficherListeReparation.setToolTipText("Cliquer sur ce bouton pour afficher la liste intégralle des réparations");
			jButtonTtAfficherListeReparation.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// si une réparation est sélectionnées
					if (tabModelAppareils.getRowCount() != 0) { // Si on peut séléctionner un client
						String appareilID = ((Appareil) gestionnaireAppareils.getDetail(getJXTableDetailsAppareil().getSelectedRow())).id.toString();

						gestionnaireReparation.recherche(new Reparation(null, null, null, null, null, null, null, null, appareilID, null, null));
						if (tabModelReparation.getRowCount() != 0) {
							jTableDetailsReparation.setRowSelectionInterval(0, 0);
						}
					}
					actualiseTableReparationsAppareil();

				}
			});
		}
		return jButtonTtAfficherListeReparation;
	}

	/**
	 * This method initializes jTitledPanelReparationDetails
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelReparationDetails() {
		if (jTitledPanelReparationDetails == null) {
			jTitledPanelReparationDetails = new JXTitledPanel("Détails");
			jTitledPanelReparationDetails.setBorder(new DropShadowBorder(true));
			jTitledPanelReparationDetails.setTitleForeground(Color.white);
			jTitledPanelReparationDetails.setSize(new Dimension(312, 420));
			jTitledPanelReparationDetails.setLocation(new Point(436, 16));
			jTitledPanelReparationDetails.add(getJPanelreparationDetails(), null);
		}
		return jTitledPanelReparationDetails;
	}

	/**
	 * This method initializes jPanelreparationDetails
	 *
	 * @return javax.swing.JPanel
	 */
	private JLayeredPane getJPanelreparationDetails() {
		if (jPanelreparationDetails == null) {
			jLabelReparationNoFacture = new JLabel();
			jLabelReparationNoFacture.setBounds(new Rectangle(15, 180, 121, 16));
			jLabelReparationNoFacture.setText("N° de facture");
			jLabelReparationPaye = new JLabel();
			jLabelReparationPaye.setBounds(new Rectangle(15, 210, 121, 16));
			jLabelReparationPaye.setText("*Payé :");
			jLabelReparationPrix = new JLabel();
			jLabelReparationPrix.setBounds(new Rectangle(15, 150, 121, 16));
			jLabelReparationPrix.setText("*Prix :");
			jLabelReparationTechnicien = new JLabel();
			jLabelReparationTechnicien.setBounds(new Rectangle(15, 120, 121, 16));
			jLabelReparationTechnicien.setText("*Technicien :");
			jLabelReparationTravailEff = new JLabel();
			jLabelReparationTravailEff.setBounds(new Rectangle(15, 90, 121, 16));
			jLabelReparationTravailEff.setText("*Travail effectué :");
			jLabelReparationPanne = new JLabel();
			jLabelReparationPanne.setBounds(new Rectangle(15, 60, 121, 16));
			jLabelReparationPanne.setText("*Panne :");
			jLabelReparationDate = new JLabel();
			jLabelReparationDate.setBounds(new Rectangle(15, 30, 130, 16));
			jLabelReparationDate.setText("*Date de réparation :");
			jPanelreparationDetails = new JLayeredPane();
			jPanelreparationDetails.setLayout(null);
			jPanelreparationDetails.setBounds(new Rectangle(450, 15, 301, 421));
			jPanelreparationDetails.setName("");
			jPanelreparationDetails.add(jLabelReparationDate, null);
			jPanelreparationDetails.add(jLabelReparationPanne, null);
			jPanelreparationDetails.add(jLabelReparationTravailEff, null);
			jPanelreparationDetails.add(jLabelReparationTechnicien, null);
			jPanelreparationDetails.add(jLabelReparationPrix, null);
			jPanelreparationDetails.add(getJTextFieldReparationTechnicien(), null);
			jPanelreparationDetails.add(getjPanelBoutonsClientStock1(), null);
			jPanelreparationDetails.add(jLabelReparationPaye, null);
			jPanelreparationDetails.add(getJTextExtensReparationPanne(), null);
			jPanelreparationDetails.add(getJTextExtensReparationTravailEffectue(), null);
			jPanelreparationDetails.add(getJTextFieldReparationPrix(), null);
			jPanelreparationDetails.add(getJCheckBoxReparationPaye(), null);
			jPanelreparationDetails.add(getCalendrierReparationDate(), null);
			jPanelreparationDetails.add(getJTextFieldReparationNoFacture(), null);
			jPanelreparationDetails.add(jLabelReparationNoFacture, null);
			jPanelreparationDetails.setLayer(getJTextExtensReparationPanne(), 200);
			jPanelreparationDetails.setLayer(getJTextExtensReparationTravailEffectue(), 200);

			// Gestion de la saisie de touches du clavier dans une des zone obligatoire de la réparation
			saisieReparationListener = new KeyAdapter() {

				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() != KeyEvent.VK_ENTER) {
						verifReparation();
					}
				}
			};

			jTextExtensReparationPanne.getPane().addKeyListener(saisieReparationListener);
			jTextExtensReparationTravailEffectue.getPane().addKeyListener(saisieReparationListener);
			jTextFieldReparationTechnicien.addKeyListener(saisieReparationListener);
			jTextFieldReparationPrix.addKeyListener(saisieReparationListener);
			jTextFieldReparationNoFacture.addKeyListener(saisieReparationListener);

			calendrierReparationDate.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierReparationDate.getEditor().setEditable(false);
					verifReparation();
				}
			});
			jCheckBoxReparationPaye.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					verifReparation();
				}
			});
		}
		return jPanelreparationDetails;
	}

	/**
	 * This method initializes jTextFieldReparationTechnicien
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldReparationTechnicien() {
		if (jTextFieldReparationTechnicien == null) {
			jTextFieldReparationTechnicien = new JTextField();
			jTextFieldReparationTechnicien.setBounds(new Rectangle(150, 120, 136, 16));

		}
		return jTextFieldReparationTechnicien;
	}

	/**
	 * This method initializes jPanelBoutonsClientStock1
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getjPanelBoutonsClientStock1() {
		if (jPanelBoutonsClientStock1 == null) {
			jPanelBoutonsClientStock1 = new JPanel();
			jPanelBoutonsClientStock1.setLayout(null);
			jPanelBoutonsClientStock1.setBounds(new Rectangle(15, 270, 271, 106));
			jPanelBoutonsClientStock1.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			jPanelBoutonsClientStock1.add(getJButtonAnnulerReparation(), null);
			jPanelBoutonsClientStock1.add(getJButtonSupprimerReparation(), null);
			jPanelBoutonsClientStock1.add(getJButtonNouvelleReparation(), null);
			jPanelBoutonsClientStock1.add(getJButtonModifierReparation(), null);
			jPanelBoutonsClientStock1.add(getJButtonEnregistrerReparation(), null);
		}
		return jPanelBoutonsClientStock1;
	}

	/**
	 * This method initializes jButtonAnnulerReparation
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAnnulerReparation() {
		if (jButtonAnnulerReparation == null) {
			jButtonAnnulerReparation = new JButton();
			jButtonAnnulerReparation.setBounds(new Rectangle(60, 75, 136, 16));
			jButtonAnnulerReparation.setToolTipText("Cliquer sur ce bouton pour " + "annuler l\'ajout ou la modification en cours");
			jButtonAnnulerReparation.setMnemonic(KeyEvent.VK_A);
			jButtonAnnulerReparation.setSelected(true);
			jButtonAnnulerReparation.setText("Annuler");
			jButtonAnnulerReparation.setEnabled(false);
			jButtonAnnulerReparation.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					jButtonNouvelleReparation.setEnabled(true);

					if (tabModelReparation.getRowCount() != 0) {
						jButtonModifierReparation.setEnabled(true);
						jButtonSupprimerReparation.setEnabled(true);
					}

					if (actionR.equalsIgnoreCase("ajouter")) {
						jPanelReparations.getRootPane().setDefaultButton(jButtonNouvelleReparation);
						jButtonNouvelAppareil.requestFocus();
					} else if (actionR.equalsIgnoreCase("modifier")) {
						jPanelReparations.getRootPane().setDefaultButton(jButtonModifierReparation);
						jButtonModifierReparation.requestFocus();
					}

					jButtonEnregistrerReparation.setEnabled(false);
					jButtonAnnulerReparation.setEnabled(false);
					setReparationTexte("", "", "", "", "", "", false); // Réinitialise la saisie de l'appareil

					if (tabModelReparation.getRowCount() != 0) {
						// Récupère les détails de la réparation sélectionné
						Reparation reparationSelectione = (Reparation) gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow());

						// Affiche les détails de la réparation
						setReparationTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
					}

					// Autorise la séléction d'une autre réparation
					jTableDetailsReparation.setEnabled(true);

					// La réparation n'est plus éditable
					setReparationEditable(false);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonAnnulerReparation;
	}

	/**
	 * This method initializes jButtonSupprimerReparation
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonSupprimerReparation() {
		if (jButtonSupprimerReparation == null) {
			jButtonSupprimerReparation = new JButton();
			jButtonSupprimerReparation.setBounds(new Rectangle(150, 15, 106, 16));
			jButtonSupprimerReparation.setMnemonic(KeyEvent.VK_S);
			jButtonSupprimerReparation.setText("Supprimer");
			jButtonSupprimerReparation.setToolTipText("Cliquer sur ce bouton pour supprimer les données d\'une réparation existante");
			jButtonSupprimerReparation.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
									null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
						gestionnaireReparation.supprime(gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow()).id);
						setReparationTexte("", "", "", "", "", "", false); // L'appareil n'existe plus...

						actualiseTableReparationsAppareil();
						actualiseTableReparationsClientAEncaisser();
						actualiseTableReparationsTTAEncaisser();

						if (tabModelReparation.getRowCount() != 0) {
							jTableDetailsReparation.setRowSelectionInterval(0, 0); // Séléctionne la première réparation par défaut

							// Récupère les détails de la réparation sélectionné
							Reparation reparationSelectione = (Reparation) gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow());

							// Affiche les détails de la réparation
							setReparationTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
						} else {
							jButtonSupprimerReparation.setEnabled(false);
						}
					}
				}
			});
		}
		return jButtonSupprimerReparation;
	}

	/**
	 * This method initializes jButtonNouvelleReparation
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonNouvelleReparation() {
		if (jButtonNouvelleReparation == null) {
			jButtonNouvelleReparation = new JButton();
			jButtonNouvelleReparation.setBounds(14, 15, 106, 16);
			jButtonNouvelleReparation.setToolTipText("Cliquer sur ce bouton pour ajouter une nouvelle réparation");
			jButtonNouvelleReparation.setMnemonic(KeyEvent.VK_N);
			jButtonNouvelleReparation.setText("Nouveau");
			jButtonNouvelleReparation.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					setReparationTexte("", "", "", "", "", "", false); // Initialise la saisie
					setReparationEditable(true); // Rends la saisie possible

					// Permet d'annuler l'action en cours
					jButtonAnnulerReparation.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelreparationDetails.getRootPane().setDefaultButton(jButtonAnnulerReparation);

					// Rends les boutons présentement innutils innactifs
					jButtonNouvelleReparation.setEnabled(false);
					jButtonModifierReparation.setEnabled(false);

					// Empêche la séléction d'une autre réparation
					jTableDetailsReparation.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionR = "Ajouter";

					jTextFieldReparationTechnicien.requestFocus();

					setOngletsSelectable(false);
				}
			});

			jButtonNouvelleReparation.addKeyListener(new KeyAdapter() {

				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
										null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
							gestionnaireReparation.supprime(gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow()).id);
							setReparationTexte("", "", "", "", "", "", false); // L'appareil n'existe plus...
							actualiseTableReparationsAppareil();
							actualiseTableReparationsClientAEncaisser();
							actualiseTableReparationsTTAEncaisser();

							if (tabModelReparation.getRowCount() != 0) {
								jTableDetailsReparation.setRowSelectionInterval(0, 0); // Séléctionne la première réparation par défaut

								// Récupère les détails de la réparation sélectionné
								Reparation reparationSelectione = (Reparation) gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow());

								// Affiche les détails de la réparation
								setReparationTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
							} else {
								jButtonSupprimerReparation.setEnabled(false);
							}
						}
					}
				}
			});
			jTableDetailsReparation.addMouseListener(this); // Pour gérer les click sur la table
		}
		return jButtonNouvelleReparation;
	}

	/**
	 * This method initializes jButtonModifierReparation
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonModifierReparation() {
		if (jButtonModifierReparation == null) {
			jButtonModifierReparation = new JButton();
			jButtonModifierReparation.setBounds(new Rectangle(15, 45, 106, 16));
			jButtonModifierReparation.setMnemonic(KeyEvent.VK_M);
			jButtonModifierReparation.setText("Modifier");
			jButtonModifierReparation.setToolTipText("Cliquer sur ce bouton pour modifier les données d\'une réparation existante");
			jButtonModifierReparation.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Initialise la saisie
					Reparation reparationSelectione = (Reparation) gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow());
					setReparationTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
					setReparationEditable(true); // Rends la saisie possible

					// Rends les boutons présentement innutils innactifs
					jButtonNouvelleReparation.setEnabled(false);
					jButtonModifierReparation.setEnabled(false);

					// Permet d'annuler l'action en cours
					jButtonAnnulerReparation.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelreparationDetails.getRootPane().setDefaultButton(jButtonAnnulerReparation);

					// Empêche la séléction d'une autre réparation
					jTableDetailsReparation.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionR = "Modifier";

					jTextFieldReparationTechnicien.requestFocus();

					setOngletsSelectable(false);
				}
			});
		}
		return jButtonModifierReparation;
	}

	/**
	 * This method initializes jButtonEnregistrerReparation
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonEnregistrerReparation() {
		if (jButtonEnregistrerReparation == null) {
			jButtonEnregistrerReparation = new JButton();
			jButtonEnregistrerReparation.setBounds(new Rectangle(150, 45, 106, 16));
			jButtonEnregistrerReparation.setToolTipText("Cliquer sur ce bouton pour enregister les données d\'une réparation après un ajout ou une modification");
			jButtonEnregistrerReparation.setMnemonic(KeyEvent.VK_E);
			jButtonEnregistrerReparation.setText("Enregistrer");
			jButtonEnregistrerReparation.setEnabled(false);
			jButtonEnregistrerReparation.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					String dateReparation = "";

					// Récupère les détails du client sélectionné
					Client clientSelectione = (Client) gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());
					// Récupère et affiche les détails de l'appareil séléctionné
					Appareil appareilSelectione = (Appareil) gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow());

					if (getCalendrierReparationDate().getDate() != null) {
						dateReparation = Utilitaire.getSQLDate(getCalendrierReparationDate().getDate());
					}

					if (actionR.equalsIgnoreCase("Ajouter")) {
						// Ajoute la réparation à la base de donnée (Avec contrôle de la saisie)
						gestionnaireReparation.ajoute(new Reparation(dateReparation, "", jTextExtensReparationPanne.getText(), jTextExtensReparationTravailEffectue.getText(), jTextFieldReparationTechnicien.getText(), jTextFieldReparationPrix.getText(), jCheckBoxReparationPaye.isSelected(), jTextFieldReparationNoFacture.getText(), Integer.toString(appareilSelectione.id), Integer.toString(clientSelectione.id), null));

						jPanelreparationDetails.getRootPane().setDefaultButton(jButtonNouvelleReparation);
						jButtonNouvelleReparation.requestFocus();
						String titreInformation = "Ajout réussi.";
						String information = "La réparation sur l'appareil " + appareilSelectione.marque +
										" " + appareilSelectione.type + "\n" +
										"a bien été ajoutée dans la base de données";
						JOptionPane.showMessageDialog(getJFrame(), information, titreInformation,
										JOptionPane.INFORMATION_MESSAGE);
					} else if (actionR.equalsIgnoreCase("Modifier")) {
						// Récupère les détails de la réparation sélectionné
						Reparation reparationSelectione = (Reparation) gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow());

						// Modifie la réparation saisie dans la base de donnée (Avec contrôle de la saisie)
						gestionnaireReparation.modifie(new Reparation(dateReparation, "", jTextExtensReparationPanne.getText(), jTextExtensReparationTravailEffectue.getText(), jTextFieldReparationTechnicien.getText(), jTextFieldReparationPrix.getText(), jCheckBoxReparationPaye.isSelected(), jTextFieldReparationNoFacture.getText(), Integer.toString(appareilSelectione.id), Integer.toString(clientSelectione.id), reparationSelectione.id));

						jPanelreparationDetails.getRootPane().setDefaultButton(jButtonModifierReparation);
						jButtonModifierReparation.requestFocus();
					}
					// L'action a été effectuée
					actionR = "";

					// Désactive les actions devenues impossibles
					jButtonEnregistrerReparation.setEnabled(false);
					jButtonAnnulerReparation.setEnabled(false);

					setReparationTexte("", "", "", "", "", "", false); // Réinitialise la saisie de la réparation

					// Autorise la séléction d'une autre réparation
					jTableDetailsReparation.setEnabled(true);

					// La réparation n'est plus éditable
					setReparationEditable(false);

					actualiseTableReparationsAppareil();
					actualiseTableReparationsClientAEncaisser();
					actualiseTableReparationsTTAEncaisser();

					// Autorise la séléction d'une autre réparation
					jTableDetailsReparation.setEnabled(true);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonEnregistrerReparation;
	}

	/**
	 * This method initializes jTextExtensReparationPanne
	 *
	 * @return TextExtens
	 */
	private TextExtens getJTextExtensReparationPanne() {
		if (jTextExtensReparationPanne == null) {
			jTextExtensReparationPanne = new TextExtens(136, 200);
			jTextExtensReparationPanne.setLocation(new Point(150, 60));
			jTextExtensReparationPanne.setSize(new Dimension(136, 19));
		}
		return jTextExtensReparationPanne;
	}

	/**
	 * This method initializes jTextExtensReparationTravailEffectue
	 *
	 * @return TextExtens
	 */
	private TextExtens getJTextExtensReparationTravailEffectue() {
		if (jTextExtensReparationTravailEffectue == null) {
			jTextExtensReparationTravailEffectue = new TextExtens(136, 200);
			jTextExtensReparationTravailEffectue.setBounds(new Rectangle(150, 90, 136, 19));
		}
		return jTextExtensReparationTravailEffectue;
	}

	/**
	 * This method initializes jTextFieldReparationPrix
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldReparationPrix() {
		if (jTextFieldReparationPrix == null) {
			jTextFieldReparationPrix = new JTextField();
			jTextFieldReparationPrix.setBounds(new Rectangle(150, 150, 136, 16));
			jTextFieldReparationPrix.setDocument(modeleChamps.obtFloat(jTextFieldReparationPrix));
		}
		return jTextFieldReparationPrix;
	}

	/**
	 * This method initializes jCheckBoxReparationPaye
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getJCheckBoxReparationPaye() {
		if (jCheckBoxReparationPaye == null) {
			jCheckBoxReparationPaye = new JCheckBox();
			jCheckBoxReparationPaye.setBounds(new Rectangle(150, 210, 136, 16));
		}
		return jCheckBoxReparationPaye;
	}

	/**
	 * This method initializes calendrierReparationDate
	 *
	 * @return org.jdesktop.swingx.JXDatePicker
	 */
	private JXDatePicker getCalendrierReparationDate() {
		if (calendrierReparationDate == null) {
			calendrierReparationDate = new JXDatePicker();
			calendrierReparationDate.setLocation(new Point(150, 30));
			calendrierReparationDate.setFont(new Font("Dialog", Font.PLAIN, 11));
			calendrierReparationDate.setSize(new Dimension(136, 19));
			calendrierReparationDate.getEditor().setDisabledTextColor(Color.BLACK);
			// si l'utilisateur veut entrer les dates manuellement
			if (Definition.dateManuelle) {
				calendrierReparationDate.setFormats(Definition.formatDateManuelle);
			} else {
				calendrierReparationDate.setFormats(Definition.formatDateAutomatique);
			}

			calendrierReparationDate.getEditor().setEditable(false);

			calendrierReparationDate.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierReparationDate.getEditor().setEditable(true);
				}
			});
		}
		return calendrierReparationDate;
	}

	/**
	 * This method initializes jPanelListeClients
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelListeClients() {
		if (jPanelListeClients == null) {
			jPanelListeClients = new JPanel();
			jPanelListeClients.setLayout(null);
			jPanelListeClients.setBounds(new Rectangle(11, 12, 393, 416));
			jPanelListeClients.add(getJScrollPaneDetailsClient(), null);
			jPanelListeClients.add(getJButtonClientRechercher(), null);
			jPanelListeClients.add(getJButtonTtAfficherListeClient(), null);
		}
		return jPanelListeClients;
	}

	/**
	 * This method initializes jScrollPaneDetailsClient
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPaneDetailsClient() {
		if (jScrollPaneDetailsClient == null) {
			jScrollPaneDetailsClient = new JScrollPane();
			jScrollPaneDetailsClient.setBounds(new Rectangle(15, 30, 361, 346));
			jScrollPaneDetailsClient.setViewportView(getJXTableDetailsClient());
		}
		return jScrollPaneDetailsClient;
	}

	/**
	 * This method initializes jTableDetailsClient
	 *
	 * @return javax.swing.JXTable
	 */
	private JXTable getJXTableDetailsClient() {
		if (jTableDetailsClient == null) {
			jTableDetailsClient = new JXTable();
		}
		return jTableDetailsClient;
	}

	/**
	 * This method initializes jButtonClientRechercher
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonClientRechercher() {
		if (jButtonClientRechercher == null) {
			jButtonClientRechercher = new JButton();
			jButtonClientRechercher.setBounds(new Rectangle(30, 390, 136, 16));
			jButtonClientRechercher.setMnemonic(KeyEvent.VK_R);
			jButtonClientRechercher.setSelected(true);
			jButtonClientRechercher.setText("Rechercher");
			jButtonClientRechercher.setToolTipText("Cliquer sur ce bouton pour rechercher un client");
			jButtonClientRechercher.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					clientRecherche = new FenetreRechercheClient(modeleChamps).affiche(true);
					if (clientRecherche != null) {
						gestionnaireClient.recherche(clientRecherche);
						if (tabModelClient.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
						{
							jTableDetailsClient.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
						}
					}
					// actualise les listes
					actualiseTableClients();
					actualiseTableAppareilsClients();
					actualiseTableReparationsAppareil();
					actualiseTableReparationsClientAEncaisser();
				}
			});
		}
		return jButtonClientRechercher;
	}

	/**
	 * This method initializes jButtonTtAfficherListeClient
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonTtAfficherListeClient() {
		if (jButtonTtAfficherListeClient == null) {
			jButtonTtAfficherListeClient = new JButton();
			jButtonTtAfficherListeClient.setBounds(new Rectangle(225, 390, 136, 16));
			jButtonTtAfficherListeClient.setMnemonic(KeyEvent.VK_T);
			jButtonTtAfficherListeClient.setText("Tout afficher");
			jButtonTtAfficherListeClient.setToolTipText("Cliquer sur ce bouton pour afficher la liste intégralle des clients");
			jButtonTtAfficherListeClient.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					gestionnaireClient.recherche(new Client(null, "", "", "", "", "", "", "", ""));
					if (tabModelClient.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
					{
						jTableDetailsClient.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
					}
					// actualise les listes
					actualiseTableClients();
					actualiseTableAppareilsClients();
					actualiseTableReparationsAppareil();
					actualiseTableReparationsClientAEncaisser();
				}
			});
		}
		return jButtonTtAfficherListeClient;
	}

	/**
	 * This method initializes jPanelAEncaisserClient1
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelAEncaisserClient1() {
		if (jPanelAEncaisserClient1 == null) {
			jPanelAEncaisserClient1 = new JPanel();
			jPanelAEncaisserClient1.setLayout(new GridBagLayout());
			jPanelAEncaisserClient1.setBounds(new Rectangle(0, 0, 0, 0));
		}
		return jPanelAEncaisserClient1;
	}

	/**
	 * This method initializes jTitledPanelListeReparationAEncaisser
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelListeReparationAEncaisser() {
		if (jTitledPanelListeReparationAEncaisser == null) {
			jTitledPanelListeReparationAEncaisser = new JXTitledPanel("Liste des clients");
			jTitledPanelListeReparationAEncaisser.setBounds(new Rectangle(15, 15, 401, 451));
			jTitledPanelListeReparationAEncaisser.setBorder(new DropShadowBorder(true));
			jTitledPanelListeReparationAEncaisser.setTitleForeground(Color.white);
			jTitledPanelListeReparationAEncaisser.setTitle("Liste des réparations");
			jTitledPanelListeReparationAEncaisser.add(getJPanelListeReparationAEncaisser(), null);
		}
		return jTitledPanelListeReparationAEncaisser;
	}

	/**
	 * This method initializes jPanelListeReparationAEncaisser
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelListeReparationAEncaisser() {
		if (jPanelListeReparationAEncaisser == null) {
			jPanelListeReparationAEncaisser = new JPanel();
			jPanelListeReparationAEncaisser.setLayout(null);
			jPanelListeReparationAEncaisser.setBounds(new Rectangle(11, 12, 393, 416));
			jPanelListeReparationAEncaisser.add(getJScrollPaneReparationAEncaisser(), null);
			jPanelListeReparationAEncaisser.add(getJButtonReparationAEncaisserRechercher(), null);
			jPanelListeReparationAEncaisser.add(getJButtonTtAfficherListeReparationAEncaisser(), null);
		}
		return jPanelListeReparationAEncaisser;
	}

	/**
	 * This method initializes jScrollPaneReparationAEncaisser
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPaneReparationAEncaisser() {
		if (jScrollPaneReparationAEncaisser == null) {
			jScrollPaneReparationAEncaisser = new JScrollPane();
			jScrollPaneReparationAEncaisser.setBounds(new Rectangle(15, 30, 361, 346));
			jScrollPaneReparationAEncaisser.setViewportView(getJXTableDetailsReparationAEncaisser());
		}
		return jScrollPaneReparationAEncaisser;
	}

	/**
	 * This method initializes jTableDetailsReparationAEncaisser
	 *
	 * @return javax.swing.JXTable
	 */
	private JXTable getJXTableDetailsReparationAEncaisser() {
		if (jTableDetailsReparationAEncaisser == null) {
			jTableDetailsReparationAEncaisser = new JXTable();
		}
		return jTableDetailsReparationAEncaisser;
	}

	/**
	 * This method initializes jButtonReparationAEncaisserRechercher
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonReparationAEncaisserRechercher() {
		if (jButtonReparationAEncaisserRechercher == null) {
			jButtonReparationAEncaisserRechercher = new JButton();
			jButtonReparationAEncaisserRechercher.setBounds(new Rectangle(30, 390, 136, 16));
			jButtonReparationAEncaisserRechercher.setMnemonic(KeyEvent.VK_R);
			jButtonReparationAEncaisserRechercher.setSelected(true);
			jButtonReparationAEncaisserRechercher.setText("Rechercher");
			jButtonReparationAEncaisserRechercher.setToolTipText("Cliquer sur ce bouton pour rechercher une réparation non payée");
			jButtonReparationAEncaisserRechercher.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					reparationRecherchee = new FenetreRechercheReparation(true).affiche(true);
					if (reparationRecherchee != null) {
						if (tabModelClient.getRowCount() != 0) { // Si on peut séléctionner un client
							reparationRecherchee.proprietaire = (gestionnaireClient.getDetail(getJXTableDetailsClient().getSelectedRow())).id.toString();
							gestionnaireReparationAEncaisser.recherche(reparationRecherchee);
							if (tabModelReparationAEncaisser.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
							{
								jTableDetailsReparationAEncaisser.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
							}
						}
						actualiseTableReparationsClientAEncaisser();
					}
				}
			});
		}
		return jButtonReparationAEncaisserRechercher;
	}

	/**
	 * This method initializes jButtonTtAfficherListeReparationAEncaisser
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonTtAfficherListeReparationAEncaisser() {
		if (jButtonTtAfficherListeReparationAEncaisser == null) {
			jButtonTtAfficherListeReparationAEncaisser = new JButton();
			jButtonTtAfficherListeReparationAEncaisser.setBounds(new Rectangle(225, 390, 136, 16));
			jButtonTtAfficherListeReparationAEncaisser.setMnemonic(KeyEvent.VK_T);
			jButtonTtAfficherListeReparationAEncaisser.setText("Tout afficher");
			jButtonTtAfficherListeReparationAEncaisser.setToolTipText("Cliquer sur ce bouton pour afficher la liste intégralle des réparations non payées");
			jButtonTtAfficherListeReparationAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (getJXTableDetailsClient().getSelectedRow() >= 0) {
						String proprietaireID = (gestionnaireClient.getDetail(getJXTableDetailsClient().getSelectedRow())).id.toString();

						gestionnaireReparationAEncaisser.recherche(new Reparation(null, null, null, null, null, null, false, null, null, proprietaireID, null));
					}
					if (tabModelReparationAEncaisser.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
					{
						jTableDetailsReparationAEncaisser.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
					}
					actualiseTableReparationsClientAEncaisser();

				}
			});
		}
		return jButtonTtAfficherListeReparationAEncaisser;
	}

	/**
	 * This method initializes jTitledPanelReparationDetailsAEncaisser
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelReparationDetailsAEncaisser() {
		if (jTitledPanelReparationDetailsAEncaisser == null) {
			jTitledPanelReparationDetailsAEncaisser = new JXTitledPanel("Détails");
			jTitledPanelReparationDetailsAEncaisser.setBounds(new Rectangle(450, 15, 311, 451));
			jTitledPanelReparationDetailsAEncaisser.setBorder(new DropShadowBorder(true));
			jTitledPanelReparationDetailsAEncaisser.setTitleForeground(Color.white);
			jTitledPanelReparationDetailsAEncaisser.add(getJPanelreparationDetailsAEncaisser(), null);
		}
		return jTitledPanelReparationDetailsAEncaisser;
	}

	/**
	 * This method initializes jPanelreparationDetails1
	 *
	 * @return javax.swing.JLayeredPane
	 */
	private JLayeredPane getJPanelreparationDetailsAEncaisser() {
		if (jPanelreparationDetails1 == null) {
			jLabelReparationAEncaisserNoFacture = new JLabel();
			jLabelReparationAEncaisserNoFacture.setBounds(new Rectangle(15, 180, 121, 16));
			jLabelReparationAEncaisserNoFacture.setText("N° de facture");
			jLabelReparationAEncaisserPaye = new JLabel();
			jLabelReparationAEncaisserPaye.setBounds(new Rectangle(15, 210, 121, 16));
			jLabelReparationAEncaisserPaye.setText("*Payé :");
			jLabelReparationPrixAEncaisser = new JLabel();
			jLabelReparationPrixAEncaisser.setBounds(new Rectangle(15, 150, 121, 16));
			jLabelReparationPrixAEncaisser.setText("*Prix :");
			jLabelReparationTechnicienAEncaisser = new JLabel();
			jLabelReparationTechnicienAEncaisser.setBounds(new Rectangle(15, 120, 121, 16));
			jLabelReparationTechnicienAEncaisser.setText("*Technicien :");
			jLabelReparationTravailEffAEncaisser = new JLabel();
			jLabelReparationTravailEffAEncaisser.setBounds(new Rectangle(15, 90, 121, 16));
			jLabelReparationTravailEffAEncaisser.setText("*Travail effectué :");
			jLabelReparationPanneAEncaisser = new JLabel();
			jLabelReparationPanneAEncaisser.setBounds(new Rectangle(15, 60, 121, 16));
			jLabelReparationPanneAEncaisser.setText("*Panne :");
			jLabelReparationDateAEncaisser = new JLabel();
			jLabelReparationDateAEncaisser.setBounds(new Rectangle(15, 30, 130, 16));
			jLabelReparationDateAEncaisser.setText("*Date de réparation :");
			jPanelreparationDetails1 = new JLayeredPane();
			jPanelreparationDetails1.setLayout(null);
			jPanelreparationDetails1.setBounds(new Rectangle(450, 15, 301, 421));
			jPanelreparationDetails1.setName("");
			jPanelreparationDetails1.add(jLabelReparationDateAEncaisser, null);
			jPanelreparationDetails1.add(jLabelReparationPanneAEncaisser, null);
			jPanelreparationDetails1.add(jLabelReparationTravailEffAEncaisser, null);
			jPanelreparationDetails1.add(jLabelReparationTechnicienAEncaisser, null);
			jPanelreparationDetails1.add(jLabelReparationPrixAEncaisser, null);
			jPanelreparationDetails1.add(getJTextFieldReparationTechnicienAEncaisser(), null);
			jPanelreparationDetails1.add(getJPanelBoutonsClientAEncaisser(), null);
			jPanelreparationDetails1.add(jLabelReparationAEncaisserPaye, null);
			jPanelreparationDetails1.add(getJTextExtensReparationPanne1(), null);
			jPanelreparationDetails1.add(getJTextExtensReparationTravailEffectue1(), null);
			jPanelreparationDetails1.add(getJTextFieldReparationPrix1(), null);
			jPanelreparationDetails1.add(getJCheckBoxReparationPaye1(), null);
			jPanelreparationDetails1.add(getCalendrierAEncaisserDate(), null);
			jPanelreparationDetails1.add(jLabelReparationAEncaisserNoFacture, null);
			jPanelreparationDetails1.add(getJTextFieldReparationAEncaisserNoFacture(), null);
			jPanelreparationDetails1.setLayer(getJTextExtensReparationPanne1(), 200);
			jPanelreparationDetails1.setLayer(getJTextExtensReparationTravailEffectue1(), 200);
		}
		return jPanelreparationDetails1;
	}

	/**
	 * This method initializes jTextFieldReparationTechnicien1
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldReparationTechnicienAEncaisser() {
		if (jTextFieldReparationTechnicienAEncaisser == null) {
			jTextFieldReparationTechnicienAEncaisser = new JTextField();
			jTextFieldReparationTechnicienAEncaisser.setBounds(new Rectangle(150, 120, 136, 16));
		}
		return jTextFieldReparationTechnicienAEncaisser;
	}

	/**
	 * This method initializes jPanelBoutonsClientStock11
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelBoutonsClientAEncaisser() {
		if (jPanelBoutonsClientStock11 == null) {
			jPanelBoutonsClientStock11 = new JPanel();
			jPanelBoutonsClientStock11.setLayout(null);
			jPanelBoutonsClientStock11.setBounds(new Rectangle(15, 300, 271, 106));
			jPanelBoutonsClientStock11.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			jPanelBoutonsClientStock11.add(getJButtonAnnulerReparationAEncaisser(), null);
			jPanelBoutonsClientStock11.add(getJButtonSupprimerReparationAEncaisser(), null);
			jPanelBoutonsClientStock11.add(getJButtonNouvelleReparationAEncaisser(), null);
			jPanelBoutonsClientStock11.add(getJButtonModifierReparationAEncaisser(), null);
			jPanelBoutonsClientStock11.add(getJButtonEnregistrerReparationAEncaisser(), null);
		}
		return jPanelBoutonsClientStock11;
	}

	/**
	 * This method initializes jButtonAnnulerReparation1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAnnulerReparationAEncaisser() {
		if (jButtonAnnulerReparationAEncaisser == null) {
			jButtonAnnulerReparationAEncaisser = new JButton();
			jButtonAnnulerReparationAEncaisser.setBounds(new Rectangle(60, 75, 136, 16));
			jButtonAnnulerReparationAEncaisser.setToolTipText("Cliquer sur ce bouton pour " + "annuler la modification en cours");
			jButtonAnnulerReparationAEncaisser.setMnemonic(KeyEvent.VK_A);
			jButtonAnnulerReparationAEncaisser.setSelected(true);
			jButtonAnnulerReparationAEncaisser.setText("Annuler");
			jButtonAnnulerReparationAEncaisser.setEnabled(false);
			jButtonAnnulerReparationAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (tabModelReparationAEncaisser.getRowCount() != 0) {
						jButtonModifierReparationAEncaisser.setEnabled(true);
						jButtonSupprimerReparationAEncaisser.setEnabled(true);
					}

					if (actionRA.equalsIgnoreCase("modifier")) {
						jPanelAEncaisserClient.getRootPane().setDefaultButton(jButtonModifierReparationAEncaisser);
						jButtonModifierReparationAEncaisser.requestFocus();
					}

					jButtonEnregistrerReparationAEncaisser.setEnabled(false);
					jButtonAnnulerReparationAEncaisser.setEnabled(false);
					setReparationAEncaisserTexte("", "", "", "", "", "", false); // Réinitialise la saisie de l'appareil

					if (tabModelReparationAEncaisser.getRowCount() != 0) {
						// Récupère les détails de la réparation sélectionné
						Reparation reparationSelectione = (Reparation) gestionnaireReparationAEncaisser.getDetail(jTableDetailsReparationAEncaisser.getSelectedRow());

						// Affiche les détails de la réparation
						setReparationAEncaisserTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
					}

					// Autorise la séléction d'une autre réparation
					jTableDetailsReparationAEncaisser.setEnabled(true);

					// La réparation n'est plus éditable
					setReparationAEncaisserEditable(false);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonAnnulerReparationAEncaisser;
	}

	/**
	 * This method initializes jButtonSupprimerReparation1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonSupprimerReparationAEncaisser() {
		if (jButtonSupprimerReparationAEncaisser == null) {
			jButtonSupprimerReparationAEncaisser = new JButton();
			jButtonSupprimerReparationAEncaisser.setBounds(new Rectangle(150, 15, 106, 16));
			jButtonSupprimerReparationAEncaisser.setMnemonic(KeyEvent.VK_S);
			jButtonSupprimerReparationAEncaisser.setText("Supprimer");
			jButtonSupprimerReparationAEncaisser.setToolTipText("Cliquer sur ce bouton pour supprimer les données d\'une réparation existante");
			jButtonSupprimerReparationAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
									null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
						gestionnaireReparationAEncaisser.supprime(gestionnaireReparationAEncaisser.getDetail(jTableDetailsReparationAEncaisser.getSelectedRow()).id);
						setReparationAEncaisserTexte("", "", "", "", "", "", false); // L'appareil n'existe plus...
						actualiseTableReparationsTTAEncaisser();
						actualiseTableReparationsClientAEncaisser();
						actualiseTableReparationsTTAEncaisser();

						if (tabModelReparationAEncaisser.getRowCount() != 0) {
							jTableDetailsReparationAEncaisser.setRowSelectionInterval(0, 0); // Séléctionne la première réparation par défaut

							// Récupère les détails de la réparation sélectionné
							Reparation reparationSelectione = (Reparation) gestionnaireReparationAEncaisser.getDetail(jTableDetailsReparationAEncaisser.getSelectedRow());

							// Affiche les détails de la réparation
							setReparationAEncaisserTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
						} else {
							jButtonSupprimerReparationAEncaisser.setEnabled(false);
						}
					}
				}
			});
		}
		return jButtonSupprimerReparationAEncaisser;
	}

	/**
	 * This method initializes jButtonNouvelleReparation1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonNouvelleReparationAEncaisser() {
		if (jButtonNouvelleReparationAEncaisser == null) {
			jButtonNouvelleReparationAEncaisser = new JButton();
			jButtonNouvelleReparationAEncaisser.setBounds(new Rectangle(15, 15, 106, 16));
			jButtonNouvelleReparationAEncaisser.setToolTipText("Cliquer sur ce bouton pour ajouter un nouveau client");
			jButtonNouvelleReparationAEncaisser.setMnemonic(KeyEvent.VK_N);
			jButtonNouvelleReparationAEncaisser.setText("Nouveau");
			jButtonNouvelleReparationAEncaisser.setNextFocusableComponent(getJButtonNouvelleReparationAEncaisser());
			jButtonNouvelleReparationAEncaisser.setVisible(false);
		}
		return jButtonNouvelleReparationAEncaisser;
	}

	/**
	 * This method initializes jButtonModifierReparation1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonModifierReparationAEncaisser() {
		if (jButtonModifierReparationAEncaisser == null) {
			jButtonModifierReparationAEncaisser = new JButton();
			jButtonModifierReparationAEncaisser.setBounds(new Rectangle(15, 45, 106, 16));
			jButtonModifierReparationAEncaisser.setMnemonic(KeyEvent.VK_M);
			jButtonModifierReparationAEncaisser.setText("Modifier");
			jButtonModifierReparationAEncaisser.setToolTipText("Cliquer sur ce bouton pour modifier les données d\'une réparation existante");
			jButtonModifierReparationAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Initialise la saisie
					Reparation reparationSelectione = (Reparation) gestionnaireReparationAEncaisser.getDetail(jTableDetailsReparationAEncaisser.getSelectedRow());
					setReparationAEncaisserTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
					setReparationAEncaisserEditable(true); // Rends la saisie possible

					// Rends les boutons présentement innutils innactifs
					jButtonModifierReparationAEncaisser.setEnabled(false);

					// Permet d'annuler l'action en cours
					jButtonAnnulerReparationAEncaisser.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelAEncaisserClient.getRootPane().setDefaultButton(jButtonAnnulerReparationAEncaisser);

					// Empêche la séléction d'une autre réparation
					jTableDetailsReparationAEncaisser.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionRA = "Modifier";

					jTextFieldReparationTechnicienAEncaisser.requestFocus();

					setOngletsSelectable(false);
				}
			});
		}
		return jButtonModifierReparationAEncaisser;
	}

	/**
	 * This method initializes jButtonEnregistrerReparation1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonEnregistrerReparationAEncaisser() {
		if (jButtonEnregistrerReparationAEncaisser == null) {
			jButtonEnregistrerReparationAEncaisser = new JButton();
			jButtonEnregistrerReparationAEncaisser.setBounds(new Rectangle(150, 45, 106, 16));
			jButtonEnregistrerReparationAEncaisser.setToolTipText("Cliquer sur ce bouton pour enregister les données d\'une réparation après une modification");
			jButtonEnregistrerReparationAEncaisser.setMnemonic(KeyEvent.VK_E);
			jButtonEnregistrerReparationAEncaisser.setText("Enregistrer");
			jButtonEnregistrerReparationAEncaisser.setEnabled(false);
			jButtonEnregistrerReparationAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					String dateReparation = "";

					// Récupère les détails du client sélectionné
					Client clientSelectione = (Client) gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());

					if (calendrierAEncaisserDate.getDate() != null) {
						dateReparation = Utilitaire.getSQLDate(calendrierAEncaisserDate.getDate());
					}

					if (actionRA.equalsIgnoreCase("Modifier")) {
						// Récupère les détails de la réparation sélectionné
						Reparation reparationSelectione = (Reparation) gestionnaireReparationAEncaisser.getDetail(jTableDetailsReparationAEncaisser.getSelectedRow());

						// Modifie la réparation saisie dans la base de donnée (Avec contrôle de la saisie)
						gestionnaireReparationAEncaisser.modifie(new Reparation(dateReparation, "", jTextExtensReparationPanne1.getText(), jTextExtensReparationTravailEffectue1.getText(), jTextFieldReparationTechnicienAEncaisser.getText(), jTextFieldReparationPrix1.getText(), jCheckBoxReparationPaye1.isSelected(), jTextFieldReparationAEncaisserNoFacture.getText(), reparationSelectione.appareil, reparationSelectione.proprietaire, reparationSelectione.id));

						jPanelreparationDetails1.getRootPane().setDefaultButton(jButtonModifierReparationAEncaisser);
						jButtonModifierReparationAEncaisser.requestFocus();
					}
					// L'action a été effectuée
					actionRA = "";

					// Désactive les actions devenues impossibles
					jButtonEnregistrerReparationAEncaisser.setEnabled(false);
					jButtonAnnulerReparationAEncaisser.setEnabled(false);

					setReparationAEncaisserTexte("", "", "", "", "", "", false); // Réinitialise la saisie de la réparation

					// Autorise la séléction d'une autre réparation
					jTableDetailsReparationAEncaisser.setEnabled(true);

					// La réparation n'est plus éditable
					setReparationAEncaisserEditable(false);

					actualiseTableReparationsClientAEncaisser();
					actualiseTableReparationsTTAEncaisser();
					actualiseTableReparationsClientAEncaisser();

					// Autorise la séléction d'une autre réparation
					jTableDetailsReparationAEncaisser.setEnabled(true);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonEnregistrerReparationAEncaisser;
	}

	/**
	 * This method initializes jTextExtensReparationPanne1
	 *
	 * @return TextExtens
	 */
	private TextExtens getJTextExtensReparationPanne1() {
		if (jTextExtensReparationPanne1 == null) {
			jTextExtensReparationPanne1 = new TextExtens(136, 200);
			jTextExtensReparationPanne1.setLocation(new Point(150, 60));
			jTextExtensReparationPanne1.setSize(new Dimension(136, 19));
		}
		return jTextExtensReparationPanne1;
	}

	/**
	 * This method initializes jTextExtensReparationTravailEffectue1
	 *
	 * @return TextExtens
	 */
	private TextExtens getJTextExtensReparationTravailEffectue1() {
		if (jTextExtensReparationTravailEffectue1 == null) {
			jTextExtensReparationTravailEffectue1 = new TextExtens(136, 200);
			jTextExtensReparationTravailEffectue1.setBounds(new Rectangle(150, 90, 136, 19));
		}
		return jTextExtensReparationTravailEffectue1;
	}

	/**
	 * This method initializes jTextFieldReparationPrix1
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldReparationPrix1() {
		if (jTextFieldReparationPrix1 == null) {
			jTextFieldReparationPrix1 = new JTextField();
			jTextFieldReparationPrix1.setBounds(new Rectangle(150, 150, 136, 16));
		}
		return jTextFieldReparationPrix1;
	}

	/**
	 * This method initializes jCheckBoxReparationPaye1
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getJCheckBoxReparationPaye1() {
		if (jCheckBoxReparationPaye1 == null) {
			jCheckBoxReparationPaye1 = new JCheckBox();
			jCheckBoxReparationPaye1.setBounds(new Rectangle(150, 210, 136, 16));
		}
		return jCheckBoxReparationPaye1;
	}

	/**
	 * This method initializes calendrierAEncaisserDate
	 *
	 * @return org.jdesktop.swingx.JXDatePicker
	 */
	private JXDatePicker getCalendrierAEncaisserDate() {
		if (calendrierAEncaisserDate == null) {
			calendrierAEncaisserDate = new JXDatePicker();
			calendrierAEncaisserDate.setLocation(new Point(150, 30));
			calendrierAEncaisserDate.setFont(new Font("Dialog", Font.PLAIN, 11));
			calendrierAEncaisserDate.setSize(new Dimension(136, 19));
			calendrierAEncaisserDate.getEditor().setDisabledTextColor(Color.BLACK);
			// si l'utilisateur veut entrer les dates manuellement
			if (Definition.dateManuelle) {
				calendrierAEncaisserDate.setFormats(Definition.formatDateManuelle);
			} else {
				calendrierAEncaisserDate.setFormats(Definition.formatDateAutomatique);
			}
			calendrierAEncaisserDate.getEditor().setEditable(false);

			calendrierAEncaisserDate.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierAEncaisserDate.getEditor().setEditable(true);
				}
			});
		}
		return calendrierAEncaisserDate;
	}

	/**
	 * This method initializes jComboBoxAppareilGenre
	 *
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxAppareilGenre() {
		if (jComboBoxAppareilGenre == null) {
			// si la liste a pu être chargée
			if (listeGenreAppareil != null) {
				jComboBoxAppareilGenre = new JComboBox(listeGenreAppareil.toArray());
				// doit être défini éditbale avant dêtre décorée sinon
				// l'auto-complémentation est stricte
				jComboBoxAppareilGenre.setEditable(false);
				// pour ajouter l'auto-complémentation
				AutoCompleteDecorator.decorate(jComboBoxAppareilGenre);
				jComboBoxAppareilGenre.setSelectedIndex(-1);
			} else {
				jComboBoxAppareilGenre = new JComboBox();
				jComboBoxAppareilGenre.setEditable(false);
			}
			jComboBoxAppareilGenre.setLocation(new Point(135, 30));
			jComboBoxAppareilGenre.setSize(new Dimension(151, 16));

			jComboBoxAppareilGenre.addItemListener(new ItemListener() {

				public void itemStateChanged(ItemEvent e) {
					try {
						jComboBoxAppareilGenre.setEditable(true);
					} catch (Exception ex) {
					}
				}
			});
			jComboBoxAppareilGenre.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					jComboBoxAppareilGenre.setEditable(false);

					try {
						if (jTextFieldAppareilType.isEditable()) {
							verifChampsAppareil();
						}
					} catch (Exception ex) {
					}
				}
			});
		}
		return jComboBoxAppareilGenre;
	}

	/**
	 * This method initializes jComboBoxAppareilMarque
	 *
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxAppareilMarque() {
		if (jComboBoxAppareilMarque == null) {
			// si la liste a pu être chargée
			if (listeMarqueAppareil != null) {
				jComboBoxAppareilMarque = new JComboBox(listeMarqueAppareil.toArray());
				// doit être défini éditbale ou non avant dêtre décorée sinon
				// l'auto-complémentation est stricte
				jComboBoxAppareilMarque.setEditable(false);
				// pour ajouter l'auto-complémentation
				AutoCompleteDecorator.decorate(jComboBoxAppareilMarque);
				jComboBoxAppareilMarque.setSelectedIndex(-1);
			} else {
				jComboBoxAppareilMarque = new JComboBox();
				jComboBoxAppareilMarque.setEditable(false);
			}

			jComboBoxAppareilMarque.setBounds(new Rectangle(135, 60, 151, 16));

			jComboBoxAppareilMarque.addItemListener(new ItemListener() {

				public void itemStateChanged(ItemEvent e) {
					try {
						jComboBoxAppareilMarque.setEditable(true);
					} catch (Exception ex) {
					}
				}
			});
			jComboBoxAppareilMarque.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					jComboBoxAppareilMarque.setEditable(false);

					try {
						if (jTextFieldAppareilType.isEditable()) {
							verifChampsAppareil();
						}
					} catch (Exception ex) {
					}
				}
			});
		}
		return jComboBoxAppareilMarque;
	}

	/**
	 * This method initializes jComboBoxAppareilMarque
	 *
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxAppareilStatus() {
		// si la liste a pu être chargée
		if (jComboBoxAppareilStatus != null) {
			jComboBoxAppareilStatus = new JComboBox(status);
			// doit être défini éditbale ou non avant dêtre décorée sinon
			// l'auto-complémentation est stricte
			jComboBoxAppareilStatus.setEditable(false);
			// pour ajouter l'auto-complémentation
			AutoCompleteDecorator.decorate(jComboBoxAppareilStatus);
			jComboBoxAppareilStatus.setSelectedIndex(-1);

			jComboBoxAppareilStatus.addItemListener(new ItemListener() {

				public void itemStateChanged(ItemEvent e) {
					try {
						jComboBoxAppareilStatus.setEditable(true);
					} catch (Exception ex) {
					}
				}
			});
			jComboBoxAppareilStatus.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					jComboBoxAppareilStatus.setEditable(false);

					try {
						if (jComboBoxAppareilStatus.isEditable()) {
							verifChampsAppareil();
						}
					} catch (Exception ex) {
					}
				}
			});
		}
		return jComboBoxAppareilMarque;
	}

	/**
	 * This method initializes jTitledPanelListeAppareilStock
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelListeAppareilStock() {
		if (jTitledPanelListeAppareilStock == null) {
			jTitledPanelListeAppareilStock = new JXTitledPanel("Liste des appareils en stock");
			jTitledPanelListeAppareilStock.setBounds(new Rectangle(15, 15, 387, 496));
			jTitledPanelListeAppareilStock.setBorder(new DropShadowBorder(true));
			jTitledPanelListeAppareilStock.setTitleForeground(Color.white);
			jTitledPanelListeAppareilStock.add(getJPanelListeAppareilStock(), null);
		}
		return jTitledPanelListeAppareilStock;
	}

	/**
	 * This method initializes jPanelListeAppareilStock
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelListeAppareilStock() {
		if (jPanelListeAppareilStock == null) {
			jPanelListeAppareilStock = new JPanel();
			jPanelListeAppareilStock.setLayout(null);
			jPanelListeAppareilStock.setBounds(new Rectangle(15, 15, 373, 421));
			jPanelListeAppareilStock.add(getJScrollPaneListeAppareilsStock(), null);
			jPanelListeAppareilStock.add(getJButtonRechercherAppareilStock(), null);
			jPanelListeAppareilStock.add(getJButtonTtAfficherListeAppareilStock(), null);
		}
		return jPanelListeAppareilStock;
	}

	/**
	 * This method initializes jScrollPaneDetailsAppareils1
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPaneListeAppareilsStock() {
		if (jScrollPaneListeAppareilsStock == null) {
			jScrollPaneListeAppareilsStock = new JScrollPane();
			jScrollPaneListeAppareilsStock.setBounds(new Rectangle(15, 30, 346, 391));
			jScrollPaneListeAppareilsStock.setViewportView(getJXTableListeAppareilStock());
		}
		return jScrollPaneListeAppareilsStock;
	}

	/**
	 * This method initializes jTableDetailsAppareil1
	 *
	 * @return javax.swing.JXTable
	 */
	private JXTable getJXTableListeAppareilStock() {
		if (jTableDetailsAppareilStock == null) {
			jTableDetailsAppareilStock = new JXTable();
			// Initialise la table des appareils en stock
			jTableDetailsAppareilStock.addMouseListener(this); // Pour gérer les click sur la table
		}
		return jTableDetailsAppareilStock;
	}

	/**
	 * This method initializes jButtonAppareilRechercher1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonRechercherAppareilStock() {
		if (jButtonRechercherAppareilStock == null) {
			jButtonRechercherAppareilStock = new JButton();
			jButtonRechercherAppareilStock.setBounds(new Rectangle(30, 435, 136, 16));
			jButtonRechercherAppareilStock.setMnemonic(KeyEvent.VK_R);
			jButtonRechercherAppareilStock.setSelected(true);
			jButtonRechercherAppareilStock.setText("Rechercher");
			jButtonRechercherAppareilStock.setToolTipText("Cliquer sur ce bouton pour rechercher un appareil en stock");
			jButtonRechercherAppareilStock.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// ouvre la fenetre de recherche
					appareilRecherche = new FenetreRechercheAppareilStock(listeGenreAppareil,
									listeMarqueAppareil).affiche(true);

					if (appareilRecherche != null) {
						appareilRecherche.status = Integer.toString(0);
						gestionnaireAppareilsStock.recherche(appareilRecherche);
						if (tabModelAppareilsStock.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
						{
							jTableDetailsAppareilStock.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
						}
					}
					actualiseTableStock();

				}
			});
		}
		return jButtonRechercherAppareilStock;
	}

	/**
	 * This method initializes jButtonTtAfficherListeAppareil1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonTtAfficherListeAppareilStock() {
		if (jButtonTtAfficherListeAppareilStock == null) {
			jButtonTtAfficherListeAppareilStock = new JButton();
			jButtonTtAfficherListeAppareilStock.setBounds(new Rectangle(195, 435, 136, 16));
			jButtonTtAfficherListeAppareilStock.setMnemonic(KeyEvent.VK_T);
			jButtonTtAfficherListeAppareilStock.setText("Tout afficher");
			jButtonTtAfficherListeAppareilStock.setToolTipText("Cliquer sur ce bouton pour afficher la liste intégralle des appareils en stock");
			jButtonTtAfficherListeAppareilStock.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					String status = "0";
					gestionnaireAppareilsStock.recherche(new AppareilStock(null, "", "", "", "", "", "", "", "", "", "", null, status));
					if (tabModelAppareilsStock.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
					{
						jTableDetailsAppareilStock.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
					}
					actualiseTableStock();

				}
			});
		}
		return jButtonTtAfficherListeAppareilStock;
	}

	/**
	 * This method initializes jTitledPanelAppareilDetailsStock
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelAppareilDetailsStock() {
		if (jTitledPanelAppareilDetailsStock == null) {
			jTitledPanelAppareilDetailsStock = new JXTitledPanel("Détails");
			jTitledPanelAppareilDetailsStock.setBounds(new Rectangle(435, 15, 311, 496));
			jTitledPanelAppareilDetailsStock.setBorder(new DropShadowBorder(true));
			jTitledPanelAppareilDetailsStock.setTitleForeground(Color.white);
			jTitledPanelAppareilDetailsStock.add(getjPanelAppareilDetailsStock(), null);
		}
		return jTitledPanelAppareilDetailsStock;
	}

	/**
	 * This method initializes jPanelAppareilDetailsStock
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getjPanelAppareilDetailsStock() {
		if (jPanelAppareilDetailsStock == null) {
			jLabelAppareilNoFactureStock = new JLabel();
			jLabelAppareilNoFactureStock.setBounds(new Rectangle(15, 210, 105, 16));
			jLabelAppareilNoFactureStock.setText("N° de facture :");
			jLabelAppareilNoBulletinStock = new JLabel();
			jLabelAppareilNoBulletinStock.setBounds(new Rectangle(15, 240, 105, 16));
			jLabelAppareilNoBulletinStock.setText("N° bulletin :");
			jLabelAppareilReceptioneParStock = new JLabel();
			jLabelAppareilReceptioneParStock.setBounds(new Rectangle(15, 270, 110, 16));
			jLabelAppareilReceptioneParStock.setText("Réceptionné par :");
			jLabelAppareilPrixStock = new JLabel();
			jLabelAppareilPrixStock.setBounds(new Rectangle(15, 180, 106, 16));
			jLabelAppareilPrixStock.setText("Prix :");
			jLabelAppareilDateReceptionStock = new JLabel();
			jLabelAppareilDateReceptionStock.setBounds(new Rectangle(15, 150, 106, 16));
			jLabelAppareilDateReceptionStock.setText("Date d'arrivée :");
			jLabelAppareilNoSerieStock = new JLabel();
			jLabelAppareilNoSerieStock.setBounds(new Rectangle(15, 120, 106, 16));
			jLabelAppareilNoSerieStock.setText("N° de série:");
			jLabelAppareilTypeStock = new JLabel();
			jLabelAppareilTypeStock.setBounds(new Rectangle(15, 90, 106, 16));
			jLabelAppareilTypeStock.setText("Type :");
			jLabelAppareilMarqueStock = new JLabel();
			jLabelAppareilMarqueStock.setBounds(new Rectangle(15, 60, 106, 16));
			jLabelAppareilMarqueStock.setText("*Marque :");
			jLabelAppareilGenreStock = new JLabel();
			jLabelAppareilGenreStock.setBounds(new Rectangle(15, 30, 106, 16));
			jLabelAppareilGenreStock.setText("*Genre :");
			jLabelAppareilGenreStock.setLabelFor(jLabelAppareilGenreStock);
			jPanelAppareilDetailsStock = new JPanel();
			jPanelAppareilDetailsStock.setLayout(null);
			jPanelAppareilDetailsStock.setBounds(new Rectangle(450, 15, 301, 421));
			jPanelAppareilDetailsStock.setName("");
			jPanelAppareilDetailsStock.add(jLabelAppareilGenreStock, null);
			jPanelAppareilDetailsStock.add(jLabelAppareilMarqueStock, null);
			jPanelAppareilDetailsStock.add(jLabelAppareilTypeStock, null);
			jPanelAppareilDetailsStock.add(jLabelAppareilNoSerieStock, null);
			jPanelAppareilDetailsStock.add(jLabelAppareilDateReceptionStock, null);
			jPanelAppareilDetailsStock.add(jLabelAppareilPrixStock, null);
			jPanelAppareilDetailsStock.add(jLabelAppareilNoFactureStock, null);
			jPanelAppareilDetailsStock.add(jLabelAppareilNoBulletinStock, null);
			jPanelAppareilDetailsStock.add(jLabelAppareilReceptioneParStock, null);
			jPanelAppareilDetailsStock.add(getjTextFieldAppareilTypeStock(), null);
			jPanelAppareilDetailsStock.add(getjTextFieldAppareilNoSerieStock(), null);
			jPanelAppareilDetailsStock.add(getjTextFieldAppareilPrixStock(), null);
			jPanelAppareilDetailsStock.add(getjTextFieldAppareilNoFactureStock(), null);
			jPanelAppareilDetailsStock.add(getjTextFieldAppareilNoBulletinStock(), null);
			jPanelAppareilDetailsStock.add(getjTextFieldAppareilReceptionneParStock(), null);
			jPanelAppareilDetailsStock.add(getjPanelBoutonsClientStock2(), null);
			jPanelAppareilDetailsStock.add(getJComboBoxAppareilGenreStock(), null);
			jPanelAppareilDetailsStock.add(getJComboBoxAppareilMarqueStock(), null);
			jPanelAppareilDetailsStock.add(getJButtonAssignerAClient(), null);
			jPanelAppareilDetailsStock.add(getCalendrierStockDate(), null);

			// Gestion de la saisie de touches du clavier dans une des zone obligatoire d'un appareil
			saisieAppareilListener = new KeyAdapter() {

				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() != KeyEvent.VK_ENTER) {
						verifAppareilStock();
					}
				}
			};

			// Contrôle la saisie des champs obligatoires pour les appareils
			jTextFieldAppareilTypeStock.addKeyListener(saisieAppareilListener);
			jTextFieldAppareilNoSerieStock.addKeyListener(saisieAppareilListener);
			calendrierStockDate.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierStockDate.getEditor().setEditable(false);

					verifAppareilStock();
				}
			});
			jTextFieldAppareilPrixStock.addKeyListener(saisieAppareilListener);
			jTextFieldAppareilNoFactureStock.addKeyListener(saisieAppareilListener);
			jTextFieldAppareilNoBulletinStock.addKeyListener(saisieAppareilListener);
			jTextFieldAppareilReceptionneParStock.addKeyListener(saisieAppareilListener);

		}
		return jPanelAppareilDetailsStock;
	}

	/**
	 * This method initializes jTextFieldAppareilTypeStock
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getjTextFieldAppareilTypeStock() {
		if (jTextFieldAppareilTypeStock == null) {
			jTextFieldAppareilTypeStock = new JTextField();
			jTextFieldAppareilTypeStock.setBounds(new Rectangle(135, 90, 151, 16));
			jTextFieldAppareilTypeStock.setEditable(false);
		}
		return jTextFieldAppareilTypeStock;
	}

	/**
	 * This method initializes jTextFieldAppareilNoSerieStock
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getjTextFieldAppareilNoSerieStock() {
		if (jTextFieldAppareilNoSerieStock == null) {
			jTextFieldAppareilNoSerieStock = new JTextField();
			jTextFieldAppareilNoSerieStock.setBounds(new Rectangle(135, 120, 151, 16));
		}
		return jTextFieldAppareilNoSerieStock;
	}

	/**
	 * This method initializes jTextFieldAppareilPrixStock
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getjTextFieldAppareilPrixStock() {
		if (jTextFieldAppareilPrixStock == null) {
			jTextFieldAppareilPrixStock = new JTextField();
			jTextFieldAppareilPrixStock.setBounds(new Rectangle(135, 180, 151, 16));
			jTextFieldAppareilPrixStock.setDocument(modeleChamps.obtFloat(jTextFieldAppareilPrixStock));
		}
		return jTextFieldAppareilPrixStock;
	}

	/**
	 * This method initializes jTextFieldAppareilNoFactureStock
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getjTextFieldAppareilNoFactureStock() {
		if (jTextFieldAppareilNoFactureStock == null) {
			jTextFieldAppareilNoFactureStock = new JTextField();
			jTextFieldAppareilNoFactureStock.setBounds(new Rectangle(135, 210, 151, 16));
		}
		return jTextFieldAppareilNoFactureStock;
	}

	/**
	 * This method initializes jTextFieldAppareilNoBulletinStock
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getjTextFieldAppareilNoBulletinStock() {
		if (jTextFieldAppareilNoBulletinStock == null) {
			jTextFieldAppareilNoBulletinStock = new JTextField();
			jTextFieldAppareilNoBulletinStock.setBounds(new Rectangle(135, 240, 151, 16));
		}
		return jTextFieldAppareilNoBulletinStock;
	}

	/**
	 * This method initializes jTextFieldAppareilReceptionneParStock
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getjTextFieldAppareilReceptionneParStock() {
		if (jTextFieldAppareilReceptionneParStock == null) {
			jTextFieldAppareilReceptionneParStock = new JTextField();
			jTextFieldAppareilReceptionneParStock.setBounds(new Rectangle(135, 270, 151, 16));
		}
		return jTextFieldAppareilReceptionneParStock;
	}

	/**
	 * This method initializes jPanelBoutonsClientStock2
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getjPanelBoutonsClientStock2() {
		if (jPanelBoutonsClientStock2 == null) {
			jPanelBoutonsClientStock2 = new JPanel();
			jPanelBoutonsClientStock2.setLayout(null);
			jPanelBoutonsClientStock2.setBounds(new Rectangle(15, 330, 271, 109));
			jPanelBoutonsClientStock2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			jPanelBoutonsClientStock2.add(getJButtonAnnulerAppareilStock(), null);
			jPanelBoutonsClientStock2.add(getJButtonSupprimerAppareilStock(), null);
			jPanelBoutonsClientStock2.add(getJButtonNouvelAppareilStock(), null);
			jPanelBoutonsClientStock2.add(getJButtonModifierAppareilStock(), null);
			jPanelBoutonsClientStock2.add(getJButtonEnregistrerAppareilStock(), null);
		}
		return jPanelBoutonsClientStock2;
	}

	/**
	 * This method initializes jButtonAnnulerAppareil1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAnnulerAppareilStock() {
		if (jButtonAnnulerAppareilStock == null) {
			jButtonAnnulerAppareilStock = new JButton();
			jButtonAnnulerAppareilStock.setBounds(new Rectangle(60, 75, 136, 16));
			jButtonAnnulerAppareilStock.setToolTipText("Cliquer sur ce bouton pour " + "annuler l\'ajout ou la modification en cours");
			jButtonAnnulerAppareilStock.setMnemonic(KeyEvent.VK_A);
			jButtonAnnulerAppareilStock.setSelected(true);
			jButtonAnnulerAppareilStock.setText("Annuler");
			jButtonAnnulerAppareilStock.setEnabled(false);
			jButtonAnnulerAppareilStock.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					jButtonNouvelAppareilStock.setEnabled(true);

					if (tabModelAppareilsStock.getRowCount() != 0) {
						jButtonModifierAppareilStock.setEnabled(true);
						jButtonSupprimerAppareilStock.setEnabled(true);
					}

					if (actionAStock.equalsIgnoreCase("ajouter")) {
						jPanelAppareilDetailsStock.getRootPane().setDefaultButton(jButtonNouvelAppareilStock);
						jButtonNouvelAppareilStock.requestFocus();
					} else if (actionAStock.equalsIgnoreCase("modifier")) {
						jPanelAppareilDetailsStock.getRootPane().setDefaultButton(jButtonModifierAppareilStock);
						jButtonModifierAppareilStock.requestFocus();
					}

					jButtonAnnulerAppareilStock.setEnabled(false);
					setAppareilStockTexte("", "", "", "", "", "", "", "", "", ""); // Réinitialise la saisie de l'appareil

					if (tabModelAppareilsStock.getRowCount() != 0) {
						// Récupère les détails de l'appareil sélectionné
						AppareilStock appareilSelectione = (AppareilStock) gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow());

						// Affiche les détails de l'appareil
						setAppareilStockTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.noBulletin, appareilSelectione.receptionnePar, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);

						jButtonAssignerAClient.setEnabled(true);
					}

					// Autorise la séléction d'un autre appareil
					jTableDetailsAppareilStock.setEnabled(true);

					// L'appareil n'est plus éditable
					setAppareilStockEditable(false);

					setOngletsSelectable(true);
					jButtonEnregistrerAppareilStock.setEnabled(false);
					//xxxjButtonAssignerAClient.setEnabled(true); // Autorise l'assignation de l'appareil
				}
			});
		}
		return jButtonAnnulerAppareilStock;
	}

	/**
	 * This method initializes jButtonSupprimerAppareilStock
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonSupprimerAppareilStock() {
		if (jButtonSupprimerAppareilStock == null) {
			jButtonSupprimerAppareilStock = new JButton();
			jButtonSupprimerAppareilStock.setBounds(new Rectangle(150, 15, 106, 16));
			jButtonSupprimerAppareilStock.setMnemonic(KeyEvent.VK_S);
			jButtonSupprimerAppareilStock.setText("Supprimer");
			jButtonSupprimerAppareilStock.setToolTipText("Cliquer sur ce bouton pour supprimer les données d\'un appareil existant");
			jButtonSupprimerAppareilStock.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
									null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
						gestionnaireAppareilsStock.supprime(gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow()).id);
						setAppareilStockTexte("", "", "", "", "", "", "", "", "", ""); // L'appareil n'existe plus...
						actualiseTableStock();

						if (tabModelAppareilsStock.getRowCount() != 0) {
							jTableDetailsAppareilStock.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

							// Récupère les détails de l'appareil sélectionné
							AppareilStock appareilSelectione = (AppareilStock) gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow());

							// Affiche les détails de l'appareil
							setAppareilStockTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.noBulletin, appareilSelectione.receptionnePar, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);
						} else {
							jButtonSupprimerAppareilStock.setEnabled(false);
						}
					}
				}
			});
		}
		return jButtonSupprimerAppareilStock;
	}

	/**
	 * This method initializes jButtonAjouterAppareil1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonNouvelAppareilStock() {
		if (jButtonNouvelAppareilStock == null) {
			jButtonNouvelAppareilStock = new JButton();
			jButtonNouvelAppareilStock.setBounds(new Rectangle(15, 15, 106, 16));
			jButtonNouvelAppareilStock.setToolTipText("Cliquer sur ce bouton pour ajouter un nouvel appareil au stock");
			jButtonNouvelAppareilStock.setMnemonic(KeyEvent.VK_N);
			jButtonNouvelAppareilStock.setText("Nouveau");
			jButtonNouvelAppareilStock.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					setAppareilStockTexte("", "", "", "", "", "", "", "", "", ""); // Initialise la saisie
					setAppareilStockEditable(true); // Rends la saisie possible

					// Permet d'annuler l'action en cours
					jButtonAnnulerAppareilStock.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelStock.getRootPane().setDefaultButton(jButtonAnnulerAppareilStock);

					// Rends les boutons présentement innutils innactifs
					jButtonNouvelAppareilStock.setEnabled(false);
					jButtonModifierAppareilStock.setEnabled(false);
					jButtonAssignerAppareil.setEnabled(false);

					// Empêche la séléction d'un autre client
					jTableDetailsAppareilStock.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionAStock = "Ajouter";

					jTextFieldAppareilTypeStock.requestFocus();

					setOngletsSelectable(false);
					jButtonAssignerAClient.setEnabled(false); // Bloque l'assignation de l'appareil

					verifAppareilStock();
				}
			});
			jButtonNouvelAppareilStock.addKeyListener(new KeyAdapter() {

				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_DELETE) {
						if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
										null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
							gestionnaireAppareilsStock.supprime(gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow()).id);
							setAppareilStockTexte("", "", "", "", "", "", "", "", "", ""); // L'appareil n'existe plus...
							actualiseTableStock();
							if (tabModelAppareilsStock.getRowCount() != 0) {
								jTableDetailsAppareilStock.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

								// Récupère les détails de l'appareil sélectionné
								AppareilStock appareilSelectione = (AppareilStock) gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow());

								// Affiche les détails de l'appareil
								setAppareilStockTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.noBulletin, appareilSelectione.receptionnePar, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);
							} else {
								jButtonSupprimerAppareilStock.setEnabled(false);
							}
						}
					}
				}
			}); // Pour gérer les touches du clavier sur la table
		}
		return jButtonNouvelAppareilStock;
	}

	/**
	 * This method initializes jButtonModifierAppareilStock
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonModifierAppareilStock() {
		if (jButtonModifierAppareilStock == null) {
			jButtonModifierAppareilStock = new JButton();
			jButtonModifierAppareilStock.setBounds(new Rectangle(15, 45, 106, 16));
			jButtonModifierAppareilStock.setMnemonic(KeyEvent.VK_M);
			jButtonModifierAppareilStock.setText("Modifier");
			jButtonModifierAppareilStock.setToolTipText("Cliquer sur ce bouton pour modifier les données d\'un appareil existant");
			jButtonModifierAppareilStock.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Initialise la saisie
					AppareilStock appareilSelectione = (AppareilStock) gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow());
					setAppareilStockTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.noBulletin, appareilSelectione.receptionnePar, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);

					setAppareilStockEditable(true); // Rends la saisie possible

					// Rends les boutons présentement innutils innactifs
					jButtonNouvelAppareilStock.setEnabled(false);
					jButtonModifierAppareilStock.setEnabled(false);
					jButtonAssignerAppareil.setEnabled(false);

					// Permet d'annuler l'action en cours
					jButtonAnnulerAppareilStock.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelAppareilDetailsStock.getRootPane().setDefaultButton(jButtonAnnulerAppareilStock);

					// Empêche la séléction d'un autre appareil
					jTableDetailsAppareilStock.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionAStock = "Modifier";

					jTextFieldAppareilTypeStock.requestFocus();

					setOngletsSelectable(false);

					jButtonAssignerAClient.setEnabled(false); // Bloque l'assignation de l'appareil
				}
			});
		}
		return jButtonModifierAppareilStock;
	}

	/**
	 * This method initializes jButtonEnregistrerAppareilStock
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonEnregistrerAppareilStock() {
		if (jButtonEnregistrerAppareilStock == null) {
			jButtonEnregistrerAppareilStock = new JButton();
			jButtonEnregistrerAppareilStock.setBounds(new Rectangle(150, 45, 106, 16));
			jButtonEnregistrerAppareilStock.setToolTipText("Cliquer sur ce bouton pour enregister les données d\'un appareil après un ajout ou une modification");
			jButtonEnregistrerAppareilStock.setMnemonic(KeyEvent.VK_E);
			jButtonEnregistrerAppareilStock.setText("Enregistrer");
			jButtonEnregistrerAppareilStock.setEnabled(false);
			jButtonEnregistrerAppareilStock.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					String dateVente = "";

					if (getCalendrierStockDate().getDate() != null) {
						dateVente = Utilitaire.getSQLDate(getCalendrierStockDate().getDate());
					}

					if (actionAStock.equalsIgnoreCase("Ajouter")) {
						// Ajoute l'appareil saisi à la base de donnée (Avec contrôle de la saisie)
						gestionnaireAppareilsStock.ajoute(new AppareilStock(null,
										(String) (getJComboBoxAppareilGenreStock().getSelectedItem()),
										(String) (getJComboBoxAppareilMarqueStock().getSelectedItem()),
										getjTextFieldAppareilTypeStock().getText(),
										getjTextFieldAppareilNoSerieStock().getText(),
										dateVente,
										"",
										getjTextFieldAppareilPrixStock().getText(),
										getjTextFieldAppareilNoFactureStock().getText(),
										getjTextFieldAppareilNoBulletinStock().getText(),
										getjTextFieldAppareilReceptionneParStock().getText(),
										"0",
										"0")); // Integer.toString(getJComboBoxAppareilStatusStock().getSelectedIndex()) xxx


						jPanelAppareilDetailsStock.getRootPane().setDefaultButton(jButtonNouvelAppareilStock);
						jButtonNouvelAppareilStock.requestFocus();

						String titreInformation = "Ajout réussi.";
						String information = "L'appareil " + (String) (getJComboBoxAppareilMarqueStock().getSelectedItem()) + " " +
										getjTextFieldAppareilTypeStock().getText() + "\n" +
										"a bien été ajouté dans la base de données";
						JOptionPane.showMessageDialog(getJFrame(), information, titreInformation,
										JOptionPane.INFORMATION_MESSAGE);


					} else if (actionAStock.equalsIgnoreCase("Modifier")) {
						// Modifie l'appareil saisi à la base de donnée (Avec contrôle de la saisie)
						gestionnaireAppareilsStock.modifie(new AppareilStock(gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow()).id,
										(String) (getJComboBoxAppareilGenreStock().getSelectedItem()),
										(String) (getJComboBoxAppareilMarqueStock().getSelectedItem()),
										getjTextFieldAppareilTypeStock().getText(),
										getjTextFieldAppareilNoSerieStock().getText(),
										dateVente,
										"",
										getjTextFieldAppareilPrixStock().getText(),
										getjTextFieldAppareilNoFactureStock().getText(),
										getjTextFieldAppareilNoBulletinStock().getText(),
										getjTextFieldAppareilReceptionneParStock().getText(),
										"0",
										"0")); // Integer.toString(getJComboBoxAppareilStatusStock().getSelectedIndex()) xxx

						jPanelAppareilDetailsStock.getRootPane().setDefaultButton(jButtonModifierAppareilStock);
						jButtonModifierAppareilStock.requestFocus();
					}
					// L'action a été effectuée
					actionAStock = "";

					// Désactive les actions devenues impossibles
					jButtonEnregistrerAppareilStock.setEnabled(false);
					jButtonAnnulerAppareilStock.setEnabled(false);

					setAppareilStockTexte("", "", "", "", "", "", "", "", "", ""); // Réinitialise la saisie de l'appareil

					// Autorise la séléction d'un autre appareil
					jTableDetailsAppareilStock.setEnabled(true);

					// L'appareil n'est plus éditable
					setAppareilStockEditable(false);

					// Active les actions à nouveau possible
					jButtonNouvelAppareilStock.setEnabled(true);

					// Actualise l'affichage
					actualiseTableStock();

					// Autorise la séléction d'un autre appareil
					jTableDetailsAppareilStock.setEnabled(true);

					setOngletsSelectable(true);
					jButtonAssignerAClient.setEnabled(true); // Autorise l'assignation de l'appareil
				}
			});
		}
		return jButtonEnregistrerAppareilStock;
	}

	/**
	 * This method initializes jComboBoxAppareilGenreStock
	 *
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxAppareilGenreStock() {
		if (jComboBoxAppareilGenreStock == null) {

			// si la liste a pu être chargée
			if (listeGenreAppareil != null) {
				jComboBoxAppareilGenreStock = new JComboBox(listeGenreAppareil.toArray());
				// doit être éditbale avant dêtre décorée sinon
				// l'auto-complémentation est stricte

				// pour ajouter l'auto-complémentation
				AutoCompleteDecorator.decorate(jComboBoxAppareilGenreStock);
				jComboBoxAppareilGenreStock.setSelectedIndex(-1);
				jComboBoxAppareilGenreStock.setEditable(false);
				jComboBoxAppareilGenreStock.addItemListener(new ItemListener() {

					public void itemStateChanged(ItemEvent e) {
						try {
							jComboBoxAppareilGenreStock.setEditable(true);
						} catch (Exception ex) {
						}
					}
				});
				jComboBoxAppareilGenreStock.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						jComboBoxAppareilGenreStock.setEditable(false);

						try {
							if (jTextFieldAppareilTypeStock.isEditable()) {
								verifAppareilStock();
							}
						} catch (Exception ex) {
						}
					}
				});
			} else {
				jComboBoxAppareilGenreStock = new JComboBox();
				jComboBoxAppareilGenreStock.setEditable(true);
			}

			jComboBoxAppareilGenreStock.setLocation(new Point(135, 30));
			jComboBoxAppareilGenreStock.setSize(new Dimension(151, 16));
		}
		return jComboBoxAppareilGenreStock;
	}

	/**
	 * This method initializes jComboBoxAppareilMarqueStock
	 *
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxAppareilMarqueStock() {
		if (jComboBoxAppareilMarqueStock == null) {
			// si la liste a pu être chargée
			if (listeMarqueAppareil != null) {
				jComboBoxAppareilMarqueStock = new JComboBox(listeMarqueAppareil.toArray());
				// doit être éditbale avant dêtre décorée sinon
				// l'auto-complémentation est stricte
				jComboBoxAppareilMarqueStock.setEditable(false);
				// pour ajouter l'auto-complémentation
				AutoCompleteDecorator.decorate(jComboBoxAppareilMarqueStock);
				jComboBoxAppareilMarqueStock.setSelectedIndex(-1);
			} else {
				jComboBoxAppareilMarqueStock = new JComboBox();
				jComboBoxAppareilMarqueStock.setEditable(false);
			}

			jComboBoxAppareilMarqueStock.setBounds(new Rectangle(135, 60, 151, 16));

			jComboBoxAppareilMarqueStock.addItemListener(new ItemListener() {

				public void itemStateChanged(ItemEvent e) {
					try {
						jComboBoxAppareilMarqueStock.setEditable(true);
					} catch (Exception ex) {
					}
				}
			});
			jComboBoxAppareilMarqueStock.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					jComboBoxAppareilMarqueStock.setEditable(false);

					try {
						if (jTextFieldAppareilTypeStock.isEditable()) {
							verifAppareilStock();
						}
					} catch (Exception ex) {
					}
				}
			});
		}
		return jComboBoxAppareilMarqueStock;
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
	protected ImageIcon createImageIcon(String path,
					String description) {
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
//         System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	/**
	 * This method initializes jButtonAssignerAClient
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAssignerAClient() {
		if (jButtonAssignerAClient == null) {
			jButtonAssignerAClient = new JButton();
			jButtonAssignerAClient.setBounds(new Rectangle(60, 295, 181, 22));
			jButtonAssignerAClient.setMnemonic(KeyEvent.VK_C);
			jButtonAssignerAClient.setText("Assigner à un client");
			jButtonAssignerAClient.setToolTipText("<html>Cliquer sur ce bouton pour assigner cet appareil à un client <br> (l'onglet client va s'ouvrir et il sera alors possible de choisir le client)</html>");
			jButtonAssignerAClient.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					jButtonAssignerAppareil.setEnabled(true); // Active le bouton d'assignation pour les clients
					jButtonAnnulerAssignationAppareil.setEnabled(true);
					jButtonAssignerAClient.setEnabled(false);
					dateAssignation = new InviteDateAssignation(null).affiche();
					// Va sur le paneau des clients
					getJTabbedPanePrincipal().setSelectedComponent(getJPanelClient());
					// dans le panel détails client
					getJTabbedPaneClient().setSelectedComponent(getJPanelDetailClient());
				}
			});
		}
		return jButtonAssignerAClient;
	}

	/**
	 * This method initializes jButtonAssignerAppareil
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAssignerAppareil() {
		if (jButtonAssignerAppareil == null) {
			jButtonAssignerAppareil = new JButton();
			jButtonAssignerAppareil.setText("Assigner l'appareil");
			jButtonAssignerAppareil.setSize(new Dimension(211, 16));
			jButtonAssignerAppareil.setEnabled(false);
			jButtonAssignerAppareil.setMnemonic(KeyEvent.VK_P);
			jButtonAssignerAppareil.setLocation(new Point(45, 259));
			jButtonAssignerAppareil.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment assigner l'appareil séléctionné dans le stock pour ce client?",
									null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
						jButtonAssignerAppareil.setEnabled(false); // Assignation plus possible
						jButtonAnnulerAssignationAppareil.setEnabled(false);
						jButtonAssignerAClient.setEnabled(true);

						// Récupère les détails du client sélectionné
						Client clientSelectione = (Client) gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());
						// Récupère et affiche les détails de l'appareil et l'assigne au client sélectionné
						AppareilStock appareilSelectione = (AppareilStock) gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow());
						gestionnaireAppareils.modifie(new Appareil(appareilSelectione.id, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.type, appareilSelectione.noSerie, dateAssignation, appareilSelectione.dateVenteFin, appareilSelectione.prix, appareilSelectione.noFacture, Integer.toString(clientSelectione.id), "1"));
						actualiseTableStock();

						actualiseTableAppareilsClients();
						actualiseTableReparationsAppareil();
						actualiseTableReparationsClientAEncaisser();
						actualiseTableReparationsTTAEncaisser();
						actualiseTableVente();
					}
				}
			});
		}
		return jButtonAssignerAppareil;
	}

	/**
	 * Permet de rendre la saisie d'un client possible ou pas
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 * @param condition Vrai pour rendre éditable, faux sinon
	 */
	public void setClientEditable(Boolean condition) {
		getJTextFieldNom().setEditable(condition);
		getJTextFieldPrenom().setEditable(condition);
		getJTextFieldProfession().setEditable(condition);
		getJTextFieldTelPrincipal().setEditable(condition);
		getJTextFieldTelSecondaire().setEditable(condition);
		getJTextFieldAdresse().setEditable(condition);
		getJTextFieldNPA().setEditable(condition);
		getJTextFieldLocalite().setEditable(condition);
	}

	/**
	 * Permet d'afficher les détails d'un client
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setClientTexte(String nom, String prenom, String profession, String tel1, String tel2, String adresse, String npa, String localite) {
		jTextFieldNom.setText(nom);
		jLabelRappelNom.setText("Nom : " + nom);
		jTextFieldPrenom.setText(prenom);
		jLabelRappelPrenom.setText("Prenom : " + prenom);
		jTextFieldProfession.setText(profession);

		// Numéros sans éspaces (Pour réinitialiser et plus de sécurité pour des numéros éventuellement foireux...)
		jTextFieldTelPrincipal.setText(tel1);
		jTextFieldTelSecondaire.setText(tel2);

		// Force les éspaces pour les numéros de téléphone
		if (tel1.length() > 4) {
			if (tel1.charAt(0) != '+' && tel1.charAt(3) != ' ') {
				jTextFieldTelPrincipal.setText(tel1.substring(0, 3) + ' ' + tel1.substring(3));
			}
		}
//         else if(tel1.charAt(5) != ' ') // Décommenter pour ajouter un éspace pour les numéros commençant par un plus
//            jTextFieldTelPrincipal.setText(tel1.substring(0, 5)+' '+tel1.substring(5));
		if (tel2.length() > 4) {
			if (tel2.charAt(0) != '+' && tel2.charAt(3) != ' ') {
				jTextFieldTelSecondaire.setText(tel2.substring(0, 3) + ' ' + tel2.substring(3));
			}
		}
//         else if(tel1.charAt(5) != ' ') // Décommenter pour ajouter un éspace pour les numéros commençant par un plus
//            jTextFieldTelSecondaire.setText(tel2.substring(0, 5)+' '+tel2.substring(5));

		jTextFieldAdresse.setText(adresse);
		jTextFieldNPA.setText(npa);
		jTextFieldLocalite.setText(localite);
	}

	/**
	 * Permet de rendre la saisie d'un appareil possible ou pas
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 * @param condition Vrai pour rendre éditable, faux sinon
	 */
	public void setAppareilEditable(Boolean condition) {
		jTextFieldAppareilType.setEditable(condition);
		jTextFieldAppareilNoSerie.setEditable(condition);
		jTextFieldAppareilPrix.setEditable(condition);
		jTextFieldAppareilNoFacture.setEditable(condition);
		jComboBoxAppareilGenre.setEditable(false);
		jComboBoxAppareilMarque.setEditable(false);
		//jComboBoxAppareilStatus.setEnabled(condition);
		jComboBoxAppareilGenre.setEnabled(condition);
		jComboBoxAppareilMarque.setEnabled(condition);
		calendrierAppareilDateVente.setEnabled(condition);
		// pour éditer la date au clavier
		if (Definition.dateManuelle) {
			calendrierAppareilDateVente.getEditor().setEditable(condition);
		}

	}

	/**
	 * Permet de rendre la saisie d'un appareil du stock possible ou pas
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 * @param condition Vrai pour rendre éditable, faux sinon
	 */
	public void setAppareilStockEditable(Boolean condition) {
		jTextFieldAppareilTypeStock.setEditable(condition);
		jTextFieldAppareilNoSerieStock.setEditable(condition);
		jTextFieldAppareilPrixStock.setEditable(condition);
		jTextFieldAppareilNoFactureStock.setEditable(condition);
		jTextFieldAppareilNoBulletinStock.setEditable(condition);
		jTextFieldAppareilReceptionneParStock.setEditable(condition);
		jComboBoxAppareilGenre.setEditable(false);
		jComboBoxAppareilMarque.setEditable(false);
		//jTextFieldAppareilStatus.setEditable(condition);
		jComboBoxAppareilGenreStock.setEnabled(condition);
		jComboBoxAppareilMarqueStock.setEnabled(condition);
		calendrierStockDate.setEnabled(condition);
		// pour éditer la date au clavier
		if (Definition.dateManuelle) {
			calendrierStockDate.getEditor().setEditable(condition);
		}
	}

	/**
	 * Permet d'afficher les détails d'un appareil
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setAppareilTexte(String type, String noSerie, String dateVente, String prix, String noFacture, String genre, String marque, String status) {
		jTextFieldAppareilType.setText(type);
		jTextFieldAppareilNoSerie.setText(noSerie);

		if (!dateVente.isEmpty() && !dateVente.equalsIgnoreCase("null")) {
			calendrierAppareilDateVente.setDate(Utilitaire.getJavaDate(dateVente));
		} else {
			calendrierAppareilDateVente.setDate(null);
		}
		jTextFieldAppareilPrix.setText(prix);
		jTextFieldAppareilNoFacture.setText(noFacture);
		if (genre.isEmpty()) {
			jComboBoxAppareilGenre.setSelectedItem(null);
		} else {
			jComboBoxAppareilGenre.setSelectedItem(genre);
		}
		if (marque.isEmpty()) {
			jComboBoxAppareilMarque.setSelectedItem(null);
		} else {
			jComboBoxAppareilMarque.setSelectedItem(marque);
		}
		//jComboBoxAppareilStatus.setSelectedIndex(Integer.parseInt(status)); xxx
		setAppareilEditable(false);
	}

	/**
	 * Permet d'afficher les détails d'un appareil du stock
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setAppareilStockTexte(String type, String noSerie, String dateVente, String prix, String noFacture, String noBulletin, String receptionnePar, String genre, String marque, String status) {
		jTextFieldAppareilTypeStock.setText(type);
		jTextFieldAppareilNoSerieStock.setText(noSerie);

		if (!dateVente.isEmpty() && !dateVente.equalsIgnoreCase("null")) {
			calendrierStockDate.setDate(Utilitaire.getJavaDate(dateVente));
		} else {
			calendrierStockDate.setDate(null);
		}
		jTextFieldAppareilPrixStock.setText(prix);
		jTextFieldAppareilNoFactureStock.setText(noFacture);
		jTextFieldAppareilNoBulletinStock.setText(noBulletin);
		jTextFieldAppareilReceptionneParStock.setText(receptionnePar);
		if (genre.isEmpty()) {
			jComboBoxAppareilGenreStock.setSelectedItem(null);
		} else {
			jComboBoxAppareilGenreStock.setSelectedItem(genre);
		}
		if (marque.isEmpty()) {
			jComboBoxAppareilMarqueStock.setSelectedItem(null);
		} else {
			jComboBoxAppareilMarqueStock.setSelectedItem(marque);
		}
		//jTextFieldAppareilStatus.setText(status); xxx
		setAppareilStockEditable(false);
	}

	/**
	 * Permet d'afficher les détails d'une réparation
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setReparationTexte(String dateReparation, String panne, String travailEffectue, String technicien, String prix, String noFacture, Boolean paye) {
		if (!dateReparation.isEmpty() && !dateReparation.equalsIgnoreCase("null")) {
			calendrierReparationDate.setDate(Utilitaire.getJavaDate(dateReparation));
		} else {
			calendrierReparationDate.setDate(null);
		}

		jTextExtensReparationPanne.getPane().setText(panne);
		jTextExtensReparationTravailEffectue.getPane().setText(travailEffectue);
		jTextFieldReparationTechnicien.setText(technicien);
		jTextFieldReparationPrix.setText(prix);
		jTextFieldReparationNoFacture.setText(noFacture);
		jCheckBoxReparationPaye.setSelected(paye);

		setReparationEditable(false);
	}

	/**
	 * Permet de rendre éditable ou non une réparation
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setReparationEditable(Boolean condition) {
		jTextExtensReparationTravailEffectue.getPane().setEditable(condition);
		jTextExtensReparationPanne.getPane().setEditable(condition);
		jTextFieldReparationTechnicien.setEditable(condition);
		jTextFieldReparationPrix.setEditable(condition);
		jTextFieldReparationNoFacture.setEditable(condition);
		jCheckBoxReparationPaye.setEnabled(condition);
		calendrierReparationDate.setEnabled(condition);
		// pour éditer la date au clavier
		if (Definition.dateManuelle) {
			calendrierReparationDate.getEditor().setEditable(condition);
		}

		if (condition) {
			jTextExtensReparationTravailEffectue.getPane().setBackground(Color.WHITE);
			jTextExtensReparationPanne.getPane().setBackground(Color.WHITE);
		} else {
			jTextExtensReparationTravailEffectue.getPane().setBackground(jPanelreparationDetails.getBackground());
			jTextExtensReparationPanne.getPane().setBackground(jPanelreparationDetails.getBackground());
		}
	}

	/**
	 * Permet d'afficher les détails d'une réparation à encaisser
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setReparationAEncaisserTexte(String dateReparation, String panne, String travailEffectue, String technicien, String prix, String noFacture, Boolean paye) {
		if (!dateReparation.isEmpty() && !dateReparation.equalsIgnoreCase("null")) {
			calendrierAEncaisserDate.setDate(Utilitaire.getJavaDate(dateReparation));
		} else {
			calendrierAEncaisserDate.setDate(null);
		}

		jTextExtensReparationPanne1.getPane().setText(panne);
		jTextExtensReparationTravailEffectue1.getPane().setText(travailEffectue);
		jTextFieldReparationTechnicienAEncaisser.setText(technicien);
		jTextFieldReparationPrix1.setText(prix);
		jTextFieldReparationAEncaisserNoFacture.setText(noFacture);
		jCheckBoxReparationPaye1.setSelected(paye);

		setReparationAEncaisserEditable(false);
	}

	/**
	 * Permet de rendre éditable ou non une réparation à encaisser
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setReparationAEncaisserEditable(Boolean condition) {
		jTextExtensReparationTravailEffectue1.getPane().setEditable(condition);
		jTextExtensReparationPanne1.getPane().setEditable(condition);
		jTextFieldReparationTechnicienAEncaisser.setEditable(condition);
		jTextFieldReparationPrix1.setEditable(condition);
		jTextFieldReparationAEncaisserNoFacture.setEditable(condition);
		jCheckBoxReparationPaye1.setEnabled(condition);
		calendrierAEncaisserDate.setEnabled(condition);
		// pour éditer la date au clavier
		if (Definition.dateManuelle) {
			calendrierAEncaisserDate.getEditor().setEditable(condition);
		}

		if (condition) {
			jTextExtensReparationTravailEffectue1.getPane().setBackground(Color.WHITE);
			jTextExtensReparationPanne1.getPane().setBackground(Color.WHITE);
		} else {
			jTextExtensReparationTravailEffectue1.getPane().setBackground(jPanelreparationDetails.getBackground());
			jTextExtensReparationPanne1.getPane().setBackground(jPanelreparationDetails.getBackground());
		}
	}

	/**
	 * Permet d'afficher les détails de toutes les réparations à encaisser
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setReparationTTAEncaisserTexte(String dateReparation, String panne, String travailEffectue, String technicien, String prix, String noFacture, Boolean paye) {
		if (!dateReparation.isEmpty() && !dateReparation.equalsIgnoreCase("null")) {
			calendrierReparationDateTTAEncaisser.setDate(Utilitaire.getJavaDate(dateReparation));
		} else {
			calendrierReparationDateTTAEncaisser.setDate(null);
		}

		jTextExtensReparationPanneTTAEncaisser.getPane().setText(panne);
		jTextExtensReparationTravailEffectueTTAEncaisser.getPane().setText(travailEffectue);
		jTextFieldReparationTechnicienTTAEncaisser.setText(technicien);
		jTextFieldReparationPrixTTAEncaisser.setText(prix);
		jTextFieldTTAEncaisserNoFacture.setText(noFacture);
		jCheckBoxReparationPayeTTAEncaisser.setSelected(paye);

		setReparationTTAEncaisserEditable(false);
	}

	/**
	 * Permet de rendre éditable ou non toutes les réparations à encaisser
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setReparationTTAEncaisserEditable(Boolean condition) {
		jTextExtensReparationTravailEffectueTTAEncaisser.getPane().setEditable(condition);
		jTextExtensReparationPanneTTAEncaisser.getPane().setEditable(condition);
		jTextFieldReparationTechnicienTTAEncaisser.setEditable(condition);
		jTextFieldReparationPrixTTAEncaisser.setEditable(condition);
		jTextFieldTTAEncaisserNoFacture.setEditable(condition);
		jCheckBoxReparationPayeTTAEncaisser.setEnabled(condition);
		calendrierReparationDateTTAEncaisser.setEnabled(condition);
		// pour éditer la date au clavier
		if (Definition.dateManuelle) {
			calendrierReparationDateTTAEncaisser.getEditor().setEditable(condition);
		}

		if (condition) {
			jTextExtensReparationTravailEffectueTTAEncaisser.getPane().setBackground(Color.WHITE);
			jTextExtensReparationPanneTTAEncaisser.getPane().setBackground(Color.WHITE);
		} else {
			jTextExtensReparationTravailEffectueTTAEncaisser.getPane().setBackground(jPanelreparationDetails.getBackground());
			jTextExtensReparationPanneTTAEncaisser.getPane().setBackground(jPanelreparationDetails.getBackground());
		}
	}

	/**
	 * Permet d'afficher les détails des appareils vendus
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 */
	public void setAppareilVenduTexte(String type, String noSerie, String dateVente, String prix,
					String noFacture, String genre, String marque, String status, String nomProprietaire,
					String prenomProprietaire, String localiteProprietaire) {
		jTextFieldAppareilVenduType.setText(type);
		jTextFieldAppareilVenduNoSerie.setText(noSerie);
		jTextFieldAppareilVenduClientNom.setText(nomProprietaire);
		jTextFieldAppareilVenduClientPrenom.setText(prenomProprietaire);
		jTextFieldAppareilVenduClientLocalite.setText(localiteProprietaire);


		if (!dateVente.isEmpty() && !dateVente.equalsIgnoreCase("null")) {
			calendrierVenteDate.setDate(Utilitaire.getJavaDate(dateVente));
		} else {
			calendrierVenteDate.setDate(null);
		}
		jTextFieldAppareilVenduPrix.setText(prix);
		jTextFieldAppareilVenduNoFacture.setText(noFacture);
		if (genre.isEmpty()) {
			jComboBoxAppareilGenre11.setSelectedItem(null);
		} else {
			jComboBoxAppareilGenre11.setSelectedItem(genre);
		}
		if (marque.isEmpty()) {
			jComboBoxAppareilMarque11.setSelectedItem(null);
		} else {
			jComboBoxAppareilMarque11.setSelectedItem(marque);
		}
		//jComboBoxAppareilStatus.setSelectedIndex(Integer.parseInt(status)); xxx
		setAppareilVenduEditable(false);
	}

	/**
	 * Permet de rendre la saisie d'un appareil vendu possible ou pas
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 * @param condition Vrai pour rendre éditable, faux sinon
	 */
	public void setAppareilVenduEditable(Boolean condition) {
		jTextFieldAppareilVenduType.setEditable(condition);
		jTextFieldAppareilVenduNoSerie.setEditable(condition);
		jTextFieldAppareilVenduPrix.setEditable(condition);
		jTextFieldAppareilVenduNoFacture.setEditable(condition);
		//jComboBoxAppareilGenre.setEditable(condition);
		//jComboBoxAppareilMarque.setEditable(condition);
		//jComboBoxAppareilStatus.setEnabled(condition);
		jComboBoxAppareilGenre11.setEnabled(condition);
		jComboBoxAppareilMarque11.setEnabled(condition);
		calendrierVenteDate.setEnabled(condition);
		// pour éditer la date au clavier
		if (Definition.dateManuelle) {
			calendrierVenteDate.getEditor().setEditable(condition);
		}

	}

	/**
	 * Permet de rendre le changement d'onglets possible ou pas
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 * @param condition Vrai pour rendre éditable, faux sinon
	 */
	public void setOngletsSelectable(Boolean condition) {
		jTabbedPanePrincipal.setEnabled(condition);
		jTabbedPaneClient.setEnabled(condition);
		jTabbedPaneAppareils.setEnabled(condition);
	}

	public void mouseReleased(MouseEvent e) {
		// Si les éléments clickables sont déjà créés
		if (jTableDetailsClient != null && jTableDetailsAppareil != null && jTableDetailsAppareilStock != null && jTableDetailsReparation != null && jTableDetailsReparationAEncaisser != null && jTableDetailsAppareilVendu != null && jTableDetailsReparationTTAEncaisser != null) {

			// Si on click sur la table de détail des clients et qu'elle est active
			if (e.getComponent() == jTableDetailsClient && jTableDetailsClient.isEnabled()) {
				Client clientSelectione = null;
				// Si un ligne est sélectionnée
				if (jTableDetailsClient.getSelectedRow() >= 0) // Récupère les détails du client sélectionné
				{
					clientSelectione = gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());
				}

				// siune ligne était bien sélectionnée
				if (clientSelectione != null) {
					setClientTexte(clientSelectione.nom, clientSelectione.prenom, clientSelectione.profession, clientSelectione.noTelPrincipal, clientSelectione.noTelSecondaire, clientSelectione.adresse, clientSelectione.NPA, clientSelectione.localite);
					jButtonModifierClient.setEnabled(true);
					jButtonSupprimerClient.setEnabled(true);
				} else {
					setClientTexte("", "", "", "", "", "", "", "");
					jButtonModifierClient.setEnabled(false);
					jButtonSupprimerClient.setEnabled(false);
				}

				actualiseTableAppareilsClients();
				actualiseTableReparationsAppareil();
				actualiseTableReparationsClientAEncaisser();
				actualiseTableReparationsTTAEncaisser();
				actualiseTableVente();
			}

			// Si on click sur la table de détail des appareils et qu'elle est active
			if (e.getComponent() == jTableDetailsAppareil && jTableDetailsAppareil.isEnabled()) {
				// si un appareil est sélectionnée (ctrl permet la déselection)
				if (jTableDetailsAppareil.getSelectedRow() >= 0) {
					// Récupère et affiche les détails de l'appareil séléctionné
					Appareil appareilSelectione = gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow());
					setAppareilTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);
				} else {
					setAppareilTexte("", "", "", "", "", "", "", "");
				}

				actualiseTableReparationsAppareil();
				actualiseTableReparationsTTAEncaisser();
				actualiseTableReparationsClientAEncaisser();
			}

			// Si on click sur la table de détail des appareils en stock et qu'elle est active
			if (e.getComponent() == jTableDetailsAppareilStock && jTableDetailsAppareilStock.isEnabled()) {
				// si un appareil est sélectionnée (ctrl permet la déselection)
				if (jTableDetailsAppareilStock.getSelectedRow() >= 0) {
					// Récupère les détails de l'appareil sélectionné
					AppareilStock appareilSelectione = (AppareilStock) gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow());
					setAppareilStockTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.noBulletin, appareilSelectione.receptionnePar, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);
				} else {
					setAppareilStockTexte("", "", "", "", "", "", "", "", "", "");
				}

			}


			// Si on click sur la table de détail des réparations et qu'elle est active
			if (e.getComponent() == jTableDetailsReparation && jTableDetailsReparation.isEnabled()) {
				// si une réparation est sélectionnée (ctrl permet la déselection)
				if (jTableDetailsReparation.getSelectedRow() >= 0) {
					// Récupère et affiche les détails de la réparation séléctionné
					Reparation reparationlSelectione = (Reparation) gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow());
					setReparationTexte(reparationlSelectione.dateReparationDebut, reparationlSelectione.probleme, reparationlSelectione.travailEffectue, reparationlSelectione.technicien, reparationlSelectione.prix, reparationlSelectione.noFacture, reparationlSelectione.paye);
				} else {
					setReparationTexte("", "", "", "", "", "", false);
				}
			}

			// Si on click sur la table de détail de toutes les réparations à encaisser du client et qu'elle est active
			if (e.getComponent() == jTableDetailsReparationAEncaisser && jTableDetailsReparationAEncaisser.isEnabled()) {
				// si une réparation est sélectionnée (ctrl permet la déselection)
				if (jTableDetailsReparationAEncaisser.getSelectedRow() >= 0) {
					// Récupère et affiche les détails de la réparation séléctionné
					Reparation reparationlSelectione = (Reparation) gestionnaireReparationAEncaisser.getDetail(jTableDetailsReparationAEncaisser.getSelectedRow());
					setReparationAEncaisserTexte(reparationlSelectione.dateReparationDebut, reparationlSelectione.probleme, reparationlSelectione.travailEffectue, reparationlSelectione.technicien, reparationlSelectione.prix, reparationlSelectione.noFacture, reparationlSelectione.paye);
				} else {
					setReparationAEncaisserTexte("", "", "", "", "", "", false);
				}
			}

			// Si on click sur la table des appareils vendus et qu'elle est active
			if (e.getComponent() == jTableDetailsAppareilVendu && jTableDetailsAppareilVendu.isEnabled()) {
				// si un appareil est sélectionnée (ctrl permet la déselection)
				if (jTableDetailsAppareilVendu.getSelectedRow() >= 0) {
					// Récupère les détails de l'appareil sélectionné
					Appareil appareilSelectione = gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow());
					Client proprietaire = gestionnaireAppareilVendu.getProprietaire(appareilSelectione);

					// Affiche les détails de l'appareil
					setAppareilVenduTexte(appareilSelectione.type, appareilSelectione.noSerie,
									appareilSelectione.dateVenteDebut, appareilSelectione.prix,
									appareilSelectione.noFacture, appareilSelectione.genre,
									appareilSelectione.marque, appareilSelectione.status,
									proprietaire.nom, proprietaire.prenom, proprietaire.localite);
				} else {
					setAppareilVenduTexte("", "", "", "", "", "", "", "", "", "", "");
				}
			}


			// Si on click sur la table de détail de toutes les réparations à encaisser et qu'elle est active
			if (e.getComponent() == jTableDetailsReparationTTAEncaisser && jTableDetailsReparationTTAEncaisser.isEnabled()) {
				// si une réparation est sélectionnée (ctrl permet la déselection)
				if (jTableDetailsReparationTTAEncaisser.getSelectedRow() >= 0) {
					// Récupère et affiche les détails de la réparation séléctionné
					Reparation reparationlSelectione = (Reparation) gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow());
					setReparationTTAEncaisserTexte(reparationlSelectione.dateReparationDebut, reparationlSelectione.probleme, reparationlSelectione.travailEffectue, reparationlSelectione.technicien, reparationlSelectione.prix, reparationlSelectione.noFacture, reparationlSelectione.paye);
				} else {
					setReparationTTAEncaisserTexte("", "", "", "", "", "", false);
				}
			}
		}
	}

	// On implémente...
	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	/**
	 * This method initializes jTitledPanelListeAppareilVendu
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelListeAppareilVendu() {
		if (jTitledPanelListeAppareilVendu == null) {
			jTitledPanelListeAppareilVendu = new JXTitledPanel("Liste des appareils vendus");
			jTitledPanelListeAppareilVendu.setBorder(new DropShadowBorder(true));
			jTitledPanelListeAppareilVendu.setTitleForeground(Color.white);
			jTitledPanelListeAppareilVendu.setLocation(new Point(15, 15));
			jTitledPanelListeAppareilVendu.setSize(new Dimension(387, 496));
			jTitledPanelListeAppareilVendu.add(getJPanelListeAppareilVendu(), null);
		}
		return jTitledPanelListeAppareilVendu;
	}

	/**
	 * This method initializes jPanelListeAppareilVendu
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelListeAppareilVendu() {
		if (jPanelListeAppareilVendu == null) {
			jPanelListeAppareilVendu = new JPanel();
			jPanelListeAppareilVendu.setLayout(null);
			jPanelListeAppareilVendu.setBounds(new Rectangle(15, 15, 373, 421));
			jPanelListeAppareilVendu.add(getJScrollPaneListeAppareilsVentes(), null);
			jPanelListeAppareilVendu.add(getJButtonRechercherAppareilVendu(), null);
			jPanelListeAppareilVendu.add(getJButtonTtAfficherListeAppareilVendu(), null);
		}
		return jPanelListeAppareilVendu;
	}

	/**
	 * This method initializes jScrollPaneDetailsAppareilsStock1
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPaneListeAppareilsVentes() {
		if (jScrollPaneDetailsAppareilsStock1 == null) {
			jScrollPaneDetailsAppareilsStock1 = new JScrollPane();
			jScrollPaneDetailsAppareilsStock1.setBounds(new Rectangle(15, 30, 346, 391));
			jScrollPaneDetailsAppareilsStock1.setViewportView(getJXTableDetailsAppareilVendu());
		}
		return jScrollPaneDetailsAppareilsStock1;
	}

	/**
	 * This method initializes jTableDetailsAppareilVendu
	 *
	 * @return javax.swing.JXTable
	 */
	private JXTable getJXTableDetailsAppareilVendu() {
		if (jTableDetailsAppareilVendu == null) {
			jTableDetailsAppareilVendu = new JXTable();
		}
		return jTableDetailsAppareilVendu;
	}

	/**
	 * This method initializes jButtonRechercherAppareilVendu
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonRechercherAppareilVendu() {
		if (jButtonRechercherAppareilVendu == null) {
			jButtonRechercherAppareilVendu = new JButton();
			jButtonRechercherAppareilVendu.setBounds(new Rectangle(30, 435, 136, 16));
			jButtonRechercherAppareilVendu.setMnemonic(KeyEvent.VK_R);
			jButtonRechercherAppareilVendu.setSelected(true);
			jButtonRechercherAppareilVendu.setText("Rechercher");
			jButtonRechercherAppareilVendu.setToolTipText("Cliquer sur ce bouton pour rechercher un appareil vendu");
			jButtonRechercherAppareilVendu.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// ouvre la fenetre de recherche
					appareilRecherche = new FenetreRechercheAppareil(listeGenreAppareil,
									listeMarqueAppareil).affiche(true);
					if (appareilRecherche != null) {
						appareilRecherche.status = Integer.toString(1);
						gestionnaireAppareilVendu.recherche(appareilRecherche);
						if (tabModelAppareilVendu.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
						{
							jTableDetailsAppareilVendu.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
						}
						actualiseTableVente();
					}
				}
			});
		}
		return jButtonRechercherAppareilVendu;
	}

	/**
	 * This method initializes jButtonTtAfficherListeAppareilVendu
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonTtAfficherListeAppareilVendu() {
		if (jButtonTtAfficherListeAppareilVendu == null) {
			jButtonTtAfficherListeAppareilVendu = new JButton();
			jButtonTtAfficherListeAppareilVendu.setBounds(new Rectangle(195, 435, 136, 16));
			jButtonTtAfficherListeAppareilVendu.setMnemonic(KeyEvent.VK_T);
			jButtonTtAfficherListeAppareilVendu.setText("Tout afficher");
			jButtonTtAfficherListeAppareilVendu.setToolTipText("Cliquer sur ce bouton pour afficher la liste intégralle des appareils vendus");
			jButtonTtAfficherListeAppareilVendu.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					String status = "1";
					gestionnaireAppareilVendu.recherche(new Appareil(null, "", "", "", "", "", "", "", "", null, status));
					if (tabModelAppareilVendu.getRowCount() != 0) // Si il y a au moins une réparation à encaisser
					{
						jTableDetailsAppareilVendu.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut
					}
					actualiseTableVente();
				}
			});
		}
		return jButtonTtAfficherListeAppareilVendu;
	}

	/**
	 * This method initializes jTitledPanelAppareilVenduDetails
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelAppareilVenduDetails() {
		if (jTitledPanelAppareilVenduDetails == null) {
			jTitledPanelAppareilVenduDetails = new JXTitledPanel("Détails");
			jTitledPanelAppareilVenduDetails.setBorder(new DropShadowBorder(true));
			jTitledPanelAppareilVenduDetails.setTitleForeground(Color.white);
			jTitledPanelAppareilVenduDetails.setLocation(new Point(435, 15));
			jTitledPanelAppareilVenduDetails.setSize(new Dimension(311, 496));
			jTitledPanelAppareilVenduDetails.add(getJPanelAppareilVenduDetails(), BorderLayout.CENTER);
		}
		return jTitledPanelAppareilVenduDetails;
	}

	/**
	 * This method initializes jPanelAppareilVenduDetails
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelAppareilVenduDetails() {
		if (jPanelAppareilVenduDetails == null) {
			jLabelAppareilVenduNoFacture = new JLabel();
			jLabelAppareilVenduNoFacture.setBounds(11, 209, 105, 16);
			jLabelAppareilVenduNoFacture.setText("N° de facture :");
			jLabelAppareilVenduPrix = new JLabel();
			jLabelAppareilVenduPrix.setBounds(11, 179, 105, 16);
			jLabelAppareilVenduPrix.setText("Prix :");
			jLabelAppareilVenduDateVente = new JLabel();
			jLabelAppareilVenduDateVente.setBounds(11, 151, 105, 16);
			jLabelAppareilVenduDateVente.setText("Date de vente :");
			jLabelAppareilVenduNoSerie = new JLabel();
			jLabelAppareilVenduNoSerie.setBounds(11, 119, 105, 16);
			jLabelAppareilVenduNoSerie.setText("N° de série:");
			jLabelAppareilVenduType = new JLabel();
			jLabelAppareilVenduType.setBounds(11, 89, 105, 16);
			jLabelAppareilVenduType.setText("Type :");
			jLabelAppareilVenduMarque = new JLabel();
			jLabelAppareilVenduMarque.setBounds(11, 60, 105, 16);
			jLabelAppareilVenduMarque.setText("*Marque :");
			jLabelAppareilVenduGenre = new JLabel();
			jLabelAppareilVenduGenre.setBounds(11, 29, 105, 16);
			jLabelAppareilVenduGenre.setText("*Genre :");
			jLabelAppareilVenduGenre.setLabelFor(jLabelAppareilVenduGenre);
			jPanelAppareilVenduDetails = new JPanel();
			jPanelAppareilVenduDetails.setLayout(null);
			jPanelAppareilVenduDetails.setBounds(new Rectangle(450, 15, 301, 421));
			jPanelAppareilVenduDetails.setName("");
			jPanelAppareilVenduDetails.setPreferredSize(new java.awt.Dimension(300, -25));
			jPanelAppareilVenduDetails.add(jLabelAppareilVenduGenre, null);
			jPanelAppareilVenduDetails.add(jLabelAppareilVenduMarque, null);
			jPanelAppareilVenduDetails.add(jLabelAppareilVenduType, null);
			jPanelAppareilVenduDetails.add(jLabelAppareilVenduNoSerie, null);
			jPanelAppareilVenduDetails.add(jLabelAppareilVenduDateVente, null);
			jPanelAppareilVenduDetails.add(jLabelAppareilVenduPrix, null);
			jPanelAppareilVenduDetails.add(jLabelAppareilVenduNoFacture, null);
			jPanelAppareilVenduDetails.add(getJTextFieldAppareilVenduType(), null);
			jPanelAppareilVenduDetails.add(getJTextFieldAppareilVenduNoSerie(), null);
			jPanelAppareilVenduDetails.add(getJTextFieldAppareilVenduPrix(), null);
			jPanelAppareilVenduDetails.add(getJTextFieldAppareilVenduNoFacture(), null);
			// jPanelAppareilVenduDetails.add(getjPanelBoutonsClientStock21(), null);
			jPanelAppareilVenduDetails.add(getJComboBoxAppareilVenduGenre(), null);
			jPanelAppareilVenduDetails.add(getJComboBoxAppareilVenduMarque(), null);
			jPanelAppareilVenduDetails.add(getCalendrierVenteDate(), null);
			jPanelAppareilVenduDetails.add(getJPanel1());
			jPanelAppareilVenduDetails.add(getJPanel2());
		}
		return jPanelAppareilVenduDetails;
	}

	/**
	 * This method initializes jTextFieldAppareilVenduType
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldAppareilVenduType() {
		if (jTextFieldAppareilVenduType == null) {
			jTextFieldAppareilVenduType = new JTextField();
			jTextFieldAppareilVenduType.setBounds(new Rectangle(135, 90, 151, 16));
		}
		return jTextFieldAppareilVenduType;
	}

	/**
	 * This method initializes jTextFieldAppareilVenduNoSerie
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldAppareilVenduNoSerie() {
		if (jTextFieldAppareilVenduNoSerie == null) {
			jTextFieldAppareilVenduNoSerie = new JTextField();
			jTextFieldAppareilVenduNoSerie.setBounds(new Rectangle(135, 120, 151, 16));
		}
		return jTextFieldAppareilVenduNoSerie;
	}

	/**
	 * This method initializes jTextFieldAppareilVenduPrix
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldAppareilVenduPrix() {
		if (jTextFieldAppareilVenduPrix == null) {
			jTextFieldAppareilVenduPrix = new JTextField();
			jTextFieldAppareilVenduPrix.setBounds(new Rectangle(135, 180, 151, 16));
			jTextFieldAppareilVenduPrix.setDocument(modeleChamps.obtFloat(jTextFieldAppareilVenduPrix));
		}
		return jTextFieldAppareilVenduPrix;
	}

	/**
	 * This method initializes jTextFieldAppareilVenduNoFacture
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldAppareilVenduNoFacture() {
		if (jTextFieldAppareilVenduNoFacture == null) {
			jTextFieldAppareilVenduNoFacture = new JTextField();
			jTextFieldAppareilVenduNoFacture.setBounds(new Rectangle(135, 210, 151, 16));
		}
		return jTextFieldAppareilVenduNoFacture;
	}

	/**
	 * This method initializes jButtonSupprimerAppareil11
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonSupprimerAppareilVendu() {
		if (jButtonSupprimerAppareilVendu == null) {
			jButtonSupprimerAppareilVendu = new JButton();
			jButtonSupprimerAppareilVendu.setBounds(new Rectangle(150, 15, 106, 16));
			jButtonSupprimerAppareilVendu.setMnemonic(KeyEvent.VK_S);
			jButtonSupprimerAppareilVendu.setText("Supprimer");
			jButtonSupprimerAppareilVendu.setToolTipText("Cliquer sur ce bouton pour supprimer les données d\'un appareil vendu (réparations comrpises)");
			jButtonSupprimerAppareilVendu.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
									null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
						gestionnaireAppareilVendu.supprime(gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow()).id);
						setAppareilTexte("", "", "", "", "", "", "", ""); // L'appareil n'existe plus...
						actualiseTableVente();
						if (tabModelAppareilVendu.getRowCount() != 0) {
							jTableDetailsAppareilVendu.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

							// Récupère les détails de l'appareil sélectionné
							Appareil appareilSelectione = gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow());

							Client proprietaire = gestionnaireAppareilVendu.getProprietaire(appareilSelectione);

							// Affiche les détails de l'appareil
							setAppareilVenduTexte(appareilSelectione.type, appareilSelectione.noSerie,
											appareilSelectione.dateVenteDebut, appareilSelectione.prix,
											appareilSelectione.noFacture, appareilSelectione.genre,
											appareilSelectione.marque, appareilSelectione.status,
											proprietaire.nom, proprietaire.prenom, proprietaire.localite);
						} else {
							jButtonSupprimerAppareilVendu.setEnabled(false);
						}
					}
				}
			});
		}
		return jButtonSupprimerAppareilVendu;
	}

	/**
	 * This method initializes jComboBoxAppareilGenre11
	 *
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxAppareilVenduGenre() {
		if (jComboBoxAppareilGenre11 == null) {
			jComboBoxAppareilGenre11 = new JComboBox(listeGenreAppareil.toArray());
			jComboBoxAppareilGenre11.setLocation(new Point(135, 30));
			jComboBoxAppareilGenre11.setEditable(true);
			jComboBoxAppareilGenre11.setSelectedIndex(-1);
			jComboBoxAppareilGenre11.setSize(new Dimension(151, 16));

			jComboBoxAppareilGenre11.addItemListener(new ItemListener() {

				public void itemStateChanged(ItemEvent e) {
					try {
						jComboBoxAppareilGenre11.setEditable(false);
					} catch (Exception ex) {
					}
				}
			});
			jComboBoxAppareilGenre11.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					jComboBoxAppareilGenre11.setEditable(false);

					try {
						if (jComboBoxAppareilGenre11.isEditable()) {
							verifAppareilVente();
						}
					} catch (Exception ex) {
					}
				}
			});
		}
		return jComboBoxAppareilGenre11;
	}

	/**
	 * This method initializes jComboBoxAppareilMarque11
	 *
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxAppareilVenduMarque() {
		if (jComboBoxAppareilMarque11 == null) {
			jComboBoxAppareilMarque11 = new JComboBox(listeMarqueAppareil.toArray());
			jComboBoxAppareilMarque11.setBounds(new Rectangle(135, 60, 151, 16));
			jComboBoxAppareilMarque11.setSelectedIndex(-1);

			jComboBoxAppareilMarque11.addItemListener(new ItemListener() {

				public void itemStateChanged(ItemEvent e) {
					try {
						jComboBoxAppareilMarque11.setEditable(false);
					} catch (Exception ex) {
					}
				}
			});
			jComboBoxAppareilMarque11.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					jComboBoxAppareilMarque11.setEditable(false);

					try {
						if (jComboBoxAppareilMarque11.isEditable()) {
							verifAppareilVente();
						}
					} catch (Exception ex) {
					}
				}
			});
		}
		return jComboBoxAppareilMarque11;
	}

	/**
	 * This method initializes jTitledPanelListeTTRepparationAEncaisser
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelListeTTRepparationAEncaisser() {
		if (jTitledPanelListeTTRepparationAEncaisser == null) {
			jTitledPanelListeTTRepparationAEncaisser = new JXTitledPanel(
							"Liste des appareils vendus");
			jTitledPanelListeTTRepparationAEncaisser.setBorder(new DropShadowBorder(true));
			jTitledPanelListeTTRepparationAEncaisser.setTitleForeground(Color.white);
			jTitledPanelListeTTRepparationAEncaisser.setLocation(new Point(15, 15));
			jTitledPanelListeTTRepparationAEncaisser.setSize(new Dimension(387, 496));
			jTitledPanelListeTTRepparationAEncaisser.setTitle("Liste des réparations à encaisser");
			jTitledPanelListeTTRepparationAEncaisser.add(getJPanelListeReparationTTAEncaisser(), null);
		}
		return jTitledPanelListeTTRepparationAEncaisser;
	}

	/**
	 * This method initializes jPanelListeReparationTTAEncaisser
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelListeReparationTTAEncaisser() {
		if (jPanelListeReparationTTAEncaisser == null) {
			jPanelListeReparationTTAEncaisser = new JPanel();
			jPanelListeReparationTTAEncaisser.setLayout(null);
			jPanelListeReparationTTAEncaisser.setBounds(new Rectangle(15, 15, 373, 421));
			jPanelListeReparationTTAEncaisser.add(getJScrollPaneDetailsAppareilsTTAEncaisser(), null);
			jPanelListeReparationTTAEncaisser.add(getJButtonRechercherReparationTTAEncaisser(), null);
			jPanelListeReparationTTAEncaisser.add(getJButtonTtAfficherListeReparationTTAEncaisser(), null);
		}
		return jPanelListeReparationTTAEncaisser;
	}

	/**
	 * This method initializes jScrollPaneDetailsAppareilsStock11
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPaneDetailsAppareilsTTAEncaisser() {
		if (jScrollPaneDetailsAppareilsStock11 == null) {
			jScrollPaneDetailsAppareilsStock11 = new JScrollPane();
			jScrollPaneDetailsAppareilsStock11.setBounds(new Rectangle(15, 30, 346, 391));
			jScrollPaneDetailsAppareilsStock11.setViewportView(getJXTableDetailsReparationTTAEncaisser());
		}
		return jScrollPaneDetailsAppareilsStock11;
	}

	/**
	 * This method initializes jTableDetailsReparationTTAEncaisser
	 *
	 * @return javax.swing.JXTable
	 */
	private JXTable getJXTableDetailsReparationTTAEncaisser() {
		if (jTableDetailsReparationTTAEncaisser == null) {
			jTableDetailsReparationTTAEncaisser = new JXTable();
		}
		return jTableDetailsReparationTTAEncaisser;
	}

	/**
	 * This method initializes jButtonRechercherReparationTTAEncaisser
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonRechercherReparationTTAEncaisser() {
		if (jButtonRechercherReparationTTAEncaisser == null) {
			jButtonRechercherReparationTTAEncaisser = new JButton();
			jButtonRechercherReparationTTAEncaisser.setBounds(new Rectangle(30, 435, 136, 16));
			jButtonRechercherReparationTTAEncaisser.setMnemonic(KeyEvent.VK_R);
			jButtonRechercherReparationTTAEncaisser.setSelected(true);
			jButtonRechercherReparationTTAEncaisser.setText("Rechercher");
			jButtonRechercherReparationTTAEncaisser.setToolTipText("Cliquer sur ce bouton pour rechercher une réparation impayée");
			jButtonRechercherReparationTTAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					reparationRecherchee = new FenetreRechercheReparation(true).affiche(true);

					if (reparationRecherchee != null) {
						reparationRecherchee.paye = false;
						gestionnaireReparationTTAEncaisser.recherche(reparationRecherchee);
						if (tabModelReparationTTAEncaisser.getRowCount() != 0) {
							jTableDetailsReparationTTAEncaisser.setRowSelectionInterval(0, 0); // Séléctionne la première reparation par défaut
						}
						actualiseTableReparationsTTAEncaisser();
					}

					actualiseTableReparationsTTAEncaisser();
				}
			});
		}
		return jButtonRechercherReparationTTAEncaisser;
	}

	/**
	 * This method initializes jButtonTtAfficherListeReparationTTAEncaisser
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonTtAfficherListeReparationTTAEncaisser() {
		if (jButtonTtAfficherListeReparationTTAEncaisser == null) {
			jButtonTtAfficherListeReparationTTAEncaisser = new JButton();
			jButtonTtAfficherListeReparationTTAEncaisser.setBounds(new Rectangle(195, 435, 136, 16));
			jButtonTtAfficherListeReparationTTAEncaisser.setMnemonic(KeyEvent.VK_T);
			jButtonTtAfficherListeReparationTTAEncaisser.setText("Tout afficher");
			jButtonTtAfficherListeReparationTTAEncaisser.setToolTipText("Cliquer sur ce bouton pour afficher la liste intégralle des réparations impayées");
			jButtonTtAfficherListeReparationTTAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					gestionnaireReparationTTAEncaisser.recherche(new Reparation(null, null, null, null, null, null, false, null, null, null, null));
					if (tabModelReparationTTAEncaisser.getRowCount() != 0) {
						jTableDetailsReparationTTAEncaisser.setRowSelectionInterval(0, 0);
					}

					actualiseTableReparationsTTAEncaisser();
				}
			});
		}
		return jButtonTtAfficherListeReparationTTAEncaisser;
	}

	/**
	 * This method initializes jTitledPanelReparationDetails11
	 *
	 * @return org.jdesktop.swingx.JXTitledPanel
	 */
	private JXTitledPanel getJTitledPanelReparationDetails11() {
		if (jTitledPanelDetailsReparationTTAEncaisser == null) {
			jTitledPanelDetailsReparationTTAEncaisser = new JXTitledPanel("Détails");
			jTitledPanelDetailsReparationTTAEncaisser.setBorder(new DropShadowBorder(true));
			jTitledPanelDetailsReparationTTAEncaisser.setTitleForeground(Color.white);
			jTitledPanelDetailsReparationTTAEncaisser.setLocation(new Point(435, 15));
			jTitledPanelDetailsReparationTTAEncaisser.setSize(new Dimension(311, 496));
			jTitledPanelDetailsReparationTTAEncaisser.add(getJPanelReparationDetailsTTAEncaisser(), null);
		}
		return jTitledPanelDetailsReparationTTAEncaisser;
	}

	/**
	 * This method initializes jPanelReparationDetailsTTAEncaisser
	 *
	 * @return javax.swing.JLayeredPane
	 */
	private JLayeredPane getJPanelReparationDetailsTTAEncaisser() {
		if (jPanelReparationDetailsTTAEncaisser == null) {
			jLabelTTAEncaisserNoFacture = new JLabel();
			jLabelTTAEncaisserNoFacture.setBounds(new Rectangle(15, 180, 121, 16));
			jLabelTTAEncaisserNoFacture.setText("N° de facture :");
			jLabelReparationPayeTTAEncaisser = new JLabel();
			jLabelReparationPayeTTAEncaisser.setBounds(new Rectangle(15, 210, 121, 16));
			jLabelReparationPayeTTAEncaisser.setText("*Payé :");
			jLabelReparationPrixTTAEncaisser = new JLabel();
			jLabelReparationPrixTTAEncaisser.setBounds(new Rectangle(15, 150, 121, 16));
			jLabelReparationPrixTTAEncaisser.setText("*Prix :");
			jLabelReparationTechnicienTTAEncaisser = new JLabel();
			jLabelReparationTechnicienTTAEncaisser.setBounds(new Rectangle(15, 120, 121, 16));
			jLabelReparationTechnicienTTAEncaisser.setText("*Technicien :");
			jLabelReparationTravailEffTTAEncaisser = new JLabel();
			jLabelReparationTravailEffTTAEncaisser.setBounds(new Rectangle(15, 90, 121, 16));
			jLabelReparationTravailEffTTAEncaisser.setText("*Travail effectué :");
			jLabelReparationPanneTTAEncaisser = new JLabel();
			jLabelReparationPanneTTAEncaisser.setBounds(new Rectangle(15, 60, 121, 16));
			jLabelReparationPanneTTAEncaisser.setText("*Panne :");
			jLabelReparationDateTTAEncaisser = new JLabel();
			jLabelReparationDateTTAEncaisser.setBounds(new Rectangle(15, 30, 130, 16));
			jLabelReparationDateTTAEncaisser.setText("*Date de réparation :");
			jPanelReparationDetailsTTAEncaisser = new JLayeredPane();
			jPanelReparationDetailsTTAEncaisser.setLayout(null);
			jPanelReparationDetailsTTAEncaisser.setBounds(new Rectangle(450, 15, 301, 421));
			jPanelReparationDetailsTTAEncaisser.setName("");
			jPanelReparationDetailsTTAEncaisser.add(jLabelReparationDateTTAEncaisser, null);
			jPanelReparationDetailsTTAEncaisser.add(jLabelReparationPanneTTAEncaisser, null);
			jPanelReparationDetailsTTAEncaisser.add(jLabelReparationTravailEffTTAEncaisser, null);
			jPanelReparationDetailsTTAEncaisser.add(jLabelReparationTechnicienTTAEncaisser, null);
			jPanelReparationDetailsTTAEncaisser.add(jLabelReparationPrixTTAEncaisser, null);
			jPanelReparationDetailsTTAEncaisser.add(getJTextFieldReparationTechnicienTTAEncaisser(), null);
			jPanelReparationDetailsTTAEncaisser.add(getJPanelBoutonsClientTTAEncaisser(), null);
			jPanelReparationDetailsTTAEncaisser.add(jLabelReparationPayeTTAEncaisser, null);
			jPanelReparationDetailsTTAEncaisser.add(getJTextExtensReparationPanneTTAEncaisser(), null);
			jPanelReparationDetailsTTAEncaisser.add(getJTextExtensReparationTravailEffectueTTAEncaisser(), null);
			jPanelReparationDetailsTTAEncaisser.add(getJTextFieldReparationPrixTTAEncaisser(), null);
			jPanelReparationDetailsTTAEncaisser.add(getJCheckBoxReparationPayeTTAEncaisser(), null);
			jPanelReparationDetailsTTAEncaisser.add(getCalendrierReparationDateTTAEncaisser(), null);
			jPanelReparationDetailsTTAEncaisser.add(getJTextFieldTTAEncaisserNoFacture(), null);
			jPanelReparationDetailsTTAEncaisser.add(jLabelTTAEncaisserNoFacture, null);
			jPanelReparationDetailsTTAEncaisser.setLayer(getJTextExtensReparationPanneTTAEncaisser(), 200);
			jPanelReparationDetailsTTAEncaisser.setLayer(getJTextExtensReparationTravailEffectueTTAEncaisser(), 200);

		}
		return jPanelReparationDetailsTTAEncaisser;
	}

	/**
	 * This method initializes jTextFieldReparationTechnicienTTAEncaisser
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldReparationTechnicienTTAEncaisser() {
		if (jTextFieldReparationTechnicienTTAEncaisser == null) {
			jTextFieldReparationTechnicienTTAEncaisser = new JTextField();
			jTextFieldReparationTechnicienTTAEncaisser.setBounds(new Rectangle(150, 120, 136, 16));
		}
		return jTextFieldReparationTechnicienTTAEncaisser;
	}

	/**
	 * This method initializes jPanelBoutonsClientTTAEncaisser
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelBoutonsClientTTAEncaisser() {
		if (jPanelBoutonsClientTTAEncaisser == null) {
			jPanelBoutonsClientTTAEncaisser = new JPanel();
			jPanelBoutonsClientTTAEncaisser.setLayout(null);
			jPanelBoutonsClientTTAEncaisser.setBounds(new Rectangle(15, 345, 271, 106));
			jPanelBoutonsClientTTAEncaisser.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			jPanelBoutonsClientTTAEncaisser.add(getJButtonAnnulerReparationTTAEncaisser(), null);
			jPanelBoutonsClientTTAEncaisser.add(getJButtonSupprimerReparationTTAEncaisser(), null);
			jPanelBoutonsClientTTAEncaisser.add(getJButtonModifierReparationTTAEncaisser(), null);
			jPanelBoutonsClientTTAEncaisser.add(getJButtonEnregistrerReparationTTAEncaisser(), null);
		}
		return jPanelBoutonsClientTTAEncaisser;
	}

	/**
	 * This method initializes jButtonAnnulerReparationTTAEncaisser
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAnnulerReparationTTAEncaisser() {
		if (jButtonAnnulerReparationTTAEncaisser == null) {
			jButtonAnnulerReparationTTAEncaisser = new JButton();
			jButtonAnnulerReparationTTAEncaisser.setBounds(new Rectangle(60, 75, 136, 16));
			jButtonAnnulerReparationTTAEncaisser.setToolTipText("Cliquer sur ce bouton pour " + "annuler la modification en cours");
			jButtonAnnulerReparationTTAEncaisser.setMnemonic(KeyEvent.VK_A);
			jButtonAnnulerReparationTTAEncaisser.setSelected(true);
			jButtonAnnulerReparationTTAEncaisser.setText("Annuler");
			jButtonAnnulerReparationTTAEncaisser.setEnabled(false);
			jButtonAnnulerReparationTTAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (tabModelReparationTTAEncaisser.getRowCount() != 0) {
						jButtonModifierReparationTTAEncaisser.setEnabled(true);
						jButtonSupprimerReparationTTAEncaisser.setEnabled(true);
					}

					if (actionRTTA.equalsIgnoreCase("modifier")) {
						jPanelReparationTTAEncaisser.getRootPane().setDefaultButton(jButtonModifierReparationTTAEncaisser);
						jButtonModifierReparationTTAEncaisser.requestFocus();
					}

					jButtonEnregistrerReparationTTAEncaisser.setEnabled(false);
					jButtonAnnulerReparationTTAEncaisser.setEnabled(false);
					setReparationTTAEncaisserTexte("", "", "", "", "", "", false); // Réinitialise la saisie de l'appareil

					if (tabModelReparationTTAEncaisser.getRowCount() != 0) {
						// Récupère les détails de la réparation sélectionné
						Reparation reparationSelectione = (Reparation) gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow());

						// Affiche les détails de la réparation
						setReparationTTAEncaisserTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
					}

					// Autorise la séléction d'une autre réparation
					jTableDetailsReparationTTAEncaisser.setEnabled(true);

					// La réparation n'est plus éditable
					setReparationTTAEncaisserEditable(false);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonAnnulerReparationTTAEncaisser;
	}

	/**
	 * This method initializes jButtonSupprimerReparationTTAEncaisser
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonSupprimerReparationTTAEncaisser() {
		if (jButtonSupprimerReparationTTAEncaisser == null) {
			jButtonSupprimerReparationTTAEncaisser = new JButton();
			jButtonSupprimerReparationTTAEncaisser.setBounds(new Rectangle(150, 15, 106, 16));
			jButtonSupprimerReparationTTAEncaisser.setMnemonic(KeyEvent.VK_S);
			jButtonSupprimerReparationTTAEncaisser.setText("Supprimer");
			jButtonSupprimerReparationTTAEncaisser.setToolTipText("Cliquer sur ce bouton pour supprimer les données d\'une réparation existante");
			jButtonSupprimerReparationTTAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (JOptionPane.showConfirmDialog(jFrame, "Voulez-vous vraiment supprimer l'élément séléctionné?",
									null, JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
						gestionnaireReparationTTAEncaisser.supprime(gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow()).id);
						setReparationTTAEncaisserTexte("", "", "", "", "", "", false); // L'appareil n'existe plus...

						actualiseTableReparationsAppareil();
						actualiseTableReparationsTTAEncaisser();
						actualiseTableReparationsClientAEncaisser();

						if (tabModelReparationTTAEncaisser.getRowCount() != 0) {
							jTableDetailsReparationTTAEncaisser.setRowSelectionInterval(0, 0); // Séléctionne la première réparation par défaut

							// Récupère les détails de la réparation sélectionné
							Reparation reparationSelectione = (Reparation) gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow());

							// Affiche les détails de la réparation
							setReparationTTAEncaisserTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
						} else {
							jButtonSupprimerReparationTTAEncaisser.setEnabled(false);
						}
					}
				}
			});
		}
		return jButtonSupprimerReparationTTAEncaisser;
	}

	/**
	 * This method initializes jButtonModifierReparationTTAEncaisser
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonModifierReparationTTAEncaisser() {
		if (jButtonModifierReparationTTAEncaisser == null) {
			jButtonModifierReparationTTAEncaisser = new JButton();
			jButtonModifierReparationTTAEncaisser.setBounds(new Rectangle(15, 45, 106, 16));
			jButtonModifierReparationTTAEncaisser.setMnemonic(KeyEvent.VK_M);
			jButtonModifierReparationTTAEncaisser.setText("Modifier");
			jButtonModifierReparationTTAEncaisser.setToolTipText("Cliquer sur ce bouton pour modifier les données d\'une réparation existante");

			jButtonModifierReparationTTAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Initialise la saisie
					Reparation reparationSelectione = (Reparation) gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow());
					setReparationTTAEncaisserTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
					setReparationTTAEncaisserEditable(true); // Rends la saisie possible

					// Rends les boutons présentement innutils innactifs
					jButtonNouvelleReparation.setEnabled(false);
					jButtonModifierReparation.setEnabled(false);

					// Permet d'annuler l'action en cours
					jButtonAnnulerReparationTTAEncaisser.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelReparationTTAEncaisser.getRootPane().setDefaultButton(jButtonAnnulerReparationTTAEncaisser);

					// Empêche la séléction d'une autre réparation
					jTableDetailsReparationTTAEncaisser.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionRTTA = "Modifier";

					jTextFieldReparationTechnicienTTAEncaisser.requestFocus();

					setOngletsSelectable(false);
				}
			});
		}
		return jButtonModifierReparationTTAEncaisser;
	}

	/**
	 * This method initializes jButtonEnregistrerReparationTTAEncaisser
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonEnregistrerReparationTTAEncaisser() {
		if (jButtonEnregistrerReparationTTAEncaisser == null) {
			jButtonEnregistrerReparationTTAEncaisser = new JButton();
			jButtonEnregistrerReparationTTAEncaisser.setBounds(new Rectangle(150, 45, 106, 16));
			jButtonEnregistrerReparationTTAEncaisser.setToolTipText("Cliquer sur ce bouton pour enregister les données d\'une réparation après une modification");
			jButtonEnregistrerReparationTTAEncaisser.setMnemonic(KeyEvent.VK_E);
			jButtonEnregistrerReparationTTAEncaisser.setText("Enregistrer");
			jButtonEnregistrerReparationTTAEncaisser.setEnabled(false);
			jButtonEnregistrerReparationTTAEncaisser.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					String dateReparation = "";

					if (getCalendrierReparationDateTTAEncaisser().getDate() != null) {
						dateReparation = Utilitaire.getSQLDate(getCalendrierReparationDateTTAEncaisser().getDate());
					}

					if (actionRTTA.equalsIgnoreCase("Modifier")) {
						// Récupère les détails de la réparation sélectionné
						Reparation reparationSelectione = (Reparation) gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow());

						// Modifie la réparation saisie dans la base de donnée (Avec contrôle de la saisie)
						gestionnaireReparationTTAEncaisser.modifie(new Reparation(dateReparation, "", jTextExtensReparationPanneTTAEncaisser.getText(), jTextExtensReparationTravailEffectueTTAEncaisser.getText(), jTextFieldReparationTechnicienTTAEncaisser.getText(), jTextFieldReparationPrixTTAEncaisser.getText(), jCheckBoxReparationPayeTTAEncaisser.isSelected(), jTextFieldTTAEncaisserNoFacture.getText(), reparationSelectione.appareil, reparationSelectione.proprietaire, reparationSelectione.id));

						jPanelReparationDetailsTTAEncaisser.getRootPane().setDefaultButton(jButtonModifierReparationTTAEncaisser);
						jButtonModifierReparationTTAEncaisser.requestFocus();
					}
					// L'action a été effectuée
					actionRTTA = "";

					// Désactive les actions devenues impossibles
					jButtonEnregistrerReparationTTAEncaisser.setEnabled(false);
					jButtonAnnulerReparationTTAEncaisser.setEnabled(false);

					setReparationTTAEncaisserTexte("", "", "", "", "", "", false); // Réinitialise la saisie de la réparation

					// Autorise la séléction d'une autre réparation
					jTableDetailsReparationTTAEncaisser.setEnabled(true);

					// La réparation n'est plus éditable
					setReparationTTAEncaisserEditable(false);

					actualiseTableReparationsTTAEncaisser();
					actualiseTableVente();

					// Autorise la séléction d'une autre réparation
					jTableDetailsReparationTTAEncaisser.setEnabled(true);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonEnregistrerReparationTTAEncaisser;
	}

	/**
	 * This method initializes jTextExtensReparationPanneTTAEncaisser
	 *
	 * @return graphisme.TextExtens
	 */
	private TextExtens getJTextExtensReparationPanneTTAEncaisser() {
		if (jTextExtensReparationPanneTTAEncaisser == null) {
			jTextExtensReparationPanneTTAEncaisser = new TextExtens(136, 200);
			jTextExtensReparationPanneTTAEncaisser.setLocation(new Point(150, 60));
			jTextExtensReparationPanneTTAEncaisser.setSize(new Dimension(136, 19));
		}
		return jTextExtensReparationPanneTTAEncaisser;
	}

	/**
	 * This method initializes jTextExtensReparationTravailEffectueTTAEncaisser
	 *
	 * @return graphisme.TextExtens
	 */
	private TextExtens getJTextExtensReparationTravailEffectueTTAEncaisser() {
		if (jTextExtensReparationTravailEffectueTTAEncaisser == null) {
			jTextExtensReparationTravailEffectueTTAEncaisser = new TextExtens(136, 200);
			jTextExtensReparationTravailEffectueTTAEncaisser.setBounds(new Rectangle(150, 90, 136, 19));
		}
		return jTextExtensReparationTravailEffectueTTAEncaisser;
	}

	/**
	 * This method initializes jTextFieldReparationPrixTTAEncaisser
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldReparationPrixTTAEncaisser() {
		if (jTextFieldReparationPrixTTAEncaisser == null) {
			jTextFieldReparationPrixTTAEncaisser = new JTextField();
			jTextFieldReparationPrixTTAEncaisser.setBounds(new Rectangle(150, 150, 136, 16));
		}
		return jTextFieldReparationPrixTTAEncaisser;
	}

	/**
	 * This method initializes jCheckBoxReparationPayeTTAEncaisser
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getJCheckBoxReparationPayeTTAEncaisser() {
		if (jCheckBoxReparationPayeTTAEncaisser == null) {
			jCheckBoxReparationPayeTTAEncaisser = new JCheckBox();
			jCheckBoxReparationPayeTTAEncaisser.setBounds(new Rectangle(150, 210, 136, 16));
		}
		return jCheckBoxReparationPayeTTAEncaisser;
	}

	/**
	 * This method initializes calendrierReparationDateTTAEncaisser
	 *
	 * @return org.jdesktop.swingx.JXDatePicker
	 */
	private JXDatePicker getCalendrierReparationDateTTAEncaisser() {
		if (calendrierReparationDateTTAEncaisser == null) {
			calendrierReparationDateTTAEncaisser = new JXDatePicker();
			calendrierReparationDateTTAEncaisser.setLocation(new Point(150, 30));
			calendrierReparationDateTTAEncaisser.setFont(new Font("Dialog", Font.PLAIN, 11));
			calendrierReparationDateTTAEncaisser.setSize(new Dimension(136, 19));
			calendrierReparationDateTTAEncaisser.getEditor().setDisabledTextColor(Color.BLACK);
			// si l'utilisateur veut entrer les dates manuellement
			if (Definition.dateManuelle) {
				calendrierReparationDateTTAEncaisser.setFormats(Definition.formatDateManuelle);
			} else {
				calendrierReparationDateTTAEncaisser.setFormats(Definition.formatDateAutomatique);
			}

			calendrierReparationDateTTAEncaisser.getEditor().setEditable(false);

			calendrierReparationDateTTAEncaisser.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierReparationDateTTAEncaisser.getEditor().setEditable(true);
				}
			});
		}
		return calendrierReparationDateTTAEncaisser;
	}

	/**
	 * This method initializes jButtonNouvelAppareil
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonNouvelAppareil() {
		if (jButtonNouvelAppareil == null) {
			jButtonNouvelAppareil = new JButton();
			jButtonNouvelAppareil.setBounds(14, 15, 106, 16);
			jButtonNouvelAppareil.setToolTipText("Cliquer sur ce bouton pour ajouter un nouvel appareil");
			jButtonNouvelAppareil.setMnemonic(KeyEvent.VK_N);
			jButtonNouvelAppareil.setText("Nouveau");
			jButtonNouvelAppareil.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					setAppareilTexte("", "", "", "", "", "", "", ""); // Initialise la saisie
					setAppareilEditable(true); // Rends la saisie possible

					// Permet d'annuler l'action en cours
					jButtonAnnulerAppareil.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelClientDetails.getRootPane().setDefaultButton(jButtonAnnulerAppareil);

					// Rends les boutons présentement innutils innactifs
					jButtonNouvelAppareil.setEnabled(false);
					jButtonModifierAppareil.setEnabled(false);

					// Empêche la séléction d'un autre client
					jTableDetailsAppareil.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionA = "Ajouter";

					jTextFieldAppareilType.requestFocus();

					setOngletsSelectable(false);

					verifChampsAppareil();
				}
			});
		}
		return jButtonNouvelAppareil;
	}

	/**
	 * This method initializes jTextFieldReparationNoFacture
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldReparationNoFacture() {
		if (jTextFieldReparationNoFacture == null) {
			jTextFieldReparationNoFacture = new JTextField();
			jTextFieldReparationNoFacture.setBounds(new Rectangle(150, 180, 136, 16));
		}
		return jTextFieldReparationNoFacture;
	}

	/**
	 * This method initializes jTextFieldReparationAEncaisserNoFacture
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldReparationAEncaisserNoFacture() {
		if (jTextFieldReparationAEncaisserNoFacture == null) {
			jTextFieldReparationAEncaisserNoFacture = new JTextField();
			jTextFieldReparationAEncaisserNoFacture.setBounds(new Rectangle(150, 180, 136, 16));
		}
		return jTextFieldReparationAEncaisserNoFacture;
	}

	/**
	 * This method initializes jTextFieldTTAEncaisserNoFacture
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldTTAEncaisserNoFacture() {
		if (jTextFieldTTAEncaisserNoFacture == null) {
			jTextFieldTTAEncaisserNoFacture = new JTextField();
			jTextFieldTTAEncaisserNoFacture.setBounds(new Rectangle(150, 180, 136, 16));
		}
		return jTextFieldTTAEncaisserNoFacture;
	}

	private Integer getClientSelectionne() {
		// pour indiquer qu'on désire les appareils du client sélectionné
		return getJXTableDetailsClient().getSelectedRow() >= 0 ? ((Client) gestionnaireClient.getDetail(getJXTableDetailsClient().getSelectedRow())).id : 0;
	}

	/**
	 * This method initializes calendrierAppareilDateVente
	 *
	 * @return org.jdesktop.swingx.JXDatePicker
	 */
	private JXDatePicker getCalendrierAppareilDateVente() {
		if (calendrierAppareilDateVente == null) {
			calendrierAppareilDateVente = new JXDatePicker();
			calendrierAppareilDateVente.setFont(new Font("Dialog", Font.PLAIN, 11));
			calendrierAppareilDateVente.setSize(new Dimension(151, 20));
			calendrierAppareilDateVente.setLocation(new Point(135, 150));
			calendrierAppareilDateVente.getEditor().setDisabledTextColor(Color.BLACK);
			// si l'utilisateur veut entrer les dates manuellement
			if (Definition.dateManuelle) {
				calendrierAppareilDateVente.setFormats(Definition.formatDateManuelle);
			} else {
				calendrierAppareilDateVente.setFormats(Definition.formatDateAutomatique);
			}

			calendrierAppareilDateVente.getEditor().setEditable(false);

			calendrierAppareilDateVente.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierAppareilDateVente.getEditor().setEditable(true);
				}
			});
		}
		return calendrierAppareilDateVente;
	}

	/**
	 * This method initializes jMenuBDD
	 *
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenuBDD() {
		if (jMenuBDD == null) {
			jMenuBDD = new JMenu();
			jMenuBDD.setText("Base de données");
			jMenuBDD.setMnemonic(KeyEvent.VK_B);
			jMenuBDD.add(getJMenuItemSauvegarder());
			jMenuBDD.add(getJMenuItemRestaurer());
		}
		return jMenuBDD;
	}

	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemSauvegarder() {
		if (jMenuItemSauvegarder == null) {
			jMenuItemSauvegarder = new JMenuItem();
			jMenuItemSauvegarder.setText("Sauvegarder la base de données...");
			jMenuItemSauvegarder.setMnemonic(KeyEvent.VK_S);
			jMenuItemSauvegarder.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					JFileChooser dialogue = new JFileChooser();
					int retour = dialogue.showSaveDialog(jFrame);

					if (retour == JFileChooser.APPROVE_OPTION) {
						fichier = dialogue.getSelectedFile().getPath();

						try {
							new Backup(fichier);
							String titreInformation = "Sauvegarde réussie!";
							String information = "La sauvegarde de la base de données a été\n" +
											"effectuée sans erreur dans le fichier " + fichier + ".";
							// affiche un message d'avertissement
							JOptionPane.showMessageDialog(getJFrame(), information, titreInformation,
											JOptionPane.INFORMATION_MESSAGE);

						} catch (ErreurBackup e1) {
							String titreErreur = "Errreur de sauvegarde";
							String erreur = "Un problème lors de la sauvegarde de la base de données\n" +
											"dans le fichier " + fichier + " est survenu.\n" +
											"Veuillez vérifier que le chemin du fichier est correct\n";
							// affiche un message d'erreur
							JOptionPane.showMessageDialog(getJFrame(), erreur, titreErreur,
											JOptionPane.ERROR_MESSAGE);
						}


					}
				}
			});
		}
		return jMenuItemSauvegarder;
	}

	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemRestaurer() {
		if (jMenuItemRestaurer == null) {
			jMenuItemRestaurer = new JMenuItem();
			jMenuItemRestaurer.setText("Restaurer la base de données...");
			jMenuItemRestaurer.setMnemonic(KeyEvent.VK_S);
			jMenuItemRestaurer.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					JFileChooser dialogue = new JFileChooser();
					int retour = dialogue.showOpenDialog(jFrame);
					if (retour == JFileChooser.APPROVE_OPTION) {
						fichier = dialogue.getSelectedFile().getPath();
						try {
							new Restore(fichier);
							String titreInformation = "Restauration réussie!";
							String information = "La restauration du fichier  " + fichier +
											" a été effectuée avec succès.";
							// affiche un d'avertissement
							JOptionPane.showMessageDialog(getJFrame(), information, titreInformation,
											JOptionPane.INFORMATION_MESSAGE);

							// Pas hyper optimal, mais pour simplifier...
							actualiseTableClients();
							actualiseTableAppareilsClients();
							actualiseTableReparationsAppareil();
							actualiseTableReparationsClientAEncaisser();
							actualiseTableReparationsTTAEncaisser();
							actualiseTableStock();
							actualiseTableVente();
						} catch (ErreurRestore e1) {
							String titreErreur = "Errreur de restauration";
							String erreur = "Un problème lors de la restauration de la base de données\n" +
											"à partir de " + fichier + " est survenu.\n" +
											"Veuillez vérifier que le nom du fichier est correct\n" +
											"et qu'il s'agit bien d'un fichier de sauvegarde MySQL.";
							// affiche un message d'erreur
							JOptionPane.showMessageDialog(getJFrame(), erreur, titreErreur,
											JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			});
		}
		return jMenuItemRestaurer;
	}

	/**
	 * This method initializes calendrierStockDate
	 *
	 * @return org.jdesktop.swingx.JXDatePicker
	 */
	private JXDatePicker getCalendrierStockDate() {
		if (calendrierStockDate == null) {
			calendrierStockDate = new JXDatePicker();
			calendrierStockDate.setLocation(new Point(135, 150));
			calendrierStockDate.setFont(new Font("Dialog", Font.PLAIN, 11));
			calendrierStockDate.setSize(new Dimension(151, 19));
			calendrierStockDate.getEditor().setDisabledTextColor(Color.BLACK);
			// si l'utilisateur veut entrer les dates manuellement
			if (Definition.dateManuelle) {
				calendrierStockDate.setFormats(Definition.formatDateManuelle);
			} else {
				calendrierStockDate.setFormats(Definition.formatDateAutomatique);
			}

			calendrierStockDate.getEditor().setEditable(false);

			calendrierStockDate.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierStockDate.getEditor().setEditable(true);
				}
			});
		}
		return calendrierStockDate;
	}

	/**
	 * This method initializes calendrierVenteDate
	 *
	 * @return org.jdesktop.swingx.JXDatePicker
	 */
	private JXDatePicker getCalendrierVenteDate() {
		if (calendrierVenteDate == null) {
			calendrierVenteDate = new JXDatePicker();
			calendrierVenteDate.setLocation(new Point(135, 150));
			calendrierVenteDate.setFont(new Font("Dialog", Font.PLAIN, 11));
			calendrierVenteDate.setSize(new Dimension(151, 19));
			calendrierVenteDate.getEditor().setDisabledTextColor(Color.BLACK);
			// si l'utilisateur veut entrer les dates manuellement
			if (Definition.dateManuelle) {
				calendrierVenteDate.setFormats(Definition.formatDateManuelle);
			} else {
				calendrierVenteDate.setFormats(Definition.formatDateAutomatique);
			}

			calendrierVenteDate.getEditor().setEditable(false);

			calendrierVenteDate.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					calendrierVenteDate.getEditor().setEditable(true);
				}
			});
		}
		return calendrierVenteDate;
	}

	/**
	 * This method initializes jButtonAnnulerAssignationAppareil
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAnnulerAssignationAppareil() {
		if (jButtonAnnulerAssignationAppareil == null) {
			jButtonAnnulerAssignationAppareil = new JButton();
			jButtonAnnulerAssignationAppareil.setText("Annuler l'assignation");
			jButtonAnnulerAssignationAppareil.setSize(new Dimension(211, 16));
			jButtonAnnulerAssignationAppareil.setLocation(new Point(45, 280));
			jButtonAnnulerAssignationAppareil.setMnemonic(KeyEvent.VK_A);
			jButtonAnnulerAssignationAppareil.setEnabled(false);

			jButtonAnnulerAssignationAppareil.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					jButtonAssignerAppareil.setEnabled(false); // Active le bouton d'assignation pour les clients
					jButtonAnnulerAssignationAppareil.setEnabled(false);
					jButtonAssignerAClient.setEnabled(true);
				}
			});
		}
		return jButtonAnnulerAssignationAppareil;
	}

	private void actualiseTableClients() {
		// Affiche les différentes listes
		gestionnaireClient.afficherListe();

		if (tabModelClient.getRowCount() != 0) { // Si on peut séléctionner un client
			jTableDetailsClient.setRowSelectionInterval(0, 0); // Séléctionne le premier client par défaut

			// Récupère les détails du client sélectionné
			Client clientSelectione = gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());

			// Affiche les détails du client
			setClientTexte(clientSelectione.nom, clientSelectione.prenom, clientSelectione.profession, clientSelectione.noTelPrincipal, clientSelectione.noTelSecondaire, clientSelectione.adresse, clientSelectione.NPA, clientSelectione.localite);

			jButtonModifierClient.setEnabled(true);
			jButtonSupprimerClient.setEnabled(true);
		} else { // S'il n'y en a pas dans la base de donnée
			setClientTexte("", "", "", "", "", "", "", "");
			jButtonModifierClient.setEnabled(false);
			jButtonSupprimerClient.setEnabled(false);
		}
		setClientEditable(false);   // Ne permet pas l'édition du client par défaut

	}

	private void actualiseTableAppareilsClients() {
		if (tabModelClient.getRowCount() != 0 && jTableDetailsClient.getSelectedRow() >= 0) {
			// Récupère les détails du client sélectionné
			Client clientSelectione = gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());

			// Affiche les détails des appareils
			gestionnaireAppareils.setParentID(clientSelectione.id, null);
			jButtonNouvelAppareil.setEnabled(true);
		} else {
			setAppareilTexte("", "", "", "", "", "", "", "");
			jButtonNouvelAppareil.setEnabled(false);
			jButtonModifierAppareil.setEnabled(false);
			jButtonSupprimerAppareil.setEnabled(false);
			gestionnaireAppareils.setParentID(-1, -1);
		}

		gestionnaireAppareils.afficherListe();

		if (tabModelAppareils.getRowCount() != 0) { // Si le client a au moins un appareil
			jTableDetailsAppareil.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

			// Récupère et affiche les détails de l'appareil séléctionné
			Appareil appareilSelectione = (Appareil) gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow());
			setAppareilTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);

			jButtonModifierAppareil.setEnabled(true);
			jButtonSupprimerAppareil.setEnabled(true);
		} else {
			setAppareilTexte("", "", "", "", "", "", "", "");
			jButtonModifierAppareil.setEnabled(false);
			jButtonSupprimerAppareil.setEnabled(false);
		}
		setAppareilEditable(false); // Ne permet pas l'édition des appareils du client par défaut

	}

	private void actualiseTableReparationsAppareil() {
		if (tabModelClient.getRowCount() != 0 && jTableDetailsClient.getSelectedRow() >= 0 && tabModelAppareils.getRowCount() != 0 && jTableDetailsAppareil.getSelectedRow() >= 0) {
			// Récupère les détails du client sélectionné
			Client clientSelectione = gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());

			// Récupère et affiche les détails de l'appareil séléctionné
			Appareil appareilSelectione = gestionnaireAppareils.getDetail(jTableDetailsAppareil.getSelectedRow());

			// Affiche les réparations
			gestionnaireReparation.setParentID(appareilSelectione.id, clientSelectione.id);
			gestionnaireReparation.afficherListe();


			if (tabModelReparation.getRowCount() != 0) {
				jTableDetailsReparation.setRowSelectionInterval(0, 0); // Séléctionne la première réparation par défaut
				// Récupère les détails de la réparation sélectionné
				Reparation reparationSelectione = gestionnaireReparation.getDetail(jTableDetailsReparation.getSelectedRow());

				setReparationTexte(reparationSelectione.dateReparationDebut, reparationSelectione.probleme, reparationSelectione.travailEffectue, reparationSelectione.technicien, reparationSelectione.prix, reparationSelectione.noFacture, reparationSelectione.paye);
				jButtonModifierReparation.setEnabled(true);
				jButtonSupprimerReparation.setEnabled(true);
			} else {
				jButtonModifierReparation.setEnabled(false);
				jButtonSupprimerReparation.setEnabled(false);
				setReparationTexte("", "", "", "", "", "", false);
			}

			jButtonNouvelleReparation.setEnabled(true);
		} else {
			jButtonNouvelleReparation.setEnabled(false);
			jButtonModifierReparation.setEnabled(false);
			jButtonSupprimerReparation.setEnabled(false);
			gestionnaireReparation.setParentID(-1, -1);
			setReparationTexte("", "", "", "", "", "", false);
			// Affiche les réparations
			gestionnaireReparation.afficherListe();
		}

	}

	private void actualiseTableReparationsClientAEncaisser() {
		if (tabModelClient.getRowCount() != 0 && jTableDetailsClient.getSelectedRow() >= 0) {
			// Récupère les détails du client sélectionné
			Client clientSelectione = gestionnaireClient.getDetail(jTableDetailsClient.getSelectedRow());

			// Affiche les détails des factures à encaisser
			gestionnaireReparationAEncaisser.setParentID(null, clientSelectione.id);
			gestionnaireReparationAEncaisser.afficherListe();
			jButtonNouvelleReparationAEncaisser.setEnabled(true);
		} else {
			jButtonNouvelleReparation.setEnabled(false);
			jButtonModifierReparationAEncaisser.setEnabled(false);
			jButtonSupprimerReparationAEncaisser.setEnabled(false);
			gestionnaireReparationAEncaisser.setParentID(-1, -1);
			gestionnaireReparationAEncaisser.afficherListe();
		}

		if (tabModelReparationAEncaisser.getRowCount() != 0) { // Si le client a au moins une facture non-payée
			jTableDetailsReparationAEncaisser.setRowSelectionInterval(0, 0); // Séléctionne la première réparation non payée par défaut

			// Récupère et affiche les détails de la réparation séléctionné
			Reparation reparationlSelectione = (Reparation) gestionnaireReparationAEncaisser.getDetail(jTableDetailsReparationAEncaisser.getSelectedRow());
			setReparationAEncaisserTexte(reparationlSelectione.dateReparationDebut, reparationlSelectione.probleme, reparationlSelectione.travailEffectue, reparationlSelectione.technicien, reparationlSelectione.prix, reparationlSelectione.noFacture, reparationlSelectione.paye);

			jButtonModifierReparationAEncaisser.setEnabled(true);
			jButtonSupprimerReparationAEncaisser.setEnabled(true);
		} else {
			jButtonModifierReparationAEncaisser.setEnabled(false);
			jButtonSupprimerReparationAEncaisser.setEnabled(false);
			setReparationAEncaisserTexte("", "", "", "", "", "", false);
			gestionnaireReparationAEncaisser.setParentID(-1, -1);
			gestionnaireReparationAEncaisser.afficherListe();
		}
		setReparationAEncaisserEditable(false); // Ne permet pas l'édition par défaut

	}

	private void actualiseTableStock() {
		// Affiche les détails des appareils
		gestionnaireAppareilsStock.setParentID(0, 0);
		// Affiche les différentes listes
		gestionnaireAppareilsStock.afficherListe();


		if (tabModelAppareilsStock.getRowCount() != 0) { // Si on peut séléctionner un appareil en stock
			jTableDetailsAppareilStock.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

			// Récupère les détails de l'appareil sélectionné
			AppareilStock appareilSelectione = (AppareilStock) gestionnaireAppareilsStock.getDetail(jTableDetailsAppareilStock.getSelectedRow());
			setAppareilStockTexte(appareilSelectione.type, appareilSelectione.noSerie, appareilSelectione.dateVenteDebut, appareilSelectione.prix, appareilSelectione.noFacture, appareilSelectione.noBulletin, appareilSelectione.receptionnePar, appareilSelectione.genre, appareilSelectione.marque, appareilSelectione.status);

			jButtonModifierAppareilStock.setEnabled(true);
			jButtonSupprimerAppareilStock.setEnabled(true);
			jButtonAssignerAClient.setEnabled(true);
		} else { // S'il n'y en a pas dans la base de donnée
			jButtonModifierAppareilStock.setEnabled(false);
			jButtonSupprimerAppareilStock.setEnabled(false);
			// Affiche les détails des appareils
			setAppareilStockTexte("", "", "", "", "", "", "", "", "", "");
			jButtonAssignerAClient.setEnabled(false);

		}
		setAppareilStockEditable(false); // Ne permet pas l'édition par défaut

	}

	private void actualiseTableVente() {
		// Affiche les détails des appareils
		gestionnaireAppareilVendu.setParentID(null, null);
		// Affiche tous les appareils en vente listes
		gestionnaireAppareilVendu.afficherListe();


		if (tabModelAppareilVendu.getRowCount() != 0) { // Si on peut séléctionner un appareil en vente
			jTableDetailsAppareilVendu.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

			// Récupère les détails de l'appareil sélectionné
			Appareil appareilSelectione = gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow());
			Client proprietaire = gestionnaireAppareilVendu.getProprietaire(appareilSelectione);

			// Affiche les détails de l'appareil
			setAppareilVenduTexte(appareilSelectione.type, appareilSelectione.noSerie,
							appareilSelectione.dateVenteDebut, appareilSelectione.prix,
							appareilSelectione.noFacture, appareilSelectione.genre,
							appareilSelectione.marque, appareilSelectione.status,
							proprietaire.nom, proprietaire.prenom, proprietaire.localite);
			getJButtonModifierAppareilVendu().setEnabled(true);
			getJButtonSupprimerAppareilVendu().setEnabled(true);
		} else { // S'il n'y en a pas dans la base de donnée
			jButtonModifierAppareilVendu.setEnabled(false);
			getJButtonSupprimerAppareilVendu().setEnabled(false);
			// Affiche les détails des appareils
			setAppareilVenduTexte("", "", "", "", "", "", "", "", "", "", "");
		}
		setAppareilVenduEditable(false); // Ne permet pas l'édition par défaut

	}

	private void actualiseTableReparationsTTAEncaisser() {
		// Affiche les détails des réparations à encaisser
		gestionnaireReparationTTAEncaisser.setParentID(null, null);
		gestionnaireReparationTTAEncaisser.afficherListe();

		if (tabModelReparationTTAEncaisser.getRowCount() != 0) { // Si il y a au moins une réparation à encaisser
			jTableDetailsReparationTTAEncaisser.setRowSelectionInterval(0, 0); // Séléctionne le premier appareil par défaut

			// Récupère et affiche les détails de la réparation séléctionné
			Reparation reparationlSelectione = gestionnaireReparationTTAEncaisser.getDetail(jTableDetailsReparationTTAEncaisser.getSelectedRow());
			setReparationTTAEncaisserTexte(reparationlSelectione.dateReparationDebut, reparationlSelectione.probleme, reparationlSelectione.travailEffectue, reparationlSelectione.technicien, reparationlSelectione.prix, reparationlSelectione.noFacture, reparationlSelectione.paye);

			jButtonModifierReparationTTAEncaisser.setEnabled(true);
			jButtonSupprimerReparationTTAEncaisser.setEnabled(true);
		} else {
			jButtonModifierReparationTTAEncaisser.setEnabled(false);
			jButtonSupprimerReparationTTAEncaisser.setEnabled(false);
			// Affiche les détails des réparations à encaisser
			setReparationTTAEncaisserTexte("", "", "", "", "", "", false);
		}
		setReparationTTAEncaisserEditable(false); // Ne permet pas l'édition par défaut
	}

	/**
	 * Launches this application
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // get the native OS look and feel

		// si l'utilisateur veut entrer les dates manuellement
		if (args.length > 0 && args[0].equals("manuelle")) {
			definition.Definition.dateManuelle = true;
		}

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				FenetreAffichage application = new FenetreAffichage();
				application.getJFrame().setVisible(true);
			}
		});
	}

	/**
	 * pour les copiers coller
	 * @return
	 */
	private ApplicationActionMap getAppActionMap() {
		return Application.getInstance().getContext().getActionMap(this);
	}

	private JMenuItem getJMenuItemCopier() {
		if (jMenuItemCopier == null) {
			jMenuItemCopier = new JMenuItem();
			jMenuItemCopier.setAction(getAppActionMap().get("copy"));
			jMenuItemCopier.setText("Copier");
			jMenuItemCopier.setMnemonic('p');
		}
		return jMenuItemCopier;
	}

	private JMenuItem getJMenuItemCouper() {
		if (jMenuItemCouper == null) {
			jMenuItemCouper = new JMenuItem();
			jMenuItemCouper.setAction(getAppActionMap().get("cut"));
			jMenuItemCouper.setText("Couper");
			jMenuItemCouper.setMnemonic('C');
		}
		return jMenuItemCouper;
	}

	private JMenuItem getJMenuItemColler() {
		if (jMenuItemColler == null) {
			jMenuItemColler = new JMenuItem();
			jMenuItemColler.setAction(getAppActionMap().get("paste"));
			jMenuItemColler.setText("Coller");
			jMenuItemColler.setMnemonic('o');
		}
		return jMenuItemColler;
	}

	private JMenuItem getJMenuItemEffacer() {
		if (jMenuItemEffacer == null) {
			jMenuItemEffacer = new JMenuItem();
			jMenuItemEffacer.setAction(getAppActionMap().get("delete"));
			jMenuItemEffacer.setText("Suprimmer");
			jMenuItemEffacer.setMnemonic('S');
			jMenuItemEffacer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		}
		return jMenuItemEffacer;
	}

	private JMenu getJMenuEdition() {
		if (jMenuEdition == null) {
			jMenuEdition = new JMenu();
			jMenuEdition.setText("Édition");
			jMenuEdition.setMnemonic(KeyEvent.VK_E);
			jMenuEdition.setName("jMenuEdition");
			jMenuEdition.add(getJMenuItemCouper());
			jMenuEdition.add(getJMenuItemCopier());
			jMenuEdition.add(getJMenuItemColler());
			jMenuEdition.add(getJMenuItemEffacer());

		}
		return jMenuEdition;
	}

	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			jPanel1.setLayout(null);
			jPanel1.setBounds(12, 232, 277, 106);
			jPanel1.setBorder(BorderFactory.createTitledBorder("Propriétaire"));
			jPanel1.add(getJTextFieldAppareilVenduClientNom());
			jPanel1.add(getJTextFieldAppareilVenduClientPrenom());
			jPanel1.add(getJTextFieldAppareilVenduClientLocalite());
			jPanel1.add(getJLabelAppareilVenduClientNom());
			jPanel1.add(getJLabelAppareilVenduClientPrenom());
			jPanel1.add(getJLabelAppareilVenduClientLocalite());
		}
		return jPanel1;
	}

	private JTextField getJTextFieldAppareilVenduClientNom() {
		if (jTextFieldAppareilVenduClientNom == null) {
			jTextFieldAppareilVenduClientNom = new JTextField();
			jTextFieldAppareilVenduClientNom.setBounds(118, 21, 151, 16);
			jTextFieldAppareilVenduClientNom.setEditable(false);
		}
		return jTextFieldAppareilVenduClientNom;
	}

	private JTextField getJTextFieldAppareilVenduClientPrenom() {
		if (jTextFieldAppareilVenduClientPrenom == null) {
			jTextFieldAppareilVenduClientPrenom = new JTextField();
			jTextFieldAppareilVenduClientPrenom.setBounds(118, 51, 151, 16);
			jTextFieldAppareilVenduClientPrenom.setName("jTextFieldAppareilVenduClientPrenom");
			jTextFieldAppareilVenduClientPrenom.setEditable(false);
		}
		return jTextFieldAppareilVenduClientPrenom;
	}

	private JTextField getJTextFieldAppareilVenduClientLocalite() {
		if (jTextFieldAppareilVenduClientLocalite == null) {
			jTextFieldAppareilVenduClientLocalite = new JTextField();
			jTextFieldAppareilVenduClientLocalite.setBounds(118, 81, 151, 16);
			jTextFieldAppareilVenduClientLocalite.setName("jTextFieldAppareilVenduClientLocalite");
			jTextFieldAppareilVenduClientLocalite.setEditable(false);
		}
		return jTextFieldAppareilVenduClientLocalite;
	}

	private JLabel getJLabelAppareilVenduClientNom() {
		if (jLabelAppareilVenduClientNom == null) {
			jLabelAppareilVenduClientNom = new JLabel();
			jLabelAppareilVenduClientNom.setBounds(5, 21, 37, 15);
			jLabelAppareilVenduClientNom.setName("jLabelAppareilVenduClientNom");
			jLabelAppareilVenduClientNom.setText("Nom :");

		}
		return jLabelAppareilVenduClientNom;
	}

	private JLabel getJLabelAppareilVenduClientPrenom() {
		if (jLabelAppareilVenduClientPrenom == null) {
			jLabelAppareilVenduClientPrenom = new JLabel();
			jLabelAppareilVenduClientPrenom.setBounds(5, 51, 56, 15);
			jLabelAppareilVenduClientPrenom.setName("jLabelAppareilVenduClientPrenom");
			jLabelAppareilVenduClientPrenom.setText("Prénom :");
		}
		return jLabelAppareilVenduClientPrenom;
	}

	private JLabel getJLabelAppareilVenduClientLocalite() {
		if (jLabelAppareilVenduClientLocalite == null) {
			jLabelAppareilVenduClientLocalite = new JLabel();
			jLabelAppareilVenduClientLocalite.setBounds(5, 81, 57, 15);
			jLabelAppareilVenduClientLocalite.setName("jLabelAppareilVenduClientLocalite");
			jLabelAppareilVenduClientLocalite.setText("Localité :");
		}
		return jLabelAppareilVenduClientLocalite;
	}

	private JPanel getJPanel2() {
		if (jPanel2 == null) {
			jPanel2 = new JPanel();
			jPanel2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			jPanel2.setLayout(null);
			jPanel2.setBounds(12, 345, 274, 106);
			jPanel2.setLayout(null);
			jPanel2.add(getJButtonSupprimerAppareilVendu());
			jPanel2.add(getJButtonModifierAppareilVendu());
			jPanel2.add(getJButtonAnnulerAppareilVendu());
			jPanel2.add(getJButtonEnregistrerAppareilVendu());
		}
		return jPanel2;
	}

	/**
	 * This method initializes jButtonEnregistrerAppareilStock
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonEnregistrerAppareilVendu() {
		if (jButtonEnregistrerAppareilVendu == null) {
			jButtonEnregistrerAppareilVendu = new JButton();
			jButtonEnregistrerAppareilVendu.setBounds(new Rectangle(150, 45, 106, 16));
			jButtonEnregistrerAppareilVendu.setToolTipText("Cliquer sur ce bouton pour enregister les données d\'un appareil après une modification");
			jButtonEnregistrerAppareilVendu.setMnemonic(KeyEvent.VK_E);
			jButtonEnregistrerAppareilVendu.setText("Enregistrer");
			jButtonEnregistrerAppareilVendu.setEnabled(false);
			jButtonEnregistrerAppareilVendu.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					String dateVente = "";

					if (getCalendrierVenteDate().getDate() != null) {
						dateVente = Utilitaire.getSQLDate(getCalendrierVenteDate().getDate());
					}

					Appareil ancienAppareil = gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow());
					// Modifie l'appareil saisi à la base de donnée (Avec contrôle de la saisie)
					gestionnaireAppareilVendu.modifie(new Appareil(ancienAppareil.id,
									(String) (getJComboBoxAppareilVenduGenre().getSelectedItem()),
									(String) (getJComboBoxAppareilVenduMarque().getSelectedItem()),
									getJTextFieldAppareilVenduType().getText(),
									getJTextFieldAppareilVenduNoSerie().getText(),
									dateVente,
									"",
									getJTextFieldAppareilVenduPrix().getText(),
									getJTextFieldAppareilVenduNoFacture().getText(),
									ancienAppareil.proprietaire,
									ancienAppareil.status)); // Integer.toString(getJComboBoxAppareilStatusStock().getSelectedIndex()) xxx

					jPanelAppareilVenduDetails.getRootPane().setDefaultButton(jButtonModifierAppareilVendu);
					jButtonModifierAppareilVendu.requestFocus();


					// Désactive les actions devenues impossibles
					jButtonEnregistrerAppareilVendu.setEnabled(false);
					jButtonAnnulerAppareilVendu.setEnabled(false);

					setAppareilVenduTexte("", "", "", "", "", "", "", "", "", "", ""); // Réinitialise la saisie de l'appareil

					// Autorise la séléction d'un autre appareil
					jTableDetailsAppareilVendu.setEnabled(true);

					// L'appareil n'est plus éditable
					setAppareilVenduEditable(false);


					// Actualise l'affichage
					actualiseTableVente();

					// Autorise la séléction d'un autre appareil
					jTableDetailsAppareilVendu.setEnabled(true);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonEnregistrerAppareilVendu;
	}

	/**
	 * This method initializes jButtonAnnulerAppareil
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAnnulerAppareilVendu() {
		if (jButtonAnnulerAppareilVendu == null) {
			jButtonAnnulerAppareilVendu = new JButton();
			jButtonAnnulerAppareilVendu.setBounds(new Rectangle(60, 75, 136, 16));
			jButtonAnnulerAppareilVendu.setToolTipText("Cliquer sur ce bouton pour " + "annuler l\'ajout ou la modification en cours");
			jButtonAnnulerAppareilVendu.setMnemonic(KeyEvent.VK_A);
			jButtonAnnulerAppareilVendu.setSelected(true);
			jButtonAnnulerAppareilVendu.setText("Annuler");
			jButtonAnnulerAppareilVendu.setEnabled(false);
			jButtonAnnulerAppareilVendu.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {

					if (tabModelAppareilVendu.getRowCount() != 0) {
						jButtonModifierAppareilVendu.setEnabled(true);
						jButtonSupprimerAppareilVendu.setEnabled(true);
					}

					jPanelAppareilVenduDetails.getRootPane().setDefaultButton(jButtonModifierAppareilVendu);
					jButtonModifierAppareilVendu.requestFocus();


					jButtonEnregistrerAppareilVendu.setEnabled(false);
					jButtonAnnulerAppareilVendu.setEnabled(false);
					setAppareilVenduTexte("", "", "", "", "", "", "", "", "", "", ""); // Réinitialise la saisie de l'appareil

					if (tabModelAppareilVendu.getRowCount() != 0) {
						// Récupère les détails de l'appareil sélectionné
						Appareil appareilSelectione = gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow());

						Client proprietaire = gestionnaireAppareilVendu.getProprietaire(appareilSelectione);
						// Affiche les détails de l'appareil
						setAppareilVenduTexte(appareilSelectione.type, appareilSelectione.noSerie,
										appareilSelectione.dateVenteDebut, appareilSelectione.prix,
										appareilSelectione.noFacture, appareilSelectione.genre,
										appareilSelectione.marque, appareilSelectione.status,
										proprietaire.nom, proprietaire.prenom, proprietaire.localite);
					}

					// Autorise la séléction d'un autre appareil
					jTableDetailsAppareilVendu.setEnabled(true);

					// L'appareil n'est plus éditable
					setAppareilVenduEditable(false);

					setOngletsSelectable(true);
				}
			});
		}
		return jButtonAnnulerAppareilVendu;
	}

	private JButton getJButtonModifierAppareilVendu() {
		if (jButtonModifierAppareilVendu == null) {

			jButtonModifierAppareilVendu = new JButton();
			jButtonModifierAppareilVendu.setBounds(new Rectangle(15, 45, 106, 16));
			jButtonModifierAppareilVendu.setMnemonic(KeyEvent.VK_M);
			jButtonModifierAppareilVendu.setText("Modifier");
			jButtonModifierAppareilVendu.setToolTipText("Cliquer sur ce bouton pour modifier les données d\'un apapreil vendu existant");

			jButtonModifierAppareilVendu.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Initialise la saisie
					Appareil appareilSelectione = (Appareil) gestionnaireAppareilVendu.getDetail(jTableDetailsAppareilVendu.getSelectedRow());
					Client proprietaire = gestionnaireAppareilVendu.getProprietaire(appareilSelectione);

					// Affiche les détails de l'appareil
					setAppareilVenduTexte(appareilSelectione.type, appareilSelectione.noSerie,
									appareilSelectione.dateVenteDebut, appareilSelectione.prix,
									appareilSelectione.noFacture, appareilSelectione.genre,
									appareilSelectione.marque, appareilSelectione.status,
									proprietaire.nom, proprietaire.prenom, proprietaire.localite);
					setAppareilVenduEditable(true); // Rends la saisie possible

					// Rends les boutons présentement innutils innactifs
					jButtonModifierAppareilVendu.setEnabled(false);

					// Permet d'annuler l'action en cours
					jButtonAnnulerAppareilVendu.setEnabled(true);

					// Permet d'annuler l'action par défaut
					jPanelVentes.getRootPane().setDefaultButton(jButtonAnnulerAppareilVendu);

					// Empêche la séléction d'un autre appareil
					jTableDetailsAppareilVendu.setEnabled(false);

					// Prends en compte l'action qu'aura "enregistrer"
					actionAV = "Modifier";

					jTextFieldAppareilVenduType.requestFocus();

					setOngletsSelectable(false);
				}
			});
		}
		return jButtonModifierAppareilVendu;
	}
}
