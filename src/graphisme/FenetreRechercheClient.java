/*----------------------------------------------------------------------------------
   Fichier       : FenetreRechercheClient.java

   Date          : 24.05.2008

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Permet de rechercher un client.

	Remarque(s)   : -
                  
	VM			     : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package graphisme;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.Document;

import donnees.Appareil;
import donnees.Client;

public class FenetreRechercheClient extends JDialog {
	/**
	 * ... description de la méthode/classe/package/variable ...
	 * @author Lucien Chaubert / Louis Jaquier
	 */
   private JPanel jPanelClientDetails1 = null;
   private JLabel jLabelRechercheClientPrenom = null;
   private JLabel jLabelRechercheClientProfession = null;
   private JLabel jLabelRechercheClientAdresse = null;
   private JLabel jLabelRechercheClientNPA = null;
   private JLabel jLabelRechercheClientVille = null;
   private JLabel jLabelRechercheClientTelPrincipal = null;
   private JTextField jTextFieldRechercheNom = null;
   private JTextField jTextFieldRecherchePrenom = null;
   private JTextField jTextFieldRechercheProfession = null;
   private JTextField jTextFieldRechercheAdresse = null;
   private JTextField jTextFieldRechercheNPA = null;
   private JTextField jTextFieldRechercheLocalite = null;
   private JTextField jTextFieldRechercheTel = null;
   private JLabel jLabelRechercheClientNom = null;
   private JPanel jContentPane = null;
   private JButton jButtonRechercheClientOK = null;
   private JLabel jLabelRemplirChampsRechercheClient = null;
   // pour le modèle des champs
   private ModeleChamps modeleChamps;	
   private Client clientRecherche = null;
	
   
   
	  /**
    * This method initializes jDialogRechercheClient  
    *    
    * @return javax.swing.JDialog   
    */
   public FenetreRechercheClient(ModeleChamps modeleChamps) {
         super();
         this.modeleChamps = modeleChamps;
         this.setSize(new Dimension(340, 338));
         // pour centrer la fenêtre
         this.setLocationRelativeTo(null);
         this.setTitle("Rechercher un client");
         this.setModal(true);
         this.setResizable(false);
         this.setContentPane(getJContentPane());
         this.getRootPane().setDefaultButton(getJButtonRechercheClientOK());
   }
   
   public Client affiche(Boolean val)
   {
   	this.setVisible(true);
   	
   	return clientRecherche;
   }
   
   /**
    * This method initializes jPanelClientDetails1 
    *    
    * @return javax.swing.JPanel 
    */
   private JPanel getJPanelClientDetails1() {
      if (jPanelClientDetails1 == null) {
         jLabelRemplirChampsRechercheClient = new JLabel();
         jLabelRemplirChampsRechercheClient.setBounds(new Rectangle(30, 15, 256, 16));
         jLabelRemplirChampsRechercheClient.setText("Remplir les champs désirés :");
         jLabelRechercheClientNom = new JLabel();
         jLabelRechercheClientNom.setBounds(new Rectangle(30, 60, 106, 16));
         jLabelRechercheClientNom.setText("Nom :");
         jLabelRechercheClientTelPrincipal = new JLabel();
         jLabelRechercheClientTelPrincipal.setBounds(new Rectangle(30, 240, 106, 16));
         jLabelRechercheClientTelPrincipal.setText("N° tél :");
         jLabelRechercheClientVille = new JLabel();
         jLabelRechercheClientVille.setBounds(new Rectangle(30, 210, 106, 16));
         jLabelRechercheClientVille.setText("Localité :");
         jLabelRechercheClientNPA = new JLabel();
         jLabelRechercheClientNPA.setBounds(new Rectangle(30, 180, 106, 16));
         jLabelRechercheClientNPA.setText("NPA :");
         jLabelRechercheClientAdresse = new JLabel();
         jLabelRechercheClientAdresse.setBounds(new Rectangle(30, 150, 106, 16));
         jLabelRechercheClientAdresse.setText("Adresse :");
         jLabelRechercheClientProfession = new JLabel();
         jLabelRechercheClientProfession.setBounds(new Rectangle(30, 120, 106, 16));
         jLabelRechercheClientProfession.setText("Profession :");
         jLabelRechercheClientPrenom = new JLabel();
         jLabelRechercheClientPrenom.setBounds(new Rectangle(30, 90, 106, 16));

         jLabelRechercheClientPrenom.setText("Prenom :");
         jPanelClientDetails1 = new JPanel();
         jPanelClientDetails1.setLayout(null);
         jPanelClientDetails1.setName("");
         jPanelClientDetails1.add(jLabelRechercheClientPrenom, null);
         jPanelClientDetails1.add(jLabelRechercheClientProfession, null);
         jPanelClientDetails1.add(jLabelRechercheClientAdresse, null);
         jPanelClientDetails1.add(jLabelRechercheClientNPA, null);
         jPanelClientDetails1.add(jLabelRechercheClientVille, null);
         jPanelClientDetails1.add(jLabelRechercheClientTelPrincipal, null);
         jPanelClientDetails1.add(getJTextFieldRechercheNom(), null);
         jPanelClientDetails1.add(getJTextFieldRecherchePrenom(), null);
         jPanelClientDetails1.add(getJTextFieldRechercheProfession(), null);
         jPanelClientDetails1.add(getJTextFieldRechercheAdresse(), null);
         jPanelClientDetails1.add(getJTextFieldRechercheNPA(), null);
         jPanelClientDetails1.add(getJTextFieldRechercheLocalite(), null);
         jPanelClientDetails1.add(getJTextFieldRechercheTel(), null);
         jPanelClientDetails1.add(jLabelRechercheClientNom, null);
         jPanelClientDetails1.add(getJButtonRechercheClientOK(), null);
         jPanelClientDetails1.add(jLabelRemplirChampsRechercheClient, null);
      }
      return jPanelClientDetails1;
   }
   
   
   /**
    * This method initializes jTextFieldNom1 
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheNom() {
      if (jTextFieldRechercheNom == null) {
         jTextFieldRechercheNom = new JTextField();
         jTextFieldRechercheNom.setBounds(new Rectangle(150, 60, 151, 16));
      }
      return jTextFieldRechercheNom;
   }


   /**
    * This method initializes jTextFieldPrenom1 
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRecherchePrenom() {
      if (jTextFieldRecherchePrenom == null) {
         jTextFieldRecherchePrenom = new JTextField();
         jTextFieldRecherchePrenom.setBounds(new Rectangle(150, 90, 151, 16));
      }
      return jTextFieldRecherchePrenom;
   }


   /**
    * This method initializes jTextFieldRechercheProfession 
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheProfession() {
      if (jTextFieldRechercheProfession == null) {
         jTextFieldRechercheProfession = new JTextField();
         jTextFieldRechercheProfession.setBounds(new Rectangle(150, 120, 151, 16));
      }
      return jTextFieldRechercheProfession;
   }


   /**
    * This method initializes jTextFieldAdresse1   
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheAdresse() {
      if (jTextFieldRechercheAdresse == null) {
         jTextFieldRechercheAdresse = new JTextField();
         jTextFieldRechercheAdresse.setBounds(new Rectangle(150, 150, 151, 16));
      }
      return jTextFieldRechercheAdresse;
   }


   /**
    * This method initializes jTextFieldNPA1 
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheNPA() {
      if (jTextFieldRechercheNPA == null) {
         jTextFieldRechercheNPA = new JTextField("", 0);
         jTextFieldRechercheNPA.setBounds(new Rectangle(150, 180, 151, 16));
         Document document = modeleChamps.obtNPA(getJTextFieldRechercheNPA());
         jTextFieldRechercheNPA.setDocument(document);
      }
      return jTextFieldRechercheNPA;
   }

   /**
    * This method initializes jTextFieldLocalite1  
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheLocalite() {
      if (jTextFieldRechercheLocalite == null) {
         jTextFieldRechercheLocalite = new JTextField();
         jTextFieldRechercheLocalite.setBounds(new Rectangle(150, 210, 151, 16));
      }
      return jTextFieldRechercheLocalite;
   }


   /**
    * This method initializes jTextFieldTelPrincipal1 
    *    
    * @return javax.swing.JTextField   
    */
   private JTextField getJTextFieldRechercheTel() {
      if (jTextFieldRechercheTel == null) {
         jTextFieldRechercheTel = new JTextField("", 0);
         jTextFieldRechercheTel.setBounds(new Rectangle(150, 240, 151, 16));
         jTextFieldRechercheTel.setDocument(modeleChamps.obtTel(getJTextFieldRechercheTel()));
      }
      return jTextFieldRechercheTel;
   }


   /**
    * This method initializes jContentPane   
    *    
    * @return javax.swing.JPanel 
    */
   private JPanel getJContentPane() {
      if (jContentPane == null) {
         jContentPane = new JPanel();
         jContentPane.setLayout(new BorderLayout());
         jContentPane.add(getJPanelClientDetails1(), BorderLayout.CENTER);
      }
      return jContentPane;
   }


   /**
    * This method initializes jButtonRechercheClientOK   
    *    
    * @return javax.swing.JButton   
    */
   private JButton getJButtonRechercheClientOK() {
      if (jButtonRechercheClientOK == null) {
         jButtonRechercheClientOK = new JButton();
         jButtonRechercheClientOK.setBounds(new Rectangle(106, 279, 122, 16));
         jButtonRechercheClientOK.setText("OK");

         jButtonRechercheClientOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
               		clientRecherche = new Client(null,
                     getJTextFieldRechercheNom().getText(),
                     getJTextFieldRecherchePrenom().getText(),
                     getJTextFieldRechercheProfession().getText(),
                     getJTextFieldRechercheAdresse().getText(),
                     getJTextFieldRechercheNPA().getText(),
                     getJTextFieldRechercheLocalite().getText(),
                     getJTextFieldRechercheTel().getText(),
                     getJTextFieldRechercheTel().getText());
               
               		FenetreRechercheClient.this.dispose();

            }
         });
      }
      return jButtonRechercheClientOK;
   }


}
