/*----------------------------------------------------------------------------------
   Fichier       : TextExtens.java

   Date          : 13.03.2008

   Auteur        : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Permet de gérer l'extension de texte.

   Remarque(s)   : A mettre sur un layerpanel et mettre a 200 le setLayer

   VW            : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package graphisme;
import java.awt.*; import javax.swing.*; import javax.swing.border.EtchedBorder;
import org.jdesktop.swingx.border.DropShadowBorder;

public class TextExtens extends JScrollPane {
   private JTextPane jTextPane;
   private int defaultX;
   private int defaultY;

   public void setSize(int x, int y)
   {
      super.setSize(x, y);
      this.defaultX = x;
      this.defaultY = y;
   }

   public String getText()
   {
      return jTextPane.getText();
   }

   public void setBounds(Rectangle rect)
   {
      super.setBounds(rect);
      defaultX = (int)rect.getSize().getWidth();
      defaultY = (int)rect.getSize().getHeight();
   }
   public TextExtens(final int extendedHeight, final int extendedWidth)
   {
      jTextPane = new JTextPane();
      this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
      this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
      this.setViewportView(jTextPane);

      jTextPane.addMouseListener(new java.awt.event.MouseAdapter() {   

         public void mouseExited(java.awt.event.MouseEvent e) {
            //if (jTextPane.isEditable()){
               int c;
               if (!((c=TextExtens.this.getParent().getMousePosition().x) <=
                  TextExtens.this.getSize().width + TextExtens.this.getX()
                  && c 
                  >= TextExtens.this.getX() + jTextPane.getSize().width))
               {
                  Elexit();
               }
            //}

         }
         public void mouseEntered(java.awt.event.MouseEvent e) 
         {
            //if (jTextPane.isEditable()){
               //jTextPane.setVisible(false);
               TextExtens.super.setSize(extendedHeight,extendedWidth);
               TextExtens.this.setBorder(new DropShadowBorder(true));
               TextExtens.this.requestFocus();
               jTextPane.requestFocus();
               if (jTextPane.isEditable())
               // pour que le curseur se trouve en fin de texte
               jTextPane.setCaretPosition(jTextPane.getText().length());
               //jTextPane.setVisible(true);
            //}
         }
      });

      this.addMouseListener(new java.awt.event.MouseAdapter() {
         public void mouseExited(java.awt.event.MouseEvent e) {
            //if (jTextPane.isEditable())
               Elexit();
         }
      });
   }

   private void Elexit()
   {
      //TextExtens.this.setVisible(false);
      super.setSize(defaultX,defaultY);
      // définit les marges du texte
      jTextPane.setMargin(new Insets(-2,3,3,3));
      jTextPane.setCaretPosition(0);
      TextExtens.this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
      //TextExtens.this.setVisible(true);
      jTextPane.requestFocus();
   }
   
   public JTextPane getPane(){
      return jTextPane;
   }
}