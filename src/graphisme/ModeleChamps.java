/*----------------------------------------------------------------------------------
   Fichier       : ModeleChamps.java

   Date          : 02.03.2008

   Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Permet de contrôler la saisie dans les champs textuels.

   Remarque(s)   : -
                  
   VW			     : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package graphisme;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class ModeleChamps {
	/**
	 * Permet la saisie d'un NPA
	 * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
	 * @param textField Le champ textuel
	 */
	public PlainDocument obtNPA(final JTextField  textField)
	{
	   // Modèle du champ textuel.
	   return new PlainDocument(){
	      // Contrôle la saisie. (Contient que des chiffres, ne commence pas par un 0 et max 4 caractères)
	      public void insertString (int offs, String str, AttributeSet a) throws BadLocationException{
	         if (super.getLength() < 4 && ((str.charAt(0) == '0' && !textField.getText().isEmpty())
	               || (str.charAt(0) >= '1'  && str.charAt(0) <= '9')))
	            super.insertString(offs, str, a); // Si la saisie est OK, on la prends en compte
	      }
	   };
   }
	
	/**
    * Permet la saisie d'un numéro de téléphone
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param textField Le champ textuel
    */
	public PlainDocument obtTel(final JTextField  textField)
	{
	   // Modèle du champ textuel.
	   return  new PlainDocument(){
         // Contrôle la saisie. (Contient soit 10 chiffres soit un "+" et 11 chiffres)
         public void insertString (int offs, String str, AttributeSet a) throws BadLocationException{
            if ((textField.getText().isEmpty() && str.charAt(0) == '+')
                  || ((str.charAt(0) == ' ') && (textField.getText().length() == 3) && (textField.getText().charAt(0) != '+'))
                  || ((str.charAt(0) >= '0' && str.charAt(0) <= '9') && ((super.getLength() < 10)
                        || (textField.getText().charAt(3) == ' ' && super.getLength() < 11)
                        || (textField.getText().charAt(0) == '+' && super.getLength() < 12))))
               super.insertString(offs, str, a); // Si la saisie est OK, on la prends en compte
         }
      };
	}
	
	/**
    * Permet la saisie d'un float
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param textField Le champ textuel
    */
   public PlainDocument obtFloat(final JTextField  textField)
   {
      // Modèle du champ textuel.
      return new PlainDocument(){
         // Contrôle la saisie. (Contient que des chiffres, ne commence pas par un 0 et max 4 caractères)
         public void insertString (int offs, String str, AttributeSet a) throws BadLocationException{
            try{
               Float.parseFloat(textField.getText()+str.charAt(0));
               super.insertString(offs, str, a); // Si la saisie est OK, on la prends en compte
            }
            catch(Exception e){}
         }
      };
   }
   
   /**
    * Permet la saisie d'un status
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param textField Le champ textuel
    */
   public PlainDocument obtStatus(final JTextField  textField)
   {
      // Modèle du champ textuel.
      return new PlainDocument(){
         // Contrôle la saisie. (Contient que des chiffres, ne commence pas par un 0 et max 4 caractères)
         public void insertString (int offs, String str, AttributeSet a) throws BadLocationException{
            try{
               if (textField.getText().isEmpty() && (str.charAt(0) == '0' || str.charAt(0) == '1' || str.charAt(0) == '2'))
                  super.insertString(offs, str, a); // Si la saisie est OK, on la prends en compte
            }
            catch(Exception e){}
         }
      };
   }
}