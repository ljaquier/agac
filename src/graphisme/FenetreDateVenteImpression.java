/*----------------------------------------------------------------------------------
   Fichier       : FenetreRechercheAppareil.java

   Date          : 24.05.2008

	Auteur		  : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But           : Permet de rechercher un appareil.

	Remarque(s)   : -
                  
	VM			     : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package graphisme;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.jdesktop.application.Application;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import gestion.Utilitaire;

import definition.Definition;
import donnees.Appareil;

public class FenetreDateVenteImpression extends JDialog {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	   private JLabel jLabelInvite;
	   private JLabel jLabelDatefin;
	   private JLabel jLabelDateDebut;
	   private JButton jButtonRechercheAppareilOK = null;
	   private JPanel jContentPaneRechercheAppareil = null;

	   private String dateDebut;
	   private String dateFin;

	   
	   private JXDatePicker calendrierRechercheAppareilDateDebut = null;
	   private JXDatePicker calendrierRechercheAppareilDateFin = null;

	   
	   /**
	    * This method initializes jDialogRechercheAppareil   
	    *    
	    * @return javax.swing.JDialog   
	    */
	   public FenetreDateVenteImpression()
   	 {
	   		super();

	        
	   		this.setTitle("Imprimer la liste des ventes");
	         this.setSize(313, 198);
	         // pour centrer la fenêtre
	         this.setLocationRelativeTo(null);
	         ;
	         this.setModal(true);
	         this.setResizable(false);
	         this.getRootPane().
	         setDefaultButton(getJButtonRechercheAppareilOK());
	     
	   }
	   
	   public String[] affiche(Boolean val)
	   {
	   	String[] dates = new String[2];
	   	this.setContentPane(getJContentPaneRechercheAppareil());
	   	this.setVisible(true);

	   	dates[0] = dateDebut;
	   	dates[1] = dateFin;
	   	
	   	return dates;
	   }
	   
	   /**
	    * This method initializes jContentPaneRechercheAppareil 
	    *    
	    * @return javax.swing.JPanel 
	    */
	   public JPanel getJContentPaneRechercheAppareil() {
	      if (jContentPaneRechercheAppareil == null) {
	         jContentPaneRechercheAppareil = new JPanel();
	         jContentPaneRechercheAppareil.setLayout(null);
	         jContentPaneRechercheAppareil.setPreferredSize(new java.awt.Dimension(315, 105));

	         jContentPaneRechercheAppareil.add(getJButtonRechercheAppareilOK(), null);
	         jContentPaneRechercheAppareil.add(getCalendrierRechercheAppareilDateDebut(), null);
	         jContentPaneRechercheAppareil.add(getCalendrierRechercheAppareilDateFin(), null);
	         jContentPaneRechercheAppareil.add(getJLabelDateDebut());
	         jContentPaneRechercheAppareil.add(getJLabelDateFin());
	         jContentPaneRechercheAppareil.add(getJLabelInvite());
	      }
	      return jContentPaneRechercheAppareil;
	   }

	   /**
	    * This method initializes calendrierRechercheAppareilDateDebut   
	    *    
	    * @return org.jdesktop.swingx.JXDatePicker  
	    */
	   private JXDatePicker getCalendrierRechercheAppareilDateDebut() {
	      if (calendrierRechercheAppareilDateDebut == null) {
	         calendrierRechercheAppareilDateDebut = new JXDatePicker();
	         calendrierRechercheAppareilDateDebut.setFont(new Font("Dialog", Font.PLAIN, 11));
	         calendrierRechercheAppareilDateDebut.setFormats(Definition.formatDateManuelle);
	         calendrierRechercheAppareilDateDebut.setLocation(new Point(165, 180));
	         calendrierRechercheAppareilDateDebut.setBounds(138, 49, 136, 20);
	      }
	      return calendrierRechercheAppareilDateDebut;
	   }


	   /**
	    * This method initializes calendrierRechercheAppareilDateFin  
	    *    
	    * @return org.jdesktop.swingx.JXDatePicker  
	    */
	   private JXDatePicker getCalendrierRechercheAppareilDateFin() {
	      if (calendrierRechercheAppareilDateFin == null) {
	         calendrierRechercheAppareilDateFin = new JXDatePicker();
	         calendrierRechercheAppareilDateFin.setFont(new Font("Dialog", Font.PLAIN, 11));
	         calendrierRechercheAppareilDateFin.setFormats(Definition.formatDateManuelle);
	         calendrierRechercheAppareilDateFin.setLocation(new Point(165, 210));
	         calendrierRechercheAppareilDateFin.setBounds(138, 88, 136, 20);
	         calendrierRechercheAppareilDateFin.setVisible(true);
	      }
	      return calendrierRechercheAppareilDateFin;
	   }
	   


   /**
    * This method initializes jButtonRechercheAppareilOK 
    *    
    * @return javax.swing.JButton   
    */
   private JButton getJButtonRechercheAppareilOK() {
      if (jButtonRechercheAppareilOK == null) {
         jButtonRechercheAppareilOK = new JButton();
         jButtonRechercheAppareilOK.setText("OK");
         jButtonRechercheAppareilOK.setLocation(new Point(105, 345));
         jButtonRechercheAppareilOK.setBounds(91, 132, 119, 14);
	       jButtonRechercheAppareilOK
	             .addActionListener(new java.awt.event.ActionListener() {
	                public void actionPerformed(java.awt.event.ActionEvent e) {
	                   dateDebut = Utilitaire.getSQLDate(getCalendrierRechercheAppareilDateDebut().getDate()); 
	                   dateFin = Utilitaire.getSQLDate(getCalendrierRechercheAppareilDateFin().getDate()); 
	                   
	                   FenetreDateVenteImpression.this.dispose();
	                }
	             });
      }
      return jButtonRechercheAppareilOK;
   }
   
   private JLabel getJLabelDateDebut() {
   	if(jLabelDateDebut == null) {
   		jLabelDateDebut = new JLabel();
   		jLabelDateDebut.setBounds(22, 54, 98, 15);
   		jLabelDateDebut.setName("jLabelDateDebut");
   		jLabelDateDebut.setText("Date de début :");

   	}
   	return jLabelDateDebut;
   }
   
   private JLabel getJLabelDateFin() {
   	if(jLabelDatefin == null) {
   		jLabelDatefin = new JLabel();
   		jLabelDatefin.setName("jLabelDatefin");
   		jLabelDatefin.setText("Date de fin :");

   		jLabelDatefin.setBounds(22, 88, 98, 15);
   	}
   	return jLabelDatefin;
   }
   
   private JLabel getJLabelInvite() {
   	if(jLabelInvite == null) {
   		jLabelInvite = new JLabel();
   		jLabelInvite.setBounds(12, 12, 279, 15);
   		jLabelInvite.setName("jLabelInvite");
   		jLabelInvite.setText("Veuillez entrer l'interval à imprimer :");
   	}
   	return jLabelInvite;
   }

}