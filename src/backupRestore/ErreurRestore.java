/*----------------------------------------------------------------------------------
   Fichier     : ErreurRestore.java

   Date        : 09.04.2008

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Permet de lever une exception dans la Class Restore lorsqu'une
                 erreur survient.

   Remarque(s) : -
                  
   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package backupRestore;

public class ErreurRestore extends Exception {

}