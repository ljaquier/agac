/*----------------------------------------------------------------------------------
   Fichier     : Restore.java

   Date        : 09.04.2008

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Permetre de restorer une base de données sauvegardée.

   Remarque(s) : Pour utiliser cette outil, il faut le programme "mysql"
                 installé sur la machine. Le chemin du programme doivent être
                 mis dans le fichier de configuration.
                  
   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package backupRestore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Restore
{
   static final private String urlFichierConf = "./agac.conf";

   private String mysql;
   private String url;
   private String user;
   private String password;

   /**
    * Constructeur de la class Restore
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param urlBackupFile : Nom du fichier pour restorer la base
    * @throws ErreurRestore : Erreur lors de l'exécution de la restoration
    */ 
   public Restore(String urlBackupFile) throws ErreurRestore
   {
      try
      {
         int exitValue;
         lireDonneesConfigurations();
         if(urlBackupFile != null)
         {
            String connect;
            if(password.equals(""))
               connect = new String(mysql+" -u"+user+" -h"+url+" -e \"source "+urlBackupFile+"\"");
            else
               connect = new String(mysql+" -u"+user+" -p"+password+" -h"+url+" -e \"source "+urlBackupFile+"\"");
            
            // Sélection de l'OS
            String OS = System.getProperty("os.name");
            String windows = new String("windows");
            CharSequence seq = windows.subSequence(0, windows.length()-1);
            // Si Windows
            if(OS.toLowerCase().contains(seq))
            {
               // Création du fichier de lancement de dump pour windows
               File tmpFile = new File("./tmp.bat" );
               FileOutputStream out = new FileOutputStream(tmpFile);
               out.write(connect.getBytes());
               out.close();
               // Exécution du fichier tmp pour windows
               exitValue = Runtime.getRuntime().exec("./tmp.bat").waitFor();
               tmpFile.delete();
            }
            // Si un autre OS
            else
            {
               // Création du fichier de lancement de dump pour linux
               File tmpFile = new File("./tmp.sql" );
               FileOutputStream out = new FileOutputStream(tmpFile);
               out.write(connect.getBytes());
               out.close();
               // Exécution du fichier tmp pour linux
               exitValue = Runtime.getRuntime().exec(new String[]{"/bin/bash", "./tmp.sql"}).waitFor();
               tmpFile.delete();
            }
            
      ////// Traitement des erreurs //////
            if(exitValue != 0)
               throw new ErreurRestore();
         }
      } catch (IOException e) {
         throw new ErreurRestore();
      } catch (InterruptedException e) {
         throw new ErreurRestore();
      }
      ////////////////////////////////////
   }

   /**
    * Permet de lire les données utilisent dans le fichier de configuration
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @throws IOException : Erreur de lecteur dans le fichier de configuration
    */ 
   private void lireDonneesConfigurations() throws IOException
   {
      Properties prop = new Properties();
      FileInputStream in = new FileInputStream(urlFichierConf);
      prop.load(in);
      in.close();
      // Extraction des propriétés
      this.url = prop.getProperty("url");
      this.user = prop.getProperty("user");
      this.password = prop.getProperty("password"); 
      this.mysql = prop.getProperty("mysql");
   }

   /**
    * Méthode de test
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param args : Nom du fichier où sauver
    */ 
   public static void main(String[] args)
   {
      try {
         new Restore(args[0]);
      } catch (ErreurRestore e) {
         e.printStackTrace();
      }
   }
}
