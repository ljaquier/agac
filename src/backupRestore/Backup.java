/*----------------------------------------------------------------------------------
   Fichier     : Backup.java

   Date        : 09.04.2008

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Permetre de créer une sauvegarde d'une base de données.

   Remarque(s) : Pour utiliser cette outil, il faut le programme "mysqldump"
                 installé sur la machine. Le chemin du programme doivent être
                 mis dans le fichier de configuration.
                  
   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package backupRestore;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Backup
{
   static final private String urlFichierConf = "./agac.conf";
   
   private String mysqldump;
   private String url;
   private String dbName;
   private String user;
   private String password;
   
   /**
    * Constructeur de la class Backup
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param urlBackupFile : Nom du fichier où sauvegarder la base
    * @throws ErreurBackup : Erreur lors de l'exécution de la sauvegarde
    */ 
   public Backup(String urlBackupFile) throws ErreurBackup
   {
      try
      {
         int exitValue;
            lireDonneesConfigurations();
         if(password.equals(""))
            exitValue = Runtime.getRuntime().exec(new String[]{mysqldump, "-u",user,"-h" , url, "--databases" ,
                                                  dbName,"-r", urlBackupFile}).waitFor();
         else
            exitValue = Runtime.getRuntime().exec(new String[]{mysqldump, "-u",user,"-h",url,"-p"+password , "--databases" ,
                  dbName,"-r", urlBackupFile}).waitFor();
         
      ////// Traitement des erreurs //////
         if(exitValue != 0)
            throw new ErreurBackup();
      } catch (IOException e) {
         throw new ErreurBackup();
      } catch (InterruptedException e) {
         throw new ErreurBackup();
      }
      ////////////////////////////////////
   }
   
   /**
    * Permet de lire les données utilisent dans le fichier de configuration
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @throws IOException : Erreur de lecteur dans le fichier de configuration
    */ 
   private void lireDonneesConfigurations() throws IOException
   {
      Properties prop = new Properties();
      FileInputStream in = new FileInputStream(urlFichierConf);
      prop.load(in);
      in.close();
      // Extraction des propriétés
      this.url = prop.getProperty("url");
      this.dbName = prop.getProperty("dbName");
      this.user = prop.getProperty("user");
      this.password = prop.getProperty("password"); 
      this.mysqldump = prop.getProperty("mysqldump");
   }
   
   /**
    * Méthode de test
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param args : Nom du fichier où sauver
    */ 
   public static void main(String[] args)
   {
      try {
         new Backup(args[0]);
      } catch (ErreurBackup e) {
         e.printStackTrace();
      }
   }
}
