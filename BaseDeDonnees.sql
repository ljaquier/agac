-- MySQL dump 10.11
--
-- Host: 127.0.0.1    Database: TV_Schneuwly
-- ------------------------------------------------------
-- Server version	5.0.51a-3ubuntu5.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `TV_Schneuwly`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `TV_Schneuwly` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `TV_Schneuwly`;

--
-- Table structure for table `Appareils`
--

DROP TABLE IF EXISTS `Appareils`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Appareils` (
  `idAppareil` mediumint(8) unsigned NOT NULL auto_increment,
  `genre` tinytext character set latin1 NOT NULL,
  `marque` tinytext character set latin1 NOT NULL,
  `type` tinytext character set latin1,
  `noSerie` tinytext character set latin1,
  `dateVente` date default NULL,
  `prix` float unsigned default NULL,
  `noFacture` tinytext character set latin1,
  `proprietaire` smallint(5) unsigned NOT NULL default '0' COMMENT 'Stock => 0',
  `status` tinyint(1) unsigned NOT NULL default '0' COMMENT '0 : Stock; 1 : Vendu; 2 : Ne vient pas du magazin',
  `noBulletin` tinytext character set latin1,
  `receptionnePar` tinytext character set latin1,
  PRIMARY KEY  (`idAppareil`),
  KEY `proprietaire` (`proprietaire`),
  CONSTRAINT `proprietaire` FOREIGN KEY (`proprietaire`) REFERENCES `Clients` (`idClient`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Appareils`
--

LOCK TABLES `Appareils` WRITE;
/*!40000 ALTER TABLE `Appareils` DISABLE KEYS */;
INSERT INTO `Appareils` VALUES (2,'Lecteur CD','Philips','asdasd','adsasd',NULL,21323,'',14,2, '', ''),(3,'Lecteur CD','Philips','','',NULL,NULL,'',14,2, '', '');
/*!40000 ALTER TABLE `Appareils` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Clients`
--

DROP TABLE IF EXISTS `Clients`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Clients` (
  `idClient` smallint(5) unsigned NOT NULL auto_increment COMMENT 'de 1 à  n',
  `nom` tinytext character set latin1 NOT NULL,
  `prenom` tinytext character set latin1 NOT NULL,
  `profession` tinytext character set latin1,
  `noTel1` tinytext character set latin1,
  `noTel2` tinytext character set latin1,
  `adresse` tinytext character set latin1 NOT NULL,
  `npa` smallint(4) unsigned NOT NULL,
  `ville` tinytext character set latin1 NOT NULL,
  PRIMARY KEY  (`idClient`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Clients`
--

LOCK TABLES `Clients` WRITE;
/*!40000 ALTER TABLE `Clients` DISABLE KEYS */;
INSERT INTO `Clients` VALUES (0,'Stock','Stock','Stock','Stock','Stock','Stock',0000,'Stock');
INSERT INTO `Clients` VALUES (14,'gug','jbkjb','','','','jbkjbk',2424,'56vjvj');
INSERT INTO `Clients` VALUES (2,'qqq','jbkjb','','','','jbkjbk',2424,'56vjvj');
/*!40000 ALTER TABLE `Clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reparations`
--

DROP TABLE IF EXISTS `Reparations`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Reparations` (
  `idReparation` int(10) unsigned NOT NULL auto_increment,
  `dateReparation` date NOT NULL,
  `probleme` text character set latin1 NOT NULL,
  `travailEffectue` text character set latin1 NOT NULL,
  `technicien` tinytext character set latin1 NOT NULL,
  `prix` float unsigned NOT NULL,
  `paye` tinyint(1) NOT NULL,
  `noFacture` tinytext character set latin1,
  `appareil` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY  (`idReparation`),
  KEY `appareil` (`appareil`),
  CONSTRAINT `appareil` FOREIGN KEY (`appareil`) REFERENCES `Appareils` (`idAppareil`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Reparations`
--

LOCK TABLES `Reparations` WRITE;
/*!40000 ALTER TABLE `Reparations` DISABLE KEYS */;
/*!40000 ALTER TABLE `Reparations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2008-06-15 12:32:16
